Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.Serialization

Imports TCPServer_Common
Imports TCPServer_Common.PostMessageUtils

Imports NaplesGlobals

Public Class CtaTCPClient
  ' *****************************************************************************************************
  ' CTATCPClient CLASS
  ' -----------------
  '
  ' 
  ' ' MessageFolder
  '
  ' *****************************************************************************************************
  Inherits ThreadWrapperBase

  ' MessageReceived
  Public Event MessageReceivedEvent(ByVal sender As Object, ByVal e As MessageReceivedEventArgs)

  Private NewMessagesQueue As New Queue
  Private MessageWorkItems As New Hashtable

  Private ConnectionList As New ArrayList
  Private CTAClientID As Guid = Guid.NewGuid

  Private _CloseDownFlag As Boolean

  Private DefaultMessageServerName As String = ""
  Private DefaultMessageServerIpAddress As IPAddress = IPAddress.None

  Public Sub New()
    MyBase.New()
    Me.Thread.Name = "CTATCPClient"
  End Sub

  Public Sub New(ByVal pServerName As String, ByVal pServerAddress As IPAddress)
    Me.New()

    DefaultMessageServerName = pServerName
    DefaultMessageServerIpAddress = pServerAddress

    ' Add default server connection.
    If Not (IPAddress.None.Equals(DefaultMessageServerIpAddress)) Then
      ConnectionList.Add(New ServerConnectionClass(DefaultMessageServerName, DefaultMessageServerIpAddress))
    End If

  End Sub

  Public Property CloseDown() As Boolean
    Get
      Return _CloseDownFlag
    End Get
    Set(ByVal Value As Boolean)
      _CloseDownFlag = Value
    End Set
  End Property

  Protected Overrides Sub DoTask()

    Initialise()

    RunDataServer()

    TidyUp()

  End Sub

  Private Sub Initialise()
    ' Initialise Data Server

    _CloseDownFlag = False


  End Sub

  Private Sub TidyUp()
    Dim thisConnection As ServerConnectionClass

    ' On ending the Server

    Try
      For Each thisConnection In ConnectionList
        Try
          If (Not (thisConnection.Stream Is Nothing)) Then
            PostToMessageServer(thisConnection.Stream, TCP_ServerMessageType.Client_Disconnect, FCP_Application.Genoa, "", "")
          End If
          thisConnection.Stream.Close()
          thisConnection.Client.Close()
        Catch ex As Exception
        Finally
          thisConnection.Client = Nothing
          thisConnection = Nothing
        End Try
      Next
    Catch ex As Exception
    End Try

    Try
      ConnectionList.Clear()
    Catch ex As Exception
    End Try

  End Sub


  ' *******************************************************************
  ' Post New Message 
  ' *******************************************************************
  Public Sub PostNewMessage(ByVal pMessageFolder As MessageFolder)
    SyncLock NewMessagesQueue
      NewMessagesQueue.Enqueue(pMessageFolder)
    End SyncLock
  End Sub


  ' *******************************************************************
  '
  ' *******************************************************************
  Public Sub OnMessageReceived(ByVal pMessageReceived As MessageReceivedEventArgs)
    RaiseEvent MessageReceivedEvent(Me, pMessageReceived)
  End Sub


  ' *******************************************************************
  '
  ' *******************************************************************

  Private Sub RunDataServer()
    Dim LoopWorkDone As Boolean
    Dim ConnectionCount As Integer
    Dim MaxMessageCounter As Integer
    Dim thisConnection As ServerConnectionClass
    Dim DefaultServerConnection As ServerConnectionClass
    Dim MessageHeader As TCPServerHeader
    Dim SerializeFormatter As New BinaryFormatter
    Dim ServerCount As Integer

    DefaultServerConnection = Nothing

    ' ******************************************************
    ' Work loop.
    ' ******************************************************

    While (_CloseDownFlag = False)
      LoopWorkDone = False

      ' ******************************************************
      ' Check for new messages to send
      ' ******************************************************

      If Me.NewMessagesQueue.Count > 0 Then
        Dim MessageObject As Object

        ' ******************************************************
        ' Retrieve next Message
        ' ******************************************************

        SyncLock NewMessagesQueue
          MessageObject = NewMessagesQueue.Dequeue
        End SyncLock

        ' ******************************************************
        ' Only accept Message folder objects, (Is this Sensible ?)
        ' ******************************************************

        If MessageObject.GetType Is GetType(MessageFolder) Then
          Dim ThisMessageFolder As MessageFolder
          Dim ObjectCount As Integer
          Dim ObjectsToSend As Integer

          ThisMessageFolder = CType(MessageObject, MessageFolder)
          ObjectsToSend = ThisMessageFolder.HeaderMessage.FollowingMessageCount

          If Not (ThisMessageFolder.FollowingObjects Is Nothing) Then
            If ThisMessageFolder.FollowingObjects.Count < ObjectsToSend Then
              ObjectsToSend = ThisMessageFolder.FollowingObjects.Count
            End If
          Else
            ObjectsToSend = 0
          End If

          ' ******************************************************
          ' Resolve Default server connection.
          ' ******************************************************

          If (DefaultServerConnection Is Nothing) Then
            For Each thisConnection In ConnectionList
              If (thisConnection.ApplicationType And FCP_Application.MessageServer) = FCP_Application.MessageServer Then
                DefaultServerConnection = thisConnection
                Exit For
              End If
            Next
          End If

          ' ******************************************************
          ' Post message.
          ' ******************************************************

          If Not (DefaultServerConnection Is Nothing) AndAlso (ConnectToMessageServer(DefaultServerConnection) = True) Then
            If (ThisMessageFolder.HeaderMessage.FollowingMessageCount <> ObjectsToSend) Then
              ThisMessageFolder.HeaderMessage.FollowingMessageCount = ObjectsToSend
            End If

            PostToMessageServer(DefaultServerConnection.Stream, ThisMessageFolder.HeaderMessage)
            ' AddTCPServerWrapperClass and Send objects.

            If (ThisMessageFolder.HeaderMessage.FollowingMessageCount > 0) AndAlso (ObjectsToSend > 0) Then
              For ObjectCount = 1 To ObjectsToSend
                Try
                  PostToMessageServer(DefaultServerConnection.Stream, AddTCPServerWrapperClass(ThisMessageFolder.FollowingObjects(ObjectCount - 1)))
                Catch ex As Exception
                End Try

              Next
            End If
          Else
            DefaultServerConnection = Nothing
          End If
        End If
      End If

      ' ******************************************************
      ' Check for received messages
      ' ******************************************************

      ServerCount = 0
      If (ConnectionList.Count > 0) Then

        ' ******************************************************
        ' Loop through available connections
        ' ******************************************************

        For ConnectionCount = 0 To (ConnectionList.Count - 1)

          Try
            If (ConnectionCount < ConnectionList.Count) Then
              thisConnection = ConnectionList(ConnectionCount)

              ' Count Message Servers

              If (thisConnection.ApplicationType And FCP_Application.MessageServer) = FCP_Application.MessageServer Then
                ServerCount += 1

                If (DefaultServerConnection Is Nothing) Then
                  DefaultServerConnection = thisConnection
                  Exit For
                End If
              End If
            Else
              thisConnection = Nothing
            End If
          Catch ex As Exception
            thisConnection = Nothing
          End Try

          If (Not (thisConnection Is Nothing)) AndAlso (ConnectToMessageServer(thisConnection) = True) Then
            ' *************************************************
            ' Check for Incoming messages
            ' *************************************************

            MaxMessageCounter = 0
            While (Not (thisConnection Is Nothing)) AndAlso (thisConnection.MsgServerStream.DataAvailable = True) AndAlso (MaxMessageCounter < 5)
              Try
                MessageHeader = DirectCast(SerializeFormatter.Deserialize(thisConnection.MsgServerStream), TCPServerHeader)
                thisConnection.LastWakeupTime = Now()
              Catch ex As Exception
                MessageHeader = Nothing
              End Try

              If (Not (MessageHeader Is Nothing)) Then
                Dim FollowingObjects As New ArrayList
                Dim Counter As Integer

                ' Take in Following objects, if any.

                If MessageHeader.FollowingMessageCount > 0 Then

                  For Counter = 1 To MessageHeader.FollowingMessageCount
                    Try
                      FollowingObjects.Add(SerializeFormatter.Deserialize(thisConnection.MsgServerStream))
                    Catch ex As Exception
                    End Try
                  Next
                End If

                ' Strip off Wrapper objects as necessary.
                If (FollowingObjects.Count > 0) Then
                  For Counter = 0 To (FollowingObjects.Count - 1)
                    Try
                      If (FollowingObjects(Counter).GetType Is GetType(TCPServerObjectWrapper)) Then
                        Dim ThisObjectWrapper As TCPServerObjectWrapper
                        Dim NewObject As Object

                        ThisObjectWrapper = CType(FollowingObjects(Counter), TCPServerObjectWrapper)
                        NewObject = FollowingObjects(Counter)

                        Select Case ThisObjectWrapper.ObjectFormat.ToUpper
                          Case "BINARYFORMATTER"       ' BinaryFormatter
                            Try
                              NewObject = SerializeFormatter.Deserialize(New MemoryStream(ThisObjectWrapper.ObjectData))
                            Catch ex As Exception
                              NewObject = FollowingObjects(Counter)
                            End Try

                          Case "XMLFORMATTER"
                            Select Case ThisObjectWrapper.ObjectType

                              Case "ANOther"
                                ' More Code Here ....
                                '
                                '
                                '
                            End Select


                        End Select

                        FollowingObjects(Counter) = NewObject

                      End If
                    Catch ex As Exception
                    End Try
                  Next
                End If

                ' Process Message

                Select Case MessageHeader.MessageType

                  Case TCP_ServerMessageType.Server_ApplicationUpdateMessage, TCP_ServerMessageType.Client_ApplicationUpdateMessage
                    RaiseEvent MessageReceivedEvent(MessageHeader, New MessageReceivedEventArgs(MessageHeader, FollowingObjects))

                  Case TCP_ServerMessageType.Server_MessageRejected
                    ' ******************************************************
                    ' Reply Rejected ?
                    ' 
                    ' If a data reply was rejected, then cancel the related work object, if
                    ' it exists.
                    ' ******************************************************







                  Case TCP_ServerMessageType.Server_Touchbase
                    ' ******************************************************
                    ' TouchBase Message
                    ' ******************************************************

                    Call PostToMessageServer(thisConnection.MsgServerStream, TCP_ServerMessageType.Client_AcknowledgeOK)

                  Case TCP_ServerMessageType.Server_Disconnect, TCP_ServerMessageType.Client_Disconnect
                    ' ******************************************************
                    ' Disconnect, Remove this Connection from the collection
                    ' ******************************************************

                  Case TCP_ServerMessageType.Client_RequestConnect, TCP_ServerMessageType.Server_RequestConnect
                    ' ******************************************************
                    ' Connection Requested
                    ' ******************************************************

                    ' Start a dialogue.

                    PostToMessageServer(thisConnection.MsgServerStream, TCP_ServerMessageType.Client_SetGUID, FCP_Application.Genoa, "", CTAClientID.ToString)
                    PostToMessageServer(thisConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Genoa, "", "")
                    PostToMessageServer(thisConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Renaissance, "", "")


                  Case TCP_ServerMessageType.Client_RegisterApplication
                    thisConnection.ApplicationType = thisConnection.ApplicationType Or MessageHeader.Application


                  Case Else    ' MessageHeader.MessageType
                    ' Ignore other Messages



                End Select



              End If

            End While
          End If
        Next
      End If

      ' Delete Marked Connections




      ' Ensure the Default connection persists
      If (DefaultServerConnection IsNot Nothing) AndAlso ConnectToMessageServer(DefaultServerConnection) = False Then
        DefaultServerConnection = Nothing
      End If

      'If Not (IPAddress.None.Equals(DefaultMessageServerIpAddress)) Then
      '  ConnectionList.Add(New ServerConnectionClass(DefaultMessageServerName, DefaultMessageServerIpAddress))
      'End If


      If (LoopWorkDone = False) Then
        Threading.Thread.Sleep(25)
      End If
    End While

  End Sub



  Private Function ConnectToMessageServer(ByVal pServerConnection As ServerConnectionClass) As Boolean

    ' *******************************************************
    ' If the Stream appears to be valid, then Exit OK.
    ' *******************************************************

    If Not (pServerConnection.MsgServerStream Is Nothing) Then
      Return True
      Exit Function
    End If

    ' *******************************************************
    '
    ' *******************************************************

    If (pServerConnection.MsgServerIPAddress.Equals(IPAddress.None)) Then
      Return False
      Exit Function
    End If

    ' *******************************************************
    ' Initiate the Connection.
    ' *******************************************************

    Try
      pServerConnection.MsgServerClient = New TcpClient(TCPServerGlobals.RENAISSANCE_NETWORK_FAMILY)
      pServerConnection.MsgServerClient.ReceiveTimeout = TCPServerGlobals.FCP_Server_ReceiveTimeout
      pServerConnection.MsgServerClient.SendTimeout = TCPServerGlobals.FCP_Server_SendTimeout
      pServerConnection.MsgServerClient.NoDelay = True
      pServerConnection.MsgServerClient.ReceiveBufferSize = (2 ^ 14) ' 16K
      pServerConnection.MsgServerClient.SendBufferSize = (2 ^ 14)  ' 16K

      pServerConnection.MsgServerClient.Connect(pServerConnection.MsgServerIPAddress, TCPServerGlobals.FCP_ServerPort)

      pServerConnection.MsgServerStream = pServerConnection.MsgServerClient.GetStream()

      ' Start a dialogue.
      PostToMessageServer(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RequestConnect, FCP_Application.Genoa, "", "")
      PostToMessageServer(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_SetGUID, FCP_Application.Genoa, "", CTAClientID.ToString)
      PostToMessageServer(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Genoa, "", "")
      PostToMessageServer(pServerConnection.MsgServerStream, TCP_ServerMessageType.Client_RegisterApplication, FCP_Application.Renaissance, "", "")

    Catch ex As Exception

      Try
        pServerConnection.MsgServerClient.Close()
      Catch ex1 As Exception
      Finally
        pServerConnection.MsgServerStream = Nothing
      End Try

      Return False
      Exit Function
    End Try

    Return True

  End Function


  Public Function AddTCPServerWrapperClass(ByRef pSourceObject As Object) As TCPServerObjectWrapper
    Dim SerializeFormatter As New BinaryFormatter
    Dim ThisWrapperObject As TCPServerObjectWrapper

    If (Not (pSourceObject.GetType Is GetType(TCPServerObjectWrapper))) Then
      Try
        ThisWrapperObject = New TCPServerObjectWrapper
        ThisWrapperObject.ObjectType = pSourceObject.GetType.Name
        ThisWrapperObject.ObjectFormat = "BinaryFormatter"

        Dim ms As New MemoryStream
        SerializeFormatter.Serialize(ms, pSourceObject)
        ms.Position = 0
        ThisWrapperObject.ObjectData = ms.ToArray()
        ms.Close()
      Catch ex As Exception
        ThisWrapperObject = New TCPServerObjectWrapper
      End Try

    Else
      ThisWrapperObject = pSourceObject
    End If

    Return ThisWrapperObject

  End Function


  Private Class ServerConnectionClass
    Inherits TcpConnClass

    ' *************************************************
    ' Simple class used to manage connection to the TCP Server.
    ' *************************************************

    Public MsgServerName As String
    Public MsgServerIPAddress As IPAddress
    Public LastWakeupTime As Date

    Public Sub New()
      Me.GUID = System.Guid.NewGuid
      MsgServerName = ""
      MsgServerIPAddress = IPAddress.None
      Me.Stream = Nothing
      Me.Client = Nothing
      LastWakeupTime = Now()
    End Sub

    Public Sub New(ByVal pMsgServerName As String, ByVal pMsgServerIPAddress As IPAddress)
      Me.New()
      Me.MsgServerName = pMsgServerName
      Me.MsgServerIPAddress = pMsgServerIPAddress
    End Sub

    Public Property MsgServerStream() As NetworkStream
      Get
        Return Me.Stream
      End Get
      Set(ByVal Value As NetworkStream)
        Me.Stream = Value
      End Set
    End Property

    Public Property MsgServerClient() As TcpClient
      Get
        Return Me.Client
      End Get
      Set(ByVal Value As TcpClient)
        Me.Client = Value
      End Set
    End Property
  End Class



  Private Class ClientWorkClass
    ' *************************************************
    ' Simple Class used to manage active work items being processed bt the TCP Client / Data Servers
    ' *************************************************

    Public ClientRequestId As Guid

    Public ServerConnection As ServerConnectionClass

    Public MessageHeader As TCPServerHeader   ' 
    Public MessageBody As Object  ' e.g. NaplesDataRequest object

    Public DataRequestID As Guid
    Public DataRequest As Object  ' e.g. DataServerRequest object
    Public DataItemID As Guid
    Public DataItem As Object  ' e.g. DataServerRTItem object

    Public Sub New(ByRef pServerConnection As ServerConnectionClass, ByVal pMessageHeader As TCPServerHeader, ByVal pMessageBody As Object, ByVal pClientRequestId As Guid)
      Me.ClientRequestId = pClientRequestId
      Me.ServerConnection = pServerConnection
      Me.MessageHeader = pMessageHeader
      Me.MessageBody = pMessageBody
    End Sub

    Public Sub New(ByRef pServerConnection As ServerConnectionClass, ByVal pMessageHeader As TCPServerHeader, ByVal pMessageBody As Object)
      Me.MessageHeader = pMessageHeader
      Me.MessageBody = pMessageBody
      Me.ServerConnection = pServerConnection
      Me.ClientRequestId = Guid.NewGuid

      Me.DataRequestID = Guid.Empty
      Me.DataRequest = Nothing
      Me.DataItemID = Guid.Empty
      Me.DataItem = Nothing

      If Not (pMessageBody Is Nothing) Then
        If (pMessageBody.GetType Is GetType(NaplesDataRequest)) Then
          Me.ClientRequestId = CType(pMessageBody, NaplesDataRequest).RequestID
        End If
      End If
    End Sub

    Public Sub New(ByRef pServerConnection As ServerConnectionClass, ByRef pMessageHeader As TCPServerHeader, ByRef pMessageBody As Object, ByVal pRequestID As Guid, ByRef pDataRequest As Object)
      Me.New(pServerConnection, pMessageHeader, pMessageBody)
      Me.DataRequest = pDataRequest
      Me.DataRequestID = pRequestID
    End Sub

    Private Sub New()
      ' Enforce the use of Constructor arguments
    End Sub

  End Class

End Class
