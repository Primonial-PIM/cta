﻿Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports RenaissanceGlobals.Globals
Imports RenaissanceUtilities
Imports RenaissancePertracAndStatsGlobals

Public Enum SimulationParameterEnum As Integer

  None = 0
  InstrumentID
  InterestRateID
  DataStartDate

  MovingAverage_Days

  MovingAverage_ExponentialPricingAverage
  MovingAverage_ExponentialPricingLambda
  MovingAverage_ExponentialWeightedAverage
  MovingAverage_ExponentialWeightedAverageLambda
  MovingAverage_LinearWeightedAverage
  MovingAverage_LinearWeightedAverageLambda
  MovingAverage_VolatilityWeightedDuration
  MovingAverage_VolatilityWeightedDurationLambda

  InitialNAV  ' Notional Starting NAV

  Trading_AllowLongPosition
  Trading_AllowShortPosition

  Trading_RebalanceThreshold
  Trading_BaselineGearing
  Trading_MaxDrawdown
  Trading_DrawdownMonthlyReset
  Trading_DrawdownResetMonths
  Trading_TradingCosts
  Trading_FundingSpread
  Trading_StartDate
  Trading_FirstTradeAtStart
  Trading_VolatilityWeighting_StdDevHighVsAverage
  Trading_VolatilityWeighting_StdDevVsGivenDuration
  Trading_VolatilityWeighting_VolWeightDuration
  Trading_VolatilityWeighting_VolWeightCap
  Trading_VolatilityWeighting_Exponent
  Trading_BandWeighting_ByIndexZScore
  Trading_BandWeighting_IndexZScoreThreshold
  Trading_BandWeighting_ByAverageZScore
  Trading_BandWeighting_AverageZScoreThreshold
  Trading_BandWeighting_ZScoreStdDevPeriod
  Trading_BandWeighting_ByPercentageBand
  Trading_BandWeighting_PercentageBandThreshold
  Trading_BandWeighting_WeightingDecayFactor
  Trading_BandWeighting_ReInvestmentEvaluationPeriod
  Trading_BandWeighting_DailyWeightDeltaLimit
  Trading_BandWeighting_LocalHighExclusionPeriod
  Trading_BandWeighting_PhaseOutAtCrossing
  Trading_CrossingUnweightPoint

End Enum

<Serializable()> _
Public Class DailyPortfolioClass
  Public PortfolioDate As Date
  Public StockPrice As Double
  Public AverageLevel As Double
  Public InterestRate As Double

  Public IsUpCrossing As Boolean
  Public IsDownCrossing As Boolean

  Public CashOpen As Double
  Public CashClose As Double
  Public AssetOpen As Double
  Public AssetClose As Double
  Public ModelAssetOpen As Double
  Public ModelAssetClose As Double
  Public NAVOpen As Double
  Public NAVClose As Double

  Public BestNav As Double
  Public BestNavDate As Date
  Public DrawDown As Double
  Public DrawDownCount As Integer
  Public OneMonthDrawDown As Double
  Public MonthsSinceDrawDownLimitBreach As Integer

  Public CompoundTargetGearing As Double
  Public VolatilityGearingModifier As Double
  Public TradingBandGearingModifier As Double
  Public CumulativeTradingFees As Double
  Public InterestPaid As Double
  Public InterestReceived As Double

  Public Sub New()

    PortfolioDate = #1/1/1900#
    StockPrice = 0
    AverageLevel = 0
    InterestRate = 0

    IsUpCrossing = False
    IsDownCrossing = False

    CashOpen = 0
    CashClose = 0
    AssetOpen = 0
    AssetClose = 0
    NAVOpen = 0
    NAVClose = 0
    ModelAssetOpen = 0
    ModelAssetClose = 0

    BestNav = 0.0#
    BestNavDate = #1/1/1900#
    DrawDown = 0.0#
    DrawDownCount = 0
    OneMonthDrawDown = 0.0#
    MonthsSinceDrawDownLimitBreach = 0

    CompoundTargetGearing = 0.0#
    VolatilityGearingModifier = 1.0#
    TradingBandGearingModifier = 1.0#

    CumulativeTradingFees = 0.0#
    InterestPaid = 0.0#
    InterestReceived = 0.0#
  End Sub

  Public ReadOnly Property IsCrossing() As Boolean
    Get
      Return (IsUpCrossing Or IsDownCrossing)
    End Get
  End Property

  Public ReadOnly Property CumulativeInterest() As Double
    Get
      Return (InterestPaid + InterestReceived)
    End Get
  End Property

End Class

<Serializable()> _
Public Class SimulationParameterClass
  Implements ISerializable

  Private _SerialiseArrays As Boolean

  ' Update Flags

  Private _SeriesToUpdate As Boolean
  Private _InterestRateToUpdate As Boolean
  Private _AverageSeriesToUpdate As Boolean
  Private _TradeHistoryToUpdate As Boolean
  Private _TradingBandToUpdate As Boolean
  Private _StatsDatePeriod As RenaissanceGlobals.DealingPeriod

  ' Simulation Parameters

  Private _InstrumentID As Integer
  Private _InterestRateID As Integer
  Private _DataStartDate As Date

  Private _MovingAverage_Days As Integer

  Private _MovingAverage_ExponentialPricingAverage As Boolean
  Private _MovingAverage_ExponentialPricingLambda As Double
  Private _MovingAverage_ExponentialWeightedAverage As Boolean
  Private _MovingAverage_ExponentialWeightedAverageLambda As Double
  Private _MovingAverage_LinearWeightedAverage As Boolean
  Private _MovingAverage_LinearWeightedAverageLambda As Double
  Private _MovingAverage_VolatilityWeightedDuration As Boolean
  Private _MovingAverage_VolatilityWeightedDurationLambda As Double

  Private _InitialNAV As Double ' Notional Starting NAV

  Private _Trading_AllowLongPosition As Boolean
  Private _Trading_AllowShortPosition As Boolean

  Private _Trading_RebalanceThreshold As Double
  Private _Trading_BaselineGearing As Double
  Private _Trading_MaxDrawdown As Double
  Private _Trading_DeltaMoveLimit As Double
  Private _Trading_DrawdownMonthlyReset As Boolean
  Private _Trading_DrawdownResetMonths As Integer
  Private _Trading_TradingCosts As Double
  Private _Trading_FundingSpread As Double
  Private _Trading_StartDate As Date
  Private _Trading_FirstTradeAtStart As Boolean
  Private _Trading_VolatilityWeighting_StdDevHighVsAverage As Boolean
  Private _Trading_VolatilityWeighting_StdDevVsGivenDuration As Boolean
  Private _Trading_VolatilityWeighting_VolWeightDuration As Integer
  Private _Trading_VolatilityWeighting_VolWeightCap As Double
  Private _Trading_VolatilityWeighting_Exponent As Double
  Private _Trading_BandWeighting_ByIndexZScore As Boolean
  Private _Trading_BandWeighting_IndexZScoreThreshold As Double
  Private _Trading_BandWeighting_ByAverageZScore As Boolean
  Private _Trading_BandWeighting_AverageZScoreThreshold As Double
  Private _Trading_BandWeighting_ZScoreStdDevPeriod As Double
  Private _Trading_BandWeighting_ByPercentageBand As Boolean
  Private _Trading_BandWeighting_PercentageBandThreshold As Double
  Private _Trading_BandWeighting_WeightingDecayFactor As Double
  Private _Trading_BandWeighting_ReInvestmentEvaluationPeriod As Integer
  Private _Trading_BandWeighting_DailyWeightDeltaLimit As Double
  Private _Trading_BandWeighting_LocalHighExclusionPeriod As Integer
  Private _Trading_BandWeighting_PhaseOutAtCrossing As Boolean
  Private _Trading_CrossingUnweightPoint As Double

  Private _Result_MaxDrawdown As Double
  Private _Result_IRR As Double
  Private _Result_ITDReturn As Double
  Private _Result_FirstDate As Date
  Private _Result_LastDate As Date

  ' Series Data

  Public InstrumentDate(-1) As Date
  Public InstrumentReturns(-1) As Double
  Public InstrumentNAV(-1) As Double
  Public InstrumentStDev(-1) As Double

  Public InterestRateDate(-1) As Date
  Public InterestRateReturns(-1) As Double
  Public InterestRateNAV(-1) As Double

  Public AverageSeriesDate(-1) As Date
  Public AverageSeriesReturns(-1) As Double
  Public AverageSeriesNAV(-1) As Double

  Public PortfolioHistory(-1) As DailyPortfolioClass
  Public BandGearing_StDevSeries(-1) As Double
  Public BandGearing_AverageSeries(-1) As Double

  ' NAV Series

  Private _PortfolioDateSeries() As Date
  Private _PortfolioNAVSeries() As Double
  Private _Lock_PortfolioSeries As Object

  Public Sub New()

    _SeriesToUpdate = False
    _InterestRateToUpdate = False
    _AverageSeriesToUpdate = False
    _TradeHistoryToUpdate = False
    _TradingBandToUpdate = False
    _StatsDatePeriod = RenaissanceGlobals.DealingPeriod.Daily

    _InstrumentID = 0
    _InterestRateID = 0
    _DataStartDate = RenaissanceGlobals.Globals.Renaissance_BaseDate

    _MovingAverage_Days = 220
    _InitialNAV = 1000000.0#

    _MovingAverage_ExponentialPricingAverage = False
    _MovingAverage_ExponentialPricingLambda = 1.0#
    _MovingAverage_ExponentialWeightedAverage = False
    _MovingAverage_ExponentialWeightedAverageLambda = 1.0#
    _MovingAverage_LinearWeightedAverage = False
    _MovingAverage_LinearWeightedAverageLambda = 1.0#
    _MovingAverage_VolatilityWeightedDuration = False
    _MovingAverage_VolatilityWeightedDurationLambda = 1.0#

    _Trading_AllowLongPosition = True
    _Trading_AllowShortPosition = True

    _Trading_RebalanceThreshold = 0.0#
    _Trading_BaselineGearing = 1.0#
    _Trading_MaxDrawdown = 1.0#
    _Trading_DeltaMoveLimit = 0.0#
    _Trading_DrawdownMonthlyReset = False
    _Trading_DrawdownResetMonths = 1
    _Trading_TradingCosts = 0.0#
    _Trading_FundingSpread = 0.0#
    _Trading_StartDate = Renaissance_BaseDate
    _Trading_FirstTradeAtStart = True
    _Trading_VolatilityWeighting_StdDevHighVsAverage = False
    _Trading_VolatilityWeighting_StdDevVsGivenDuration = False
    _Trading_VolatilityWeighting_VolWeightDuration = 220
    _Trading_VolatilityWeighting_VolWeightCap = 2.0#
    _Trading_VolatilityWeighting_Exponent = 1.0#

    _Trading_BandWeighting_ByIndexZScore = False
    _Trading_BandWeighting_IndexZScoreThreshold = 2.0#
    _Trading_BandWeighting_ByAverageZScore = False
    _Trading_BandWeighting_AverageZScoreThreshold = 2.0#
    _Trading_BandWeighting_ZScoreStdDevPeriod = 220
    _Trading_BandWeighting_ByPercentageBand = False
    _Trading_BandWeighting_PercentageBandThreshold = 5.0#
    _Trading_BandWeighting_WeightingDecayFactor = 1.0#
    _Trading_BandWeighting_ReInvestmentEvaluationPeriod = 5
    _Trading_BandWeighting_DailyWeightDeltaLimit = 0.0#
    _Trading_BandWeighting_LocalHighExclusionPeriod = 5
    _Trading_BandWeighting_PhaseOutAtCrossing = False
    _Trading_CrossingUnweightPoint = 0.0#

    _PortfolioDateSeries = Nothing
    _PortfolioNAVSeries = Nothing
    _Lock_PortfolioSeries = New Object
  End Sub

  Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)

    Me.New()

    Dim VersionNumber As Double = 0.0#

    Try
      VersionNumber = info.GetDouble("SPC_Version")
    Catch ex As Exception
      VersionNumber = 0.0#
    End Try

    SerialiseArrays = info.GetInt32("SPC_SerialiseArrays")

    InstrumentID = info.GetInt32("SPC_InstrumentID")
    InterestRateID = info.GetInt32("SPC_InterestRateID")
    DataStartDate = info.GetDateTime("SPC_DataStartDate")

    Try
      StatsDatePeriod = CType(info.GetInt32("SPC_StatsDatePeriod"), RenaissanceGlobals.DealingPeriod)
    Catch ex As Exception
      StatsDatePeriod = RenaissanceGlobals.DealingPeriod.Daily
    End Try

    Try
      InitialNAV = info.GetInt32("SPC_InitialNAV")
    Catch ex As Exception
      InitialNAV = 1000000.0#
    End Try

    MovingAverageDays = info.GetInt32("SPC_MovingAverage_Days")
    MovingAverage_ExponentialPricingAverage = info.GetBoolean("SPC_MovingAverage_ExponentialPricingAverage")
    MovingAverage_ExponentialPricingLambda = info.GetDouble("SPC_MovingAverage_ExponentialPricingLambda")
    MovingAverage_ExponentialWeightedAverage = info.GetBoolean("SPC_MovingAverage_ExponentialWeightedAverage")
    MovingAverage_ExponentialWeightedAverageLambda = info.GetDouble("SPC_MovingAverage_ExponentialWeightedAverageLambda")
    MovingAverage_LinearWeightedAverage = info.GetBoolean("SPC_MovingAverage_LinearWeightedAverage")
    MovingAverage_LinearWeightedAverageLambda = info.GetDouble("SPC_MovingAverage_LinearWeightedAverageLambda")
    MovingAverage_VolatilityWeightedDuration = info.GetBoolean("SPC_MovingAverage_VolatilityWeightedDuration")
    MovingAverage_VolatilityWeightedDurationLambda = info.GetDouble("SPC_MovingAverage_VolatilityWeightedDurationLambda")

    If VersionNumber >= 1.2# Then
      Trading_AllowLongPosition = info.GetBoolean("SPC_Trading_AllowLongPosition")
      Trading_AllowShortPosition = info.GetBoolean("SPC_Trading_AllowShortPosition")
    End If

    Trading_RebalanceThreshold = info.GetDouble("SPC_Trading_RebalanceThreshold")
    Trading_BaselineGearing = info.GetDouble("SPC_Trading_BaselineGearing")
    Trading_MaxDrawdown = info.GetDouble("SPC_Trading_MaxDrawdown")

    If VersionNumber >= 1.0# Then
      Try
        Trading_DrawdownMonthlyReset = info.GetBoolean("SPC_Trading_DrawdownMonthlyReset")
        Trading_DrawdownResetMonths = info.GetInt32("SPC_Trading_DrawdownResetMonths")
      Catch ex As Exception
      End Try
    Else
      Trading_DrawdownMonthlyReset = False
      Trading_DrawdownResetMonths = 1
    End If

    Trading_TradingCosts = info.GetDouble("SPC_Trading_TradingCosts")
    Trading_FundingSpread = info.GetDouble("SPC_Trading_FundingSpread")
    Trading_StartDate = info.GetDateTime("SPC_Trading_StartDate")
    Trading_FirstTradeAtStart = info.GetBoolean("SPC_Trading_FirstTradeAtStart")
    Trading_VolatilityWeighting_StdDevHighVsAverage = info.GetBoolean("SPC_Trading_VolatilityWeighting_StdDevHighVsAverage")
    Trading_VolatilityWeighting_StdDevVsGivenDuration = info.GetBoolean("SPC_Trading_VolatilityWeighting_StdDevVsGivenDuration")
    Trading_VolatilityWeighting_VolWeightDuration = info.GetInt32("SPCTrading_VolatilityWeighting_VolWeightDuration")
    Trading_VolatilityWeighting_VolWeightCap = info.GetDouble("SPC_Trading_VolatilityWeighting_VolWeightCap")
    Trading_VolatilityWeighting_Exponent = info.GetDouble("SPC_Trading_VolatilityWeighting_Exponent")
    Trading_BandWeighting_ByIndexZScore = info.GetBoolean("SPC_Trading_BandWeighting_ByIndexZScore")
    Trading_BandWeighting_IndexZScoreThreshold = info.GetDouble("SPC_Trading_BandWeighting_IndexZScoreThreshold")
    Trading_BandWeighting_ByAverageZScore = info.GetBoolean("SPC_Trading_BandWeighting_ByAverageZScore")
    Trading_BandWeighting_AverageZScoreThreshold = info.GetDouble("SPC_Trading_BandWeighting_AverageZScoreThreshold")
    Trading_BandWeighting_ZScoreStdDevPeriod = info.GetDouble("SPC_Trading_BandWeighting_ZScoreStdDevPeriod")
    Trading_BandWeighting_ByPercentageBand = info.GetBoolean("SPC_Trading_BandWeighting_ByPercentageBand")
    Trading_BandWeighting_PercentageBandThreshold = info.GetDouble("SPC_Trading_BandWeighting_PercentageBandThreshold")
    Trading_BandWeighting_WeightingDecayFactor = info.GetDouble("SPC_Trading_BandWeighting_WeightingDecayFactor")
    Trading_BandWeighting_ReInvestmentEvaluationPeriod = info.GetInt32("SPC_Trading_BandWeighting_ReInvestmentEvaluationPeriod")
    Trading_BandWeighting_DailyWeightDeltaLimit = info.GetDouble("SPC_Trading_BandWeighting_DailyWeightDeltaLimit")
    Trading_BandWeighting_LocalHighExclusionPeriod = info.GetInt32("SPC_Trading_BandWeighting_LocalHighExclusionPeriod")

    If VersionNumber >= 1.1# Then
      Trading_BandWeighting_PhaseOutAtCrossing = info.GetBoolean("SPC_Trading_BandWeighting_PhaseOutAtCrossing")
      Trading_CrossingUnweightPoint = info.GetDouble("SPC_Trading_CrossingUnweightPoint")
    Else
      Trading_BandWeighting_PhaseOutAtCrossing = False
      Trading_CrossingUnweightPoint = 0.0#
    End If

    Trading_DeltaMoveLimit = 0.0#
    If VersionNumber >= 1.3# Then
      Trading_DeltaMoveLimit = info.GetDouble("SPC_Trading_DeltaMoveLimit")
    End If

    Result_MaxDrawdown = info.GetDouble("SPC_Result_MaxDrawdown")
    Result_IRR = info.GetDouble("SPC_Result_IRR")
    Result_ITDReturn = info.GetDouble("SPC_Result_ITDReturn")
    Result_FirstDate = info.GetDateTime("SPC_Result_FirstDate")

    Try
      Result_LastDate = info.GetDateTime("SPC_Result_LastDate")
    Catch ex As SerializationException
      ' An element with the specified name is not found in the current instance
      Result_LastDate = Renaissance_BaseDate
    Catch ex As Exception
      Result_LastDate = Renaissance_BaseDate
    End Try

    If SerialiseArrays Then

      InstrumentDate = DirectCast(info.GetValue("SPC_InstrumentDate", GetType(Date())), Date())
      InstrumentReturns = DirectCast(info.GetValue("SPC_InstrumentReturns", GetType(Double())), Double())
      InstrumentNAV = DirectCast(info.GetValue("SPC_InstrumentNAV", GetType(Double())), Double())
      InstrumentStDev = DirectCast(info.GetValue("SPC_InstrumentStDev", GetType(Double())), Double())

      InstrumentDate = DirectCast(info.GetValue("SPC_InterestRateDate", GetType(Date())), Date())
      InterestRateReturns = DirectCast(info.GetValue("SPC_InterestRateReturns", GetType(Double())), Double())
      InterestRateNAV = DirectCast(info.GetValue("SPC_InterestRateNAV", GetType(Double())), Double())

      AverageSeriesDate = DirectCast(info.GetValue("SPC_AverageSeriesDate", GetType(Date())), Date())
      AverageSeriesReturns = DirectCast(info.GetValue("SPC_AverageSeriesReturns", GetType(Double())), Double())
      AverageSeriesNAV = DirectCast(info.GetValue("SPC_AverageSeriesNAV", GetType(Double())), Double())

      PortfolioHistory = DirectCast(info.GetValue("SPC_PortfolioHistory", GetType(DailyPortfolioClass())), DailyPortfolioClass())
      BandGearing_StDevSeries = DirectCast(info.GetValue("SPC_BandGearing_StDevSeries", GetType(Double())), Double())
      BandGearing_AverageSeries = DirectCast(info.GetValue("SPC_BandGearing_AverageSeries", GetType(Double())), Double())

    End If

  End Sub

  Public Property SerialiseArrays() As Boolean
    Get
      Return _SerialiseArrays
    End Get
    Set(ByVal value As Boolean)
      _SerialiseArrays = value
    End Set
  End Property

  Public Property SeriesToUpdate() As Boolean
    Get
      Return _SeriesToUpdate
    End Get
    Set(ByVal value As Boolean)
      _SeriesToUpdate = value

      If (value) Then
        AverageSeriesToUpdate = True
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property InterestRateToUpdate() As Boolean
    Get
      Return _InterestRateToUpdate
    End Get
    Set(ByVal value As Boolean)
      _InterestRateToUpdate = value
      If (value) Then
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property AverageSeriesToUpdate() As Boolean
    Get
      Return _AverageSeriesToUpdate
    End Get
    Set(ByVal value As Boolean)
      _AverageSeriesToUpdate = value

      If (value) Then
        If (Trading_BandWeighting) Then
          TradingBandToUpdate = True
        End If

        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property TradeHistoryToUpdate() As Boolean
    Get
      Return _TradeHistoryToUpdate
    End Get
    Set(ByVal value As Boolean)
      _TradeHistoryToUpdate = value

      If (value) Then
        _Result_MaxDrawdown = 0.0#
        _Result_IRR = 0.0#
        _Result_ITDReturn = 0.0#
        _Result_FirstDate = Renaissance_BaseDate
        _Result_LastDate = Renaissance_BaseDate

        ClearPortfolioResultSeries()
      End If
    End Set
  End Property

  Public Sub ClearPortfolioResultSeries()
    _PortfolioDateSeries = Nothing
    _PortfolioNAVSeries = Nothing
  End Sub

  Public Property TradingBandToUpdate() As Boolean
    Get
      Return _TradingBandToUpdate
    End Get
    Set(ByVal value As Boolean)
      _TradingBandToUpdate = value

      If (value) Then
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property InstrumentID() As Integer
    Get
      Return _InstrumentID
    End Get
    Set(ByVal value As Integer)
      If Not (value = _InstrumentID) Then
        _InstrumentID = value

        SeriesToUpdate = True
      End If
    End Set
  End Property

  Public Property InterestRateID() As Integer
    Get
      Return _InterestRateID
    End Get
    Set(ByVal value As Integer)
      If Not (value = _InterestRateID) Then
        _InterestRateID = value
        InterestRateToUpdate = True
      End If
    End Set
  End Property

  Public Property DataStartDate() As Date
    Get
      Return _DataStartDate
    End Get
    Set(ByVal value As Date)
      If Not (value = _DataStartDate) Then
        _DataStartDate = value
        SeriesToUpdate = True
        InterestRateToUpdate = True
      End If
    End Set
  End Property

  Public Property StatsDatePeriod() As RenaissanceGlobals.DealingPeriod
    Get
      Return _StatsDatePeriod
    End Get
    Set(ByVal value As RenaissanceGlobals.DealingPeriod)
      If Not (value = _StatsDatePeriod) Then
        _StatsDatePeriod = value
        SeriesToUpdate = True
        InterestRateToUpdate = True
      End If
    End Set
  End Property

  Public Property InitialNAV() As Double
    Get
      Return _InitialNAV
    End Get
    Set(ByVal value As Double)
      If Not (Math.Abs(value) = _InitialNAV) Then
        _InitialNAV = Math.Abs(value)
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property MovingAverageDays() As Integer
    Get
      Return _MovingAverage_Days
    End Get
    Set(ByVal value As Integer)
      If Not (MovingAverageDays = value) Then
        _MovingAverage_Days = value
        AverageSeriesToUpdate = True
      End If
    End Set
  End Property

  Public Property MovingAverage_ExponentialPricingAverage() As Boolean
    Get
      Return _MovingAverage_ExponentialPricingAverage
    End Get
    Set(ByVal value As Boolean)
      If (value <> _MovingAverage_ExponentialPricingAverage) Then
        _MovingAverage_ExponentialPricingAverage = value

        AverageSeriesToUpdate = True

        If (value) Then
          MovingAverage_ExponentialWeightedAverage = False
          MovingAverage_LinearWeightedAverage = False
          MovingAverage_VolatilityWeightedDuration = False
        End If
      End If
    End Set
  End Property

  Public Property MovingAverage_ExponentialPricingLambda() As Double
    Get
      Return _MovingAverage_ExponentialPricingLambda
    End Get
    Set(ByVal value As Double)
      If Not (value = _MovingAverage_ExponentialPricingLambda) Then
        _MovingAverage_ExponentialPricingLambda = value

        AverageSeriesToUpdate = True
      End If
    End Set
  End Property

  Public Property MovingAverage_ExponentialWeightedAverage() As Boolean
    Get
      Return _MovingAverage_ExponentialWeightedAverage
    End Get
    Set(ByVal value As Boolean)
      If (value <> _MovingAverage_ExponentialWeightedAverage) Then
        _MovingAverage_ExponentialWeightedAverage = value

        AverageSeriesToUpdate = True

        If (value) Then
          MovingAverage_ExponentialPricingAverage = False
          MovingAverage_LinearWeightedAverage = False
          MovingAverage_VolatilityWeightedDuration = False
        End If
      End If
    End Set
  End Property

  Public Property MovingAverage_ExponentialWeightedAverageLambda() As Double
    Get
      Return _MovingAverage_ExponentialWeightedAverageLambda
    End Get
    Set(ByVal value As Double)
      If Not (value = _MovingAverage_ExponentialWeightedAverageLambda) Then
        _MovingAverage_ExponentialWeightedAverageLambda = value

        AverageSeriesToUpdate = True
      End If
    End Set
  End Property

  Public Property MovingAverage_LinearWeightedAverage() As Boolean
    Get
      Return _MovingAverage_LinearWeightedAverage
    End Get
    Set(ByVal value As Boolean)
      If (value <> _MovingAverage_LinearWeightedAverage) Then
        _MovingAverage_LinearWeightedAverage = value

        AverageSeriesToUpdate = True

        If (value) Then
          MovingAverage_ExponentialPricingAverage = False
          MovingAverage_ExponentialWeightedAverage = False
          MovingAverage_VolatilityWeightedDuration = False
        End If
      End If
    End Set
  End Property

  Public Property MovingAverage_LinearWeightedAverageLambda() As Double
    Get
      Return _MovingAverage_LinearWeightedAverageLambda
    End Get
    Set(ByVal value As Double)
      If Not (value = _MovingAverage_LinearWeightedAverageLambda) Then
        _MovingAverage_LinearWeightedAverageLambda = value

        AverageSeriesToUpdate = True
      End If
    End Set
  End Property

  Public Property MovingAverage_VolatilityWeightedDuration() As Boolean
    Get
      Return _MovingAverage_VolatilityWeightedDuration
    End Get
    Set(ByVal value As Boolean)
      If (value <> _MovingAverage_VolatilityWeightedDuration) Then
        _MovingAverage_VolatilityWeightedDuration = value

        AverageSeriesToUpdate = True

        If (value) Then
          MovingAverage_ExponentialPricingAverage = False
          MovingAverage_ExponentialWeightedAverage = False
          MovingAverage_LinearWeightedAverage = False
        End If
      End If
    End Set
  End Property

  Public Property MovingAverage_VolatilityWeightedDurationLambda() As Double
    Get
      Return _MovingAverage_VolatilityWeightedDurationLambda
    End Get
    Set(ByVal value As Double)
      If Not (value = _MovingAverage_VolatilityWeightedDurationLambda) Then
        _MovingAverage_VolatilityWeightedDurationLambda = value

        AverageSeriesToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_AllowLongPosition() As Boolean
    Get
      Return _Trading_AllowLongPosition
    End Get
    Set(ByVal value As Boolean)
      If Not (value = _Trading_AllowLongPosition) Then
        _Trading_AllowLongPosition = value
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_AllowShortPosition() As Boolean
    Get
      Return _Trading_AllowShortPosition
    End Get
    Set(ByVal value As Boolean)
      If Not (value = _Trading_AllowShortPosition) Then
        _Trading_AllowShortPosition = value
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_RebalanceThreshold() As Double
    Get
      Return _Trading_RebalanceThreshold
    End Get
    Set(ByVal value As Double)
      If Not (Math.Abs(value) = _Trading_RebalanceThreshold) Then
        _Trading_RebalanceThreshold = Math.Abs(value)
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_BaselineGearing() As Double
    Get
      Return _Trading_BaselineGearing
    End Get
    Set(ByVal value As Double)
      If Not (Math.Abs(value) = _Trading_BaselineGearing) Then
        _Trading_BaselineGearing = Math.Abs(value)
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_MaxDrawdown() As Double
    Get
      Return _Trading_MaxDrawdown
    End Get
    Set(ByVal value As Double)
      If Not (Math.Abs(value) = _Trading_MaxDrawdown) Then
        _Trading_MaxDrawdown = Math.Abs(value)
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_DeltaMoveLimit() As Double
    Get
      Return _Trading_DeltaMoveLimit
    End Get
    Set(ByVal value As Double)
      If Not (Math.Abs(value) = _Trading_DeltaMoveLimit) Then
        _Trading_DeltaMoveLimit = Math.Abs(value)
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_DrawdownMonthlyReset() As Boolean
    Get
      Return _Trading_DrawdownMonthlyReset
    End Get
    Set(ByVal value As Boolean)
      If Not (value = _Trading_DrawdownMonthlyReset) Then
        _Trading_DrawdownMonthlyReset = value
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_DrawdownResetMonths() As Integer
    Get
      Return _Trading_DrawdownResetMonths
    End Get
    Set(ByVal value As Integer)
      If Not (Math.Abs(value) = _Trading_DrawdownResetMonths) Then
        _Trading_DrawdownResetMonths = Math.Abs(value)
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_TradingCosts() As Double
    Get
      Return _Trading_TradingCosts
    End Get
    Set(ByVal value As Double)
      If Not (Math.Abs(value) = _Trading_TradingCosts) Then
        _Trading_TradingCosts = Math.Abs(value)
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_FundingSpread() As Double
    Get
      Return _Trading_FundingSpread
    End Get
    Set(ByVal value As Double)
      If Not (Math.Abs(value) = _Trading_FundingSpread) Then
        _Trading_FundingSpread = Math.Abs(value)
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_StartDate() As Date
    Get
      Return _Trading_StartDate
    End Get
    Set(ByVal value As Date)
      If Not (value = _Trading_StartDate) Then
        _Trading_StartDate = value
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_FirstTradeAtStart() As Boolean
    Get
      Return _Trading_FirstTradeAtStart
    End Get
    Set(ByVal value As Boolean)
      If Not (value = _Trading_FirstTradeAtStart) Then
        _Trading_FirstTradeAtStart = value
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public ReadOnly Property Trading_VolatilityWeighting() As Boolean
    Get
      Return (Trading_VolatilityWeighting_StdDevHighVsAverage Or Trading_VolatilityWeighting_StdDevVsGivenDuration)
    End Get
  End Property

  Public Property Trading_VolatilityWeighting_StdDevHighVsAverage() As Boolean
    Get
      Return _Trading_VolatilityWeighting_StdDevHighVsAverage
    End Get
    Set(ByVal value As Boolean)
      If Not (value = _Trading_VolatilityWeighting_StdDevHighVsAverage) Then
        _Trading_VolatilityWeighting_StdDevHighVsAverage = value

        TradeHistoryToUpdate = True

        If (value) Then
          Trading_VolatilityWeighting_StdDevVsGivenDuration = False
        End If
      End If
    End Set
  End Property

  Public Property Trading_VolatilityWeighting_StdDevVsGivenDuration() As Boolean
    Get
      Return _Trading_VolatilityWeighting_StdDevVsGivenDuration
    End Get
    Set(ByVal value As Boolean)
      If Not (value = _Trading_VolatilityWeighting_StdDevVsGivenDuration) Then
        _Trading_VolatilityWeighting_StdDevVsGivenDuration = value

        TradeHistoryToUpdate = True

        If (value) Then
          Trading_VolatilityWeighting_StdDevHighVsAverage = False
        End If
      End If
    End Set
  End Property

  Public Property Trading_VolatilityWeighting_VolWeightDuration() As Integer
    Get
      Return _Trading_VolatilityWeighting_VolWeightDuration
    End Get
    Set(ByVal value As Integer)
      If Not (value = _Trading_VolatilityWeighting_VolWeightDuration) Then
        _Trading_VolatilityWeighting_VolWeightDuration = value
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_VolatilityWeighting_Exponent() As Double
    Get
      Return _Trading_VolatilityWeighting_Exponent
    End Get
    Set(ByVal value As Double)
      If Not (value = _Trading_VolatilityWeighting_Exponent) Then
        _Trading_VolatilityWeighting_Exponent = value
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_VolatilityWeighting_VolWeightCap() As Double
    Get
      Return _Trading_VolatilityWeighting_VolWeightCap
    End Get
    Set(ByVal value As Double)
      If Not (value = _Trading_VolatilityWeighting_VolWeightCap) Then
        _Trading_VolatilityWeighting_VolWeightCap = value
        TradeHistoryToUpdate = True
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_ByIndexZScore() As Boolean
    Get
      Return _Trading_BandWeighting_ByIndexZScore
    End Get
    Set(ByVal value As Boolean)
      If Not (value = _Trading_BandWeighting_ByIndexZScore) Then
        _Trading_BandWeighting_ByIndexZScore = value

        TradingBandToUpdate = True

        If (value) Then
          Trading_BandWeighting_ByAverageZScore = False
          Trading_BandWeighting_ByPercentageBand = False
        End If
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_ByAverageZScore() As Boolean
    Get
      Return _Trading_BandWeighting_ByAverageZScore
    End Get
    Set(ByVal value As Boolean)
      If Not (value = _Trading_BandWeighting_ByAverageZScore) Then
        _Trading_BandWeighting_ByAverageZScore = value

        TradingBandToUpdate = True

        If (value) Then
          Trading_BandWeighting_ByIndexZScore = False
          Trading_BandWeighting_ByPercentageBand = False
        End If
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_ByPercentageBand() As Boolean
    Get
      Return _Trading_BandWeighting_ByPercentageBand
    End Get
    Set(ByVal value As Boolean)
      If Not (value = _Trading_BandWeighting_ByPercentageBand) Then
        _Trading_BandWeighting_ByPercentageBand = value

        TradingBandToUpdate = True

        If (value) Then
          Trading_BandWeighting_ByIndexZScore = False
          Trading_BandWeighting_ByAverageZScore = False
        End If
      End If
    End Set
  End Property

  Public ReadOnly Property Trading_BandWeighting() As Boolean
    Get
      Return (Trading_BandWeighting_ByIndexZScore OrElse Trading_BandWeighting_ByAverageZScore OrElse Trading_BandWeighting_ByPercentageBand)
    End Get
  End Property

  Public Property Trading_BandWeighting_IndexZScoreThreshold() As Double
    Get
      Return _Trading_BandWeighting_IndexZScoreThreshold
    End Get
    Set(ByVal value As Double)
      If Not (value = _Trading_BandWeighting_IndexZScoreThreshold) Then
        _Trading_BandWeighting_IndexZScoreThreshold = value

        If (Trading_BandWeighting_ByIndexZScore) Then
          TradingBandToUpdate = True
        End If
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_AverageZScoreThreshold() As Double
    Get
      Return _Trading_BandWeighting_AverageZScoreThreshold
    End Get
    Set(ByVal value As Double)
      If Not (value = _Trading_BandWeighting_AverageZScoreThreshold) Then
        _Trading_BandWeighting_AverageZScoreThreshold = value

        If (Trading_BandWeighting_ByAverageZScore) Then
          TradingBandToUpdate = True
        End If
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_ZScoreStdDevPeriod() As Integer
    Get
      Return _Trading_BandWeighting_ZScoreStdDevPeriod
    End Get
    Set(ByVal value As Integer)
      If Not (value = _Trading_BandWeighting_ZScoreStdDevPeriod) Then
        _Trading_BandWeighting_ZScoreStdDevPeriod = value

        If (Trading_BandWeighting) Then
          TradingBandToUpdate = True
        End If
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_PercentageBandThreshold() As Double
    Get
      Return _Trading_BandWeighting_PercentageBandThreshold
    End Get
    Set(ByVal value As Double)
      If Not (value = _Trading_BandWeighting_PercentageBandThreshold) Then
        _Trading_BandWeighting_PercentageBandThreshold = value

        If (Trading_BandWeighting_ByPercentageBand) Then
          TradingBandToUpdate = True
        End If
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_WeightingDecayFactor() As Double
    Get
      Return _Trading_BandWeighting_WeightingDecayFactor
    End Get
    Set(ByVal value As Double)
      If Not (value = _Trading_BandWeighting_WeightingDecayFactor) Then
        _Trading_BandWeighting_WeightingDecayFactor = value

        If (Trading_BandWeighting) Then
          TradingBandToUpdate = True
        End If
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_DailyWeightDeltaLimit() As Double
    Get
      Return _Trading_BandWeighting_DailyWeightDeltaLimit
    End Get
    Set(ByVal value As Double)
      If Not (value = _Trading_BandWeighting_DailyWeightDeltaLimit) Then
        _Trading_BandWeighting_DailyWeightDeltaLimit = value

        If (Trading_BandWeighting) Then
          TradingBandToUpdate = True
        End If
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_ReInvestmentEvaluationPeriod() As Integer
    Get
      Return _Trading_BandWeighting_ReInvestmentEvaluationPeriod
    End Get
    Set(ByVal value As Integer)
      If Not (value = _Trading_BandWeighting_ReInvestmentEvaluationPeriod) Then
        _Trading_BandWeighting_ReInvestmentEvaluationPeriod = value

        If (Trading_BandWeighting) Then
          TradingBandToUpdate = True
        End If
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_LocalHighExclusionPeriod() As Integer
    Get
      Return _Trading_BandWeighting_LocalHighExclusionPeriod
    End Get
    Set(ByVal value As Integer)
      If Not (value = _Trading_BandWeighting_LocalHighExclusionPeriod) Then
        _Trading_BandWeighting_LocalHighExclusionPeriod = value

        If (Trading_BandWeighting) Then
          TradingBandToUpdate = True
        End If
      End If
    End Set
  End Property

  Public Property Trading_BandWeighting_PhaseOutAtCrossing() As Boolean
    Get
      Return _Trading_BandWeighting_PhaseOutAtCrossing
    End Get
    Set(ByVal value As Boolean)
      If Not (value = _Trading_BandWeighting_PhaseOutAtCrossing) Then
        _Trading_BandWeighting_PhaseOutAtCrossing = value

        If (Trading_BandWeighting) Then
          TradeHistoryToUpdate = True
        End If
      End If
    End Set
  End Property

  Public Property Trading_CrossingUnweightPoint() As Double
    Get
      Return _Trading_CrossingUnweightPoint
    End Get
    Set(ByVal value As Double)
      If Not (value = _Trading_CrossingUnweightPoint) Then
        _Trading_CrossingUnweightPoint = value

        If (Trading_BandWeighting) Then
          TradeHistoryToUpdate = True
        End If
      End If
    End Set
  End Property

  Public Property Result_MaxDrawdown() As Double
    Get
      Return _Result_MaxDrawdown
    End Get
    Set(ByVal value As Double)
      _Result_MaxDrawdown = value
    End Set
  End Property

  Public Property Result_IRR() As Double
    Get
      Return _Result_IRR
    End Get
    Set(ByVal value As Double)
      _Result_IRR = value
    End Set
  End Property

  Public Property Result_ITDReturn() As Double
    Get
      Return _Result_ITDReturn
    End Get
    Set(ByVal value As Double)
      _Result_ITDReturn = value
    End Set
  End Property

  Public Property Result_FirstDate() As Date
    Get
      Return _Result_FirstDate
    End Get
    Set(ByVal value As Date)
      _Result_FirstDate = value
    End Set
  End Property

  Public Property Result_LastDate() As Date
    Get
      Return _Result_LastDate
    End Get
    Set(ByVal value As Date)
      _Result_LastDate = value
    End Set
  End Property

  Public ReadOnly Property PortfolioDateSeries() As Date()
    Get

      SyncLock _Lock_PortfolioSeries

        ' Series Already Exists ?

        If (_PortfolioDateSeries IsNot Nothing) Then
          Return _PortfolioDateSeries
        End If

        Dim RVal() As Date = Nothing

        Try

          If Me.PortfolioHistory IsNot Nothing Then
            Dim StartIndex As Integer
            Dim DateIndex As Integer
            Dim InitialNAV As Double

            For DateIndex = 0 To (PortfolioHistory.Length - 1)
              If (PortfolioHistory(DateIndex) IsNot Nothing) Then
                InitialNAV = PortfolioHistory(DateIndex).NAVClose
                StartIndex = DateIndex
                Exit For
              End If
            Next

            ReDim _PortfolioDateSeries(PortfolioHistory.Length - (StartIndex + 1))
            ReDim _PortfolioNAVSeries(PortfolioHistory.Length - (StartIndex + 1))

            For DateIndex = StartIndex To (PortfolioHistory.Length - 1)
              _PortfolioDateSeries(DateIndex - StartIndex) = PortfolioHistory(DateIndex).PortfolioDate

              If (InitialNAV <> 0.0#) Then
                _PortfolioNAVSeries(DateIndex - StartIndex) = (PortfolioHistory(DateIndex).NAVClose * 100.0#) / InitialNAV
              End If
            Next

            RVal = _PortfolioDateSeries
          End If

        Catch ex As Exception
        End Try

        Return RVal

      End SyncLock
    End Get
  End Property

  Public ReadOnly Property PortfolioNAVSeries() As Double()
    Get

      SyncLock _Lock_PortfolioSeries

        ' Series Already Exists ?

        If (_PortfolioNAVSeries IsNot Nothing) Then
          Return _PortfolioNAVSeries
        End If

        Dim RVal() As Double = Nothing

        Try

          If Me.PortfolioHistory IsNot Nothing Then
            Dim StartIndex As Integer
            Dim DateIndex As Integer
            Dim InitialNAV As Double

            For DateIndex = 0 To (PortfolioHistory.Length - 1)
              If (PortfolioHistory(DateIndex) IsNot Nothing) Then
                InitialNAV = PortfolioHistory(DateIndex).NAVClose
                StartIndex = DateIndex
                Exit For
              End If
            Next

            ReDim _PortfolioDateSeries(PortfolioHistory.Length - (StartIndex + 1))
            ReDim _PortfolioNAVSeries(PortfolioHistory.Length - (StartIndex + 1))

            For DateIndex = StartIndex To (PortfolioHistory.Length - 1)
              _PortfolioDateSeries(DateIndex - StartIndex) = PortfolioHistory(DateIndex).PortfolioDate

              If (InitialNAV <> 0.0#) Then
                _PortfolioNAVSeries(DateIndex - StartIndex) = (PortfolioHistory(DateIndex).NAVClose * 100.0#) / InitialNAV
              End If
            Next

            RVal = _PortfolioNAVSeries
          End If

        Catch ex As Exception
        End Try

        Return RVal

      End SyncLock
    End Get
  End Property

  Public Sub GetObjectData(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext) Implements System.Runtime.Serialization.ISerializable.GetObjectData

    info.AddValue("SPC_SerialiseArrays", SerialiseArrays)
    info.AddValue("SPC_Version", CDbl(1.3))

    info.AddValue("SPC_InstrumentID", InstrumentID)
    info.AddValue("SPC_InterestRateID", InterestRateID)
    info.AddValue("SPC_DataStartDate", DataStartDate)
    info.AddValue("SPC_StatsDatePeriod", CInt(StatsDatePeriod))

    info.AddValue("SPC_InitialNAV", InitialNAV)

    info.AddValue("SPC_MovingAverage_Days", MovingAverageDays)
    info.AddValue("SPC_MovingAverage_ExponentialPricingAverage", MovingAverage_ExponentialPricingAverage)
    info.AddValue("SPC_MovingAverage_ExponentialPricingLambda", MovingAverage_ExponentialPricingLambda)
    info.AddValue("SPC_MovingAverage_ExponentialWeightedAverage", MovingAverage_ExponentialWeightedAverage)
    info.AddValue("SPC_MovingAverage_ExponentialWeightedAverageLambda", MovingAverage_ExponentialWeightedAverageLambda)
    info.AddValue("SPC_MovingAverage_LinearWeightedAverage", MovingAverage_LinearWeightedAverage)
    info.AddValue("SPC_MovingAverage_LinearWeightedAverageLambda", MovingAverage_LinearWeightedAverageLambda)
    info.AddValue("SPC_MovingAverage_VolatilityWeightedDuration", MovingAverage_VolatilityWeightedDuration)
    info.AddValue("SPC_MovingAverage_VolatilityWeightedDurationLambda", MovingAverage_VolatilityWeightedDurationLambda)

    info.AddValue("SPC_Trading_AllowLongPosition", Trading_AllowLongPosition)
    info.AddValue("SPC_Trading_AllowShortPosition", Trading_AllowShortPosition)

    info.AddValue("SPC_Trading_RebalanceThreshold", Trading_RebalanceThreshold)
    info.AddValue("SPC_Trading_BaselineGearing", Trading_BaselineGearing)
    info.AddValue("SPC_Trading_DeltaMoveLimit", Trading_DeltaMoveLimit) ' As of version 1.3
    info.AddValue("SPC_Trading_MaxDrawdown", Trading_MaxDrawdown)
    info.AddValue("SPC_Trading_DrawdownMonthlyReset", Trading_DrawdownMonthlyReset) ' As of Version 1.0
    info.AddValue("SPC_Trading_DrawdownResetMonths", Trading_DrawdownResetMonths) ' As of Version 1.0
    info.AddValue("SPC_Trading_TradingCosts", Trading_TradingCosts)
    info.AddValue("SPC_Trading_FundingSpread", Trading_FundingSpread)
    info.AddValue("SPC_Trading_StartDate", Trading_StartDate)
    info.AddValue("SPC_Trading_FirstTradeAtStart", Trading_FirstTradeAtStart)
    info.AddValue("SPC_Trading_VolatilityWeighting_StdDevHighVsAverage", Trading_VolatilityWeighting_StdDevHighVsAverage)
    info.AddValue("SPC_Trading_VolatilityWeighting_StdDevVsGivenDuration", Trading_VolatilityWeighting_StdDevVsGivenDuration)
    info.AddValue("SPCTrading_VolatilityWeighting_VolWeightDuration", Trading_VolatilityWeighting_VolWeightDuration)
    info.AddValue("SPC_Trading_VolatilityWeighting_VolWeightCap", Trading_VolatilityWeighting_VolWeightCap)
    info.AddValue("SPC_Trading_VolatilityWeighting_Exponent", Trading_VolatilityWeighting_Exponent)
    info.AddValue("SPC_Trading_BandWeighting_ByIndexZScore", Trading_BandWeighting_ByIndexZScore)
    info.AddValue("SPC_Trading_BandWeighting_IndexZScoreThreshold", Trading_BandWeighting_IndexZScoreThreshold)
    info.AddValue("SPC_Trading_BandWeighting_ByAverageZScore", Trading_BandWeighting_ByAverageZScore)
    info.AddValue("SPC_Trading_BandWeighting_AverageZScoreThreshold", Trading_BandWeighting_AverageZScoreThreshold)
    info.AddValue("SPC_Trading_BandWeighting_ZScoreStdDevPeriod", Trading_BandWeighting_ZScoreStdDevPeriod)
    info.AddValue("SPC_Trading_BandWeighting_ByPercentageBand", Trading_BandWeighting_ByPercentageBand)
    info.AddValue("SPC_Trading_BandWeighting_PercentageBandThreshold", Trading_BandWeighting_PercentageBandThreshold)
    info.AddValue("SPC_Trading_BandWeighting_WeightingDecayFactor", Trading_BandWeighting_WeightingDecayFactor)
    info.AddValue("SPC_Trading_BandWeighting_ReInvestmentEvaluationPeriod", Trading_BandWeighting_ReInvestmentEvaluationPeriod)
    info.AddValue("SPC_Trading_BandWeighting_DailyWeightDeltaLimit", Trading_BandWeighting_DailyWeightDeltaLimit)
    info.AddValue("SPC_Trading_BandWeighting_LocalHighExclusionPeriod", Trading_BandWeighting_LocalHighExclusionPeriod)
    info.AddValue("SPC_Trading_BandWeighting_PhaseOutAtCrossing", Trading_BandWeighting_PhaseOutAtCrossing)
    info.AddValue("SPC_Trading_CrossingUnweightPoint", Trading_CrossingUnweightPoint)

    info.AddValue("SPC_Result_MaxDrawdown", Result_MaxDrawdown)
    info.AddValue("SPC_Result_IRR", Result_IRR)
    info.AddValue("SPC_Result_ITDReturn", Result_ITDReturn)
    info.AddValue("SPC_Result_FirstDate", Result_FirstDate)
    info.AddValue("SPC_Result_LastDate", Result_LastDate)

    If (SerialiseArrays) Then
      info.AddValue("SPC_InstrumentDate", InstrumentDate, GetType(Date()))
      info.AddValue("SPC_InstrumentReturns", InstrumentReturns, GetType(Double()))
      info.AddValue("SPC_InstrumentNAV", InstrumentNAV, GetType(Double()))
      info.AddValue("SPC_InstrumentStDev", InstrumentStDev, GetType(Double()))

      info.AddValue("SPC_InterestRateDate", InterestRateDate, GetType(Date()))
      info.AddValue("SPC_InterestRateReturns", InterestRateReturns, GetType(Double()))
      info.AddValue("SPC_InterestRateNAV", InterestRateNAV, GetType(Double()))

      info.AddValue("SPC_AverageSeriesDate", AverageSeriesDate, GetType(Date()))
      info.AddValue("SPC_AverageSeriesReturns", AverageSeriesReturns, GetType(Double()))
      info.AddValue("SPC_AverageSeriesNAV", AverageSeriesNAV, GetType(Double()))

      info.AddValue("SPC_PortfolioHistory", PortfolioHistory, GetType(DailyPortfolioClass()))
      info.AddValue("SPC_BandGearing_StDevSeries", BandGearing_StDevSeries, GetType(Double()))
      info.AddValue("SPC_BandGearing_AverageSeries", BandGearing_AverageSeries, GetType(Double()))
    End If

  End Sub

  Public Shared Function SerialiseParameterClass(ByVal Simulation As SimulationParameterClass) As String

    Dim ms As New System.IO.MemoryStream
    Dim Formatter As New BinaryFormatter
    Dim DefinitionString As String = ""

    Formatter.Serialize(ms, Simulation)
    ms.Position = 0
    DefinitionString = Convert.ToBase64String(ms.ToArray())
    ms.Close()

    Return DefinitionString

  End Function

  Public Shared Function DeSerialiseParameterClass(ByVal SimulationDefinition As String) As SimulationParameterClass

    Try
      If (SimulationDefinition.length > 1) Then
        Return CType((New BinaryFormatter).Deserialize(New System.IO.MemoryStream(Convert.FromBase64String(SimulationDefinition))), CTASimulationFunctions.SimulationParameterClass)
      End If
    Catch ex As Exception
    End Try

    Return New CTASimulationFunctions.SimulationParameterClass

  End Function

End Class

Public Class TradeSimulation
  Private _SimulationLock As Object

  Sub New()
    _SimulationLock = New Object
  End Sub

  Private Sub UpdateSeries(ByVal pStatFunctions As StatsSeriesProvider, ByVal SimulationParameters As SimulationParameterClass)
    ' *********************************************************************************
    '
    '
    '
    ' *********************************************************************************

    Try

      If (SimulationParameters.SeriesToUpdate) Then

        If (SimulationParameters.InstrumentID > 0) Then

          SimulationParameters.InstrumentDate = pStatFunctions.DateSeries(SimulationParameters.StatsDatePeriod, SimulationParameters.InstrumentID, False, SimulationParameters.MovingAverageDays, 1.0#, True, SimulationParameters.DataStartDate, Renaissance_EndDate_Data)
          SimulationParameters.InstrumentReturns = pStatFunctions.ReturnSeries(SimulationParameters.StatsDatePeriod, SimulationParameters.InstrumentID, False, SimulationParameters.MovingAverageDays, 1.0#, True, SimulationParameters.DataStartDate, Renaissance_EndDate_Data)
          SimulationParameters.InstrumentNAV = pStatFunctions.NAVSeries(SimulationParameters.StatsDatePeriod, SimulationParameters.InstrumentID, False, SimulationParameters.MovingAverageDays, 1.0#, True, SimulationParameters.DataStartDate, Renaissance_EndDate_Data, 1.0#, False, 100.0#)

          If (SimulationParameters.InstrumentDate IsNot Nothing) Then
            SimulationParameters.InstrumentStDev = Array.CreateInstance(GetType(Double), SimulationParameters.InstrumentDate.Length)
            Array.Copy(pStatFunctions.StdDevSeries(SimulationParameters.StatsDatePeriod, SimulationParameters.InstrumentID, False, SimulationParameters.MovingAverageDays, 1.0#, False, SimulationParameters.DataStartDate, Renaissance_EndDate_Data), 0, SimulationParameters.InstrumentStDev, 0, SimulationParameters.InstrumentStDev.Length)
          Else
            ReDim SimulationParameters.InstrumentDate(-1)
            ReDim SimulationParameters.InstrumentReturns(-1)
            ReDim SimulationParameters.InstrumentNAV(-1)
            ReDim SimulationParameters.InstrumentStDev(-1)

            ReDim SimulationParameters.AverageSeriesDate(-1)
            ReDim SimulationParameters.AverageSeriesReturns(-1)
            ReDim SimulationParameters.AverageSeriesNAV(-1)
          End If

        Else
          SimulationParameters.AverageSeriesToUpdate = False

          ReDim SimulationParameters.InstrumentDate(-1)
          ReDim SimulationParameters.InstrumentReturns(-1)
          ReDim SimulationParameters.InstrumentNAV(-1)
          ReDim SimulationParameters.InstrumentStDev(-1)

          ReDim SimulationParameters.AverageSeriesDate(-1)
          ReDim SimulationParameters.AverageSeriesReturns(-1)
          ReDim SimulationParameters.AverageSeriesNAV(-1)

        End If

        SimulationParameters.SeriesToUpdate = False

      End If

      ' *********************************************************************************
      ' Calculate Average Series.
      ' *********************************************************************************

      If (SimulationParameters.AverageSeriesToUpdate) AndAlso (SimulationParameters.InstrumentID > 0) Then
        Try
          Dim WeightMask(SimulationParameters.MovingAverageDays) As Double
          Dim Lambda As Double
          Dim Index As Integer
          Dim DateIndex As Integer
          Dim AverageCount As Integer
          Dim AverageSum As Double
          Dim SumWeight As Double

          Dim ThisInstrumentNAV() As Double

          SimulationParameters.AverageSeriesDate = Array.CreateInstance(GetType(Date), SimulationParameters.InstrumentDate.Length)
          SimulationParameters.AverageSeriesReturns = Array.CreateInstance(GetType(Double), SimulationParameters.InstrumentDate.Length)
          SimulationParameters.AverageSeriesNAV = Array.CreateInstance(GetType(Double), SimulationParameters.InstrumentDate.Length)

          Array.Copy(SimulationParameters.InstrumentDate, SimulationParameters.AverageSeriesDate, SimulationParameters.InstrumentDate.Length)

          ' Normalise and Raise-to-Power Instrument NAV Series, if necessary.
          ' Preserve original NAV series for restore later.

          If (SimulationParameters.MovingAverage_ExponentialPricingAverage) Then
            ThisInstrumentNAV = Array.CreateInstance(GetType(Double), SimulationParameters.InstrumentDate.Length)
            Array.Copy(SimulationParameters.InstrumentNAV, ThisInstrumentNAV, SimulationParameters.InstrumentNAV.Length)
            Lambda = SimulationParameters.MovingAverage_ExponentialPricingLambda

            For DateIndex = 0 To (SimulationParameters.InstrumentDate.Length - 1)
              ThisInstrumentNAV(DateIndex) = (ThisInstrumentNAV(DateIndex) / SimulationParameters.InstrumentNAV(0)) ^ Lambda
            Next
          Else
            ThisInstrumentNAV = SimulationParameters.InstrumentNAV
          End If

          ' Calculate Average as required

          If (SimulationParameters.MovingAverage_ExponentialWeightedAverage) Then

            Lambda = SimulationParameters.MovingAverage_ExponentialWeightedAverageLambda

            WeightMask(0) = 1.0#

            For Index = 1 To (WeightMask.Length - 1)
              WeightMask(Index) = WeightMask(Index - 1) * Lambda
            Next

            For DateIndex = 0 To (SimulationParameters.InstrumentDate.Length - 1)
              SumWeight = 0.0#
              AverageSum = 0.0#
              AverageCount = 0

              For Index = Math.Max(0, (DateIndex - SimulationParameters.MovingAverageDays) + 1) To DateIndex
                SumWeight += WeightMask(DateIndex - Index)
                AverageSum += ThisInstrumentNAV(Index) * WeightMask(DateIndex - Index)
                AverageCount += 1
              Next

              If (SumWeight <> 0) Then
                SimulationParameters.AverageSeriesNAV(DateIndex) = (AverageSum / SumWeight)
              End If

            Next

          ElseIf (SimulationParameters.MovingAverage_LinearWeightedAverage) Then

            Dim Multiplier As Double = 1.0#

            Lambda = (1.0# - SimulationParameters.MovingAverage_LinearWeightedAverageLambda) / SimulationParameters.MovingAverageDays

            WeightMask(0) = 1.0#

            For Index = 1 To (WeightMask.Length - 1)
              WeightMask(Index) = WeightMask(Index - 1) - Lambda
            Next

            For DateIndex = 0 To (SimulationParameters.InstrumentDate.Length - 1)
              SumWeight = 0.0#
              AverageSum = 0.0#
              AverageCount = 0

              For Index = Math.Max(0, (DateIndex - SimulationParameters.MovingAverageDays) + 1) To DateIndex
                SumWeight += WeightMask(DateIndex - Index)
                AverageSum += ThisInstrumentNAV(Index) * WeightMask(DateIndex - Index)
                AverageCount += 1
              Next

              If (SumWeight <> 0) Then
                SimulationParameters.AverageSeriesNAV(DateIndex) = (AverageSum / SumWeight)
              End If

            Next

          ElseIf (SimulationParameters.MovingAverage_VolatilityWeightedDuration) Then

          Else 'If (Radio_UnAdjustedAverage.Checked) Then

            For DateIndex = 0 To (SimulationParameters.InstrumentDate.Length - 1)
              SumWeight = 0.0#
              AverageSum = 0.0#
              AverageCount = 0

              For Index = Math.Max(0, (DateIndex - SimulationParameters.MovingAverageDays) + 1) To DateIndex
                SumWeight += 1.0# ' WeightMask(DateIndex - Index)
                AverageSum += ThisInstrumentNAV(Index) ' * WeightMask(DateIndex - Index)
                AverageCount += 1
              Next

              If (SumWeight <> 0.0#) Then
                SimulationParameters.AverageSeriesNAV(DateIndex) = (AverageSum / SumWeight)
              End If

            Next

          End If

          ' De-Power & De-Normalise Average NAV series, if necessary.

          ThisInstrumentNAV = Nothing

          If (SimulationParameters.MovingAverage_ExponentialPricingAverage) Then

            Lambda = SimulationParameters.MovingAverage_ExponentialPricingLambda

            For DateIndex = 0 To (SimulationParameters.AverageSeriesNAV.Length - 1)
              SimulationParameters.AverageSeriesNAV(DateIndex) = (SimulationParameters.AverageSeriesNAV(DateIndex) ^ (1.0# / Lambda)) * SimulationParameters.InstrumentNAV(0)
            Next
          End If

          ' Calculate Average Series Returns.

          For DateIndex = 1 To (SimulationParameters.InstrumentDate.Length - 1)
            If (SimulationParameters.AverageSeriesNAV(DateIndex - 1) <> 0) Then
              SimulationParameters.AverageSeriesReturns(DateIndex) = (SimulationParameters.AverageSeriesNAV(DateIndex) / SimulationParameters.AverageSeriesNAV(DateIndex - 1)) - 1.0#
            End If
          Next

        Catch ex As Exception
        End Try

      End If

      SimulationParameters.AverageSeriesToUpdate = False

      ' *********************************************************************************
      '
      ' *********************************************************************************

      If (SimulationParameters.InterestRateToUpdate) Then

        If (SimulationParameters.InterestRateID > 0) Then

          SimulationParameters.InterestRateDate = pStatFunctions.DateSeries(SimulationParameters.StatsDatePeriod, SimulationParameters.InterestRateID, False, SimulationParameters.MovingAverageDays, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
          SimulationParameters.InterestRateReturns = pStatFunctions.ReturnSeries(SimulationParameters.StatsDatePeriod, SimulationParameters.InterestRateID, False, SimulationParameters.MovingAverageDays, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
          SimulationParameters.InterestRateNAV = pStatFunctions.NAVSeries(SimulationParameters.StatsDatePeriod, SimulationParameters.InterestRateID, False, SimulationParameters.MovingAverageDays, 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data, 1.0#, False, 100.0#)

        Else

          ReDim SimulationParameters.InterestRateDate(-1)
          ReDim SimulationParameters.InterestRateReturns(-1)
          ReDim SimulationParameters.InterestRateNAV(-1)

        End If

        SimulationParameters.InterestRateToUpdate = False

      End If

    Catch ex As Exception
    End Try

  End Sub

  Public Function RunTrandingSimulation(ByVal pStatFunctions As StatsSeriesProvider, ByVal SimulationParameters As SimulationParameterClass, ByVal ParentStatsID As ULong, ByRef DependencyList As List(Of ULong)) As Boolean
    ' *********************************************************************************
    '
    '
    ' *********************************************************************************
    '        If (ParentStatsID > 0) AndAlso (DependencyList IsNot Nothing) Then

    SyncLock _SimulationLock

      Dim MaxDrawDown As Double = 0
      Dim ThisPortfolioClass As DailyPortfolioClass
      Dim DateIndex As Integer
      Dim StartIndex As Integer = 1

      ' Update Underlying series data, if necessary.

      Try
        If SimulationParameters.SeriesToUpdate OrElse SimulationParameters.InterestRateToUpdate OrElse SimulationParameters.AverageSeriesToUpdate Then
          UpdateSeries(pStatFunctions, SimulationParameters)
        End If
      Catch ex As Exception
        Return False
      End Try

      Try
        If (SimulationParameters.InstrumentID > 0) AndAlso (ParentStatsID > 0) AndAlso (DependencyList IsNot Nothing) Then
          DependencyList.Add(pStatFunctions.SetDependsOnMe(ParentStatsID, pStatFunctions.GetStatCacheKey(SimulationParameters.StatsDatePeriod, CULng(SimulationParameters.InstrumentID), False)))
        End If

        If (SimulationParameters.InterestRateID > 0) AndAlso (ParentStatsID > 0) AndAlso (DependencyList IsNot Nothing) Then
          DependencyList.Add(pStatFunctions.SetDependsOnMe(ParentStatsID, pStatFunctions.GetStatCacheKey(SimulationParameters.StatsDatePeriod, CULng(SimulationParameters.InterestRateID), False)))
        End If

      Catch ex As Exception
      End Try

      Try

        Dim IR_DateIndex As Integer
        Dim IR_StartIndex As Integer
        Dim IR_MAXDateIndex As Integer = (-1)
        Dim MovingAverageDays As Integer = SimulationParameters.MovingAverageDays

        Dim dt As Double = (1.0# / pStatFunctions.AnnualPeriodCount(SimulationParameters.StatsDatePeriod))
        Dim IR_Spread As Double = 0.0#
        Dim GammaLimit As Double = SimulationParameters.Trading_RebalanceThreshold
        Dim VolModifier As Double = 1.0#
        Dim BandGearingModifier As Double = 1.0#
        Dim thisVol As Double
        Dim Gearing As Double = SimulationParameters.Trading_BaselineGearing
        Dim NetPositionModifier As Double

        Dim DrawDownLimit As Double = Math.Abs(SimulationParameters.Trading_MaxDrawdown)
        Dim TradingCostsPercentage As Double = Math.Abs(SimulationParameters.Trading_TradingCosts)
        Dim ThisTradingFee As Double
        Dim FirstCrossingHasOccurred As Boolean = False
        Dim PositionSign As Double = 1.0#

        ReDim SimulationParameters.PortfolioHistory(SimulationParameters.InstrumentDate.Length - 1)

        Dim LnReturns(-1) As Double
        Dim InstrumentVolSeries(-1) As Double
        Dim BandGearing_BandWidth As Double
        Dim BandGearing_BoundaryValue As Double

        Dim TradingVolSeries(-1) As Double
        Dim AvgOfTradingVolSeries(-1) As Double
        Dim AvgOfInstrumentVolSeries(-1) As Double
        Dim Trading_VolatilityExponent As Double = SimulationParameters.Trading_VolatilityWeighting_Exponent

        Dim MonthStartNAV As Double
        Dim MonthlyDrawdown As Double
        Dim InDrawdown As Boolean = False
        Dim BandUsage As Double

        If (SimulationParameters.InstrumentDate.Length <= 0) Then
          Return False
          Exit Function
        End If

        If (SimulationParameters.Trading_StartDate > SimulationParameters.InstrumentDate(0)) Then
          StartIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(SimulationParameters.StatsDatePeriod, SimulationParameters.InstrumentDate(0), SimulationParameters.Trading_StartDate)
        End If

        If (SimulationParameters.InterestRateNAV IsNot Nothing) AndAlso (SimulationParameters.InterestRateNAV.Length > 0) Then

          IR_StartIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(SimulationParameters.StatsDatePeriod, SimulationParameters.InterestRateDate(0), SimulationParameters.InstrumentDate(0))
          IR_DateIndex = IR_StartIndex
          IR_MAXDateIndex = (SimulationParameters.InterestRateNAV.Length - 1)

        End If

        ' *********************************************************************************
        ' Initialise Trading Simulation :
        ' *********************************************************************************

        SimulationParameters.ClearPortfolioResultSeries()

        Dim LastPortfolioClass As DailyPortfolioClass

        ' Set Vol Weight Series

        If (SimulationParameters.Trading_VolatilityWeighting) Then

          Dim SubIndex As Integer
          Dim MaxReturn As Double
          Dim ThisReturn As Double
          Dim SqrtOfPeriods As Double = Math.Sqrt(MovingAverageDays)
          Dim SumOfTradingVolSeries As Double
          Dim SumOfInstrumentVolSeries As Double
          Dim CalibrationEndDate As Date = CDate("1 Dec 2008")
          Dim CalibrationEndIndex As Integer = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(SimulationParameters.StatsDatePeriod, SimulationParameters.InstrumentDate(0), CalibrationEndDate)

          If (SimulationParameters.Trading_VolatilityWeighting_StdDevHighVsAverage) Then
            LnReturns = Array.CreateInstance(GetType(Double), SimulationParameters.InstrumentStDev.Length)
            InstrumentVolSeries = Array.CreateInstance(GetType(Double), SimulationParameters.InstrumentStDev.Length)
            AvgOfTradingVolSeries = Array.CreateInstance(GetType(Double), SimulationParameters.InstrumentStDev.Length)

            SumOfTradingVolSeries = 0.0#

            LnReturns(0) = Math.Abs(Math.Log(SimulationParameters.InstrumentReturns(1) + 1.0#)) ' (1) is not a typo - Replicate spreadsheet.
            LnReturns(1) = Math.Abs(Math.Log(SimulationParameters.InstrumentReturns(1) + 1.0#))

            For DateIndex = 2 To (SimulationParameters.InstrumentReturns.Length - 1) ' Start at 2 to omit wacky first values

              LnReturns(DateIndex) = Math.Abs(Math.Log(SimulationParameters.InstrumentReturns(DateIndex) + 1.0#))

              MaxReturn = LnReturns(DateIndex)

              For SubIndex = Math.Max(0, ((DateIndex - MovingAverageDays) + 1)) To DateIndex
                ThisReturn = LnReturns(SubIndex)
                If MaxReturn < ThisReturn Then
                  MaxReturn = ThisReturn
                End If
              Next

              InstrumentVolSeries(DateIndex) = SqrtOfPeriods * (MaxReturn ^ Trading_VolatilityExponent) ' 

              If (DateIndex = 2) Then
                InstrumentVolSeries(0) = InstrumentVolSeries(2)
                InstrumentVolSeries(1) = InstrumentVolSeries(2)
                AvgOfTradingVolSeries(0) = InstrumentVolSeries(2)
                AvgOfTradingVolSeries(1) = InstrumentVolSeries(2)

                SumOfTradingVolSeries += InstrumentVolSeries(DateIndex) + InstrumentVolSeries(DateIndex)
              End If

              SumOfTradingVolSeries += InstrumentVolSeries(DateIndex)
              AvgOfTradingVolSeries(DateIndex) = SumOfTradingVolSeries / CDbl(DateIndex + 1)
            Next

            ' Back fill 'AvgOfTradingVolSeries' for the entire calibration period.

            For DateIndex = 0 To Math.Min(CalibrationEndIndex - 1, (AvgOfTradingVolSeries.Length - 1))
              AvgOfTradingVolSeries(DateIndex) = AvgOfTradingVolSeries(Math.Min(CalibrationEndIndex, (AvgOfTradingVolSeries.Length - 1)))
            Next

          ElseIf (SimulationParameters.Trading_VolatilityWeighting_StdDevVsGivenDuration) Then

            InstrumentVolSeries = SimulationParameters.InstrumentStDev
            TradingVolSeries = Array.CreateInstance(GetType(Double), SimulationParameters.InstrumentDate.Length)
            AvgOfTradingVolSeries = Array.CreateInstance(GetType(Double), SimulationParameters.InstrumentDate.Length)
            AvgOfInstrumentVolSeries = Array.CreateInstance(GetType(Double), SimulationParameters.InstrumentDate.Length)
            Array.Copy(pStatFunctions.StdDevSeries(SimulationParameters.StatsDatePeriod, SimulationParameters.InstrumentID, False, SimulationParameters.Trading_VolatilityWeighting_VolWeightDuration, 1.0#, False, SimulationParameters.DataStartDate, Renaissance_EndDate_Data), 0, TradingVolSeries, 0, TradingVolSeries.Length)

            SumOfTradingVolSeries = 0.0#
            SumOfInstrumentVolSeries = 0.0#

            If (InstrumentVolSeries.Length > MovingAverageDays) Then
              For DateIndex = 0 To (MovingAverageDays - 1)
                AvgOfInstrumentVolSeries(DateIndex) = InstrumentVolSeries(MovingAverageDays)
              Next

              For DateIndex = MovingAverageDays To (InstrumentVolSeries.Length - 1)
                SumOfInstrumentVolSeries += InstrumentVolSeries(DateIndex)
                AvgOfInstrumentVolSeries(DateIndex) = SumOfInstrumentVolSeries / CDbl((DateIndex - MovingAverageDays) + 1)
              Next
            End If

            If (TradingVolSeries.Length > SimulationParameters.Trading_VolatilityWeighting_VolWeightDuration) Then
              For DateIndex = 0 To (SimulationParameters.Trading_VolatilityWeighting_VolWeightDuration - 1)
                AvgOfTradingVolSeries(DateIndex) = TradingVolSeries(SimulationParameters.Trading_VolatilityWeighting_VolWeightDuration)
              Next

              For DateIndex = SimulationParameters.Trading_VolatilityWeighting_VolWeightDuration To (TradingVolSeries.Length - 1)
                SumOfTradingVolSeries += TradingVolSeries(DateIndex)
                AvgOfTradingVolSeries(DateIndex) = SumOfTradingVolSeries / CDbl((DateIndex - SimulationParameters.Trading_VolatilityWeighting_VolWeightDuration) + 1)
              Next
            End If

          End If

        End If

        ' Band Gearing Series

        If (SimulationParameters.Trading_BandWeighting) Then
          Dim SdIndex As Integer

          If (SimulationParameters.Trading_BandWeighting_ByIndexZScore) Then

            pStatFunctions.CalcAvgStDevSeries(SimulationParameters.InstrumentNAV, SimulationParameters.BandGearing_AverageSeries, SimulationParameters.BandGearing_StDevSeries, SimulationParameters.Trading_BandWeighting_ZScoreStdDevPeriod, 1.0#)
            BandGearing_BoundaryValue = SimulationParameters.Trading_BandWeighting_IndexZScoreThreshold

            ' Backfill StdDev Series

            If (SimulationParameters.BandGearing_StDevSeries.Length > SimulationParameters.Trading_BandWeighting_ZScoreStdDevPeriod) Then
              For SdIndex = 0 To (SimulationParameters.Trading_BandWeighting_ZScoreStdDevPeriod - 1)
                SimulationParameters.BandGearing_StDevSeries(SdIndex) = SimulationParameters.BandGearing_StDevSeries(SimulationParameters.Trading_BandWeighting_ZScoreStdDevPeriod)
              Next
            End If

          ElseIf (SimulationParameters.Trading_BandWeighting_ByAverageZScore) Then

            pStatFunctions.CalcAvgStDevSeries(SimulationParameters.AverageSeriesNAV, SimulationParameters.BandGearing_AverageSeries, SimulationParameters.BandGearing_StDevSeries, SimulationParameters.Trading_BandWeighting_ZScoreStdDevPeriod, 1.0#)
            BandGearing_BoundaryValue = SimulationParameters.Trading_BandWeighting_AverageZScoreThreshold

            ' Backfill StdDev Series

            If (SimulationParameters.BandGearing_StDevSeries.Length > SimulationParameters.Trading_BandWeighting_ZScoreStdDevPeriod) Then
              For SdIndex = 0 To (SimulationParameters.Trading_BandWeighting_ZScoreStdDevPeriod - 1)
                SimulationParameters.BandGearing_StDevSeries(SdIndex) = SimulationParameters.BandGearing_StDevSeries(SimulationParameters.Trading_BandWeighting_ZScoreStdDevPeriod)
              Next
            End If

          ElseIf (SimulationParameters.Trading_BandWeighting_ByPercentageBand) Then

            BandGearing_BoundaryValue = SimulationParameters.Trading_BandWeighting_PercentageBandThreshold

          End If

        Else

          If (SimulationParameters.BandGearing_StDevSeries IsNot Nothing) AndAlso (SimulationParameters.BandGearing_StDevSeries.Length > 0) Then
            SimulationParameters.BandGearing_StDevSeries = Array.CreateInstance(GetType(Double), 0)
          End If

          If (SimulationParameters.BandGearing_AverageSeries IsNot Nothing) AndAlso (SimulationParameters.BandGearing_AverageSeries.Length > 0) Then
            SimulationParameters.BandGearing_AverageSeries = Array.CreateInstance(GetType(Double), 0)
          End If

        End If

        ' *********************************************************************************
        ' Set Initial Portfolio State.
        ' *********************************************************************************

        ThisPortfolioClass = New DailyPortfolioClass
        SimulationParameters.PortfolioHistory(StartIndex - 1) = ThisPortfolioClass

        ThisPortfolioClass.PortfolioDate = SimulationParameters.InstrumentDate(StartIndex - 1)
        ThisPortfolioClass.CashOpen = SimulationParameters.InitialNAV ' 1000000.0# ' SimulationParameters.InstrumentNAV(StartIndex - 1) ' 1000000
        ThisPortfolioClass.CashClose = ThisPortfolioClass.CashOpen
        ThisPortfolioClass.AssetOpen = 0.0#
        ThisPortfolioClass.AssetClose = ThisPortfolioClass.AssetOpen
        ThisPortfolioClass.NAVOpen = ThisPortfolioClass.CashOpen + ThisPortfolioClass.AssetOpen
        ThisPortfolioClass.NAVClose = ThisPortfolioClass.NAVOpen
        ThisPortfolioClass.StockPrice = SimulationParameters.InstrumentNAV(StartIndex - 1)
        ThisPortfolioClass.AverageLevel = SimulationParameters.AverageSeriesNAV(StartIndex - 1)

        ThisPortfolioClass.BestNav = ThisPortfolioClass.NAVOpen
        ThisPortfolioClass.BestNavDate = SimulationParameters.InstrumentDate(StartIndex - 1)
        ThisPortfolioClass.DrawDown = 0.0#
        ThisPortfolioClass.DrawDownCount = 0
        ThisPortfolioClass.OneMonthDrawDown = 0.0#
        ThisPortfolioClass.MonthsSinceDrawDownLimitBreach = 0

        ThisPortfolioClass.CompoundTargetGearing = 0.0# ' Gearing * VolModifier * BandGearingModifier
        ThisPortfolioClass.VolatilityGearingModifier = VolModifier
        ThisPortfolioClass.TradingBandGearingModifier = BandGearingModifier

        MonthStartNAV = ThisPortfolioClass.NAVOpen
        MonthlyDrawdown = 0.0#

        ' For each period in the Trading Simulation, Calculate shift in NAV and required trades.

        For DateIndex = StartIndex To (SimulationParameters.InstrumentDate.Length - 1)
          ThisTradingFee = 0.0#

          ' Previous day state.

          LastPortfolioClass = SimulationParameters.PortfolioHistory(DateIndex - 1)

          ' Initialise Todays portfolio....

          ThisPortfolioClass = New DailyPortfolioClass
          SimulationParameters.PortfolioHistory(DateIndex) = ThisPortfolioClass

          ThisPortfolioClass.PortfolioDate = SimulationParameters.InstrumentDate(DateIndex)
          ThisPortfolioClass.StockPrice = SimulationParameters.InstrumentNAV(DateIndex)
          ThisPortfolioClass.AverageLevel = SimulationParameters.AverageSeriesNAV(DateIndex)

          ' Get Month Start NAV if this is a new month 
          ' Exit 'InDrawDown' condition as appropriate.

          If (ThisPortfolioClass.PortfolioDate.Month <> LastPortfolioClass.PortfolioDate.Month) OrElse (ThisPortfolioClass.PortfolioDate.Year <> LastPortfolioClass.PortfolioDate.Year) Then
            MonthStartNAV = LastPortfolioClass.NAVClose
            MonthlyDrawdown = 0.0#

            ThisPortfolioClass.MonthsSinceDrawDownLimitBreach = LastPortfolioClass.MonthsSinceDrawDownLimitBreach + 1

            If (SimulationParameters.Trading_DrawdownMonthlyReset) AndAlso (InDrawdown) Then
              If (ThisPortfolioClass.MonthsSinceDrawDownLimitBreach >= SimulationParameters.Trading_DrawdownResetMonths) Then
                InDrawdown = False
              End If
            End If

          Else
            ThisPortfolioClass.MonthsSinceDrawDownLimitBreach = LastPortfolioClass.MonthsSinceDrawDownLimitBreach
          End If

          If (SimulationParameters.InterestRateNAV IsNot Nothing) AndAlso (SimulationParameters.InterestRateNAV.Length > 0) Then

            ThisPortfolioClass.InterestRate = Math.Max(0.0#, SimulationParameters.InterestRateNAV(Math.Min(Math.Max(0, IR_DateIndex), IR_MAXDateIndex)))

            ' IR Spread on Cash balance.

            If (LastPortfolioClass.CashClose >= 0.0#) Then
              IR_Spread = -(SimulationParameters.Trading_FundingSpread)
            Else
              IR_Spread = SimulationParameters.Trading_FundingSpread
            End If

          Else
            ThisPortfolioClass.InterestRate = 0
            IR_Spread = 0
          End If

          ' Opening Cash & Asset values, including todays investment returns.

          ThisPortfolioClass.CashOpen = LastPortfolioClass.CashClose * (1.0# + Math.Max(0.0#, ((ThisPortfolioClass.InterestRate / 100.0#) + IR_Spread))) ^ dt
          ThisPortfolioClass.AssetOpen = LastPortfolioClass.AssetClose * (1.0# + SimulationParameters.InstrumentReturns(DateIndex))
          ThisPortfolioClass.NAVOpen = ThisPortfolioClass.AssetOpen + ThisPortfolioClass.CashOpen
          ThisPortfolioClass.ModelAssetOpen = LastPortfolioClass.ModelAssetClose * (1.0# + SimulationParameters.InstrumentReturns(DateIndex))

          If (LastPortfolioClass.CashClose >= 0.0#) Then
            ThisPortfolioClass.InterestReceived = LastPortfolioClass.InterestReceived + (ThisPortfolioClass.CashOpen - LastPortfolioClass.CashClose)
            ThisPortfolioClass.InterestPaid = LastPortfolioClass.InterestPaid
          Else
            ThisPortfolioClass.InterestReceived = LastPortfolioClass.InterestReceived
            ThisPortfolioClass.InterestPaid = LastPortfolioClass.InterestPaid + (ThisPortfolioClass.CashOpen - LastPortfolioClass.CashClose)
          End If

          ' Crossing ?

          If (LastPortfolioClass.StockPrice <= LastPortfolioClass.AverageLevel) AndAlso (ThisPortfolioClass.StockPrice > ThisPortfolioClass.AverageLevel) Then
            ' Up Crossing
            ThisPortfolioClass.IsUpCrossing = True
            FirstCrossingHasOccurred = True
          ElseIf (LastPortfolioClass.StockPrice >= LastPortfolioClass.AverageLevel) AndAlso (ThisPortfolioClass.StockPrice < ThisPortfolioClass.AverageLevel) Then
            ' Down Crossing
            ThisPortfolioClass.IsDownCrossing = True
            FirstCrossingHasOccurred = True
          End If

          ' Trade ?

          ThisPortfolioClass.AssetClose = ThisPortfolioClass.AssetOpen
          ThisPortfolioClass.CashClose = ThisPortfolioClass.CashOpen

          ' First Check drawdown conditions.

          If (SimulationParameters.Trading_DrawdownMonthlyReset) Then

            If (MonthlyDrawdown > SimulationParameters.Trading_MaxDrawdown) AndAlso (Not InDrawdown) Then
              InDrawdown = True
              ThisPortfolioClass.MonthsSinceDrawDownLimitBreach = 0
            End If

            ' Don't Drawdown if Crossing
            ' Reset Month Start NAV ( For calculation of Month Draw Down )

            If (ThisPortfolioClass.IsCrossing) AndAlso (InDrawdown) Then
              MonthStartNAV = ThisPortfolioClass.NAVOpen
              InDrawdown = False
            End If

          Else

            If (Not SimulationParameters.Trading_DrawdownMonthlyReset) AndAlso (LastPortfolioClass.DrawDown >= DrawDownLimit) Then
              InDrawdown = True
            Else
              InDrawdown = False
            End If

          End If

          ' If OK, Calculate market exposure.

          If (Not InDrawdown) AndAlso (FirstCrossingHasOccurred Or SimulationParameters.Trading_FirstTradeAtStart) Then

            ' Not In DrawDown and trading is allowed, so....

            Dim AssetImbalance As Double
            Dim NAV2 As Double

            If (ThisPortfolioClass.StockPrice < ThisPortfolioClass.AverageLevel) Then
              PositionSign = -1.0#
            Else
              PositionSign = 1.0#
            End If

            ' Model Asset is CPI style, i.e. multiple of current NAV

            ' Calculate VolModifier

            If (SimulationParameters.Trading_VolatilityWeighting) Then
              If (SimulationParameters.Trading_VolatilityWeighting_StdDevHighVsAverage) Then

                thisVol = InstrumentVolSeries(DateIndex)

                If (thisVol = 0.0#) Then
                  VolModifier = 1.0#
                Else
                  VolModifier = AvgOfTradingVolSeries(DateIndex) / thisVol
                End If

              ElseIf (SimulationParameters.Trading_VolatilityWeighting_StdDevVsGivenDuration) Then

                VolModifier = (InstrumentVolSeries(Math.Max(MovingAverageDays, DateIndex)) ^ Trading_VolatilityExponent) / (TradingVolSeries(Math.Max(SimulationParameters.Trading_VolatilityWeighting_VolWeightDuration, DateIndex)) ^ Trading_VolatilityExponent)

                VolModifier *= (AvgOfTradingVolSeries(DateIndex) / AvgOfInstrumentVolSeries(DateIndex))
              End If

              VolModifier = Math.Min(SimulationParameters.Trading_VolatilityWeighting_VolWeightCap, VolModifier)

            End If

            ' Calculate Band Gearing Modifier

            If (SimulationParameters.Trading_BandWeighting) AndAlso (DateIndex >= 5) Then

              BandGearingModifier = 1.0#

              ' Calculate Band 'Width'

              If (SimulationParameters.Trading_BandWeighting_ByIndexZScore) Then ' AndAlso (DateIndex >= SimulationParameters.Trading_BandWeighting_ZScoreStdDevPeriod) Then <- Not needed now since series is back filled.

                BandGearing_BandWidth = (SimulationParameters.BandGearing_StDevSeries(DateIndex) * BandGearing_BoundaryValue)

              ElseIf (SimulationParameters.Trading_BandWeighting_ByAverageZScore) Then ' AndAlso (DateIndex >= SimulationParameters.Trading_BandWeighting_ZScoreStdDevPeriod) Then

                BandGearing_BandWidth = (SimulationParameters.BandGearing_StDevSeries(DateIndex) * BandGearing_BoundaryValue)

              ElseIf (SimulationParameters.Trading_BandWeighting_ByPercentageBand) Then

                BandGearing_BandWidth = (SimulationParameters.AverageSeriesNAV(DateIndex) * BandGearing_BoundaryValue)

              Else

                BandGearing_BandWidth = -1.0# ' Leave alone.

              End If

              ' Calculate Modifier.

              If (BandGearing_BandWidth = 0.0#) Then

                BandGearingModifier = 0.0#

              ElseIf (BandGearing_BandWidth > 0.0#) Then

                ' Calculate Raw Modifier.

                If (Math.Abs(SimulationParameters.InstrumentNAV(DateIndex) - SimulationParameters.AverageSeriesNAV(DateIndex)) <= BandGearing_BandWidth) OrElse (ThisPortfolioClass.IsCrossing) Then

                  ' NAV is Inside the Band

                  BandUsage = (Math.Abs(SimulationParameters.InstrumentNAV(DateIndex) - SimulationParameters.AverageSeriesNAV(DateIndex)) / BandGearing_BandWidth)

                  If (SimulationParameters.Trading_BandWeighting_PhaseOutAtCrossing) AndAlso (BandUsage < SimulationParameters.Trading_CrossingUnweightPoint) AndAlso (SimulationParameters.Trading_CrossingUnweightPoint > 0.0#) Then

                    BandGearingModifier = Math.Max(0.0#, BandUsage / SimulationParameters.Trading_CrossingUnweightPoint)

                  Else

                    BandGearingModifier = 1.0#

                  End If

                Else

                  ' NAV is Outside the band.

                  BandGearingModifier = Math.Max(0.0#, 1.0# - (((Math.Abs(SimulationParameters.InstrumentNAV(DateIndex) - SimulationParameters.AverageSeriesNAV(DateIndex)) / BandGearing_BandWidth) - 1.0#) / SimulationParameters.Trading_BandWeighting_WeightingDecayFactor))

                End If

                ' Apply Limitations to the BandGearingModifier

                If (DateIndex > 0) Then

                  If (Not ThisPortfolioClass.IsCrossing) Then

                    ' Re-Investment Limit

                    If (BandGearingModifier > LastPortfolioClass.TradingBandGearingModifier) AndAlso (DateIndex >= SimulationParameters.Trading_BandWeighting_ReInvestmentEvaluationPeriod) Then
                      ' Re-Investment

                      If ((SimulationParameters.InstrumentNAV(DateIndex) - SimulationParameters.InstrumentNAV(DateIndex - SimulationParameters.Trading_BandWeighting_ReInvestmentEvaluationPeriod)) * PositionSign) < 0 Then

                        BandGearingModifier = LastPortfolioClass.TradingBandGearingModifier

                      End If

                    End If

                    ' Limit Daily shift in Modifier to the Given Delta limit.

                    If Math.Abs(BandGearingModifier - LastPortfolioClass.TradingBandGearingModifier) > SimulationParameters.Trading_BandWeighting_DailyWeightDeltaLimit Then
                      If (BandGearingModifier > LastPortfolioClass.TradingBandGearingModifier) Then
                        BandGearingModifier = LastPortfolioClass.TradingBandGearingModifier + SimulationParameters.Trading_BandWeighting_DailyWeightDeltaLimit
                      Else
                        ' Allow Positions to be Cut as fast as you like.

                        'BandGearingModifier = LastPortfolioClass.TradingBandGearingModifier - Numeric_Trading_BandDeltaLimit.Value
                      End If
                    End If

                    ' Local high test ?

                    If (SimulationParameters.Trading_BandWeighting_LocalHighExclusionPeriod > 0) AndAlso (BandGearingModifier < 1.0#) Then
                      Dim TestIndex As Integer
                      Dim LocalHigh As Double
                      Dim HighIsRecent As Boolean = False

                      LocalHigh = SimulationParameters.InstrumentNAV(Math.Max(0, DateIndex - (SimulationParameters.Trading_BandWeighting_LocalHighExclusionPeriod * 2)))

                      For TestIndex = Math.Max(0, DateIndex - (SimulationParameters.Trading_BandWeighting_LocalHighExclusionPeriod * 2)) To DateIndex

                        If (PositionSign > 0.0#) Then
                          ' Long Position, Test for local high

                          If SimulationParameters.InstrumentNAV(TestIndex) > LocalHigh Then
                            LocalHigh = SimulationParameters.InstrumentNAV(TestIndex)

                            If (TestIndex > (DateIndex - SimulationParameters.Trading_BandWeighting_LocalHighExclusionPeriod)) Then
                              HighIsRecent = True
                              Exit For
                            End If
                          End If

                        Else
                          ' Short position, test for local low.
                          ' 'LocalHigh' really represents Local Low for this bit of code.

                          If SimulationParameters.InstrumentNAV(TestIndex) < LocalHigh Then
                            LocalHigh = SimulationParameters.InstrumentNAV(TestIndex)

                            If (TestIndex > (DateIndex - SimulationParameters.Trading_BandWeighting_LocalHighExclusionPeriod)) Then
                              HighIsRecent = True
                              Exit For
                            End If
                          End If

                        End If

                      Next

                      If (HighIsRecent) Then
                        BandGearingModifier = LastPortfolioClass.TradingBandGearingModifier
                      End If
                    End If

                    ' Else  ' ThisPortfolioClass.IsCrossing

                  End If ' If (Not ThisPortfolioClass.IsCrossing) Then

                End If ' If (DateIndex > 0)  

              End If ' ElseIf (BandGearing_BandWidth > 0.0#) Then

            End If ' SimulationParameters.Trading_BandWeighting

            ' Compound Gearing & Allow Long / Short 

            NetPositionModifier = VolModifier * BandGearingModifier * Gearing

            If (PositionSign > 0) AndAlso (Not SimulationParameters.Trading_AllowLongPosition) Then
              NetPositionModifier = 0.0#
            ElseIf (PositionSign < 0) AndAlso (Not SimulationParameters.Trading_AllowShortPosition) Then
              NetPositionModifier = 0.0#
            End If

            ThisPortfolioClass.ModelAssetClose = ThisPortfolioClass.NAVOpen * NetPositionModifier * PositionSign

            '' Dev.
            '' Limit Asset move.
            'Dim Dev_TradeLimit As Double = 0.25#
            'If Math.Abs(ThisPortfolioClass.ModelAssetClose - LastPortfolioClass.ModelAssetClose) > (LastPortfolioClass.NAVClose * Dev_TradeLimit) Then
            '  If (ThisPortfolioClass.ModelAssetClose > 0) Then
            '    ThisPortfolioClass.ModelAssetClose = LastPortfolioClass.ModelAssetClose - (LastPortfolioClass.NAVClose * Dev_TradeLimit)
            '  Else
            '    ThisPortfolioClass.ModelAssetClose = LastPortfolioClass.ModelAssetClose + (LastPortfolioClass.NAVClose * Dev_TradeLimit)
            '  End If
            'End If

            '' NetPositionModifier * PositionSign

            'If Math.Abs(ThisPortfolioClass.ModelAssetClose - LastPortfolioClass.ModelAssetClose) > (LastPortfolioClass.NAVClose * SimulationParameters.Trading_DeltaMoveLimit) Then
            '  If (ThisPortfolioClass.ModelAssetClose < LastPortfolioClass.ModelAssetClose) Then
            '    ThisPortfolioClass.ModelAssetClose = LastPortfolioClass.ModelAssetClose - (LastPortfolioClass.NAVClose * SimulationParameters.Trading_DeltaMoveLimit)
            '  Else
            '    ThisPortfolioClass.ModelAssetClose = LastPortfolioClass.ModelAssetClose + (LastPortfolioClass.NAVClose * SimulationParameters.Trading_DeltaMoveLimit)
            '  End If
            'End If

            ' Check for IntraMonth DrawDown.

            If (SimulationParameters.Trading_DrawdownMonthlyReset) Then

              If ((ThisPortfolioClass.AssetClose + ThisPortfolioClass.CashClose) < MonthStartNAV) Then

                MonthlyDrawdown = Math.Abs(((ThisPortfolioClass.AssetClose + ThisPortfolioClass.CashClose) / MonthStartNAV) - 1.0#)

                If (MonthlyDrawdown > SimulationParameters.Trading_MaxDrawdown) Then
                  InDrawdown = True
                  ThisPortfolioClass.MonthsSinceDrawDownLimitBreach = 0
                  ThisPortfolioClass.ModelAssetClose = 0
                End If

              End If

            End If

            ' In reality, Apply rehedge limit and other modifiers

            AssetImbalance = ThisPortfolioClass.ModelAssetClose - ThisPortfolioClass.AssetOpen

            ' If Imbalance exceeds Gamma Limit, then trade.

            If (Math.Abs(AssetImbalance / ThisPortfolioClass.ModelAssetClose) >= GammaLimit) OrElse (ThisPortfolioClass.IsCrossing) OrElse (InDrawdown) OrElse (NetPositionModifier = 0.0#) Then

              ' For a BUY  Trade, NAV2 = (NAV1+(Asset1*(AssetReturn+TradingCosts+(TradingCosts*AssetReturn))))/(1+(TradingCosts*Gearing))
              ' For a SELL Trade, NAV2 = (NAV1+(Asset1*(AssetReturn-TradingCosts-(TradingCosts*AssetReturn))))/(1-(TradingCosts*Gearing))

              ' Calculate Target NAV, accounting for the impact of trading costs.

              If (AssetImbalance > 0) Then ' Buy

                NAV2 = (LastPortfolioClass.NAVClose + (LastPortfolioClass.AssetClose * (SimulationParameters.InstrumentReturns(DateIndex) + TradingCostsPercentage + (TradingCostsPercentage * SimulationParameters.InstrumentReturns(DateIndex))))) / (1.0# + (TradingCostsPercentage * NetPositionModifier))

              Else ' Sell

                NAV2 = (LastPortfolioClass.NAVClose + (LastPortfolioClass.AssetClose * (SimulationParameters.InstrumentReturns(DateIndex) - TradingCostsPercentage - (TradingCostsPercentage * SimulationParameters.InstrumentReturns(DateIndex))))) / (1.0# - (TradingCostsPercentage * NetPositionModifier))

              End If

              ' Apply Trade.

              If (SimulationParameters.Trading_DeltaMoveLimit > 0.0#) AndAlso (Math.Abs((NAV2 * NetPositionModifier * PositionSign) - ThisPortfolioClass.AssetOpen) > (ThisPortfolioClass.NAVOpen * SimulationParameters.Trading_DeltaMoveLimit)) Then

                ' Target is > Current holding 

                If (NAV2 * NetPositionModifier * PositionSign) > ThisPortfolioClass.AssetOpen Then
                  ThisPortfolioClass.AssetClose = Math.Min((NAV2 * NetPositionModifier * PositionSign), (ThisPortfolioClass.AssetOpen + (ThisPortfolioClass.NAVOpen * SimulationParameters.Trading_DeltaMoveLimit)))
                Else
                  ThisPortfolioClass.AssetClose = Math.Max((NAV2 * NetPositionModifier * PositionSign), (ThisPortfolioClass.AssetOpen - (ThisPortfolioClass.NAVOpen * SimulationParameters.Trading_DeltaMoveLimit)))
                End If
                ThisTradingFee = Math.Abs((ThisPortfolioClass.AssetClose - ThisPortfolioClass.AssetOpen) * TradingCostsPercentage)
                ThisPortfolioClass.CashClose = (ThisPortfolioClass.CashOpen - (ThisPortfolioClass.AssetClose - ThisPortfolioClass.AssetOpen)) - ThisTradingFee

              Else
                ' No Limit To Trading 

                ThisPortfolioClass.AssetClose = NAV2 * NetPositionModifier * PositionSign
                ThisTradingFee = Math.Abs((ThisPortfolioClass.AssetClose - ThisPortfolioClass.AssetOpen) * TradingCostsPercentage)
                ThisPortfolioClass.CashClose = (ThisPortfolioClass.CashOpen - (ThisPortfolioClass.AssetClose - ThisPortfolioClass.AssetOpen)) - ThisTradingFee
                ThisPortfolioClass.ModelAssetClose = ThisPortfolioClass.AssetClose

              End If

            End If

          Else

            ' In DrawDown

            ThisPortfolioClass.ModelAssetClose = 0.0#
            ThisPortfolioClass.AssetClose = 0.0#
            ThisTradingFee = Math.Abs((ThisPortfolioClass.AssetClose - ThisPortfolioClass.AssetOpen) * TradingCostsPercentage)
            ThisPortfolioClass.CashClose = (ThisPortfolioClass.CashOpen - (ThisPortfolioClass.AssetClose - ThisPortfolioClass.AssetOpen)) - ThisTradingFee

          End If

          ThisPortfolioClass.NAVClose = ThisPortfolioClass.AssetClose + ThisPortfolioClass.CashClose
          ThisPortfolioClass.CumulativeTradingFees = LastPortfolioClass.CumulativeTradingFees + ThisTradingFee
          ThisPortfolioClass.CompoundTargetGearing = NetPositionModifier
          ThisPortfolioClass.VolatilityGearingModifier = VolModifier
          ThisPortfolioClass.TradingBandGearingModifier = BandGearingModifier

          ' DrawDown calculations

          If (ThisPortfolioClass.NAVClose < LastPortfolioClass.BestNav) Then
            ThisPortfolioClass.BestNav = LastPortfolioClass.BestNav
            ThisPortfolioClass.BestNavDate = LastPortfolioClass.BestNavDate
            ThisPortfolioClass.DrawDown = Math.Abs((ThisPortfolioClass.NAVClose / LastPortfolioClass.BestNav) - 1.0#)
            ThisPortfolioClass.DrawDownCount = LastPortfolioClass.DrawDownCount + 1

            If (MaxDrawDown < ThisPortfolioClass.DrawDown) Then
              MaxDrawDown = ThisPortfolioClass.DrawDown
            End If

          Else
            ThisPortfolioClass.BestNav = ThisPortfolioClass.NAVClose
            ThisPortfolioClass.BestNavDate = SimulationParameters.InstrumentDate(StartIndex)
            ThisPortfolioClass.DrawDown = 0.0#
            ThisPortfolioClass.DrawDownCount = 0

          End If

          If (ThisPortfolioClass.NAVClose < MonthStartNAV) Then
            MonthlyDrawdown = Math.Abs((ThisPortfolioClass.NAVClose / MonthStartNAV) - 1.0#)
          Else
            MonthlyDrawdown = 0.0#
          End If

          ThisPortfolioClass.OneMonthDrawDown = MonthlyDrawdown

          IR_DateIndex += 1

        Next

      Catch ex As Exception
        Return False
      End Try

      Try

        SimulationParameters.Result_MaxDrawdown = MaxDrawDown
        SimulationParameters.Result_IRR = (((SimulationParameters.PortfolioHistory(SimulationParameters.InstrumentDate.Length - 1).NAVClose / SimulationParameters.PortfolioHistory(StartIndex - 1).NAVOpen) ^ ((1.0# / ((SimulationParameters.PortfolioHistory(SimulationParameters.InstrumentDate.Length - 1).PortfolioDate - SimulationParameters.PortfolioHistory(StartIndex - 1).PortfolioDate).Days / 365.0#)))) - 1.0#)
        SimulationParameters.Result_ITDReturn = ((SimulationParameters.PortfolioHistory(SimulationParameters.InstrumentDate.Length - 1).NAVClose / SimulationParameters.PortfolioHistory(StartIndex - 1).NAVOpen) - 1.0#)
        SimulationParameters.Result_FirstDate = SimulationParameters.PortfolioHistory(StartIndex).PortfolioDate
        SimulationParameters.Result_LastDate = SimulationParameters.PortfolioHistory(SimulationParameters.InstrumentDate.Length - 1).PortfolioDate
        SimulationParameters.TradeHistoryToUpdate = False

      Catch ex As Exception
        Return False
      End Try

    End SyncLock

    Return True

  End Function


End Class
