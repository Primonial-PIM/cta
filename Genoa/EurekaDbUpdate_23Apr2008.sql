if not exists (select * from dbo.syscolumns where (id = object_id(N'[dbo].[tblGroupItemData]')) and (OBJECTPROPERTY(id, N'IsUserTable') = 1) and (name='GroupScalingFactor'))
BEGIN
	ALTER TABLE [dbo].[tblGroupItemData] ADD 
	[GroupScalingFactor] [float] NOT NULL
	CONSTRAINT [DF_tblGroupItemData_GroupScalingFactor] DEFAULT (1)END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldData_CacheSelect]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldData_CacheSelect]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE FUNCTION dbo.fn_tblPertracCustomFieldData_CacheSelect
	(
	@GroupID int, 
	@CustomFieldID int, 
	@KnowledgeDate datetime
	)
RETURNS TABLE
AS
	--
	-- Function to return Custon Field Data relating to the given Group.
	-- Note, this function will NOT backfill data for Non-Zero Group IDs with Data from Group Zero.
	-- Note also that @KnowledgeDate MUST NOT BE NULL.
	--
	-- NPP, Apr 2008.
	--
	RETURN 

	SELECT 	CustomFieldID,
		PertracID,
		GroupID,
		FieldNumericData,
		FieldTextData,
		FieldDateData, 
		FieldBooleanData
	FROM tblPertracCustomFieldData
	WHERE RN IN ( SELECT MAX(RN) 
		FROM tblPertracCustomFieldData 
		WHERE (GroupID = @GroupID) AND 
			((@CustomFieldID = 0) OR (CustomFieldID = @CustomFieldID)) AND
			((@KnowledgeDate <= '1 Jan 1900') OR (DateEntered <= @KnowledgeDate)) AND 
			(((DateDeleted IS NULL) OR (DateDeleted > @KnowledgeDate)) AND (NOT ((@KnowledgeDate <= '1 Jan 1900') AND (DateDeleted IS NOT NULL))))
		GROUP BY CustomFieldID, GroupID, PertracID)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelect]  TO [public]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE FUNCTION dbo.fn_tblPertracCustomFieldData_CacheSelectGroupBackfill
	(
	@GroupID int, 
	@CustomFieldID int, 
	@KnowledgeDate datetime
	)
RETURNS TABLE
AS
	--
	-- Function to return Custon Field Data relating to the given Group.
	-- Note, this function WILL return data for Group Zero AND the Requested Group.
	-- Note also that @KnowledgeDate MUST NOT BE NULL.
	--
	-- NPP, Apr 2008.
	--
	RETURN 

	SELECT 	CustomFieldID,
		PertracID,
		GroupID,
		FieldNumericData,
		FieldTextData,
		FieldDateData, 
		FieldBooleanData
	FROM tblPertracCustomFieldData
	WHERE RN IN ( SELECT MAX(RN) 
		FROM tblPertracCustomFieldData 
		WHERE ((GroupID = 0) OR (GroupID = @GroupID)) AND 
			((@CustomFieldID = 0) OR (CustomFieldID = @CustomFieldID)) AND
			(PertracID IN (SELECT GroupPertracCode FROM fn_tblGroupItems_SelectKD(@KnowledgeDate) WHERE GroupID = @GroupID)) AND 
			((@KnowledgeDate <= '1 Jan 1900') OR (DateEntered <= @KnowledgeDate)) AND 
			(((DateDeleted IS NULL) OR (DateDeleted > @KnowledgeDate)) AND (NOT ((@KnowledgeDate <= '1 Jan 1900') AND (DateDeleted IS NOT NULL))))
		GROUP BY CustomFieldID, GroupID, PertracID)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblPertracCustomFieldData_CacheSelectGroupBackfill]  TO [public]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblGroupItemData_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblGroupItemData_SelectKD]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_tblGroupsAggregated_SelectKD]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_tblGroupsAggregated_SelectKD]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE FUNCTION dbo.fn_tblGroupItemData_SelectKD 
(
@KnowledgeDate datetime
)
RETURNS @tblGroupItemData TABLE 
	(
 	RN int,
	AuditID int,
	GroupItemID int,
	GroupPertracCode int, 
	GroupLiquidity int,
	GroupHolding float,
	GroupNewHolding float,
	GroupPercent float,
	GroupExpectedReturn float,
	GroupUpperBound float, 
	GroupLowerBound float, 
	GroupFloor bit, 
	GroupCap bit, 
	GroupTradeSize float, 
	GroupBeta float, 
	GroupAlpha float, 
	GroupIndexE float, 
	GroupBetaE float, 
	GroupAlphaE float, 
	GroupStdErr float, 
	GroupScalingFactor float, 
	UserEntered varchar(20), 
	DateEntered datetime,
	DateDeleted datetime,
	PRIMARY KEY (GroupItemID)
	)
AS
	BEGIN
		INSERT INTO @tblGroupItemData
		SELECT 	[RN] ,
			[AuditID] ,
			[GroupItemID] ,
			[GroupPertracCode] , 
			[GroupLiquidity] ,
			[GroupHolding] ,
			[GroupNewHolding] ,
			[GroupPercent] ,
			[GroupExpectedReturn] ,
			[GroupUpperBound] , 
			[GroupLowerBound] , 
			[GroupFloor] , 
			[GroupCap] , 
			[GroupTradeSize] , 
			[GroupBeta] , 
			[GroupAlpha] , 
			[GroupIndexE] , 
			[GroupBetaE] , 
			[GroupAlphaE] , 
			[GroupStdErr] , 
			[GroupScalingFactor] , 
			[UserEntered] ,
			[DateEntered] ,
			[DateDeleted]
FROM tblGroupItemData
WHERE RN IN ( SELECT MAX(RN) 
              FROM tblGroupItemData 
              WHERE (((DateEntered <= @KnowledgeDate) or (DateEntered IS NULL)) OR (ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900')) AND 
                    (((DateDeleted > @KnowledgeDate) or (DateDeleted IS NULL)) AND (NOT ((ISNULL(@KnowledgeDate, '1 Jan 1900') <= '1 Jan 1900') AND (NOT (DateDeleted is NULL)))))
              GROUP BY GroupItemID )
		
	RETURN
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblGroupItemData_SelectKD]  TO [public]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE FUNCTION dbo.fn_tblGroupsAggregated_SelectKD 
(
	@GroupListID int = 0, 
	@KnowledgeDate datetime = Null
)
RETURNS TABLE
AS
RETURN 
	(
	SELECT     
		GList.GroupListID ,
		GList.GroupListName , 
		GList.GroupGroup , 
		GList.GroupDateFrom , 
		GList.GroupDateTo , 
		GItems.GroupItemID , 
		GItems.GroupPertracCode , 
		dbo.fn_GetMastername(GItems.GroupPertracCode) AS PertracName, 
		GItems.GroupIndexCode , 
		dbo.fn_GetMastername(GItems.GroupIndexCode) AS IndexName, 
		GItems.GroupSector , 
		ISNULL(GData.GroupLiquidity, 0) AS GroupLiquidity, 
		ISNULL(GData.GroupHolding,  0) AS GroupHolding, 
		ISNULL(GData.GroupNewHolding,  0) AS GroupNewHolding, 
		ISNULL(GData.GroupPercent,  0) AS GroupPercent, 
		ISNULL(GData.GroupExpectedReturn,  0) AS GroupExpectedReturn, 
		ISNULL(GData.GroupUpperBound,  0) AS GroupUpperBound, 
		ISNULL(GData.GroupLowerBound,  0) AS GroupLowerBound, 
		ISNULL(GData.GroupFloor,  0) AS GroupFloor, 
		ISNULL(GData.GroupCap,  0) AS GroupCap, 
		ISNULL(GData.GroupTradeSize,  0) AS GroupTradeSize, 
		ISNULL(GData.GroupBeta,  0) AS GroupBeta, 
		ISNULL(GData.GroupAlpha,  0) AS GroupAlpha, 
		ISNULL(GData.GroupIndexE,  0) AS GroupIndexE, 
		ISNULL(GData.GroupBetaE, 0) AS GroupBetaE, 
		ISNULL(GData.GroupAlphaE, 0) AS GroupAlphaE, 
		ISNULL(GData.GroupStdErr, 0) AS GroupStdErr, 
		ISNULL(GData.GroupScalingFactor, 1) As GroupScalingFactor
	FROM 
		fn_tblGroupItems_SelectKD(@KnowledgeDate) GItems LEFT JOIN
    fn_tblGroupItemData_SelectKD(@KnowledgeDate) GData ON (GItems.GroupItemID = GData.GroupItemID) AND (GItems.GroupPertracCode = GData.GroupPertracCode) LEFT JOIN
    fn_tblGroupList_SelectKD(@KnowledgeDate) GList ON GItems.GroupID = GList.GroupListID
  WHERE (GList.GroupListID = ISNULL(@GroupListID, 0)) OR (ISNULL(@GroupListID, 0) = 0)
	)                      	

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  SELECT  ON [dbo].[fn_tblGroupsAggregated_SelectKD]  TO [public]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblGroupItemData_InsertCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblGroupItemData_InsertCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblGroupItemData_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblGroupItemData_SelectCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblGroupItemData_UpdateCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblGroupItemData_UpdateCommand]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[adp_tblGroupsAggregated_SelectCommand]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[adp_tblGroupsAggregated_SelectCommand]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblGroupItemData_InsertCommand
	(
	@GroupItemID int,
	@GroupPertracCode int = 0,
	@GroupLiquidity int = 0,
	@GroupHolding float = 0,
	@GroupNewHolding float = 0,
	@GroupPercent float = 0,
	@GroupExpectedReturn float = 0,
	@GroupUpperBound float = 0, 
	@GroupLowerBound float = 0, 
	@GroupFloor bit = 0, 
	@GroupCap bit = 0, 
	@GroupTradeSize float = 0, 
	@GroupBeta float = 0, 
	@GroupAlpha float = 0, 
	@GroupIndexE float = 0, 
	@GroupBetaE float = 0, 
	@GroupAlphaE float = 0, 
	@GroupStdErr float = 0, 
	@GroupScalingFactor float = 1, 
	@KnowledgeDate datetime
	)
--
-- (adp) Adaptor, tblGroupItemData procedure
--
-- Controlled Insert of a new Genoa Group Data Item to the Venice (Renaissance) database
--
-- 
AS
	
	SET NOCOUNT OFF

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	
	BEGIN TRANSACTION
		  
	-- Insert new record.
	INSERT INTO tblGroupItemData(
		[GroupItemID] ,
		[GroupPertracCode] , 
		[GroupLiquidity] ,
		[GroupHolding] ,
		[GroupNewHolding] ,
		[GroupPercent] ,
		[GroupExpectedReturn] ,
		[GroupUpperBound] , 
		[GroupLowerBound] , 
		[GroupFloor] , 
		[GroupCap] , 
		[GroupTradeSize] , 
		[GroupBeta] , 
		[GroupAlpha] , 
		[GroupIndexE] , 
		[GroupBetaE] , 
		[GroupAlphaE] , 
		[GroupStdErr] ,
		[GroupScalingFactor]
		)
	VALUES (
		@GroupItemID ,
		@GroupPertracCode , 
		@GroupLiquidity ,
		@GroupHolding ,
		@GroupNewHolding ,
		@GroupPercent ,
		@GroupExpectedReturn ,
		@GroupUpperBound , 
		@GroupLowerBound , 
		@GroupFloor , 
		@GroupCap , 
		@GroupTradeSize , 
		@GroupBeta , 
		@GroupAlpha , 
		@GroupIndexE , 
		@GroupBetaE , 
		@GroupAlphaE , 
		@GroupStdErr , 
		@GroupScalingFactor
		)
	
	-- Select Newly created record

	SELECT 	[RN] ,
		[AuditID] ,
		[GroupItemID] ,
		[GroupPertracCode] , 
		[GroupLiquidity] ,
		[GroupHolding] ,
		[GroupNewHolding] ,
		[GroupPercent] ,
		[GroupExpectedReturn] ,
		[GroupUpperBound] , 
		[GroupLowerBound] , 
		[GroupFloor] , 
		[GroupCap] , 
		[GroupTradeSize] , 
		[GroupBeta] , 
		[GroupAlpha] , 
		[GroupIndexE] , 
		[GroupBetaE] , 
		[GroupAlphaE] , 
		[GroupStdErr] , 
		[GroupScalingFactor],
		[UserEntered] ,
		[DateEntered] ,
		[DateDeleted] 
		FROM tblGroupItemData
	WHERE (RN = SCOPE_IDENTITY())


	IF @@ERROR = 0
	  BEGIN
	    COMMIT TRANSACTION
	    RETURN SCOPE_IDENTITY()
	  END
	ELSE
	  BEGIN
	    ROLLBACK TRANSACTION
	    RETURN 0
	  END

	RETURN 0


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_InsertCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_InsertCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_InsertCommand]  TO [Genoa_GroupEdit]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_InsertCommand]  TO [Genoa_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_InsertCommand]  TO [Genoa_Admin]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblGroupItemData_SelectCommand
(
@KnowledgeDate datetime = Null
)
AS

SELECT 	[RN] ,
	[AuditID] ,
	[GroupItemID] ,
	[GroupPertracCode] ,
	[GroupLiquidity] ,
	[GroupHolding] ,
	[GroupNewHolding] ,
	[GroupPercent] ,
	[GroupExpectedReturn] ,
	[GroupUpperBound] , 
	[GroupLowerBound] , 
	[GroupFloor] , 
	[GroupCap] , 
	[GroupTradeSize] , 
	[GroupBeta] , 
	[GroupAlpha] , 
	[GroupIndexE] , 
	[GroupBetaE] , 
	[GroupAlphaE] , 
	[GroupStdErr] , 
	[GroupScalingFactor] , 
	[UserEntered] , 
	[DateEntered] ,
	[DateDeleted]
FROM fn_tblGroupItemData_SelectKD(@KnowledgeDate)
	
RETURN -1



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [Genoa_Read]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [Genoa_GroupEdit]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [Genoa_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_SelectCommand]  TO [Genoa_Admin]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.adp_tblGroupItemData_UpdateCommand
	(
	@GroupItemID int,
	@GroupPertracCode int = 0,
	@GroupLiquidity int = 0,
	@GroupHolding float = 0,
	@GroupNewHolding float = 0,
	@GroupPercent float = 0,
	@GroupExpectedReturn float = 0,
	@GroupUpperBound float = 0, 
	@GroupLowerBound float = 0, 
	@GroupFloor bit = 0, 
	@GroupCap bit = 0, 
	@GroupTradeSize float = 0, 
	@GroupBeta float = 0, 
	@GroupAlpha float = 0, 
	@GroupIndexE float = 0, 
	@GroupBetaE float = 0, 
	@GroupAlphaE float = 0, 
	@GroupStdErr float = 0, 
	@GroupScalingFactor float = 1, 
	@KnowledgeDate datetime
	)
AS
	SET NOCOUNT OFF

	DECLARE @RVal int 

	IF ((@Knowledgedate >= (getdate())) OR (ISNULL(@Knowledgedate, '1 Jan 1900') <= '1 Jan 1900')) AND 
		(ISNULL(@GroupItemID, 0) > 0)
	  BEGIN
		EXECUTE @RVal = [dbo].[adp_tblGroupItemData_InsertCommand] 
			@GroupItemID = @GroupItemID,
			@GroupPertracCode = @GroupPertracCode, 
			@GroupLiquidity = @GroupLiquidity,
			@GroupHolding = @GroupHolding,
			@GroupNewHolding = @GroupNewHolding,
			@GroupPercent = @GroupPercent,
			@GroupExpectedReturn = @GroupExpectedReturn,
			@GroupUpperBound = @GroupUpperBound, 
			@GroupLowerBound = @GroupLowerBound, 
			@GroupFloor = @GroupFloor, 
			@GroupCap = @GroupCap, 
			@GroupTradeSize = @GroupTradeSize, 
			@GroupBeta = @GroupBeta, 
			@GroupAlpha = @GroupAlpha, 
			@GroupIndexE = @GroupIndexE, 
			@GroupBetaE = @GroupBetaE, 
			@GroupAlphaE = @GroupAlphaE, 
			@GroupStdErr = @GroupStdErr, 
			@GroupScalingFactor = @GroupScalingFactor, 
			@KnowledgeDate = @KnowledgeDate
			
		RETURN @RVal
	  END
	ELSE
	  BEGIN
		SELECT 	[RN] ,
			[AuditID] ,
			[GroupItemID] ,
			[GroupPertracCode] , 
			[GroupLiquidity] ,
			[GroupHolding] ,
			[GroupNewHolding] ,
			[GroupPercent] ,
			[GroupExpectedReturn] ,
			[GroupUpperBound] , 
			[GroupLowerBound] , 
			[GroupFloor] , 
			[GroupCap] , 
			[GroupTradeSize] , 
			[GroupBeta] , 
			[GroupAlpha] , 
			[GroupIndexE] , 
			[GroupBetaE] , 
			[GroupAlphaE] , 
			[GroupStdErr] , 
			[GroupScalingFactor] ,
			[UserEntered] , 
			[DateEntered] ,
			[DateDeleted]
		FROM fn_tblGroupItemData_SelectKD(@KnowledgeDate)
		WHERE (GroupItemID = @GroupItemID)
		
		SELECT @RVal = @GroupItemID
	  END
	  
	IF @@ERROR = 0
	  BEGIN
	    RETURN @RVal
	  END
	ELSE
	  BEGIN
	    RETURN 0
	  END

	RETURN 0




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_UpdateCommand]  TO [InvestMaster_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_UpdateCommand]  TO [InvestMaster_Admin]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_UpdateCommand]  TO [Genoa_GroupEdit]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_UpdateCommand]  TO [Genoa_Manager]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupItemData_UpdateCommand]  TO [Genoa_Admin]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE PROCEDURE dbo.adp_tblGroupsAggregated_SelectCommand
(
	@GroupListID int = 0, 
	@KnowledgeDate datetime = Null
)
AS
	SET NOCOUNT ON
	
	SELECT     
		GroupListID ,
		GroupListName , 
		GroupGroup , 
		GroupDateFrom , 
    GroupDateTo , 
    GroupItemID , 
    GroupPertracCode , 
    ISNULL(PertracName, '') AS PertracName, 
    GroupIndexCode , 
    ISNULL(IndexName, '') AS IndexName , 
    GroupSector , 
    GroupLiquidity, 
    GroupHolding, 
    0.0 AS GroupTrade, 
    GroupNewHolding, 
    GroupPercent, 
    GroupExpectedReturn, 
    GroupUpperBound, 
    GroupLowerBound, 
    GroupFloor, 
    GroupCap, 
    GroupTradeSize, 
    GroupBeta, 
    GroupAlpha, 
    GroupIndexE, 
    GroupBetaE,
    GroupAlphaE, 
    GroupStdErr, 
    GroupScalingFactor
	FROM 
		fn_tblGroupsAggregated_SelectKD(@GroupListID, @KnowledgeDate)
                      
	RETURN


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupsAggregated_SelectCommand]  TO [InvestMaster_Sales]
GO

GRANT  EXECUTE  ON [dbo].[adp_tblGroupsAggregated_SelectCommand]  TO [Genoa_Read]
GO

