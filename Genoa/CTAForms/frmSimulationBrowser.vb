
Imports System.IO
Imports System.Data.SqlClient
Imports System.Windows.Forms.Clipboard
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary

Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceControls
Imports FCP_TelerikControls.FCP_RadComboBox
Imports RenaissanceDataClass
Imports RenaissanceStatFunctions
Imports CTASimulationFunctions
Imports RenaissanceUtilities.DatePeriodFunctions
Imports RenaissancePertracAndStatsGlobals

Imports C1.Win.C1FlexGrid

Public Class frmSimulationBrowser

  Inherits System.Windows.Forms.Form
  Implements StandardCTAForm

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer
  Friend WithEvents Split_Form As System.Windows.Forms.SplitContainer
  Friend WithEvents Tab_InstrumentDetails As System.Windows.Forms.TabControl
  Friend WithEvents Tab_Descriptions As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Contacts As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Performance As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Comparisons As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Charts As System.Windows.Forms.TabPage
  Friend WithEvents TabControl_Charts As System.Windows.Forms.TabControl
  Friend WithEvents Tab_Vami As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Return As System.Windows.Forms.TabPage
  Friend WithEvents Tab_StdDev As System.Windows.Forms.TabPage
  Friend WithEvents Tab_DrawDown As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Monthly As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Statistics As System.Windows.Forms.TabPage
  Friend WithEvents Chart_Prices As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Returns As Dundas.Charting.WinControl.Chart
  Friend WithEvents Grid_Performance_0 As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Label29 As System.Windows.Forms.Label
  Friend WithEvents Combo_Charts_CompareSeriesPertrac As FCP_TelerikControls.FCP_RadComboBox
  Friend WithEvents Label37 As System.Windows.Forms.Label
  Friend WithEvents Label38 As System.Windows.Forms.Label
  Friend WithEvents Label36 As System.Windows.Forms.Label
  Friend WithEvents Label35 As System.Windows.Forms.Label
  Friend WithEvents Text_Chart_RollingPeriod As RenaissanceControls.NumericTextBox
  Friend WithEvents Text_Chart_Lamda As RenaissanceControls.NumericTextBox
  Friend WithEvents Date_Charts_DateTo As System.Windows.Forms.DateTimePicker
  Friend WithEvents Date_Charts_DateFrom As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label39 As System.Windows.Forms.Label
  Friend WithEvents Chart_RollingReturn As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_StdDev As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_DrawDown As Dundas.Charting.WinControl.Chart
  Friend WithEvents Button_DefaultDate As System.Windows.Forms.Button
  Friend WithEvents TabControl_PerformanceTables As System.Windows.Forms.TabControl
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents Label_PT_0 As System.Windows.Forms.Label
  Friend WithEvents Grid_SimpleStatistics As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Label55 As System.Windows.Forms.Label
  Friend WithEvents Text_Statistics_RiskFree As RenaissanceControls.NumericTextBox
  Friend WithEvents Label54 As System.Windows.Forms.Label
  Friend WithEvents Label53 As System.Windows.Forms.Label
  Friend WithEvents Combo_Statistics_HighlightSeries As System.Windows.Forms.ComboBox
  Friend WithEvents Grid_Stats_Drawdowns As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Tab_ReturnScatter As System.Windows.Forms.TabPage
  Friend WithEvents Chart_ReturnScatter As Dundas.Charting.WinControl.Chart
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents FundBrowser_StatusLabel As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents Tab_VAR As System.Windows.Forms.TabPage
  Friend WithEvents Label58 As System.Windows.Forms.Label
  Friend WithEvents Text_Chart_VARPeriod As RenaissanceControls.NumericTextBox
  Friend WithEvents Label57 As System.Windows.Forms.Label
  Friend WithEvents Label56 As System.Windows.Forms.Label
  Friend WithEvents Combo_Charts_VARSeries As System.Windows.Forms.ComboBox
  Friend WithEvents Text_Chart_VARConfidence As RenaissanceControls.PercentageTextBox
  Friend WithEvents Label59 As System.Windows.Forms.Label
  Friend WithEvents Chart_VAR As Dundas.Charting.WinControl.Chart
  Friend WithEvents Text_ScalingFactor As RenaissanceControls.NumericTextBox
  Friend WithEvents Radio_DynamicScalingFactor As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_SingleScalingFactor As System.Windows.Forms.RadioButton
  Friend WithEvents Tab_Omega As System.Windows.Forms.TabPage
  Friend WithEvents Chart_Omega As Dundas.Charting.WinControl.Chart
  Friend WithEvents Label60 As System.Windows.Forms.Label
  Friend WithEvents Edit_OmegaReturn As RenaissanceControls.NumericTextBox
  Friend WithEvents Label61 As System.Windows.Forms.Label
  Friend WithEvents Check_OmegaRatio As System.Windows.Forms.CheckBox
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Charts_CompareGroup As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Charts_ComparePertrac As System.Windows.Forms.RadioButton
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents Combo_Charts_CompareSeriesGroup As System.Windows.Forms.ComboBox
  Friend WithEvents Check_AutoStart As System.Windows.Forms.CheckBox
  Friend WithEvents Combo_InstrumentSeries As System.Windows.Forms.ComboBox
  Friend WithEvents Combo_InterestRateSeries As System.Windows.Forms.ComboBox
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Radio_VolatilityWeightedDuration As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_LinearWeightedAverage As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_ExponentialWeightedAverage As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_UnAdjustedAverage As System.Windows.Forms.RadioButton
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents Numeric_MovingAverageDays As System.Windows.Forms.NumericUpDown
  Friend WithEvents Label7 As System.Windows.Forms.Label
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents Label9 As System.Windows.Forms.Label
  Friend WithEvents Label8 As System.Windows.Forms.Label
  Friend WithEvents DateTime_Trading_StartDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Check_ExponentialPriceAverage As System.Windows.Forms.CheckBox
  Friend WithEvents Numeric_ExponentialPrice_Lambda As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_ExponentialAverage_Lambda As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_LinearAverage_Lambda As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_VolatilityAverage_Lambda As System.Windows.Forms.NumericUpDown
  Friend WithEvents Label11 As System.Windows.Forms.Label
  Friend WithEvents Radio_Trading_FirstTradeCrossing As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Trading_FirstTradeStart As System.Windows.Forms.RadioButton
  Friend WithEvents Label12 As System.Windows.Forms.Label
  Friend WithEvents Numeric_Trading_MaxDrawdown As RenaissanceControls.PercentageTextBox
  Friend WithEvents Label13 As System.Windows.Forms.Label
  Friend WithEvents Numeric_Trading_RebalanceThreshold As RenaissanceControls.PercentageTextBox
  Friend WithEvents Label14 As System.Windows.Forms.Label
  Friend WithEvents Numeric_Trading_BaselineGearing As RenaissanceControls.PercentageTextBox
  Friend WithEvents Grid_PortfolioHistory As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Group_FirstTrade As System.Windows.Forms.Panel
  Friend WithEvents Panel7 As System.Windows.Forms.Panel
  Friend WithEvents Radio_Trading_VolWeightByHighStDev As System.Windows.Forms.RadioButton
  Friend WithEvents Label15 As System.Windows.Forms.Label
  Friend WithEvents Radio_Trading_VolWeightNone As System.Windows.Forms.RadioButton
  Friend WithEvents Label_IRR As System.Windows.Forms.Label
  Friend WithEvents Label_ITDReturn As System.Windows.Forms.Label
  Friend WithEvents Label_LastDate As System.Windows.Forms.Label
  Friend WithEvents Label_FirstDate As System.Windows.Forms.Label
  Friend WithEvents Label18 As System.Windows.Forms.Label
  Friend WithEvents Label19 As System.Windows.Forms.Label
  Friend WithEvents Chart_FrontPage_RollingReturn As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_FrontPage_Prices As Dundas.Charting.WinControl.Chart
  Friend WithEvents Label17 As System.Windows.Forms.Label
  Friend WithEvents Label16 As System.Windows.Forms.Label
  Friend WithEvents Label21 As System.Windows.Forms.Label
  Friend WithEvents Label20 As System.Windows.Forms.Label
  Friend WithEvents Label_MaxDrawdown As System.Windows.Forms.Label
  Friend WithEvents Label23 As System.Windows.Forms.Label
  Friend WithEvents Radio_Trading_VolWeight_VsSecondDuration As System.Windows.Forms.RadioButton
  Friend WithEvents Numeric_Trading_VolWeightDuration As System.Windows.Forms.NumericUpDown
  Friend WithEvents Label22 As System.Windows.Forms.Label
  Friend WithEvents Numeric_Trading_VolExponent As System.Windows.Forms.NumericUpDown
  Friend WithEvents Date_DataStartDate As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label24 As System.Windows.Forms.Label
  Friend WithEvents Numeric_TrandingCosts As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_FundingSpread As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_Shadow_RebalanceThreshold As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_Shadow_BaselineGearing As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_Shadow_MaxDrawdown As System.Windows.Forms.NumericUpDown
  Friend WithEvents TabControl_Parameters As System.Windows.Forms.TabControl
  Friend WithEvents Tab_Param_MovingAverage As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Param_Costs As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Param_TradingStyle As System.Windows.Forms.TabPage
  Friend WithEvents Check_Trading_BandWeightings As System.Windows.Forms.CheckBox
  Friend WithEvents Panel_Trading_BandWeighting As System.Windows.Forms.Panel
  Friend WithEvents Numeric_Trading_IndexZScore As System.Windows.Forms.NumericUpDown
  Friend WithEvents Radio_Trading_ByIndexZScore As System.Windows.Forms.RadioButton
  Friend WithEvents Numeric_Trading_AverageZScore As System.Windows.Forms.NumericUpDown
  Friend WithEvents Radio_Trading_ByAverageZScore As System.Windows.Forms.RadioButton
  Friend WithEvents Numeric_Shadow_PercentBand As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_Trading_PercentBand As RenaissanceControls.PercentageTextBox
  Friend WithEvents Radio_Trading_ByPercentageBand As System.Windows.Forms.RadioButton
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Numeric_Trading_WeightingDecayFactor As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_Trading_ReInvestmentEvaluationPeriod As System.Windows.Forms.NumericUpDown
  Friend WithEvents Label10 As System.Windows.Forms.Label
  Friend WithEvents Label25 As System.Windows.Forms.Label
  Friend WithEvents Numeric_Trading_BandDeltaLimit As System.Windows.Forms.NumericUpDown
  Friend WithEvents Check_VAMI_ShowBands As System.Windows.Forms.CheckBox
  Friend WithEvents Check_VAMI_ShowPortfolio As System.Windows.Forms.CheckBox
  Friend WithEvents Label26 As System.Windows.Forms.Label
  Friend WithEvents Numeric_Trading_ZScore_Period As System.Windows.Forms.NumericUpDown
  Friend WithEvents Label27 As System.Windows.Forms.Label
  Friend WithEvents Numeric_Trading_VolWeightCap As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_LocalHighExclusionPeriod As System.Windows.Forms.NumericUpDown
  Friend WithEvents Label28 As System.Windows.Forms.Label
  Friend WithEvents Menu_FileSimulation As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_SaveSimulationAs As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_LoadSimulation As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_DeleteSimulation As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Combo_DataPeriod As System.Windows.Forms.ComboBox
  Friend WithEvents Label30 As System.Windows.Forms.Label
  Friend WithEvents Check_Trading_MonthlyDrawdownReset As System.Windows.Forms.CheckBox
  Friend WithEvents Label31 As System.Windows.Forms.Label
  Friend WithEvents Label32 As System.Windows.Forms.Label
  Friend WithEvents Numeric_Trading_MonthlyDrawdownResetCount As System.Windows.Forms.NumericUpDown
  Friend WithEvents Check_Trading_PhaseOutAtCrossing As System.Windows.Forms.CheckBox
  Friend WithEvents Numeric_Trading_ShadowCrossingUnweightPoint As System.Windows.Forms.NumericUpDown
  Friend WithEvents Numeric_Trading_CrossingUnweightPoint As RenaissanceControls.PercentageTextBox
  Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents Menu_ManageFolders As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Check_Trading_AllowShort As System.Windows.Forms.CheckBox
  Friend WithEvents Check_Trading_AllowLong As System.Windows.Forms.CheckBox
  Friend WithEvents Label33 As System.Windows.Forms.Label
  Friend WithEvents Menu_SaveSimulation As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Menu_ClearSimulation As System.Windows.Forms.ToolStripMenuItem

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim ChartArea1 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend1 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series1 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series2 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea2 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend2 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series3 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series4 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSimulationBrowser))
    Dim ChartArea3 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend3 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series5 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series6 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea4 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend4 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series7 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series8 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea5 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend5 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series9 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series10 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea6 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend6 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series11 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series12 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea7 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend7 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series13 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series14 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea8 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend8 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series15 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series16 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series17 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea9 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend9 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series18 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea10 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend10 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series19 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series20 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Menu_FileSimulation = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_SaveSimulation = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_SaveSimulationAs = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_LoadSimulation = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_DeleteSimulation = New System.Windows.Forms.ToolStripMenuItem
    Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
    Me.Menu_ManageFolders = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_ClearSimulation = New System.Windows.Forms.ToolStripMenuItem
    Me.Split_Form = New System.Windows.Forms.SplitContainer
    Me.Combo_DataPeriod = New System.Windows.Forms.ComboBox
    Me.Label30 = New System.Windows.Forms.Label
    Me.TabControl_Parameters = New System.Windows.Forms.TabControl
    Me.Tab_Param_MovingAverage = New System.Windows.Forms.TabPage
    Me.Numeric_VolatilityAverage_Lambda = New System.Windows.Forms.NumericUpDown
    Me.Numeric_LinearAverage_Lambda = New System.Windows.Forms.NumericUpDown
    Me.Numeric_ExponentialAverage_Lambda = New System.Windows.Forms.NumericUpDown
    Me.Numeric_MovingAverageDays = New System.Windows.Forms.NumericUpDown
    Me.Numeric_ExponentialPrice_Lambda = New System.Windows.Forms.NumericUpDown
    Me.Label4 = New System.Windows.Forms.Label
    Me.Check_ExponentialPriceAverage = New System.Windows.Forms.CheckBox
    Me.Radio_UnAdjustedAverage = New System.Windows.Forms.RadioButton
    Me.Label8 = New System.Windows.Forms.Label
    Me.Radio_ExponentialWeightedAverage = New System.Windows.Forms.RadioButton
    Me.Radio_VolatilityWeightedDuration = New System.Windows.Forms.RadioButton
    Me.Radio_LinearWeightedAverage = New System.Windows.Forms.RadioButton
    Me.Tab_Param_Costs = New System.Windows.Forms.TabPage
    Me.Numeric_FundingSpread = New System.Windows.Forms.NumericUpDown
    Me.Numeric_TrandingCosts = New System.Windows.Forms.NumericUpDown
    Me.Label7 = New System.Windows.Forms.Label
    Me.Label5 = New System.Windows.Forms.Label
    Me.Label6 = New System.Windows.Forms.Label
    Me.Tab_Param_TradingStyle = New System.Windows.Forms.TabPage
    Me.Label33 = New System.Windows.Forms.Label
    Me.Check_Trading_AllowShort = New System.Windows.Forms.CheckBox
    Me.Check_Trading_AllowLong = New System.Windows.Forms.CheckBox
    Me.Label32 = New System.Windows.Forms.Label
    Me.Numeric_Trading_MonthlyDrawdownResetCount = New System.Windows.Forms.NumericUpDown
    Me.Check_Trading_MonthlyDrawdownReset = New System.Windows.Forms.CheckBox
    Me.Label31 = New System.Windows.Forms.Label
    Me.Panel_Trading_BandWeighting = New System.Windows.Forms.Panel
    Me.Numeric_Trading_ShadowCrossingUnweightPoint = New System.Windows.Forms.NumericUpDown
    Me.Numeric_Trading_CrossingUnweightPoint = New RenaissanceControls.PercentageTextBox
    Me.Check_Trading_PhaseOutAtCrossing = New System.Windows.Forms.CheckBox
    Me.Numeric_LocalHighExclusionPeriod = New System.Windows.Forms.NumericUpDown
    Me.Label28 = New System.Windows.Forms.Label
    Me.Label26 = New System.Windows.Forms.Label
    Me.Numeric_Trading_ZScore_Period = New System.Windows.Forms.NumericUpDown
    Me.Label25 = New System.Windows.Forms.Label
    Me.Numeric_Trading_BandDeltaLimit = New System.Windows.Forms.NumericUpDown
    Me.Numeric_Trading_ReInvestmentEvaluationPeriod = New System.Windows.Forms.NumericUpDown
    Me.Label10 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Numeric_Trading_WeightingDecayFactor = New System.Windows.Forms.NumericUpDown
    Me.Numeric_Shadow_PercentBand = New System.Windows.Forms.NumericUpDown
    Me.Numeric_Trading_PercentBand = New RenaissanceControls.PercentageTextBox
    Me.Radio_Trading_ByPercentageBand = New System.Windows.Forms.RadioButton
    Me.Numeric_Trading_AverageZScore = New System.Windows.Forms.NumericUpDown
    Me.Radio_Trading_ByAverageZScore = New System.Windows.Forms.RadioButton
    Me.Numeric_Trading_IndexZScore = New System.Windows.Forms.NumericUpDown
    Me.Radio_Trading_ByIndexZScore = New System.Windows.Forms.RadioButton
    Me.Check_Trading_BandWeightings = New System.Windows.Forms.CheckBox
    Me.Numeric_Shadow_MaxDrawdown = New System.Windows.Forms.NumericUpDown
    Me.Numeric_Shadow_BaselineGearing = New System.Windows.Forms.NumericUpDown
    Me.DateTime_Trading_StartDate = New System.Windows.Forms.DateTimePicker
    Me.Numeric_Shadow_RebalanceThreshold = New System.Windows.Forms.NumericUpDown
    Me.Panel7 = New System.Windows.Forms.Panel
    Me.Label27 = New System.Windows.Forms.Label
    Me.Numeric_Trading_VolWeightCap = New System.Windows.Forms.NumericUpDown
    Me.Label22 = New System.Windows.Forms.Label
    Me.Numeric_Trading_VolExponent = New System.Windows.Forms.NumericUpDown
    Me.Numeric_Trading_VolWeightDuration = New System.Windows.Forms.NumericUpDown
    Me.Radio_Trading_VolWeight_VsSecondDuration = New System.Windows.Forms.RadioButton
    Me.Radio_Trading_VolWeightNone = New System.Windows.Forms.RadioButton
    Me.Radio_Trading_VolWeightByHighStDev = New System.Windows.Forms.RadioButton
    Me.Label9 = New System.Windows.Forms.Label
    Me.Label15 = New System.Windows.Forms.Label
    Me.Label11 = New System.Windows.Forms.Label
    Me.Group_FirstTrade = New System.Windows.Forms.Panel
    Me.Radio_Trading_FirstTradeStart = New System.Windows.Forms.RadioButton
    Me.Radio_Trading_FirstTradeCrossing = New System.Windows.Forms.RadioButton
    Me.Numeric_Trading_MaxDrawdown = New RenaissanceControls.PercentageTextBox
    Me.Label14 = New System.Windows.Forms.Label
    Me.Label12 = New System.Windows.Forms.Label
    Me.Numeric_Trading_BaselineGearing = New RenaissanceControls.PercentageTextBox
    Me.Numeric_Trading_RebalanceThreshold = New RenaissanceControls.PercentageTextBox
    Me.Label13 = New System.Windows.Forms.Label
    Me.Date_DataStartDate = New System.Windows.Forms.DateTimePicker
    Me.Label24 = New System.Windows.Forms.Label
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label1 = New System.Windows.Forms.Label
    Me.Combo_InstrumentSeries = New System.Windows.Forms.ComboBox
    Me.Combo_InterestRateSeries = New System.Windows.Forms.ComboBox
    Me.Tab_InstrumentDetails = New System.Windows.Forms.TabControl
    Me.Tab_Descriptions = New System.Windows.Forms.TabPage
    Me.Check_VAMI_ShowBands = New System.Windows.Forms.CheckBox
    Me.Check_VAMI_ShowPortfolio = New System.Windows.Forms.CheckBox
    Me.Label_MaxDrawdown = New System.Windows.Forms.Label
    Me.Label23 = New System.Windows.Forms.Label
    Me.Label21 = New System.Windows.Forms.Label
    Me.Label20 = New System.Windows.Forms.Label
    Me.Label_IRR = New System.Windows.Forms.Label
    Me.Label_ITDReturn = New System.Windows.Forms.Label
    Me.Label_LastDate = New System.Windows.Forms.Label
    Me.Label_FirstDate = New System.Windows.Forms.Label
    Me.Label18 = New System.Windows.Forms.Label
    Me.Label19 = New System.Windows.Forms.Label
    Me.Chart_FrontPage_RollingReturn = New Dundas.Charting.WinControl.Chart
    Me.Chart_FrontPage_Prices = New Dundas.Charting.WinControl.Chart
    Me.Label17 = New System.Windows.Forms.Label
    Me.Label16 = New System.Windows.Forms.Label
    Me.Tab_Contacts = New System.Windows.Forms.TabPage
    Me.Grid_PortfolioHistory = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Tab_Performance = New System.Windows.Forms.TabPage
    Me.TabControl_PerformanceTables = New System.Windows.Forms.TabControl
    Me.TabPage1 = New System.Windows.Forms.TabPage
    Me.Label_PT_0 = New System.Windows.Forms.Label
    Me.Grid_Performance_0 = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Tab_Comparisons = New System.Windows.Forms.TabPage
    Me.Tab_Charts = New System.Windows.Forms.TabPage
    Me.Check_AutoStart = New System.Windows.Forms.CheckBox
    Me.Combo_Charts_CompareSeriesGroup = New System.Windows.Forms.ComboBox
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.Radio_Charts_CompareGroup = New System.Windows.Forms.RadioButton
    Me.Radio_Charts_ComparePertrac = New System.Windows.Forms.RadioButton
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Text_ScalingFactor = New RenaissanceControls.NumericTextBox
    Me.Radio_DynamicScalingFactor = New System.Windows.Forms.RadioButton
    Me.Radio_SingleScalingFactor = New System.Windows.Forms.RadioButton
    Me.Button_DefaultDate = New System.Windows.Forms.Button
    Me.Label39 = New System.Windows.Forms.Label
    Me.Text_Chart_RollingPeriod = New RenaissanceControls.NumericTextBox
    Me.Text_Chart_Lamda = New RenaissanceControls.NumericTextBox
    Me.Date_Charts_DateTo = New System.Windows.Forms.DateTimePicker
    Me.Date_Charts_DateFrom = New System.Windows.Forms.DateTimePicker
    Me.Label37 = New System.Windows.Forms.Label
    Me.Label38 = New System.Windows.Forms.Label
    Me.Label36 = New System.Windows.Forms.Label
    Me.Label35 = New System.Windows.Forms.Label
    Me.Combo_Charts_CompareSeriesPertrac = New FCP_TelerikControls.FCP_RadComboBox
    Me.Label29 = New System.Windows.Forms.Label
    Me.TabControl_Charts = New System.Windows.Forms.TabControl
    Me.Tab_Vami = New System.Windows.Forms.TabPage
    Me.Chart_Prices = New Dundas.Charting.WinControl.Chart
    Me.Tab_Monthly = New System.Windows.Forms.TabPage
    Me.Chart_Returns = New Dundas.Charting.WinControl.Chart
    Me.Tab_Return = New System.Windows.Forms.TabPage
    Me.Chart_RollingReturn = New Dundas.Charting.WinControl.Chart
    Me.Tab_ReturnScatter = New System.Windows.Forms.TabPage
    Me.Chart_ReturnScatter = New Dundas.Charting.WinControl.Chart
    Me.Tab_StdDev = New System.Windows.Forms.TabPage
    Me.Chart_StdDev = New Dundas.Charting.WinControl.Chart
    Me.Tab_VAR = New System.Windows.Forms.TabPage
    Me.Chart_VAR = New Dundas.Charting.WinControl.Chart
    Me.Text_Chart_VARConfidence = New RenaissanceControls.PercentageTextBox
    Me.Label59 = New System.Windows.Forms.Label
    Me.Label58 = New System.Windows.Forms.Label
    Me.Text_Chart_VARPeriod = New RenaissanceControls.NumericTextBox
    Me.Label57 = New System.Windows.Forms.Label
    Me.Label56 = New System.Windows.Forms.Label
    Me.Combo_Charts_VARSeries = New System.Windows.Forms.ComboBox
    Me.Tab_Omega = New System.Windows.Forms.TabPage
    Me.Check_OmegaRatio = New System.Windows.Forms.CheckBox
    Me.Label60 = New System.Windows.Forms.Label
    Me.Edit_OmegaReturn = New RenaissanceControls.NumericTextBox
    Me.Label61 = New System.Windows.Forms.Label
    Me.Chart_Omega = New Dundas.Charting.WinControl.Chart
    Me.Tab_DrawDown = New System.Windows.Forms.TabPage
    Me.Chart_DrawDown = New Dundas.Charting.WinControl.Chart
    Me.Tab_Statistics = New System.Windows.Forms.TabPage
    Me.Grid_Stats_Drawdowns = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Grid_SimpleStatistics = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Label55 = New System.Windows.Forms.Label
    Me.Text_Statistics_RiskFree = New RenaissanceControls.NumericTextBox
    Me.Label54 = New System.Windows.Forms.Label
    Me.Label53 = New System.Windows.Forms.Label
    Me.Combo_Statistics_HighlightSeries = New System.Windows.Forms.ComboBox
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
    Me.FundBrowser_StatusLabel = New System.Windows.Forms.ToolStripStatusLabel
    Me.RootMenu.SuspendLayout()
    Me.Split_Form.Panel1.SuspendLayout()
    Me.Split_Form.Panel2.SuspendLayout()
    Me.Split_Form.SuspendLayout()
    Me.TabControl_Parameters.SuspendLayout()
    Me.Tab_Param_MovingAverage.SuspendLayout()
    CType(Me.Numeric_VolatilityAverage_Lambda, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_LinearAverage_Lambda, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_ExponentialAverage_Lambda, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_MovingAverageDays, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_ExponentialPrice_Lambda, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Param_Costs.SuspendLayout()
    CType(Me.Numeric_FundingSpread, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_TrandingCosts, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Param_TradingStyle.SuspendLayout()
    CType(Me.Numeric_Trading_MonthlyDrawdownResetCount, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel_Trading_BandWeighting.SuspendLayout()
    CType(Me.Numeric_Trading_ShadowCrossingUnweightPoint, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_LocalHighExclusionPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Trading_ZScore_Period, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Trading_BandDeltaLimit, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Trading_ReInvestmentEvaluationPeriod, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Trading_WeightingDecayFactor, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Shadow_PercentBand, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Trading_AverageZScore, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Trading_IndexZScore, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Shadow_MaxDrawdown, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Shadow_BaselineGearing, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Shadow_RebalanceThreshold, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel7.SuspendLayout()
    CType(Me.Numeric_Trading_VolWeightCap, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Trading_VolExponent, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Numeric_Trading_VolWeightDuration, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Group_FirstTrade.SuspendLayout()
    Me.Tab_InstrumentDetails.SuspendLayout()
    Me.Tab_Descriptions.SuspendLayout()
    CType(Me.Chart_FrontPage_RollingReturn, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Chart_FrontPage_Prices, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Contacts.SuspendLayout()
    CType(Me.Grid_PortfolioHistory, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Performance.SuspendLayout()
    Me.TabControl_PerformanceTables.SuspendLayout()
    Me.TabPage1.SuspendLayout()
    CType(Me.Grid_Performance_0, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Charts.SuspendLayout()
    Me.Panel2.SuspendLayout()
    Me.Panel1.SuspendLayout()
    CType(Me.Combo_Charts_CompareSeriesPertrac, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.TabControl_Charts.SuspendLayout()
    Me.Tab_Vami.SuspendLayout()
    CType(Me.Chart_Prices, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Monthly.SuspendLayout()
    CType(Me.Chart_Returns, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Return.SuspendLayout()
    CType(Me.Chart_RollingReturn, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_ReturnScatter.SuspendLayout()
    CType(Me.Chart_ReturnScatter, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_StdDev.SuspendLayout()
    CType(Me.Chart_StdDev, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_VAR.SuspendLayout()
    CType(Me.Chart_VAR, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Omega.SuspendLayout()
    CType(Me.Chart_Omega, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_DrawDown.SuspendLayout()
    CType(Me.Chart_DrawDown, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Statistics.SuspendLayout()
    CType(Me.Grid_Stats_Drawdowns, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Grid_SimpleStatistics, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.StatusStrip1.SuspendLayout()
    Me.SuspendLayout()
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_FileSimulation})
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(1202, 24)
    Me.RootMenu.TabIndex = 12
    Me.RootMenu.Text = "MenuStrip1"
    '
    'Menu_FileSimulation
    '
    Me.Menu_FileSimulation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_SaveSimulation, Me.Menu_SaveSimulationAs, Me.ToolStripSeparator1, Me.Menu_LoadSimulation, Me.Menu_DeleteSimulation, Me.ToolStripSeparator2, Me.Menu_ManageFolders, Me.Menu_ClearSimulation})
    Me.Menu_FileSimulation.Name = "Menu_FileSimulation"
    Me.Menu_FileSimulation.Size = New System.Drawing.Size(105, 20)
    Me.Menu_FileSimulation.Text = "Saved Simulations"
    '
    'Menu_SaveSimulation
    '
    Me.Menu_SaveSimulation.Name = "Menu_SaveSimulation"
    Me.Menu_SaveSimulation.Size = New System.Drawing.Size(187, 22)
    Me.Menu_SaveSimulation.Tag = "0"
    Me.Menu_SaveSimulation.Text = "Save Simulation"
    '
    'Menu_SaveSimulationAs
    '
    Me.Menu_SaveSimulationAs.Name = "Menu_SaveSimulationAs"
    Me.Menu_SaveSimulationAs.Size = New System.Drawing.Size(187, 22)
    Me.Menu_SaveSimulationAs.Tag = "0"
    Me.Menu_SaveSimulationAs.Text = "Save Simulation As..."
    '
    'ToolStripSeparator1
    '
    Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
    Me.ToolStripSeparator1.Size = New System.Drawing.Size(184, 6)
    '
    'Menu_LoadSimulation
    '
    Me.Menu_LoadSimulation.Name = "Menu_LoadSimulation"
    Me.Menu_LoadSimulation.Size = New System.Drawing.Size(187, 22)
    Me.Menu_LoadSimulation.Text = "Load Simulation"
    '
    'Menu_DeleteSimulation
    '
    Me.Menu_DeleteSimulation.Name = "Menu_DeleteSimulation"
    Me.Menu_DeleteSimulation.Size = New System.Drawing.Size(187, 22)
    Me.Menu_DeleteSimulation.Text = "Delete Simulation"
    '
    'ToolStripSeparator2
    '
    Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
    Me.ToolStripSeparator2.Size = New System.Drawing.Size(184, 6)
    '
    'Menu_ManageFolders
    '
    Me.Menu_ManageFolders.Name = "Menu_ManageFolders"
    Me.Menu_ManageFolders.Size = New System.Drawing.Size(187, 22)
    Me.Menu_ManageFolders.Tag = "frmManageFolders"
    Me.Menu_ManageFolders.Text = "Manage Folders"
    '
    'Menu_ClearSimulation
    '
    Me.Menu_ClearSimulation.Name = "Menu_ClearSimulation"
    Me.Menu_ClearSimulation.Size = New System.Drawing.Size(187, 22)
    Me.Menu_ClearSimulation.Text = "ClearSimulation"
    '
    'Split_Form
    '
    Me.Split_Form.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_Form.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Split_Form.Location = New System.Drawing.Point(2, 26)
    Me.Split_Form.Name = "Split_Form"
    '
    'Split_Form.Panel1
    '
    Me.Split_Form.Panel1.BackColor = System.Drawing.SystemColors.Control
    Me.Split_Form.Panel1.Controls.Add(Me.Combo_DataPeriod)
    Me.Split_Form.Panel1.Controls.Add(Me.Label30)
    Me.Split_Form.Panel1.Controls.Add(Me.TabControl_Parameters)
    Me.Split_Form.Panel1.Controls.Add(Me.Date_DataStartDate)
    Me.Split_Form.Panel1.Controls.Add(Me.Label24)
    Me.Split_Form.Panel1.Controls.Add(Me.Label2)
    Me.Split_Form.Panel1.Controls.Add(Me.Label1)
    Me.Split_Form.Panel1.Controls.Add(Me.Combo_InstrumentSeries)
    Me.Split_Form.Panel1.Controls.Add(Me.Combo_InterestRateSeries)
    '
    'Split_Form.Panel2
    '
    Me.Split_Form.Panel2.Controls.Add(Me.Tab_InstrumentDetails)
    Me.Split_Form.Size = New System.Drawing.Size(1198, 734)
    Me.Split_Form.SplitterDistance = 370
    Me.Split_Form.SplitterWidth = 3
    Me.Split_Form.TabIndex = 0
    '
    'Combo_DataPeriod
    '
    Me.Combo_DataPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_DataPeriod.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_DataPeriod.Location = New System.Drawing.Point(102, 122)
    Me.Combo_DataPeriod.Name = "Combo_DataPeriod"
    Me.Combo_DataPeriod.Size = New System.Drawing.Size(130, 21)
    Me.Combo_DataPeriod.TabIndex = 27
    '
    'Label30
    '
    Me.Label30.AutoSize = True
    Me.Label30.Location = New System.Drawing.Point(8, 125)
    Me.Label30.Name = "Label30"
    Me.Label30.Size = New System.Drawing.Size(63, 13)
    Me.Label30.TabIndex = 26
    Me.Label30.Text = "Data Period"
    '
    'TabControl_Parameters
    '
    Me.TabControl_Parameters.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_Parameters.Controls.Add(Me.Tab_Param_MovingAverage)
    Me.TabControl_Parameters.Controls.Add(Me.Tab_Param_Costs)
    Me.TabControl_Parameters.Controls.Add(Me.Tab_Param_TradingStyle)
    Me.TabControl_Parameters.Location = New System.Drawing.Point(3, 153)
    Me.TabControl_Parameters.Margin = New System.Windows.Forms.Padding(1)
    Me.TabControl_Parameters.Name = "TabControl_Parameters"
    Me.TabControl_Parameters.SelectedIndex = 0
    Me.TabControl_Parameters.Size = New System.Drawing.Size(360, 574)
    Me.TabControl_Parameters.TabIndex = 25
    '
    'Tab_Param_MovingAverage
    '
    Me.Tab_Param_MovingAverage.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Param_MovingAverage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Numeric_VolatilityAverage_Lambda)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Numeric_LinearAverage_Lambda)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Numeric_ExponentialAverage_Lambda)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Numeric_MovingAverageDays)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Numeric_ExponentialPrice_Lambda)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Label4)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Check_ExponentialPriceAverage)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Radio_UnAdjustedAverage)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Label8)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Radio_ExponentialWeightedAverage)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Radio_VolatilityWeightedDuration)
    Me.Tab_Param_MovingAverage.Controls.Add(Me.Radio_LinearWeightedAverage)
    Me.Tab_Param_MovingAverage.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Param_MovingAverage.Margin = New System.Windows.Forms.Padding(1)
    Me.Tab_Param_MovingAverage.Name = "Tab_Param_MovingAverage"
    Me.Tab_Param_MovingAverage.Padding = New System.Windows.Forms.Padding(1)
    Me.Tab_Param_MovingAverage.Size = New System.Drawing.Size(352, 548)
    Me.Tab_Param_MovingAverage.TabIndex = 0
    Me.Tab_Param_MovingAverage.Text = "Moving Average"
    '
    'Numeric_VolatilityAverage_Lambda
    '
    Me.Numeric_VolatilityAverage_Lambda.DecimalPlaces = 4
    Me.Numeric_VolatilityAverage_Lambda.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
    Me.Numeric_VolatilityAverage_Lambda.Location = New System.Drawing.Point(180, 122)
    Me.Numeric_VolatilityAverage_Lambda.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_VolatilityAverage_Lambda.Name = "Numeric_VolatilityAverage_Lambda"
    Me.Numeric_VolatilityAverage_Lambda.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_VolatilityAverage_Lambda.TabIndex = 26
    Me.Numeric_VolatilityAverage_Lambda.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'Numeric_LinearAverage_Lambda
    '
    Me.Numeric_LinearAverage_Lambda.DecimalPlaces = 4
    Me.Numeric_LinearAverage_Lambda.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
    Me.Numeric_LinearAverage_Lambda.Location = New System.Drawing.Point(180, 99)
    Me.Numeric_LinearAverage_Lambda.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_LinearAverage_Lambda.Name = "Numeric_LinearAverage_Lambda"
    Me.Numeric_LinearAverage_Lambda.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_LinearAverage_Lambda.TabIndex = 25
    Me.Numeric_LinearAverage_Lambda.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'Numeric_ExponentialAverage_Lambda
    '
    Me.Numeric_ExponentialAverage_Lambda.DecimalPlaces = 4
    Me.Numeric_ExponentialAverage_Lambda.Increment = New Decimal(New Integer() {1, 0, 0, 196608})
    Me.Numeric_ExponentialAverage_Lambda.Location = New System.Drawing.Point(180, 76)
    Me.Numeric_ExponentialAverage_Lambda.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_ExponentialAverage_Lambda.Name = "Numeric_ExponentialAverage_Lambda"
    Me.Numeric_ExponentialAverage_Lambda.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_ExponentialAverage_Lambda.TabIndex = 24
    Me.Numeric_ExponentialAverage_Lambda.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'Numeric_MovingAverageDays
    '
    Me.Numeric_MovingAverageDays.Location = New System.Drawing.Point(55, 5)
    Me.Numeric_MovingAverageDays.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_MovingAverageDays.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.Numeric_MovingAverageDays.Name = "Numeric_MovingAverageDays"
    Me.Numeric_MovingAverageDays.Size = New System.Drawing.Size(80, 20)
    Me.Numeric_MovingAverageDays.TabIndex = 12
    Me.Numeric_MovingAverageDays.Value = New Decimal(New Integer() {220, 0, 0, 0})
    '
    'Numeric_ExponentialPrice_Lambda
    '
    Me.Numeric_ExponentialPrice_Lambda.Location = New System.Drawing.Point(180, 30)
    Me.Numeric_ExponentialPrice_Lambda.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_ExponentialPrice_Lambda.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.Numeric_ExponentialPrice_Lambda.Name = "Numeric_ExponentialPrice_Lambda"
    Me.Numeric_ExponentialPrice_Lambda.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_ExponentialPrice_Lambda.TabIndex = 23
    Me.Numeric_ExponentialPrice_Lambda.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Location = New System.Drawing.Point(7, 7)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(31, 13)
    Me.Label4.TabIndex = 13
    Me.Label4.Text = "Days"
    '
    'Check_ExponentialPriceAverage
    '
    Me.Check_ExponentialPriceAverage.Location = New System.Drawing.Point(10, 32)
    Me.Check_ExponentialPriceAverage.Name = "Check_ExponentialPriceAverage"
    Me.Check_ExponentialPriceAverage.Size = New System.Drawing.Size(17, 16)
    Me.Check_ExponentialPriceAverage.TabIndex = 22
    Me.Check_ExponentialPriceAverage.UseVisualStyleBackColor = True
    '
    'Radio_UnAdjustedAverage
    '
    Me.Radio_UnAdjustedAverage.AutoSize = True
    Me.Radio_UnAdjustedAverage.Location = New System.Drawing.Point(10, 53)
    Me.Radio_UnAdjustedAverage.Name = "Radio_UnAdjustedAverage"
    Me.Radio_UnAdjustedAverage.Size = New System.Drawing.Size(99, 17)
    Me.Radio_UnAdjustedAverage.TabIndex = 14
    Me.Radio_UnAdjustedAverage.TabStop = True
    Me.Radio_UnAdjustedAverage.Text = "Simple Average"
    Me.Radio_UnAdjustedAverage.UseVisualStyleBackColor = True
    '
    'Label8
    '
    Me.Label8.Location = New System.Drawing.Point(27, 32)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(138, 13)
    Me.Label8.TabIndex = 14
    Me.Label8.Text = "Exponential Price Average"
    '
    'Radio_ExponentialWeightedAverage
    '
    Me.Radio_ExponentialWeightedAverage.AutoSize = True
    Me.Radio_ExponentialWeightedAverage.Location = New System.Drawing.Point(10, 76)
    Me.Radio_ExponentialWeightedAverage.Name = "Radio_ExponentialWeightedAverage"
    Me.Radio_ExponentialWeightedAverage.Size = New System.Drawing.Size(136, 17)
    Me.Radio_ExponentialWeightedAverage.TabIndex = 15
    Me.Radio_ExponentialWeightedAverage.TabStop = True
    Me.Radio_ExponentialWeightedAverage.Text = "Exponentially Weighted"
    Me.Radio_ExponentialWeightedAverage.UseVisualStyleBackColor = True
    '
    'Radio_VolatilityWeightedDuration
    '
    Me.Radio_VolatilityWeightedDuration.AutoSize = True
    Me.Radio_VolatilityWeightedDuration.Location = New System.Drawing.Point(10, 122)
    Me.Radio_VolatilityWeightedDuration.Name = "Radio_VolatilityWeightedDuration"
    Me.Radio_VolatilityWeightedDuration.Size = New System.Drawing.Size(155, 17)
    Me.Radio_VolatilityWeightedDuration.TabIndex = 17
    Me.Radio_VolatilityWeightedDuration.TabStop = True
    Me.Radio_VolatilityWeightedDuration.Text = "Volatility Weighted Duration"
    Me.Radio_VolatilityWeightedDuration.UseVisualStyleBackColor = True
    '
    'Radio_LinearWeightedAverage
    '
    Me.Radio_LinearWeightedAverage.AutoSize = True
    Me.Radio_LinearWeightedAverage.Location = New System.Drawing.Point(10, 99)
    Me.Radio_LinearWeightedAverage.Name = "Radio_LinearWeightedAverage"
    Me.Radio_LinearWeightedAverage.Size = New System.Drawing.Size(103, 17)
    Me.Radio_LinearWeightedAverage.TabIndex = 16
    Me.Radio_LinearWeightedAverage.TabStop = True
    Me.Radio_LinearWeightedAverage.Text = "Linear Weighted"
    Me.Radio_LinearWeightedAverage.UseVisualStyleBackColor = True
    '
    'Tab_Param_Costs
    '
    Me.Tab_Param_Costs.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Param_Costs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Tab_Param_Costs.Controls.Add(Me.Numeric_FundingSpread)
    Me.Tab_Param_Costs.Controls.Add(Me.Numeric_TrandingCosts)
    Me.Tab_Param_Costs.Controls.Add(Me.Label7)
    Me.Tab_Param_Costs.Controls.Add(Me.Label5)
    Me.Tab_Param_Costs.Controls.Add(Me.Label6)
    Me.Tab_Param_Costs.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Param_Costs.Margin = New System.Windows.Forms.Padding(1)
    Me.Tab_Param_Costs.Name = "Tab_Param_Costs"
    Me.Tab_Param_Costs.Padding = New System.Windows.Forms.Padding(1)
    Me.Tab_Param_Costs.Size = New System.Drawing.Size(352, 548)
    Me.Tab_Param_Costs.TabIndex = 1
    Me.Tab_Param_Costs.Text = "Costs"
    '
    'Numeric_FundingSpread
    '
    Me.Numeric_FundingSpread.DecimalPlaces = 4
    Me.Numeric_FundingSpread.Increment = New Decimal(New Integer() {5, 0, 0, 262144})
    Me.Numeric_FundingSpread.Location = New System.Drawing.Point(145, 31)
    Me.Numeric_FundingSpread.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.Numeric_FundingSpread.Name = "Numeric_FundingSpread"
    Me.Numeric_FundingSpread.Size = New System.Drawing.Size(78, 20)
    Me.Numeric_FundingSpread.TabIndex = 26
    Me.Numeric_FundingSpread.Value = New Decimal(New Integer() {5, 0, 0, 196608})
    '
    'Numeric_TrandingCosts
    '
    Me.Numeric_TrandingCosts.DecimalPlaces = 4
    Me.Numeric_TrandingCosts.Increment = New Decimal(New Integer() {5, 0, 0, 262144})
    Me.Numeric_TrandingCosts.Location = New System.Drawing.Point(145, 8)
    Me.Numeric_TrandingCosts.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.Numeric_TrandingCosts.Name = "Numeric_TrandingCosts"
    Me.Numeric_TrandingCosts.Size = New System.Drawing.Size(78, 20)
    Me.Numeric_TrandingCosts.TabIndex = 25
    Me.Numeric_TrandingCosts.Value = New Decimal(New Integer() {25, 0, 0, 262144})
    '
    'Label7
    '
    Me.Label7.AutoSize = True
    Me.Label7.Location = New System.Drawing.Point(6, 33)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(82, 13)
    Me.Label7.TabIndex = 14
    Me.Label7.Text = "Funding Spread"
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Location = New System.Drawing.Point(228, 10)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(49, 13)
    Me.Label5.TabIndex = 12
    Me.Label5.Text = "per trade"
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Location = New System.Drawing.Point(6, 10)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(72, 13)
    Me.Label6.TabIndex = 13
    Me.Label6.Text = "Trading Costs"
    '
    'Tab_Param_TradingStyle
    '
    Me.Tab_Param_TradingStyle.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Label33)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Check_Trading_AllowShort)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Check_Trading_AllowLong)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Label32)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Numeric_Trading_MonthlyDrawdownResetCount)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Check_Trading_MonthlyDrawdownReset)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Label31)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Panel_Trading_BandWeighting)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Check_Trading_BandWeightings)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Numeric_Shadow_MaxDrawdown)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Numeric_Shadow_BaselineGearing)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.DateTime_Trading_StartDate)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Numeric_Shadow_RebalanceThreshold)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Panel7)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Label9)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Label15)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Label11)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Group_FirstTrade)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Numeric_Trading_MaxDrawdown)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Label14)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Label12)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Numeric_Trading_BaselineGearing)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Numeric_Trading_RebalanceThreshold)
    Me.Tab_Param_TradingStyle.Controls.Add(Me.Label13)
    Me.Tab_Param_TradingStyle.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Param_TradingStyle.Margin = New System.Windows.Forms.Padding(1)
    Me.Tab_Param_TradingStyle.Name = "Tab_Param_TradingStyle"
    Me.Tab_Param_TradingStyle.Size = New System.Drawing.Size(352, 548)
    Me.Tab_Param_TradingStyle.TabIndex = 2
    Me.Tab_Param_TradingStyle.Text = "Trading Style"
    '
    'Label33
    '
    Me.Label33.AutoSize = True
    Me.Label33.Location = New System.Drawing.Point(8, 59)
    Me.Label33.Name = "Label33"
    Me.Label33.Size = New System.Drawing.Size(97, 13)
    Me.Label33.TabIndex = 36
    Me.Label33.Text = "Strategy can go ...."
    '
    'Check_Trading_AllowShort
    '
    Me.Check_Trading_AllowShort.Location = New System.Drawing.Point(222, 57)
    Me.Check_Trading_AllowShort.Name = "Check_Trading_AllowShort"
    Me.Check_Trading_AllowShort.Size = New System.Drawing.Size(62, 18)
    Me.Check_Trading_AllowShort.TabIndex = 35
    Me.Check_Trading_AllowShort.Text = "Short"
    Me.Check_Trading_AllowShort.UseVisualStyleBackColor = True
    '
    'Check_Trading_AllowLong
    '
    Me.Check_Trading_AllowLong.Location = New System.Drawing.Point(146, 57)
    Me.Check_Trading_AllowLong.Name = "Check_Trading_AllowLong"
    Me.Check_Trading_AllowLong.Size = New System.Drawing.Size(62, 18)
    Me.Check_Trading_AllowLong.TabIndex = 34
    Me.Check_Trading_AllowLong.Text = "Long"
    Me.Check_Trading_AllowLong.UseVisualStyleBackColor = True
    '
    'Label32
    '
    Me.Label32.AutoSize = True
    Me.Label32.Location = New System.Drawing.Point(243, 105)
    Me.Label32.Name = "Label32"
    Me.Label32.Size = New System.Drawing.Size(42, 13)
    Me.Label32.TabIndex = 5
    Me.Label32.Text = "Months"
    '
    'Numeric_Trading_MonthlyDrawdownResetCount
    '
    Me.Numeric_Trading_MonthlyDrawdownResetCount.Location = New System.Drawing.Point(193, 102)
    Me.Numeric_Trading_MonthlyDrawdownResetCount.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_Trading_MonthlyDrawdownResetCount.Name = "Numeric_Trading_MonthlyDrawdownResetCount"
    Me.Numeric_Trading_MonthlyDrawdownResetCount.Size = New System.Drawing.Size(47, 20)
    Me.Numeric_Trading_MonthlyDrawdownResetCount.TabIndex = 4
    Me.Numeric_Trading_MonthlyDrawdownResetCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
    Me.Numeric_Trading_MonthlyDrawdownResetCount.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'Check_Trading_MonthlyDrawdownReset
    '
    Me.Check_Trading_MonthlyDrawdownReset.Location = New System.Drawing.Point(146, 104)
    Me.Check_Trading_MonthlyDrawdownReset.Name = "Check_Trading_MonthlyDrawdownReset"
    Me.Check_Trading_MonthlyDrawdownReset.Size = New System.Drawing.Size(17, 16)
    Me.Check_Trading_MonthlyDrawdownReset.TabIndex = 3
    Me.Check_Trading_MonthlyDrawdownReset.UseVisualStyleBackColor = True
    '
    'Label31
    '
    Me.Label31.Location = New System.Drawing.Point(8, 104)
    Me.Label31.Name = "Label31"
    Me.Label31.Size = New System.Drawing.Size(138, 13)
    Me.Label31.TabIndex = 17
    Me.Label31.Text = "Monthly Drawdown reset"
    '
    'Panel_Trading_BandWeighting
    '
    Me.Panel_Trading_BandWeighting.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_Trading_BandWeighting.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_Trading_ShadowCrossingUnweightPoint)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_Trading_CrossingUnweightPoint)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Check_Trading_PhaseOutAtCrossing)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_LocalHighExclusionPeriod)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Label28)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Label26)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_Trading_ZScore_Period)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Label25)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_Trading_BandDeltaLimit)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_Trading_ReInvestmentEvaluationPeriod)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Label10)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Label3)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_Trading_WeightingDecayFactor)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_Shadow_PercentBand)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_Trading_PercentBand)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Radio_Trading_ByPercentageBand)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_Trading_AverageZScore)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Radio_Trading_ByAverageZScore)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Numeric_Trading_IndexZScore)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Radio_Trading_ByIndexZScore)
    Me.Panel_Trading_BandWeighting.Location = New System.Drawing.Point(95, 289)
    Me.Panel_Trading_BandWeighting.Name = "Panel_Trading_BandWeighting"
    Me.Panel_Trading_BandWeighting.Size = New System.Drawing.Size(245, 226)
    Me.Panel_Trading_BandWeighting.TabIndex = 11
    '
    'Numeric_Trading_ShadowCrossingUnweightPoint
    '
    Me.Numeric_Trading_ShadowCrossingUnweightPoint.DecimalPlaces = 4
    Me.Numeric_Trading_ShadowCrossingUnweightPoint.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
    Me.Numeric_Trading_ShadowCrossingUnweightPoint.Location = New System.Drawing.Point(220, 196)
    Me.Numeric_Trading_ShadowCrossingUnweightPoint.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.Numeric_Trading_ShadowCrossingUnweightPoint.Name = "Numeric_Trading_ShadowCrossingUnweightPoint"
    Me.Numeric_Trading_ShadowCrossingUnweightPoint.Size = New System.Drawing.Size(18, 20)
    Me.Numeric_Trading_ShadowCrossingUnweightPoint.TabIndex = 19
    Me.Numeric_Trading_ShadowCrossingUnweightPoint.Value = New Decimal(New Integer() {2, 0, 0, 65536})
    '
    'Numeric_Trading_CrossingUnweightPoint
    '
    Me.Numeric_Trading_CrossingUnweightPoint.Location = New System.Drawing.Point(161, 196)
    Me.Numeric_Trading_CrossingUnweightPoint.Name = "Numeric_Trading_CrossingUnweightPoint"
    Me.Numeric_Trading_CrossingUnweightPoint.RenaissanceTag = Nothing
    Me.Numeric_Trading_CrossingUnweightPoint.Size = New System.Drawing.Size(60, 20)
    Me.Numeric_Trading_CrossingUnweightPoint.TabIndex = 18
    Me.Numeric_Trading_CrossingUnweightPoint.Text = "20.00%"
    Me.Numeric_Trading_CrossingUnweightPoint.TextFormat = "#,##0.00%"
    Me.Numeric_Trading_CrossingUnweightPoint.Value = 0.2
    '
    'Check_Trading_PhaseOutAtCrossing
    '
    Me.Check_Trading_PhaseOutAtCrossing.Location = New System.Drawing.Point(3, 197)
    Me.Check_Trading_PhaseOutAtCrossing.Name = "Check_Trading_PhaseOutAtCrossing"
    Me.Check_Trading_PhaseOutAtCrossing.Size = New System.Drawing.Size(149, 18)
    Me.Check_Trading_PhaseOutAtCrossing.TabIndex = 17
    Me.Check_Trading_PhaseOutAtCrossing.Text = "UnWeight at Crossings"
    Me.Check_Trading_PhaseOutAtCrossing.UseVisualStyleBackColor = True
    '
    'Numeric_LocalHighExclusionPeriod
    '
    Me.Numeric_LocalHighExclusionPeriod.Location = New System.Drawing.Point(161, 172)
    Me.Numeric_LocalHighExclusionPeriod.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_LocalHighExclusionPeriod.Name = "Numeric_LocalHighExclusionPeriod"
    Me.Numeric_LocalHighExclusionPeriod.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_LocalHighExclusionPeriod.TabIndex = 16
    '
    'Label28
    '
    Me.Label28.AutoSize = True
    Me.Label28.Location = New System.Drawing.Point(3, 174)
    Me.Label28.Name = "Label28"
    Me.Label28.Size = New System.Drawing.Size(137, 13)
    Me.Label28.TabIndex = 15
    Me.Label28.Text = "Local High exclusion period"
    '
    'Label26
    '
    Me.Label26.AutoSize = True
    Me.Label26.Location = New System.Drawing.Point(20, 54)
    Me.Label26.Name = "Label26"
    Me.Label26.Size = New System.Drawing.Size(108, 13)
    Me.Label26.TabIndex = 4
    Me.Label26.Text = "ZScore StDev Period"
    '
    'Numeric_Trading_ZScore_Period
    '
    Me.Numeric_Trading_ZScore_Period.Increment = New Decimal(New Integer() {5, 0, 0, 0})
    Me.Numeric_Trading_ZScore_Period.Location = New System.Drawing.Point(161, 52)
    Me.Numeric_Trading_ZScore_Period.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_Trading_ZScore_Period.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
    Me.Numeric_Trading_ZScore_Period.Name = "Numeric_Trading_ZScore_Period"
    Me.Numeric_Trading_ZScore_Period.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_Trading_ZScore_Period.TabIndex = 5
    Me.Numeric_Trading_ZScore_Period.Value = New Decimal(New Integer() {220, 0, 0, 0})
    '
    'Label25
    '
    Me.Label25.AutoSize = True
    Me.Label25.Location = New System.Drawing.Point(3, 150)
    Me.Label25.Name = "Label25"
    Me.Label25.Size = New System.Drawing.Size(119, 13)
    Me.Label25.TabIndex = 13
    Me.Label25.Text = "Daily Weight Delta Limit"
    '
    'Numeric_Trading_BandDeltaLimit
    '
    Me.Numeric_Trading_BandDeltaLimit.DecimalPlaces = 4
    Me.Numeric_Trading_BandDeltaLimit.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
    Me.Numeric_Trading_BandDeltaLimit.Location = New System.Drawing.Point(161, 148)
    Me.Numeric_Trading_BandDeltaLimit.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
    Me.Numeric_Trading_BandDeltaLimit.Name = "Numeric_Trading_BandDeltaLimit"
    Me.Numeric_Trading_BandDeltaLimit.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_Trading_BandDeltaLimit.TabIndex = 14
    Me.Numeric_Trading_BandDeltaLimit.Value = New Decimal(New Integer() {1, 0, 0, 65536})
    '
    'Numeric_Trading_ReInvestmentEvaluationPeriod
    '
    Me.Numeric_Trading_ReInvestmentEvaluationPeriod.Location = New System.Drawing.Point(161, 124)
    Me.Numeric_Trading_ReInvestmentEvaluationPeriod.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_Trading_ReInvestmentEvaluationPeriod.Name = "Numeric_Trading_ReInvestmentEvaluationPeriod"
    Me.Numeric_Trading_ReInvestmentEvaluationPeriod.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_Trading_ReInvestmentEvaluationPeriod.TabIndex = 12
    Me.Numeric_Trading_ReInvestmentEvaluationPeriod.Value = New Decimal(New Integer() {5, 0, 0, 0})
    '
    'Label10
    '
    Me.Label10.AutoSize = True
    Me.Label10.Location = New System.Drawing.Point(3, 126)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(160, 13)
    Me.Label10.TabIndex = 11
    Me.Label10.Text = "Re-Investment evaluation period"
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(3, 102)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(119, 13)
    Me.Label3.TabIndex = 9
    Me.Label3.Text = "Weighting Decay factor"
    '
    'Numeric_Trading_WeightingDecayFactor
    '
    Me.Numeric_Trading_WeightingDecayFactor.DecimalPlaces = 4
    Me.Numeric_Trading_WeightingDecayFactor.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
    Me.Numeric_Trading_WeightingDecayFactor.Location = New System.Drawing.Point(161, 100)
    Me.Numeric_Trading_WeightingDecayFactor.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_Trading_WeightingDecayFactor.Name = "Numeric_Trading_WeightingDecayFactor"
    Me.Numeric_Trading_WeightingDecayFactor.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_Trading_WeightingDecayFactor.TabIndex = 10
    Me.Numeric_Trading_WeightingDecayFactor.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'Numeric_Shadow_PercentBand
    '
    Me.Numeric_Shadow_PercentBand.DecimalPlaces = 4
    Me.Numeric_Shadow_PercentBand.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
    Me.Numeric_Shadow_PercentBand.Location = New System.Drawing.Point(220, 76)
    Me.Numeric_Shadow_PercentBand.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
    Me.Numeric_Shadow_PercentBand.Name = "Numeric_Shadow_PercentBand"
    Me.Numeric_Shadow_PercentBand.Size = New System.Drawing.Size(18, 20)
    Me.Numeric_Shadow_PercentBand.TabIndex = 8
    Me.Numeric_Shadow_PercentBand.Value = New Decimal(New Integer() {2, 0, 0, 65536})
    '
    'Numeric_Trading_PercentBand
    '
    Me.Numeric_Trading_PercentBand.Location = New System.Drawing.Point(161, 76)
    Me.Numeric_Trading_PercentBand.Name = "Numeric_Trading_PercentBand"
    Me.Numeric_Trading_PercentBand.RenaissanceTag = Nothing
    Me.Numeric_Trading_PercentBand.Size = New System.Drawing.Size(60, 20)
    Me.Numeric_Trading_PercentBand.TabIndex = 7
    Me.Numeric_Trading_PercentBand.Text = "20.00%"
    Me.Numeric_Trading_PercentBand.TextFormat = "#,##0.00%"
    Me.Numeric_Trading_PercentBand.Value = 0.2
    '
    'Radio_Trading_ByPercentageBand
    '
    Me.Radio_Trading_ByPercentageBand.AutoSize = True
    Me.Radio_Trading_ByPercentageBand.Location = New System.Drawing.Point(3, 77)
    Me.Radio_Trading_ByPercentageBand.Name = "Radio_Trading_ByPercentageBand"
    Me.Radio_Trading_ByPercentageBand.Size = New System.Drawing.Size(108, 17)
    Me.Radio_Trading_ByPercentageBand.TabIndex = 6
    Me.Radio_Trading_ByPercentageBand.TabStop = True
    Me.Radio_Trading_ByPercentageBand.Text = "Percentage Band"
    Me.Radio_Trading_ByPercentageBand.UseVisualStyleBackColor = True
    '
    'Numeric_Trading_AverageZScore
    '
    Me.Numeric_Trading_AverageZScore.DecimalPlaces = 2
    Me.Numeric_Trading_AverageZScore.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
    Me.Numeric_Trading_AverageZScore.Location = New System.Drawing.Point(161, 28)
    Me.Numeric_Trading_AverageZScore.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
    Me.Numeric_Trading_AverageZScore.Minimum = New Decimal(New Integer() {1, 0, 0, 65536})
    Me.Numeric_Trading_AverageZScore.Name = "Numeric_Trading_AverageZScore"
    Me.Numeric_Trading_AverageZScore.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_Trading_AverageZScore.TabIndex = 3
    Me.Numeric_Trading_AverageZScore.Value = New Decimal(New Integer() {2, 0, 0, 0})
    '
    'Radio_Trading_ByAverageZScore
    '
    Me.Radio_Trading_ByAverageZScore.AutoSize = True
    Me.Radio_Trading_ByAverageZScore.Location = New System.Drawing.Point(3, 29)
    Me.Radio_Trading_ByAverageZScore.Name = "Radio_Trading_ByAverageZScore"
    Me.Radio_Trading_ByAverageZScore.Size = New System.Drawing.Size(103, 17)
    Me.Radio_Trading_ByAverageZScore.TabIndex = 2
    Me.Radio_Trading_ByAverageZScore.TabStop = True
    Me.Radio_Trading_ByAverageZScore.Text = "Average ZScore"
    Me.Radio_Trading_ByAverageZScore.UseVisualStyleBackColor = True
    '
    'Numeric_Trading_IndexZScore
    '
    Me.Numeric_Trading_IndexZScore.DecimalPlaces = 2
    Me.Numeric_Trading_IndexZScore.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
    Me.Numeric_Trading_IndexZScore.Location = New System.Drawing.Point(161, 4)
    Me.Numeric_Trading_IndexZScore.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
    Me.Numeric_Trading_IndexZScore.Minimum = New Decimal(New Integer() {1, 0, 0, 65536})
    Me.Numeric_Trading_IndexZScore.Name = "Numeric_Trading_IndexZScore"
    Me.Numeric_Trading_IndexZScore.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_Trading_IndexZScore.TabIndex = 1
    Me.Numeric_Trading_IndexZScore.Value = New Decimal(New Integer() {2, 0, 0, 0})
    '
    'Radio_Trading_ByIndexZScore
    '
    Me.Radio_Trading_ByIndexZScore.AutoSize = True
    Me.Radio_Trading_ByIndexZScore.Location = New System.Drawing.Point(3, 4)
    Me.Radio_Trading_ByIndexZScore.Name = "Radio_Trading_ByIndexZScore"
    Me.Radio_Trading_ByIndexZScore.Size = New System.Drawing.Size(89, 17)
    Me.Radio_Trading_ByIndexZScore.TabIndex = 0
    Me.Radio_Trading_ByIndexZScore.TabStop = True
    Me.Radio_Trading_ByIndexZScore.Text = "Index ZScore"
    Me.Radio_Trading_ByIndexZScore.UseVisualStyleBackColor = True
    '
    'Check_Trading_BandWeightings
    '
    Me.Check_Trading_BandWeightings.Location = New System.Drawing.Point(8, 289)
    Me.Check_Trading_BandWeightings.Name = "Check_Trading_BandWeightings"
    Me.Check_Trading_BandWeightings.Size = New System.Drawing.Size(84, 36)
    Me.Check_Trading_BandWeightings.TabIndex = 13
    Me.Check_Trading_BandWeightings.Text = "Band Weighting"
    Me.Check_Trading_BandWeightings.UseVisualStyleBackColor = True
    '
    'Numeric_Shadow_MaxDrawdown
    '
    Me.Numeric_Shadow_MaxDrawdown.DecimalPlaces = 4
    Me.Numeric_Shadow_MaxDrawdown.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
    Me.Numeric_Shadow_MaxDrawdown.Location = New System.Drawing.Point(222, 78)
    Me.Numeric_Shadow_MaxDrawdown.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.Numeric_Shadow_MaxDrawdown.Name = "Numeric_Shadow_MaxDrawdown"
    Me.Numeric_Shadow_MaxDrawdown.Size = New System.Drawing.Size(18, 20)
    Me.Numeric_Shadow_MaxDrawdown.TabIndex = 2
    Me.Numeric_Shadow_MaxDrawdown.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'Numeric_Shadow_BaselineGearing
    '
    Me.Numeric_Shadow_BaselineGearing.DecimalPlaces = 4
    Me.Numeric_Shadow_BaselineGearing.Increment = New Decimal(New Integer() {5, 0, 0, 131072})
    Me.Numeric_Shadow_BaselineGearing.Location = New System.Drawing.Point(222, 154)
    Me.Numeric_Shadow_BaselineGearing.Name = "Numeric_Shadow_BaselineGearing"
    Me.Numeric_Shadow_BaselineGearing.Size = New System.Drawing.Size(18, 20)
    Me.Numeric_Shadow_BaselineGearing.TabIndex = 9
    Me.Numeric_Shadow_BaselineGearing.Value = New Decimal(New Integer() {1, 0, 0, 65536})
    '
    'DateTime_Trading_StartDate
    '
    Me.DateTime_Trading_StartDate.Location = New System.Drawing.Point(146, 8)
    Me.DateTime_Trading_StartDate.Name = "DateTime_Trading_StartDate"
    Me.DateTime_Trading_StartDate.Size = New System.Drawing.Size(130, 20)
    Me.DateTime_Trading_StartDate.TabIndex = 0
    Me.DateTime_Trading_StartDate.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
    '
    'Numeric_Shadow_RebalanceThreshold
    '
    Me.Numeric_Shadow_RebalanceThreshold.DecimalPlaces = 4
    Me.Numeric_Shadow_RebalanceThreshold.Increment = New Decimal(New Integer() {5, 0, 0, 196608})
    Me.Numeric_Shadow_RebalanceThreshold.Location = New System.Drawing.Point(222, 128)
    Me.Numeric_Shadow_RebalanceThreshold.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
    Me.Numeric_Shadow_RebalanceThreshold.Name = "Numeric_Shadow_RebalanceThreshold"
    Me.Numeric_Shadow_RebalanceThreshold.Size = New System.Drawing.Size(18, 20)
    Me.Numeric_Shadow_RebalanceThreshold.TabIndex = 7
    Me.Numeric_Shadow_RebalanceThreshold.Value = New Decimal(New Integer() {1, 0, 0, 131072})
    '
    'Panel7
    '
    Me.Panel7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel7.Controls.Add(Me.Label27)
    Me.Panel7.Controls.Add(Me.Numeric_Trading_VolWeightCap)
    Me.Panel7.Controls.Add(Me.Label22)
    Me.Panel7.Controls.Add(Me.Numeric_Trading_VolExponent)
    Me.Panel7.Controls.Add(Me.Numeric_Trading_VolWeightDuration)
    Me.Panel7.Controls.Add(Me.Radio_Trading_VolWeight_VsSecondDuration)
    Me.Panel7.Controls.Add(Me.Radio_Trading_VolWeightNone)
    Me.Panel7.Controls.Add(Me.Radio_Trading_VolWeightByHighStDev)
    Me.Panel7.Location = New System.Drawing.Point(95, 179)
    Me.Panel7.Name = "Panel7"
    Me.Panel7.Size = New System.Drawing.Size(245, 104)
    Me.Panel7.TabIndex = 10
    '
    'Label27
    '
    Me.Label27.AutoSize = True
    Me.Label27.Location = New System.Drawing.Point(146, 5)
    Me.Label27.Name = "Label27"
    Me.Label27.Size = New System.Drawing.Size(26, 13)
    Me.Label27.TabIndex = 2
    Me.Label27.Text = "Cap"
    '
    'Numeric_Trading_VolWeightCap
    '
    Me.Numeric_Trading_VolWeightCap.DecimalPlaces = 4
    Me.Numeric_Trading_VolWeightCap.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
    Me.Numeric_Trading_VolWeightCap.Location = New System.Drawing.Point(178, 3)
    Me.Numeric_Trading_VolWeightCap.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_Trading_VolWeightCap.Name = "Numeric_Trading_VolWeightCap"
    Me.Numeric_Trading_VolWeightCap.Size = New System.Drawing.Size(60, 20)
    Me.Numeric_Trading_VolWeightCap.TabIndex = 3
    Me.Numeric_Trading_VolWeightCap.Value = New Decimal(New Integer() {2, 0, 0, 0})
    '
    'Label22
    '
    Me.Label22.AutoSize = True
    Me.Label22.Location = New System.Drawing.Point(3, 5)
    Me.Label22.Name = "Label22"
    Me.Label22.Size = New System.Drawing.Size(52, 13)
    Me.Label22.TabIndex = 0
    Me.Label22.Text = "Exponent"
    '
    'Numeric_Trading_VolExponent
    '
    Me.Numeric_Trading_VolExponent.DecimalPlaces = 4
    Me.Numeric_Trading_VolExponent.Increment = New Decimal(New Integer() {1, 0, 0, 131072})
    Me.Numeric_Trading_VolExponent.Location = New System.Drawing.Point(66, 3)
    Me.Numeric_Trading_VolExponent.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_Trading_VolExponent.Name = "Numeric_Trading_VolExponent"
    Me.Numeric_Trading_VolExponent.Size = New System.Drawing.Size(60, 20)
    Me.Numeric_Trading_VolExponent.TabIndex = 1
    Me.Numeric_Trading_VolExponent.Value = New Decimal(New Integer() {1, 0, 0, 0})
    '
    'Numeric_Trading_VolWeightDuration
    '
    Me.Numeric_Trading_VolWeightDuration.Increment = New Decimal(New Integer() {5, 0, 0, 0})
    Me.Numeric_Trading_VolWeightDuration.Location = New System.Drawing.Point(161, 72)
    Me.Numeric_Trading_VolWeightDuration.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
    Me.Numeric_Trading_VolWeightDuration.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
    Me.Numeric_Trading_VolWeightDuration.Name = "Numeric_Trading_VolWeightDuration"
    Me.Numeric_Trading_VolWeightDuration.Size = New System.Drawing.Size(58, 20)
    Me.Numeric_Trading_VolWeightDuration.TabIndex = 7
    Me.Numeric_Trading_VolWeightDuration.Value = New Decimal(New Integer() {220, 0, 0, 0})
    '
    'Radio_Trading_VolWeight_VsSecondDuration
    '
    Me.Radio_Trading_VolWeight_VsSecondDuration.AutoSize = True
    Me.Radio_Trading_VolWeight_VsSecondDuration.Location = New System.Drawing.Point(3, 72)
    Me.Radio_Trading_VolWeight_VsSecondDuration.Name = "Radio_Trading_VolWeight_VsSecondDuration"
    Me.Radio_Trading_VolWeight_VsSecondDuration.Size = New System.Drawing.Size(124, 17)
    Me.Radio_Trading_VolWeight_VsSecondDuration.TabIndex = 6
    Me.Radio_Trading_VolWeight_VsSecondDuration.TabStop = True
    Me.Radio_Trading_VolWeight_VsSecondDuration.Text = "StdDev : Average vs"
    Me.Radio_Trading_VolWeight_VsSecondDuration.UseVisualStyleBackColor = True
    '
    'Radio_Trading_VolWeightNone
    '
    Me.Radio_Trading_VolWeightNone.AutoSize = True
    Me.Radio_Trading_VolWeightNone.Location = New System.Drawing.Point(3, 32)
    Me.Radio_Trading_VolWeightNone.Name = "Radio_Trading_VolWeightNone"
    Me.Radio_Trading_VolWeightNone.Size = New System.Drawing.Size(51, 17)
    Me.Radio_Trading_VolWeightNone.TabIndex = 4
    Me.Radio_Trading_VolWeightNone.TabStop = True
    Me.Radio_Trading_VolWeightNone.Text = "None"
    Me.Radio_Trading_VolWeightNone.UseVisualStyleBackColor = True
    '
    'Radio_Trading_VolWeightByHighStDev
    '
    Me.Radio_Trading_VolWeightByHighStDev.AutoSize = True
    Me.Radio_Trading_VolWeightByHighStDev.Location = New System.Drawing.Point(3, 52)
    Me.Radio_Trading_VolWeightByHighStDev.Name = "Radio_Trading_VolWeightByHighStDev"
    Me.Radio_Trading_VolWeightByHighStDev.Size = New System.Drawing.Size(149, 17)
    Me.Radio_Trading_VolWeightByHighStDev.TabIndex = 5
    Me.Radio_Trading_VolWeightByHighStDev.TabStop = True
    Me.Radio_Trading_VolWeightByHighStDev.Text = "StdDev : High vs Average"
    Me.Radio_Trading_VolWeightByHighStDev.UseVisualStyleBackColor = True
    '
    'Label9
    '
    Me.Label9.AutoSize = True
    Me.Label9.Location = New System.Drawing.Point(8, 12)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(94, 13)
    Me.Label9.TabIndex = 14
    Me.Label9.Text = "Trading Start Date"
    '
    'Label15
    '
    Me.Label15.Location = New System.Drawing.Point(8, 184)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(65, 39)
    Me.Label15.TabIndex = 12
    Me.Label15.Text = "Volatility Weighting"
    '
    'Label11
    '
    Me.Label11.AutoSize = True
    Me.Label11.Location = New System.Drawing.Point(8, 36)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(75, 13)
    Me.Label11.TabIndex = 15
    Me.Label11.Text = "First Trade at :"
    '
    'Group_FirstTrade
    '
    Me.Group_FirstTrade.Controls.Add(Me.Radio_Trading_FirstTradeStart)
    Me.Group_FirstTrade.Controls.Add(Me.Radio_Trading_FirstTradeCrossing)
    Me.Group_FirstTrade.Location = New System.Drawing.Point(145, 32)
    Me.Group_FirstTrade.Name = "Group_FirstTrade"
    Me.Group_FirstTrade.Size = New System.Drawing.Size(151, 21)
    Me.Group_FirstTrade.TabIndex = 33
    '
    'Radio_Trading_FirstTradeStart
    '
    Me.Radio_Trading_FirstTradeStart.AutoSize = True
    Me.Radio_Trading_FirstTradeStart.Location = New System.Drawing.Point(2, 2)
    Me.Radio_Trading_FirstTradeStart.Name = "Radio_Trading_FirstTradeStart"
    Me.Radio_Trading_FirstTradeStart.Size = New System.Drawing.Size(47, 17)
    Me.Radio_Trading_FirstTradeStart.TabIndex = 0
    Me.Radio_Trading_FirstTradeStart.TabStop = True
    Me.Radio_Trading_FirstTradeStart.Text = "Start"
    Me.Radio_Trading_FirstTradeStart.UseVisualStyleBackColor = True
    '
    'Radio_Trading_FirstTradeCrossing
    '
    Me.Radio_Trading_FirstTradeCrossing.AutoSize = True
    Me.Radio_Trading_FirstTradeCrossing.Location = New System.Drawing.Point(55, 2)
    Me.Radio_Trading_FirstTradeCrossing.Name = "Radio_Trading_FirstTradeCrossing"
    Me.Radio_Trading_FirstTradeCrossing.Size = New System.Drawing.Size(87, 17)
    Me.Radio_Trading_FirstTradeCrossing.TabIndex = 1
    Me.Radio_Trading_FirstTradeCrossing.TabStop = True
    Me.Radio_Trading_FirstTradeCrossing.Text = "First Crossing"
    Me.Radio_Trading_FirstTradeCrossing.UseVisualStyleBackColor = True
    '
    'Numeric_Trading_MaxDrawdown
    '
    Me.Numeric_Trading_MaxDrawdown.Location = New System.Drawing.Point(146, 78)
    Me.Numeric_Trading_MaxDrawdown.Name = "Numeric_Trading_MaxDrawdown"
    Me.Numeric_Trading_MaxDrawdown.RenaissanceTag = Nothing
    Me.Numeric_Trading_MaxDrawdown.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_Trading_MaxDrawdown.TabIndex = 1
    Me.Numeric_Trading_MaxDrawdown.Text = "100.00%"
    Me.Numeric_Trading_MaxDrawdown.TextFormat = "#,##0.00%"
    Me.Numeric_Trading_MaxDrawdown.Value = 1
    '
    'Label14
    '
    Me.Label14.AutoSize = True
    Me.Label14.Location = New System.Drawing.Point(8, 157)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(87, 13)
    Me.Label14.TabIndex = 19
    Me.Label14.Text = "Baseline Gearing"
    '
    'Label12
    '
    Me.Label12.AutoSize = True
    Me.Label12.Location = New System.Drawing.Point(8, 81)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(89, 13)
    Me.Label12.TabIndex = 16
    Me.Label12.Text = "Drawdown Cutoff"
    '
    'Numeric_Trading_BaselineGearing
    '
    Me.Numeric_Trading_BaselineGearing.Location = New System.Drawing.Point(146, 154)
    Me.Numeric_Trading_BaselineGearing.Name = "Numeric_Trading_BaselineGearing"
    Me.Numeric_Trading_BaselineGearing.RenaissanceTag = Nothing
    Me.Numeric_Trading_BaselineGearing.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_Trading_BaselineGearing.TabIndex = 8
    Me.Numeric_Trading_BaselineGearing.Text = "100.00%"
    Me.Numeric_Trading_BaselineGearing.TextFormat = "#,##0.00%"
    Me.Numeric_Trading_BaselineGearing.Value = 1
    '
    'Numeric_Trading_RebalanceThreshold
    '
    Me.Numeric_Trading_RebalanceThreshold.Location = New System.Drawing.Point(146, 128)
    Me.Numeric_Trading_RebalanceThreshold.Name = "Numeric_Trading_RebalanceThreshold"
    Me.Numeric_Trading_RebalanceThreshold.RenaissanceTag = Nothing
    Me.Numeric_Trading_RebalanceThreshold.Size = New System.Drawing.Size(77, 20)
    Me.Numeric_Trading_RebalanceThreshold.TabIndex = 6
    Me.Numeric_Trading_RebalanceThreshold.Text = "0.00%"
    Me.Numeric_Trading_RebalanceThreshold.TextFormat = "#,##0.00%"
    Me.Numeric_Trading_RebalanceThreshold.Value = 0
    '
    'Label13
    '
    Me.Label13.AutoSize = True
    Me.Label13.Location = New System.Drawing.Point(8, 131)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(109, 13)
    Me.Label13.TabIndex = 18
    Me.Label13.Text = "Rebalance Threshold"
    '
    'Date_DataStartDate
    '
    Me.Date_DataStartDate.Location = New System.Drawing.Point(102, 95)
    Me.Date_DataStartDate.Name = "Date_DataStartDate"
    Me.Date_DataStartDate.Size = New System.Drawing.Size(130, 20)
    Me.Date_DataStartDate.TabIndex = 24
    Me.Date_DataStartDate.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
    '
    'Label24
    '
    Me.Label24.AutoSize = True
    Me.Label24.Location = New System.Drawing.Point(8, 99)
    Me.Label24.Name = "Label24"
    Me.Label24.Size = New System.Drawing.Size(81, 13)
    Me.Label24.TabIndex = 23
    Me.Label24.Text = "Data Start Date"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(8, 51)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(62, 13)
    Me.Label2.TabIndex = 10
    Me.Label2.Text = "Rate Series"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(8, 7)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(91, 13)
    Me.Label1.TabIndex = 9
    Me.Label1.Text = "Investment Series"
    '
    'Combo_InstrumentSeries
    '
    Me.Combo_InstrumentSeries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_InstrumentSeries.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InstrumentSeries.Location = New System.Drawing.Point(5, 24)
    Me.Combo_InstrumentSeries.Name = "Combo_InstrumentSeries"
    Me.Combo_InstrumentSeries.Size = New System.Drawing.Size(357, 21)
    Me.Combo_InstrumentSeries.TabIndex = 8
    '
    'Combo_InterestRateSeries
    '
    Me.Combo_InterestRateSeries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_InterestRateSeries.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_InterestRateSeries.Location = New System.Drawing.Point(5, 68)
    Me.Combo_InterestRateSeries.Name = "Combo_InterestRateSeries"
    Me.Combo_InterestRateSeries.Size = New System.Drawing.Size(357, 21)
    Me.Combo_InterestRateSeries.TabIndex = 7
    '
    'Tab_InstrumentDetails
    '
    Me.Tab_InstrumentDetails.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Tab_InstrumentDetails.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Descriptions)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Contacts)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Performance)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Comparisons)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Charts)
    Me.Tab_InstrumentDetails.Controls.Add(Me.Tab_Statistics)
    Me.Tab_InstrumentDetails.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Tab_InstrumentDetails.Location = New System.Drawing.Point(1, 1)
    Me.Tab_InstrumentDetails.Margin = New System.Windows.Forms.Padding(1)
    Me.Tab_InstrumentDetails.Multiline = True
    Me.Tab_InstrumentDetails.Name = "Tab_InstrumentDetails"
    Me.Tab_InstrumentDetails.SelectedIndex = 0
    Me.Tab_InstrumentDetails.Size = New System.Drawing.Size(821, 730)
    Me.Tab_InstrumentDetails.TabIndex = 0
    '
    'Tab_Descriptions
    '
    Me.Tab_Descriptions.AutoScroll = True
    Me.Tab_Descriptions.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Descriptions.Controls.Add(Me.Check_VAMI_ShowBands)
    Me.Tab_Descriptions.Controls.Add(Me.Check_VAMI_ShowPortfolio)
    Me.Tab_Descriptions.Controls.Add(Me.Label_MaxDrawdown)
    Me.Tab_Descriptions.Controls.Add(Me.Label23)
    Me.Tab_Descriptions.Controls.Add(Me.Label21)
    Me.Tab_Descriptions.Controls.Add(Me.Label20)
    Me.Tab_Descriptions.Controls.Add(Me.Label_IRR)
    Me.Tab_Descriptions.Controls.Add(Me.Label_ITDReturn)
    Me.Tab_Descriptions.Controls.Add(Me.Label_LastDate)
    Me.Tab_Descriptions.Controls.Add(Me.Label_FirstDate)
    Me.Tab_Descriptions.Controls.Add(Me.Label18)
    Me.Tab_Descriptions.Controls.Add(Me.Label19)
    Me.Tab_Descriptions.Controls.Add(Me.Chart_FrontPage_RollingReturn)
    Me.Tab_Descriptions.Controls.Add(Me.Chart_FrontPage_Prices)
    Me.Tab_Descriptions.Controls.Add(Me.Label17)
    Me.Tab_Descriptions.Controls.Add(Me.Label16)
    Me.Tab_Descriptions.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Descriptions.Name = "Tab_Descriptions"
    Me.Tab_Descriptions.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Descriptions.Size = New System.Drawing.Size(813, 701)
    Me.Tab_Descriptions.TabIndex = 0
    Me.Tab_Descriptions.Text = "Summary"
    Me.Tab_Descriptions.UseVisualStyleBackColor = True
    '
    'Check_VAMI_ShowBands
    '
    Me.Check_VAMI_ShowBands.AutoSize = True
    Me.Check_VAMI_ShowBands.Location = New System.Drawing.Point(456, 16)
    Me.Check_VAMI_ShowBands.Name = "Check_VAMI_ShowBands"
    Me.Check_VAMI_ShowBands.Size = New System.Drawing.Size(56, 17)
    Me.Check_VAMI_ShowBands.TabIndex = 29
    Me.Check_VAMI_ShowBands.Text = "Bands"
    Me.Check_VAMI_ShowBands.UseVisualStyleBackColor = True
    '
    'Check_VAMI_ShowPortfolio
    '
    Me.Check_VAMI_ShowPortfolio.AutoSize = True
    Me.Check_VAMI_ShowPortfolio.Location = New System.Drawing.Point(386, 16)
    Me.Check_VAMI_ShowPortfolio.Name = "Check_VAMI_ShowPortfolio"
    Me.Check_VAMI_ShowPortfolio.Size = New System.Drawing.Size(64, 17)
    Me.Check_VAMI_ShowPortfolio.TabIndex = 28
    Me.Check_VAMI_ShowPortfolio.Text = "Portfolio"
    Me.Check_VAMI_ShowPortfolio.UseVisualStyleBackColor = True
    '
    'Label_MaxDrawdown
    '
    Me.Label_MaxDrawdown.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_MaxDrawdown.Location = New System.Drawing.Point(103, 113)
    Me.Label_MaxDrawdown.Name = "Label_MaxDrawdown"
    Me.Label_MaxDrawdown.Size = New System.Drawing.Size(191, 18)
    Me.Label_MaxDrawdown.TabIndex = 27
    '
    'Label23
    '
    Me.Label23.AutoSize = True
    Me.Label23.Location = New System.Drawing.Point(15, 114)
    Me.Label23.Name = "Label23"
    Me.Label23.Size = New System.Drawing.Size(81, 13)
    Me.Label23.TabIndex = 26
    Me.Label23.Text = "Max Drawdown"
    '
    'Label21
    '
    Me.Label21.AutoSize = True
    Me.Label21.Location = New System.Drawing.Point(316, 277)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(74, 13)
    Me.Label21.TabIndex = 25
    Me.Label21.Text = "Rolling Return"
    '
    'Label20
    '
    Me.Label20.AutoSize = True
    Me.Label20.Location = New System.Drawing.Point(316, 18)
    Me.Label20.Name = "Label20"
    Me.Label20.Size = New System.Drawing.Size(33, 13)
    Me.Label20.TabIndex = 24
    Me.Label20.Text = "VAMI"
    '
    'Label_IRR
    '
    Me.Label_IRR.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_IRR.Location = New System.Drawing.Point(103, 89)
    Me.Label_IRR.Name = "Label_IRR"
    Me.Label_IRR.Size = New System.Drawing.Size(191, 18)
    Me.Label_IRR.TabIndex = 23
    '
    'Label_ITDReturn
    '
    Me.Label_ITDReturn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_ITDReturn.Location = New System.Drawing.Point(103, 65)
    Me.Label_ITDReturn.Name = "Label_ITDReturn"
    Me.Label_ITDReturn.Size = New System.Drawing.Size(191, 18)
    Me.Label_ITDReturn.TabIndex = 22
    '
    'Label_LastDate
    '
    Me.Label_LastDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_LastDate.Location = New System.Drawing.Point(103, 41)
    Me.Label_LastDate.Name = "Label_LastDate"
    Me.Label_LastDate.Size = New System.Drawing.Size(191, 18)
    Me.Label_LastDate.TabIndex = 21
    '
    'Label_FirstDate
    '
    Me.Label_FirstDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_FirstDate.Location = New System.Drawing.Point(103, 17)
    Me.Label_FirstDate.Name = "Label_FirstDate"
    Me.Label_FirstDate.Size = New System.Drawing.Size(191, 18)
    Me.Label_FirstDate.TabIndex = 20
    '
    'Label18
    '
    Me.Label18.AutoSize = True
    Me.Label18.Location = New System.Drawing.Point(15, 90)
    Me.Label18.Name = "Label18"
    Me.Label18.Size = New System.Drawing.Size(26, 13)
    Me.Label18.TabIndex = 19
    Me.Label18.Text = "IRR"
    '
    'Label19
    '
    Me.Label19.AutoSize = True
    Me.Label19.Location = New System.Drawing.Point(15, 66)
    Me.Label19.Name = "Label19"
    Me.Label19.Size = New System.Drawing.Size(60, 13)
    Me.Label19.TabIndex = 18
    Me.Label19.Text = "ITD Return"
    '
    'Chart_FrontPage_RollingReturn
    '
    Me.Chart_FrontPage_RollingReturn.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_FrontPage_RollingReturn.BackColor = System.Drawing.Color.Azure
    Me.Chart_FrontPage_RollingReturn.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_FrontPage_RollingReturn.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_FrontPage_RollingReturn.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_FrontPage_RollingReturn.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_FrontPage_RollingReturn.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea1.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea1.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea1.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea1.AxisX.LabelStyle.Format = "Y"
    ChartArea1.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea1.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea1.AxisY.LabelStyle.Format = "P0"
    ChartArea1.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.StartFromZero = False
    ChartArea1.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea1.BackColor = System.Drawing.Color.Transparent
    ChartArea1.BorderColor = System.Drawing.Color.DimGray
    ChartArea1.Name = "Default"
    Me.Chart_FrontPage_RollingReturn.ChartAreas.Add(ChartArea1)
    Legend1.BackColor = System.Drawing.Color.Transparent
    Legend1.BorderColor = System.Drawing.Color.Transparent
    Legend1.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend1.DockToChartArea = "Default"
    Legend1.Enabled = False
    Legend1.Name = "Default"
    Me.Chart_FrontPage_RollingReturn.Legends.Add(Legend1)
    Me.Chart_FrontPage_RollingReturn.Location = New System.Drawing.Point(319, 297)
    Me.Chart_FrontPage_RollingReturn.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_FrontPage_RollingReturn.Name = "Chart_FrontPage_RollingReturn"
    Me.Chart_FrontPage_RollingReturn.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series1.BorderWidth = 2
    Series1.ChartType = "Line"
    Series1.CustomAttributes = "LabelStyle=Top"
    Series1.Name = "Series1"
    Series1.ShadowOffset = 1
    Series1.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series1.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series2.BorderWidth = 2
    Series2.ChartType = "Line"
    Series2.CustomAttributes = "LabelStyle=Top"
    Series2.Name = "Series2"
    Series2.ShadowOffset = 1
    Series2.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series2.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_FrontPage_RollingReturn.Series.Add(Series1)
    Me.Chart_FrontPage_RollingReturn.Series.Add(Series2)
    Me.Chart_FrontPage_RollingReturn.Size = New System.Drawing.Size(478, 218)
    Me.Chart_FrontPage_RollingReturn.TabIndex = 17
    Me.Chart_FrontPage_RollingReturn.Text = "Chart2"
    '
    'Chart_FrontPage_Prices
    '
    Me.Chart_FrontPage_Prices.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_FrontPage_Prices.BackColor = System.Drawing.Color.Azure
    Me.Chart_FrontPage_Prices.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_FrontPage_Prices.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_FrontPage_Prices.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_FrontPage_Prices.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_FrontPage_Prices.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea2.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea2.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea2.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea2.AxisX.LabelStyle.Format = "Y"
    ChartArea2.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea2.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea2.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea2.AxisY.LabelStyle.Format = "N0"
    ChartArea2.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea2.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.StartFromZero = False
    ChartArea2.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea2.BackColor = System.Drawing.Color.Transparent
    ChartArea2.BorderColor = System.Drawing.Color.DimGray
    ChartArea2.Name = "Default"
    Me.Chart_FrontPage_Prices.ChartAreas.Add(ChartArea2)
    Legend2.BackColor = System.Drawing.Color.Transparent
    Legend2.BorderColor = System.Drawing.Color.Transparent
    Legend2.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend2.DockToChartArea = "Default"
    Legend2.Enabled = False
    Legend2.Name = "Default"
    Me.Chart_FrontPage_Prices.Legends.Add(Legend2)
    Me.Chart_FrontPage_Prices.Location = New System.Drawing.Point(319, 38)
    Me.Chart_FrontPage_Prices.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_FrontPage_Prices.Name = "Chart_FrontPage_Prices"
    Me.Chart_FrontPage_Prices.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series3.BorderWidth = 2
    Series3.ChartType = "Line"
    Series3.CustomAttributes = "LabelStyle=Top"
    Series3.Name = "Series1"
    Series3.ShadowOffset = 1
    Series3.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series3.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series4.BorderWidth = 2
    Series4.ChartType = "Line"
    Series4.CustomAttributes = "LabelStyle=Top"
    Series4.Name = "Series2"
    Series4.ShadowOffset = 1
    Series4.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series4.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_FrontPage_Prices.Series.Add(Series3)
    Me.Chart_FrontPage_Prices.Series.Add(Series4)
    Me.Chart_FrontPage_Prices.Size = New System.Drawing.Size(478, 218)
    Me.Chart_FrontPage_Prices.TabIndex = 16
    Me.Chart_FrontPage_Prices.Text = "Chart2"
    '
    'Label17
    '
    Me.Label17.AutoSize = True
    Me.Label17.Location = New System.Drawing.Point(15, 42)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(53, 13)
    Me.Label17.TabIndex = 15
    Me.Label17.Text = "Last Date"
    '
    'Label16
    '
    Me.Label16.AutoSize = True
    Me.Label16.Location = New System.Drawing.Point(15, 17)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(52, 13)
    Me.Label16.TabIndex = 14
    Me.Label16.Text = "First Date"
    '
    'Tab_Contacts
    '
    Me.Tab_Contacts.AutoScroll = True
    Me.Tab_Contacts.Controls.Add(Me.Grid_PortfolioHistory)
    Me.Tab_Contacts.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Contacts.Name = "Tab_Contacts"
    Me.Tab_Contacts.Size = New System.Drawing.Size(813, 701)
    Me.Tab_Contacts.TabIndex = 2
    Me.Tab_Contacts.Text = "Portfolio History"
    Me.Tab_Contacts.UseVisualStyleBackColor = True
    '
    'Grid_PortfolioHistory
    '
    Me.Grid_PortfolioHistory.AllowEditing = False
    Me.Grid_PortfolioHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_PortfolioHistory.AutoGenerateColumns = False
    Me.Grid_PortfolioHistory.ColumnInfo = resources.GetString("Grid_PortfolioHistory.ColumnInfo")
    Me.Grid_PortfolioHistory.Location = New System.Drawing.Point(3, 3)
    Me.Grid_PortfolioHistory.Name = "Grid_PortfolioHistory"
    Me.Grid_PortfolioHistory.Rows.DefaultSize = 17
    Me.Grid_PortfolioHistory.Size = New System.Drawing.Size(805, 695)
    Me.Grid_PortfolioHistory.TabIndex = 2
    '
    'Tab_Performance
    '
    Me.Tab_Performance.AutoScroll = True
    Me.Tab_Performance.Controls.Add(Me.TabControl_PerformanceTables)
    Me.Tab_Performance.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Performance.Name = "Tab_Performance"
    Me.Tab_Performance.Size = New System.Drawing.Size(813, 701)
    Me.Tab_Performance.TabIndex = 3
    Me.Tab_Performance.Text = "Performance"
    Me.Tab_Performance.UseVisualStyleBackColor = True
    '
    'TabControl_PerformanceTables
    '
    Me.TabControl_PerformanceTables.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_PerformanceTables.Controls.Add(Me.TabPage1)
    Me.TabControl_PerformanceTables.Location = New System.Drawing.Point(3, 3)
    Me.TabControl_PerformanceTables.Multiline = True
    Me.TabControl_PerformanceTables.Name = "TabControl_PerformanceTables"
    Me.TabControl_PerformanceTables.SelectedIndex = 0
    Me.TabControl_PerformanceTables.Size = New System.Drawing.Size(807, 698)
    Me.TabControl_PerformanceTables.TabIndex = 0
    '
    'TabPage1
    '
    Me.TabPage1.Controls.Add(Me.Label_PT_0)
    Me.TabPage1.Controls.Add(Me.Grid_Performance_0)
    Me.TabPage1.Location = New System.Drawing.Point(4, 22)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
    Me.TabPage1.Size = New System.Drawing.Size(799, 672)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "Stock1"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'Label_PT_0
    '
    Me.Label_PT_0.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_PT_0.BackColor = System.Drawing.SystemColors.Window
    Me.Label_PT_0.Location = New System.Drawing.Point(3, 3)
    Me.Label_PT_0.Name = "Label_PT_0"
    Me.Label_PT_0.Size = New System.Drawing.Size(793, 19)
    Me.Label_PT_0.TabIndex = 0
    Me.Label_PT_0.Text = "FundName"
    '
    'Grid_Performance_0
    '
    Me.Grid_Performance_0.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Performance_0.ColumnInfo = resources.GetString("Grid_Performance_0.ColumnInfo")
    Me.Grid_Performance_0.Location = New System.Drawing.Point(3, 21)
    Me.Grid_Performance_0.Name = "Grid_Performance_0"
    Me.Grid_Performance_0.Rows.DefaultSize = 17
    Me.Grid_Performance_0.Size = New System.Drawing.Size(796, 648)
    Me.Grid_Performance_0.TabIndex = 1
    '
    'Tab_Comparisons
    '
    Me.Tab_Comparisons.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Comparisons.Name = "Tab_Comparisons"
    Me.Tab_Comparisons.Size = New System.Drawing.Size(813, 701)
    Me.Tab_Comparisons.TabIndex = 4
    Me.Tab_Comparisons.Text = "."
    Me.Tab_Comparisons.UseVisualStyleBackColor = True
    '
    'Tab_Charts
    '
    Me.Tab_Charts.Controls.Add(Me.Check_AutoStart)
    Me.Tab_Charts.Controls.Add(Me.Combo_Charts_CompareSeriesGroup)
    Me.Tab_Charts.Controls.Add(Me.Panel2)
    Me.Tab_Charts.Controls.Add(Me.Panel1)
    Me.Tab_Charts.Controls.Add(Me.Button_DefaultDate)
    Me.Tab_Charts.Controls.Add(Me.Label39)
    Me.Tab_Charts.Controls.Add(Me.Text_Chart_RollingPeriod)
    Me.Tab_Charts.Controls.Add(Me.Text_Chart_Lamda)
    Me.Tab_Charts.Controls.Add(Me.Date_Charts_DateTo)
    Me.Tab_Charts.Controls.Add(Me.Date_Charts_DateFrom)
    Me.Tab_Charts.Controls.Add(Me.Label37)
    Me.Tab_Charts.Controls.Add(Me.Label38)
    Me.Tab_Charts.Controls.Add(Me.Label36)
    Me.Tab_Charts.Controls.Add(Me.Label35)
    Me.Tab_Charts.Controls.Add(Me.Combo_Charts_CompareSeriesPertrac)
    Me.Tab_Charts.Controls.Add(Me.Label29)
    Me.Tab_Charts.Controls.Add(Me.TabControl_Charts)
    Me.Tab_Charts.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Charts.Name = "Tab_Charts"
    Me.Tab_Charts.Size = New System.Drawing.Size(813, 701)
    Me.Tab_Charts.TabIndex = 5
    Me.Tab_Charts.Text = "Charts"
    Me.Tab_Charts.UseVisualStyleBackColor = True
    '
    'Check_AutoStart
    '
    Me.Check_AutoStart.Location = New System.Drawing.Point(209, 29)
    Me.Check_AutoStart.Name = "Check_AutoStart"
    Me.Check_AutoStart.Size = New System.Drawing.Size(17, 16)
    Me.Check_AutoStart.TabIndex = 21
    Me.Check_AutoStart.UseVisualStyleBackColor = True
    '
    'Combo_Charts_CompareSeriesGroup
    '
    Me.Combo_Charts_CompareSeriesGroup.FormattingEnabled = True
    Me.Combo_Charts_CompareSeriesGroup.Location = New System.Drawing.Point(228, 2)
    Me.Combo_Charts_CompareSeriesGroup.Name = "Combo_Charts_CompareSeriesGroup"
    Me.Combo_Charts_CompareSeriesGroup.Size = New System.Drawing.Size(494, 21)
    Me.Combo_Charts_CompareSeriesGroup.TabIndex = 20
    '
    'Panel2
    '
    Me.Panel2.Controls.Add(Me.Radio_Charts_CompareGroup)
    Me.Panel2.Controls.Add(Me.Radio_Charts_ComparePertrac)
    Me.Panel2.Location = New System.Drawing.Point(91, 1)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(136, 24)
    Me.Panel2.TabIndex = 19
    '
    'Radio_Charts_CompareGroup
    '
    Me.Radio_Charts_CompareGroup.AutoSize = True
    Me.Radio_Charts_CompareGroup.Location = New System.Drawing.Point(68, 2)
    Me.Radio_Charts_CompareGroup.Name = "Radio_Charts_CompareGroup"
    Me.Radio_Charts_CompareGroup.Size = New System.Drawing.Size(54, 17)
    Me.Radio_Charts_CompareGroup.TabIndex = 16
    Me.Radio_Charts_CompareGroup.TabStop = True
    Me.Radio_Charts_CompareGroup.Text = "Group"
    Me.Radio_Charts_CompareGroup.UseVisualStyleBackColor = True
    '
    'Radio_Charts_ComparePertrac
    '
    Me.Radio_Charts_ComparePertrac.AutoSize = True
    Me.Radio_Charts_ComparePertrac.Location = New System.Drawing.Point(3, 2)
    Me.Radio_Charts_ComparePertrac.Name = "Radio_Charts_ComparePertrac"
    Me.Radio_Charts_ComparePertrac.Size = New System.Drawing.Size(59, 17)
    Me.Radio_Charts_ComparePertrac.TabIndex = 15
    Me.Radio_Charts_ComparePertrac.TabStop = True
    Me.Radio_Charts_ComparePertrac.Text = "Pertrac"
    Me.Radio_Charts_ComparePertrac.UseVisualStyleBackColor = True
    '
    'Panel1
    '
    Me.Panel1.Controls.Add(Me.Text_ScalingFactor)
    Me.Panel1.Controls.Add(Me.Radio_DynamicScalingFactor)
    Me.Panel1.Controls.Add(Me.Radio_SingleScalingFactor)
    Me.Panel1.Location = New System.Drawing.Point(496, 25)
    Me.Panel1.Margin = New System.Windows.Forms.Padding(1)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(226, 49)
    Me.Panel1.TabIndex = 18
    '
    'Text_ScalingFactor
    '
    Me.Text_ScalingFactor.Location = New System.Drawing.Point(136, 3)
    Me.Text_ScalingFactor.Name = "Text_ScalingFactor"
    Me.Text_ScalingFactor.RenaissanceTag = Nothing
    Me.Text_ScalingFactor.Size = New System.Drawing.Size(56, 20)
    Me.Text_ScalingFactor.TabIndex = 15
    Me.Text_ScalingFactor.Text = "100.0%"
    Me.Text_ScalingFactor.TextFormat = "#,##0.0%"
    Me.Text_ScalingFactor.Value = 1
    '
    'Radio_DynamicScalingFactor
    '
    Me.Radio_DynamicScalingFactor.AutoSize = True
    Me.Radio_DynamicScalingFactor.Location = New System.Drawing.Point(5, 27)
    Me.Radio_DynamicScalingFactor.Name = "Radio_DynamicScalingFactor"
    Me.Radio_DynamicScalingFactor.Size = New System.Drawing.Size(198, 17)
    Me.Radio_DynamicScalingFactor.TabIndex = 16
    Me.Radio_DynamicScalingFactor.TabStop = True
    Me.Radio_DynamicScalingFactor.Text = "Match Volatility to Comparison Series"
    Me.Radio_DynamicScalingFactor.UseVisualStyleBackColor = True
    '
    'Radio_SingleScalingFactor
    '
    Me.Radio_SingleScalingFactor.AutoSize = True
    Me.Radio_SingleScalingFactor.Location = New System.Drawing.Point(5, 4)
    Me.Radio_SingleScalingFactor.Name = "Radio_SingleScalingFactor"
    Me.Radio_SingleScalingFactor.Size = New System.Drawing.Size(125, 17)
    Me.Radio_SingleScalingFactor.TabIndex = 14
    Me.Radio_SingleScalingFactor.TabStop = True
    Me.Radio_SingleScalingFactor.Text = "Single Scaling Factor"
    Me.Radio_SingleScalingFactor.UseVisualStyleBackColor = True
    '
    'Button_DefaultDate
    '
    Me.Button_DefaultDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_DefaultDate.Location = New System.Drawing.Point(208, 50)
    Me.Button_DefaultDate.Name = "Button_DefaultDate"
    Me.Button_DefaultDate.Size = New System.Drawing.Size(18, 20)
    Me.Button_DefaultDate.TabIndex = 6
    Me.Button_DefaultDate.Text = ".."
    Me.Button_DefaultDate.UseVisualStyleBackColor = True
    '
    'Label39
    '
    Me.Label39.AutoSize = True
    Me.Label39.Location = New System.Drawing.Point(463, 29)
    Me.Label39.Name = "Label39"
    Me.Label39.Size = New System.Drawing.Size(29, 13)
    Me.Label39.TabIndex = 13
    Me.Label39.Text = "mths"
    '
    'Text_Chart_RollingPeriod
    '
    Me.Text_Chart_RollingPeriod.Location = New System.Drawing.Point(414, 25)
    Me.Text_Chart_RollingPeriod.Name = "Text_Chart_RollingPeriod"
    Me.Text_Chart_RollingPeriod.RenaissanceTag = Nothing
    Me.Text_Chart_RollingPeriod.Size = New System.Drawing.Size(43, 20)
    Me.Text_Chart_RollingPeriod.TabIndex = 10
    Me.Text_Chart_RollingPeriod.Text = "12"
    Me.Text_Chart_RollingPeriod.TextFormat = "#,##0"
    Me.Text_Chart_RollingPeriod.Value = 12
    '
    'Text_Chart_Lamda
    '
    Me.Text_Chart_Lamda.Location = New System.Drawing.Point(269, 26)
    Me.Text_Chart_Lamda.Name = "Text_Chart_Lamda"
    Me.Text_Chart_Lamda.RenaissanceTag = Nothing
    Me.Text_Chart_Lamda.Size = New System.Drawing.Size(56, 20)
    Me.Text_Chart_Lamda.TabIndex = 8
    Me.Text_Chart_Lamda.Text = "1.00"
    Me.Text_Chart_Lamda.TextFormat = "#,##0.00"
    Me.Text_Chart_Lamda.Value = 1
    '
    'Date_Charts_DateTo
    '
    Me.Date_Charts_DateTo.Location = New System.Drawing.Point(66, 50)
    Me.Date_Charts_DateTo.Name = "Date_Charts_DateTo"
    Me.Date_Charts_DateTo.Size = New System.Drawing.Size(138, 20)
    Me.Date_Charts_DateTo.TabIndex = 5
    '
    'Date_Charts_DateFrom
    '
    Me.Date_Charts_DateFrom.Location = New System.Drawing.Point(66, 26)
    Me.Date_Charts_DateFrom.Name = "Date_Charts_DateFrom"
    Me.Date_Charts_DateFrom.Size = New System.Drawing.Size(138, 20)
    Me.Date_Charts_DateFrom.TabIndex = 3
    Me.Date_Charts_DateFrom.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
    '
    'Label37
    '
    Me.Label37.AutoSize = True
    Me.Label37.Location = New System.Drawing.Point(338, 29)
    Me.Label37.Name = "Label37"
    Me.Label37.Size = New System.Drawing.Size(72, 13)
    Me.Label37.TabIndex = 9
    Me.Label37.Text = "Rolling Period"
    '
    'Label38
    '
    Me.Label38.AutoSize = True
    Me.Label38.Location = New System.Drawing.Point(225, 29)
    Me.Label38.Name = "Label38"
    Me.Label38.Size = New System.Drawing.Size(39, 13)
    Me.Label38.TabIndex = 7
    Me.Label38.Text = "Lamda"
    '
    'Label36
    '
    Me.Label36.AutoSize = True
    Me.Label36.Location = New System.Drawing.Point(4, 54)
    Me.Label36.Name = "Label36"
    Me.Label36.Size = New System.Drawing.Size(46, 13)
    Me.Label36.TabIndex = 4
    Me.Label36.Text = "Date To"
    '
    'Label35
    '
    Me.Label35.AutoSize = True
    Me.Label35.Location = New System.Drawing.Point(4, 30)
    Me.Label35.Name = "Label35"
    Me.Label35.Size = New System.Drawing.Size(56, 13)
    Me.Label35.TabIndex = 2
    Me.Label35.Text = "Date From"
    '
    'Combo_Charts_CompareSeriesPertrac
    '
    Me.Combo_Charts_CompareSeriesPertrac.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Charts_CompareSeriesPertrac.FormattingEnabled = True
    Me.Combo_Charts_CompareSeriesPertrac.Location = New System.Drawing.Point(228, 2)
    Me.Combo_Charts_CompareSeriesPertrac.MasternameCollection = Nothing
    Me.Combo_Charts_CompareSeriesPertrac.Name = "Combo_Charts_CompareSeriesPertrac"
    '
    '
    '
    Me.Combo_Charts_CompareSeriesPertrac.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
    Me.Combo_Charts_CompareSeriesPertrac.SelectNoMatch = False
    Me.Combo_Charts_CompareSeriesPertrac.Size = New System.Drawing.Size(580, 20)
    Me.Combo_Charts_CompareSeriesPertrac.TabIndex = 1
    Me.Combo_Charts_CompareSeriesPertrac.ThemeName = "ControlDefault"
    '
    'Label29
    '
    Me.Label29.AutoSize = True
    Me.Label29.Location = New System.Drawing.Point(4, 5)
    Me.Label29.Name = "Label29"
    Me.Label29.Size = New System.Drawing.Size(81, 13)
    Me.Label29.TabIndex = 0
    Me.Label29.Text = "Compare Series"
    '
    'TabControl_Charts
    '
    Me.TabControl_Charts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Vami)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Monthly)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Return)
    Me.TabControl_Charts.Controls.Add(Me.Tab_ReturnScatter)
    Me.TabControl_Charts.Controls.Add(Me.Tab_StdDev)
    Me.TabControl_Charts.Controls.Add(Me.Tab_VAR)
    Me.TabControl_Charts.Controls.Add(Me.Tab_Omega)
    Me.TabControl_Charts.Controls.Add(Me.Tab_DrawDown)
    Me.TabControl_Charts.Location = New System.Drawing.Point(3, 74)
    Me.TabControl_Charts.Margin = New System.Windows.Forms.Padding(1)
    Me.TabControl_Charts.MinimumSize = New System.Drawing.Size(300, 200)
    Me.TabControl_Charts.Name = "TabControl_Charts"
    Me.TabControl_Charts.SelectedIndex = 0
    Me.TabControl_Charts.Size = New System.Drawing.Size(805, 626)
    Me.TabControl_Charts.TabIndex = 17
    '
    'Tab_Vami
    '
    Me.Tab_Vami.Controls.Add(Me.Chart_Prices)
    Me.Tab_Vami.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Vami.Name = "Tab_Vami"
    Me.Tab_Vami.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Vami.Size = New System.Drawing.Size(797, 600)
    Me.Tab_Vami.TabIndex = 0
    Me.Tab_Vami.Text = "VAMI"
    Me.Tab_Vami.UseVisualStyleBackColor = True
    '
    'Chart_Prices
    '
    Me.Chart_Prices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Prices.BackColor = System.Drawing.Color.Azure
    Me.Chart_Prices.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Prices.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Prices.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Prices.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Prices.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea3.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea3.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea3.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea3.AxisX.LabelStyle.Format = "Y"
    ChartArea3.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea3.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea3.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea3.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea3.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea3.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea3.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea3.AxisY.LabelStyle.Format = "N0"
    ChartArea3.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea3.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea3.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea3.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea3.AxisY.StartFromZero = False
    ChartArea3.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea3.BackColor = System.Drawing.Color.Transparent
    ChartArea3.BorderColor = System.Drawing.Color.DimGray
    ChartArea3.Name = "Default"
    Me.Chart_Prices.ChartAreas.Add(ChartArea3)
    Legend3.BackColor = System.Drawing.Color.Transparent
    Legend3.BorderColor = System.Drawing.Color.Transparent
    Legend3.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend3.DockToChartArea = "Default"
    Legend3.Enabled = False
    Legend3.Name = "Default"
    Me.Chart_Prices.Legends.Add(Legend3)
    Me.Chart_Prices.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Prices.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Prices.Name = "Chart_Prices"
    Me.Chart_Prices.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series5.BorderWidth = 2
    Series5.ChartType = "Line"
    Series5.CustomAttributes = "LabelStyle=Top"
    Series5.Name = "Series1"
    Series5.ShadowOffset = 1
    Series5.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series5.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series6.BorderWidth = 2
    Series6.ChartType = "Line"
    Series6.CustomAttributes = "LabelStyle=Top"
    Series6.Name = "Series2"
    Series6.ShadowOffset = 1
    Series6.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series6.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Prices.Series.Add(Series5)
    Me.Chart_Prices.Series.Add(Series6)
    Me.Chart_Prices.Size = New System.Drawing.Size(797, 600)
    Me.Chart_Prices.TabIndex = 0
    Me.Chart_Prices.Text = "Chart2"
    '
    'Tab_Monthly
    '
    Me.Tab_Monthly.Controls.Add(Me.Chart_Returns)
    Me.Tab_Monthly.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Monthly.Name = "Tab_Monthly"
    Me.Tab_Monthly.Size = New System.Drawing.Size(797, 600)
    Me.Tab_Monthly.TabIndex = 4
    Me.Tab_Monthly.Text = "Monthly Returns"
    Me.Tab_Monthly.UseVisualStyleBackColor = True
    '
    'Chart_Returns
    '
    Me.Chart_Returns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Returns.BackColor = System.Drawing.Color.Azure
    Me.Chart_Returns.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Returns.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Returns.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Returns.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Returns.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea4.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea4.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea4.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea4.AxisX.LabelStyle.Format = "Y"
    ChartArea4.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea4.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea4.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea4.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea4.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea4.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea4.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea4.AxisY.LabelStyle.Format = "P1"
    ChartArea4.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea4.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea4.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea4.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea4.AxisY.StartFromZero = False
    ChartArea4.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea4.BackColor = System.Drawing.Color.Transparent
    ChartArea4.BorderColor = System.Drawing.Color.DimGray
    ChartArea4.Name = "Default"
    Me.Chart_Returns.ChartAreas.Add(ChartArea4)
    Legend4.BackColor = System.Drawing.Color.Transparent
    Legend4.BorderColor = System.Drawing.Color.Transparent
    Legend4.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend4.DockToChartArea = "Default"
    Legend4.Name = "Default"
    Me.Chart_Returns.Legends.Add(Legend4)
    Me.Chart_Returns.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Returns.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Returns.Name = "Chart_Returns"
    Me.Chart_Returns.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series7.Name = "Series1"
    Series7.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series7.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series8.Name = "Series2"
    Series8.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series8.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Returns.Series.Add(Series7)
    Me.Chart_Returns.Series.Add(Series8)
    Me.Chart_Returns.Size = New System.Drawing.Size(797, 600)
    Me.Chart_Returns.TabIndex = 1
    Me.Chart_Returns.Text = "Monthly Returns"
    '
    'Tab_Return
    '
    Me.Tab_Return.Controls.Add(Me.Chart_RollingReturn)
    Me.Tab_Return.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Return.Name = "Tab_Return"
    Me.Tab_Return.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Return.Size = New System.Drawing.Size(797, 600)
    Me.Tab_Return.TabIndex = 1
    Me.Tab_Return.Text = "Rolling Return"
    Me.Tab_Return.UseVisualStyleBackColor = True
    '
    'Chart_RollingReturn
    '
    Me.Chart_RollingReturn.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_RollingReturn.BackColor = System.Drawing.Color.Azure
    Me.Chart_RollingReturn.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_RollingReturn.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_RollingReturn.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_RollingReturn.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_RollingReturn.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea5.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea5.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea5.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea5.AxisX.LabelStyle.Format = "Y"
    ChartArea5.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea5.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea5.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea5.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea5.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea5.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea5.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea5.AxisY.LabelStyle.Format = "P0"
    ChartArea5.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea5.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea5.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea5.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea5.AxisY.StartFromZero = False
    ChartArea5.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea5.BackColor = System.Drawing.Color.Transparent
    ChartArea5.BorderColor = System.Drawing.Color.DimGray
    ChartArea5.Name = "Default"
    Me.Chart_RollingReturn.ChartAreas.Add(ChartArea5)
    Legend5.BackColor = System.Drawing.Color.Transparent
    Legend5.BorderColor = System.Drawing.Color.Transparent
    Legend5.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend5.DockToChartArea = "Default"
    Legend5.Enabled = False
    Legend5.Name = "Default"
    Me.Chart_RollingReturn.Legends.Add(Legend5)
    Me.Chart_RollingReturn.Location = New System.Drawing.Point(0, 0)
    Me.Chart_RollingReturn.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_RollingReturn.Name = "Chart_RollingReturn"
    Me.Chart_RollingReturn.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series9.BorderWidth = 2
    Series9.ChartType = "Line"
    Series9.CustomAttributes = "LabelStyle=Top"
    Series9.Name = "Series1"
    Series9.ShadowOffset = 1
    Series9.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series9.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series10.BorderWidth = 2
    Series10.ChartType = "Line"
    Series10.CustomAttributes = "LabelStyle=Top"
    Series10.Name = "Series2"
    Series10.ShadowOffset = 1
    Series10.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series10.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_RollingReturn.Series.Add(Series9)
    Me.Chart_RollingReturn.Series.Add(Series10)
    Me.Chart_RollingReturn.Size = New System.Drawing.Size(797, 600)
    Me.Chart_RollingReturn.TabIndex = 3
    Me.Chart_RollingReturn.Text = "Chart2"
    '
    'Tab_ReturnScatter
    '
    Me.Tab_ReturnScatter.Controls.Add(Me.Chart_ReturnScatter)
    Me.Tab_ReturnScatter.Location = New System.Drawing.Point(4, 22)
    Me.Tab_ReturnScatter.Name = "Tab_ReturnScatter"
    Me.Tab_ReturnScatter.Size = New System.Drawing.Size(797, 600)
    Me.Tab_ReturnScatter.TabIndex = 10
    Me.Tab_ReturnScatter.Text = "Return Scatter"
    Me.Tab_ReturnScatter.UseVisualStyleBackColor = True
    '
    'Chart_ReturnScatter
    '
    Me.Chart_ReturnScatter.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_ReturnScatter.BackColor = System.Drawing.Color.Azure
    Me.Chart_ReturnScatter.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_ReturnScatter.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_ReturnScatter.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_ReturnScatter.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_ReturnScatter.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea6.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea6.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea6.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea6.AxisX.LabelStyle.Format = "P0"
    ChartArea6.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea6.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea6.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea6.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea6.AxisX.StartFromZero = False
    ChartArea6.AxisX.Title = "Comparison Series"
    ChartArea6.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
    ChartArea6.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea6.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea6.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea6.AxisY.LabelStyle.Format = "P0"
    ChartArea6.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea6.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea6.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea6.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea6.AxisY.StartFromZero = False
    ChartArea6.AxisY.Title = "Instrument Series"
    ChartArea6.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea6.BackColor = System.Drawing.Color.Transparent
    ChartArea6.BorderColor = System.Drawing.Color.DimGray
    ChartArea6.Name = "Default"
    Me.Chart_ReturnScatter.ChartAreas.Add(ChartArea6)
    Legend6.BackColor = System.Drawing.Color.Transparent
    Legend6.BorderColor = System.Drawing.Color.Transparent
    Legend6.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend6.DockToChartArea = "Default"
    Legend6.Name = "Default"
    Me.Chart_ReturnScatter.Legends.Add(Legend6)
    Me.Chart_ReturnScatter.Location = New System.Drawing.Point(0, 0)
    Me.Chart_ReturnScatter.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_ReturnScatter.Name = "Chart_ReturnScatter"
    Me.Chart_ReturnScatter.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series11.ChartType = "Point"
    Series11.CustomAttributes = "LabelStyle=Top"
    Series11.MarkerBorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series11.Name = "Series1"
    Series11.ShadowOffset = 1
    Series11.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series11.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series12.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series12.ChartType = "Point"
    Series12.CustomAttributes = "LabelStyle=Top"
    Series12.MarkerBorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series12.Name = "Series2"
    Series12.ShadowOffset = 1
    Series12.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series12.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_ReturnScatter.Series.Add(Series11)
    Me.Chart_ReturnScatter.Series.Add(Series12)
    Me.Chart_ReturnScatter.Size = New System.Drawing.Size(797, 600)
    Me.Chart_ReturnScatter.TabIndex = 4
    Me.Chart_ReturnScatter.Text = "Chart2"
    '
    'Tab_StdDev
    '
    Me.Tab_StdDev.Controls.Add(Me.Chart_StdDev)
    Me.Tab_StdDev.Location = New System.Drawing.Point(4, 22)
    Me.Tab_StdDev.Name = "Tab_StdDev"
    Me.Tab_StdDev.Size = New System.Drawing.Size(797, 600)
    Me.Tab_StdDev.TabIndex = 2
    Me.Tab_StdDev.Text = "Volatility"
    Me.Tab_StdDev.UseVisualStyleBackColor = True
    '
    'Chart_StdDev
    '
    Me.Chart_StdDev.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_StdDev.BackColor = System.Drawing.Color.Azure
    Me.Chart_StdDev.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_StdDev.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_StdDev.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_StdDev.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_StdDev.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea7.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea7.AxisX.LabelStyle.Format = "Y"
    ChartArea7.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea7.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea7.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea7.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea7.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea7.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea7.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea7.AxisY.LabelStyle.Format = "P0"
    ChartArea7.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea7.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea7.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea7.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea7.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea7.BackColor = System.Drawing.Color.Transparent
    ChartArea7.BorderColor = System.Drawing.Color.DimGray
    ChartArea7.Name = "Default"
    Me.Chart_StdDev.ChartAreas.Add(ChartArea7)
    Legend7.BackColor = System.Drawing.Color.Transparent
    Legend7.BorderColor = System.Drawing.Color.Transparent
    Legend7.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend7.DockToChartArea = "Default"
    Legend7.Enabled = False
    Legend7.Name = "Default"
    Me.Chart_StdDev.Legends.Add(Legend7)
    Me.Chart_StdDev.Location = New System.Drawing.Point(0, 0)
    Me.Chart_StdDev.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_StdDev.Name = "Chart_StdDev"
    Me.Chart_StdDev.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series13.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series13.BorderWidth = 2
    Series13.ChartType = "Line"
    Series13.CustomAttributes = "LabelStyle=Top"
    Series13.Name = "Series1"
    Series13.ShadowOffset = 1
    Series13.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series13.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series14.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series14.BorderWidth = 2
    Series14.ChartType = "Line"
    Series14.CustomAttributes = "LabelStyle=Top"
    Series14.Name = "Series2"
    Series14.ShadowOffset = 1
    Series14.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series14.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_StdDev.Series.Add(Series13)
    Me.Chart_StdDev.Series.Add(Series14)
    Me.Chart_StdDev.Size = New System.Drawing.Size(797, 600)
    Me.Chart_StdDev.TabIndex = 4
    Me.Chart_StdDev.Text = "Chart2"
    '
    'Tab_VAR
    '
    Me.Tab_VAR.Controls.Add(Me.Chart_VAR)
    Me.Tab_VAR.Controls.Add(Me.Text_Chart_VARConfidence)
    Me.Tab_VAR.Controls.Add(Me.Label59)
    Me.Tab_VAR.Controls.Add(Me.Label58)
    Me.Tab_VAR.Controls.Add(Me.Text_Chart_VARPeriod)
    Me.Tab_VAR.Controls.Add(Me.Label57)
    Me.Tab_VAR.Controls.Add(Me.Label56)
    Me.Tab_VAR.Controls.Add(Me.Combo_Charts_VARSeries)
    Me.Tab_VAR.Location = New System.Drawing.Point(4, 22)
    Me.Tab_VAR.Name = "Tab_VAR"
    Me.Tab_VAR.Size = New System.Drawing.Size(797, 600)
    Me.Tab_VAR.TabIndex = 11
    Me.Tab_VAR.Text = "VAR"
    Me.Tab_VAR.UseVisualStyleBackColor = True
    '
    'Chart_VAR
    '
    Me.Chart_VAR.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_VAR.BackColor = System.Drawing.Color.Azure
    Me.Chart_VAR.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_VAR.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_VAR.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_VAR.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_VAR.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    Me.Chart_VAR.BorderSkin.PageColor = System.Drawing.Color.AliceBlue
    ChartArea8.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea8.AxisX.LabelStyle.Format = "Y"
    ChartArea8.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea8.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea8.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea8.AxisY.LabelStyle.Format = "P0"
    ChartArea8.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea8.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea8.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea8.BackColor = System.Drawing.Color.Transparent
    ChartArea8.BorderColor = System.Drawing.Color.DimGray
    ChartArea8.Name = "Default"
    Me.Chart_VAR.ChartAreas.Add(ChartArea8)
    Legend8.BackColor = System.Drawing.Color.Transparent
    Legend8.BorderColor = System.Drawing.Color.Transparent
    Legend8.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend8.DockToChartArea = "Default"
    Legend8.Enabled = False
    Legend8.Name = "Default"
    Me.Chart_VAR.Legends.Add(Legend8)
    Me.Chart_VAR.Location = New System.Drawing.Point(1, 30)
    Me.Chart_VAR.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_VAR.Name = "Chart_VAR"
    Me.Chart_VAR.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series15.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series15.BorderStyle = Dundas.Charting.WinControl.ChartDashStyle.NotSet
    Series15.BorderWidth = 2
    Series15.ChartType = "StackedArea"
    Series15.Color = System.Drawing.Color.Transparent
    Series15.CustomAttributes = "LabelStyle=Top"
    Series15.Name = "Series1"
    Series15.ShadowOffset = 1
    Series15.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series15.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series16.BackHatchStyle = Dundas.Charting.WinControl.ChartHatchStyle.Percent50
    Series16.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series16.BorderWidth = 2
    Series16.ChartType = "StackedArea"
    Series16.CustomAttributes = "LabelStyle=Top"
    Series16.Name = "Series2"
    Series16.ShadowOffset = 1
    Series16.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series16.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series17.BorderWidth = 2
    Series17.ChartType = "Line"
    Series17.Name = "Series3"
    Series17.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series17.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_VAR.Series.Add(Series15)
    Me.Chart_VAR.Series.Add(Series16)
    Me.Chart_VAR.Series.Add(Series17)
    Me.Chart_VAR.Size = New System.Drawing.Size(798, 572)
    Me.Chart_VAR.TabIndex = 8
    Me.Chart_VAR.Text = "Chart2"
    '
    'Text_Chart_VARConfidence
    '
    Me.Text_Chart_VARConfidence.Location = New System.Drawing.Point(253, 5)
    Me.Text_Chart_VARConfidence.Name = "Text_Chart_VARConfidence"
    Me.Text_Chart_VARConfidence.RenaissanceTag = Nothing
    Me.Text_Chart_VARConfidence.Size = New System.Drawing.Size(36, 20)
    Me.Text_Chart_VARConfidence.TabIndex = 4
    Me.Text_Chart_VARConfidence.Text = "95%"
    Me.Text_Chart_VARConfidence.TextFormat = "#,##0%"
    Me.Text_Chart_VARConfidence.Value = 0.95
    '
    'Label59
    '
    Me.Label59.Location = New System.Drawing.Point(176, 8)
    Me.Label59.Name = "Label59"
    Me.Label59.Size = New System.Drawing.Size(73, 18)
    Me.Label59.TabIndex = 3
    Me.Label59.Text = "Confidence"
    '
    'Label58
    '
    Me.Label58.AutoSize = True
    Me.Label58.Location = New System.Drawing.Point(139, 8)
    Me.Label58.Margin = New System.Windows.Forms.Padding(0)
    Me.Label58.Name = "Label58"
    Me.Label58.Size = New System.Drawing.Size(29, 13)
    Me.Label58.TabIndex = 2
    Me.Label58.Text = "mths"
    '
    'Text_Chart_VARPeriod
    '
    Me.Text_Chart_VARPeriod.Location = New System.Drawing.Point(96, 5)
    Me.Text_Chart_VARPeriod.Name = "Text_Chart_VARPeriod"
    Me.Text_Chart_VARPeriod.RenaissanceTag = Nothing
    Me.Text_Chart_VARPeriod.Size = New System.Drawing.Size(39, 20)
    Me.Text_Chart_VARPeriod.TabIndex = 1
    Me.Text_Chart_VARPeriod.Text = "3"
    Me.Text_Chart_VARPeriod.TextFormat = "#,##0"
    Me.Text_Chart_VARPeriod.Value = 3
    '
    'Label57
    '
    Me.Label57.Location = New System.Drawing.Point(7, 8)
    Me.Label57.Name = "Label57"
    Me.Label57.Size = New System.Drawing.Size(85, 18)
    Me.Label57.TabIndex = 0
    Me.Label57.Text = "VAR Duration"
    '
    'Label56
    '
    Me.Label56.Location = New System.Drawing.Point(310, 8)
    Me.Label56.Name = "Label56"
    Me.Label56.Size = New System.Drawing.Size(72, 18)
    Me.Label56.TabIndex = 5
    Me.Label56.Text = "VAR Series"
    '
    'Combo_Charts_VARSeries
    '
    Me.Combo_Charts_VARSeries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Charts_VARSeries.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Charts_VARSeries.Location = New System.Drawing.Point(388, 5)
    Me.Combo_Charts_VARSeries.Name = "Combo_Charts_VARSeries"
    Me.Combo_Charts_VARSeries.Size = New System.Drawing.Size(402, 21)
    Me.Combo_Charts_VARSeries.TabIndex = 6
    '
    'Tab_Omega
    '
    Me.Tab_Omega.Controls.Add(Me.Check_OmegaRatio)
    Me.Tab_Omega.Controls.Add(Me.Label60)
    Me.Tab_Omega.Controls.Add(Me.Edit_OmegaReturn)
    Me.Tab_Omega.Controls.Add(Me.Label61)
    Me.Tab_Omega.Controls.Add(Me.Chart_Omega)
    Me.Tab_Omega.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Omega.Name = "Tab_Omega"
    Me.Tab_Omega.Size = New System.Drawing.Size(797, 600)
    Me.Tab_Omega.TabIndex = 12
    Me.Tab_Omega.Text = "Omega"
    Me.Tab_Omega.UseVisualStyleBackColor = True
    '
    'Check_OmegaRatio
    '
    Me.Check_OmegaRatio.AutoSize = True
    Me.Check_OmegaRatio.Location = New System.Drawing.Point(194, 6)
    Me.Check_OmegaRatio.Name = "Check_OmegaRatio"
    Me.Check_OmegaRatio.Size = New System.Drawing.Size(161, 17)
    Me.Check_OmegaRatio.TabIndex = 6
    Me.Check_OmegaRatio.Text = "Show Ratio as a percentage"
    Me.Check_OmegaRatio.UseVisualStyleBackColor = True
    '
    'Label60
    '
    Me.Label60.AutoSize = True
    Me.Label60.Location = New System.Drawing.Point(135, 7)
    Me.Label60.Margin = New System.Windows.Forms.Padding(0)
    Me.Label60.Name = "Label60"
    Me.Label60.Size = New System.Drawing.Size(29, 13)
    Me.Label60.TabIndex = 5
    Me.Label60.Text = "mths"
    '
    'Edit_OmegaReturn
    '
    Me.Edit_OmegaReturn.Location = New System.Drawing.Point(92, 4)
    Me.Edit_OmegaReturn.Name = "Edit_OmegaReturn"
    Me.Edit_OmegaReturn.RenaissanceTag = Nothing
    Me.Edit_OmegaReturn.Size = New System.Drawing.Size(39, 20)
    Me.Edit_OmegaReturn.TabIndex = 4
    Me.Edit_OmegaReturn.Text = "1"
    Me.Edit_OmegaReturn.TextFormat = "#,##0"
    Me.Edit_OmegaReturn.Value = 1
    '
    'Label61
    '
    Me.Label61.Location = New System.Drawing.Point(3, 7)
    Me.Label61.Name = "Label61"
    Me.Label61.Size = New System.Drawing.Size(85, 18)
    Me.Label61.TabIndex = 3
    Me.Label61.Text = "Rolling Return"
    '
    'Chart_Omega
    '
    Me.Chart_Omega.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Omega.BackColor = System.Drawing.Color.Azure
    Me.Chart_Omega.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Omega.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Omega.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Omega.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Omega.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea9.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea9.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea9.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea9.AxisX.LabelStyle.Format = "P0"
    ChartArea9.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea9.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea9.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea9.AxisY.LabelStyle.Format = "N1"
    ChartArea9.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea9.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea9.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea9.BackColor = System.Drawing.Color.Transparent
    ChartArea9.BorderColor = System.Drawing.Color.DimGray
    ChartArea9.Name = "Default"
    Me.Chart_Omega.ChartAreas.Add(ChartArea9)
    Legend9.BackColor = System.Drawing.Color.Transparent
    Legend9.BorderColor = System.Drawing.Color.Transparent
    Legend9.DockToChartArea = "Default"
    Legend9.Name = "Default"
    Me.Chart_Omega.Legends.Add(Legend9)
    Me.Chart_Omega.Location = New System.Drawing.Point(0, 30)
    Me.Chart_Omega.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Omega.Name = "Chart_Omega"
    Me.Chart_Omega.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series18.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series18.BorderWidth = 2
    Series18.ChartType = "Line"
    Series18.CustomAttributes = "LabelStyle=Top"
    Series18.Name = "Series1"
    Series18.ShadowOffset = 1
    Series18.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series18.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Omega.Series.Add(Series18)
    Me.Chart_Omega.Size = New System.Drawing.Size(797, 572)
    Me.Chart_Omega.TabIndex = 1
    Me.Chart_Omega.Text = "Chart2"
    '
    'Tab_DrawDown
    '
    Me.Tab_DrawDown.Controls.Add(Me.Chart_DrawDown)
    Me.Tab_DrawDown.Location = New System.Drawing.Point(4, 22)
    Me.Tab_DrawDown.Name = "Tab_DrawDown"
    Me.Tab_DrawDown.Size = New System.Drawing.Size(797, 600)
    Me.Tab_DrawDown.TabIndex = 3
    Me.Tab_DrawDown.Text = "DrawDown"
    Me.Tab_DrawDown.UseVisualStyleBackColor = True
    '
    'Chart_DrawDown
    '
    Me.Chart_DrawDown.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_DrawDown.BackColor = System.Drawing.Color.Azure
    Me.Chart_DrawDown.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_DrawDown.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_DrawDown.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_DrawDown.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_DrawDown.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea10.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea10.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea10.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea10.AxisX.LabelStyle.Format = "Y"
    ChartArea10.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea10.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea10.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea10.AxisY.LabelStyle.Format = "P0"
    ChartArea10.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea10.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea10.BackColor = System.Drawing.Color.Transparent
    ChartArea10.BorderColor = System.Drawing.Color.DimGray
    ChartArea10.Name = "Default"
    Me.Chart_DrawDown.ChartAreas.Add(ChartArea10)
    Legend10.BackColor = System.Drawing.Color.Transparent
    Legend10.BorderColor = System.Drawing.Color.Transparent
    Legend10.Docking = Dundas.Charting.WinControl.LegendDocking.Bottom
    Legend10.DockToChartArea = "Default"
    Legend10.Name = "Default"
    Me.Chart_DrawDown.Legends.Add(Legend10)
    Me.Chart_DrawDown.Location = New System.Drawing.Point(0, 0)
    Me.Chart_DrawDown.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_DrawDown.Name = "Chart_DrawDown"
    Me.Chart_DrawDown.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series19.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series19.BorderWidth = 2
    Series19.ChartType = "Line"
    Series19.CustomAttributes = "LabelStyle=Top"
    Series19.Name = "Series1"
    Series19.ShadowOffset = 1
    Series19.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series19.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series20.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series20.BorderWidth = 2
    Series20.ChartType = "Line"
    Series20.CustomAttributes = "LabelStyle=Top"
    Series20.Name = "Series2"
    Series20.ShadowOffset = 1
    Series20.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series20.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_DrawDown.Series.Add(Series19)
    Me.Chart_DrawDown.Series.Add(Series20)
    Me.Chart_DrawDown.Size = New System.Drawing.Size(797, 600)
    Me.Chart_DrawDown.TabIndex = 4
    Me.Chart_DrawDown.Text = "Chart2"
    '
    'Tab_Statistics
    '
    Me.Tab_Statistics.AutoScroll = True
    Me.Tab_Statistics.Controls.Add(Me.Grid_Stats_Drawdowns)
    Me.Tab_Statistics.Controls.Add(Me.Grid_SimpleStatistics)
    Me.Tab_Statistics.Controls.Add(Me.Label55)
    Me.Tab_Statistics.Controls.Add(Me.Text_Statistics_RiskFree)
    Me.Tab_Statistics.Controls.Add(Me.Label54)
    Me.Tab_Statistics.Controls.Add(Me.Label53)
    Me.Tab_Statistics.Controls.Add(Me.Combo_Statistics_HighlightSeries)
    Me.Tab_Statistics.Location = New System.Drawing.Point(4, 25)
    Me.Tab_Statistics.Name = "Tab_Statistics"
    Me.Tab_Statistics.Size = New System.Drawing.Size(813, 701)
    Me.Tab_Statistics.TabIndex = 6
    Me.Tab_Statistics.Text = "Statistics"
    Me.Tab_Statistics.UseVisualStyleBackColor = True
    '
    'Grid_Stats_Drawdowns
    '
    Me.Grid_Stats_Drawdowns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Stats_Drawdowns.ColumnInfo = resources.GetString("Grid_Stats_Drawdowns.ColumnInfo")
    Me.Grid_Stats_Drawdowns.Location = New System.Drawing.Point(12, 209)
    Me.Grid_Stats_Drawdowns.MinimumSize = New System.Drawing.Size(400, 200)
    Me.Grid_Stats_Drawdowns.Name = "Grid_Stats_Drawdowns"
    Me.Grid_Stats_Drawdowns.Rows.Count = 1
    Me.Grid_Stats_Drawdowns.Rows.DefaultSize = 17
    Me.Grid_Stats_Drawdowns.Size = New System.Drawing.Size(796, 486)
    Me.Grid_Stats_Drawdowns.TabIndex = 19
    '
    'Grid_SimpleStatistics
    '
    Me.Grid_SimpleStatistics.ColumnInfo = resources.GetString("Grid_SimpleStatistics.ColumnInfo")
    Me.Grid_SimpleStatistics.Location = New System.Drawing.Point(12, 79)
    Me.Grid_SimpleStatistics.Name = "Grid_SimpleStatistics"
    Me.Grid_SimpleStatistics.Rows.Count = 7
    Me.Grid_SimpleStatistics.Rows.DefaultSize = 17
    Me.Grid_SimpleStatistics.Size = New System.Drawing.Size(502, 124)
    Me.Grid_SimpleStatistics.TabIndex = 18
    '
    'Label55
    '
    Me.Label55.AutoSize = True
    Me.Label55.Location = New System.Drawing.Point(167, 33)
    Me.Label55.Name = "Label55"
    Me.Label55.Size = New System.Drawing.Size(93, 13)
    Me.Label55.TabIndex = 17
    Me.Label55.Text = "(For Sharpe Ratio)"
    '
    'Text_Statistics_RiskFree
    '
    Me.Text_Statistics_RiskFree.Location = New System.Drawing.Point(106, 30)
    Me.Text_Statistics_RiskFree.Name = "Text_Statistics_RiskFree"
    Me.Text_Statistics_RiskFree.RenaissanceTag = Nothing
    Me.Text_Statistics_RiskFree.Size = New System.Drawing.Size(55, 20)
    Me.Text_Statistics_RiskFree.TabIndex = 16
    Me.Text_Statistics_RiskFree.Text = "3.00%"
    Me.Text_Statistics_RiskFree.TextFormat = "#,##0.00##%"
    Me.Text_Statistics_RiskFree.Value = 0.03
    '
    'Label54
    '
    Me.Label54.AutoSize = True
    Me.Label54.Location = New System.Drawing.Point(9, 33)
    Me.Label54.Name = "Label54"
    Me.Label54.Size = New System.Drawing.Size(78, 13)
    Me.Label54.TabIndex = 15
    Me.Label54.Text = "Risk Free Rate"
    '
    'Label53
    '
    Me.Label53.Location = New System.Drawing.Point(9, 6)
    Me.Label53.Name = "Label53"
    Me.Label53.Size = New System.Drawing.Size(91, 18)
    Me.Label53.TabIndex = 14
    Me.Label53.Text = "Statistics Series"
    '
    'Combo_Statistics_HighlightSeries
    '
    Me.Combo_Statistics_HighlightSeries.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Statistics_HighlightSeries.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Statistics_HighlightSeries.Location = New System.Drawing.Point(106, 3)
    Me.Combo_Statistics_HighlightSeries.Name = "Combo_Statistics_HighlightSeries"
    Me.Combo_Statistics_HighlightSeries.Size = New System.Drawing.Size(702, 21)
    Me.Combo_Statistics_HighlightSeries.TabIndex = 13
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FundBrowser_StatusLabel})
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 763)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(1202, 22)
    Me.StatusStrip1.TabIndex = 13
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'FundBrowser_StatusLabel
    '
    Me.FundBrowser_StatusLabel.Name = "FundBrowser_StatusLabel"
    Me.FundBrowser_StatusLabel.Size = New System.Drawing.Size(10, 17)
    Me.FundBrowser_StatusLabel.Text = " "
    '
    'frmSimulationBrowser
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(1202, 785)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.Split_Form)
    Me.Controls.Add(Me.RootMenu)
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(458, 339)
    Me.Name = "frmSimulationBrowser"
    Me.Text = "CTA ScratchPad"
    Me.RootMenu.ResumeLayout(False)
    Me.RootMenu.PerformLayout()
    Me.Split_Form.Panel1.ResumeLayout(False)
    Me.Split_Form.Panel1.PerformLayout()
    Me.Split_Form.Panel2.ResumeLayout(False)
    Me.Split_Form.ResumeLayout(False)
    Me.TabControl_Parameters.ResumeLayout(False)
    Me.Tab_Param_MovingAverage.ResumeLayout(False)
    Me.Tab_Param_MovingAverage.PerformLayout()
    CType(Me.Numeric_VolatilityAverage_Lambda, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_LinearAverage_Lambda, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_ExponentialAverage_Lambda, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_MovingAverageDays, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_ExponentialPrice_Lambda, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Param_Costs.ResumeLayout(False)
    Me.Tab_Param_Costs.PerformLayout()
    CType(Me.Numeric_FundingSpread, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_TrandingCosts, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Param_TradingStyle.ResumeLayout(False)
    Me.Tab_Param_TradingStyle.PerformLayout()
    CType(Me.Numeric_Trading_MonthlyDrawdownResetCount, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel_Trading_BandWeighting.ResumeLayout(False)
    Me.Panel_Trading_BandWeighting.PerformLayout()
    CType(Me.Numeric_Trading_ShadowCrossingUnweightPoint, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_LocalHighExclusionPeriod, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Trading_ZScore_Period, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Trading_BandDeltaLimit, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Trading_ReInvestmentEvaluationPeriod, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Trading_WeightingDecayFactor, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Shadow_PercentBand, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Trading_AverageZScore, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Trading_IndexZScore, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Shadow_MaxDrawdown, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Shadow_BaselineGearing, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Shadow_RebalanceThreshold, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel7.ResumeLayout(False)
    Me.Panel7.PerformLayout()
    CType(Me.Numeric_Trading_VolWeightCap, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Trading_VolExponent, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Numeric_Trading_VolWeightDuration, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Group_FirstTrade.ResumeLayout(False)
    Me.Group_FirstTrade.PerformLayout()
    Me.Tab_InstrumentDetails.ResumeLayout(False)
    Me.Tab_Descriptions.ResumeLayout(False)
    Me.Tab_Descriptions.PerformLayout()
    CType(Me.Chart_FrontPage_RollingReturn, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Chart_FrontPage_Prices, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Contacts.ResumeLayout(False)
    CType(Me.Grid_PortfolioHistory, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Performance.ResumeLayout(False)
    Me.TabControl_PerformanceTables.ResumeLayout(False)
    Me.TabPage1.ResumeLayout(False)
    CType(Me.Grid_Performance_0, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Charts.ResumeLayout(False)
    Me.Tab_Charts.PerformLayout()
    Me.Panel2.ResumeLayout(False)
    Me.Panel2.PerformLayout()
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    CType(Me.Combo_Charts_CompareSeriesPertrac, System.ComponentModel.ISupportInitialize).EndInit()
    Me.TabControl_Charts.ResumeLayout(False)
    Me.Tab_Vami.ResumeLayout(False)
    CType(Me.Chart_Prices, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Monthly.ResumeLayout(False)
    CType(Me.Chart_Returns, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Return.ResumeLayout(False)
    CType(Me.Chart_RollingReturn, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_ReturnScatter.ResumeLayout(False)
    CType(Me.Chart_ReturnScatter, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_StdDev.ResumeLayout(False)
    CType(Me.Chart_StdDev, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_VAR.ResumeLayout(False)
    Me.Tab_VAR.PerformLayout()
    CType(Me.Chart_VAR, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Omega.ResumeLayout(False)
    Me.Tab_Omega.PerformLayout()
    CType(Me.Chart_Omega, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_DrawDown.ResumeLayout(False)
    CType(Me.Chart_DrawDown, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Statistics.ResumeLayout(False)
    Me.Tab_Statistics.PerformLayout()
    CType(Me.Grid_Stats_Drawdowns, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Grid_SimpleStatistics, System.ComponentModel.ISupportInitialize).EndInit()
    Me.StatusStrip1.ResumeLayout(False)
    Me.StatusStrip1.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

  Private _IDsToChart() As Integer

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  Private WithEvents _MainForm As CTAMain
  Private _ChartsToUpdate As Boolean = False
  '  Private _AverageSeriesToUpdate As Boolean = False
  '  Private _TradeHistoryToUpdate As Boolean = False
  Private _ZoomReset As Boolean

  Private _ChartsToUpdateTime As Date = Now()

  Private FormSimulationParameters As New CTASimulationFunctions.SimulationParameterClass
  Private FormTradeSimulator As New CTASimulationFunctions.TradeSimulation

  Private LastLoadedSimulationName As String
  Private LastLoadedSimulationID As Integer

  '  Private CurrentInstrumentID As Integer = (-1)
  '  Private CurrentDataStartDate As Date = Renaissance_BaseDate
  '  Private InstrumentDate(-1) As Date
  '  Private InstrumentReturns(-1) As Double
  '  Private InstrumentNAV(-1) As Double
  '  Private InstrumentStDev(-1) As Double
  '
  '  Private CurrentInterestRateID As Integer = (-1)
  '  Private InterestRateDate(-1) As Date
  '  Private InterestRateReturns(-1) As Double
  '  Private InterestRateNAV(-1) As Double

  '  Private AverageSeriesDate(-1) As Date
  '  Private AverageSeriesReturns(-1) As Double
  '  Private AverageSeriesNAV(-1) As Double

  '  Private PortfolioHistory(-1) As DailyPortfolioClass
  '  Private BandGearing_StDevSeries(-1) As Double
  '  Private BandGearing_AverageSeries(-1) As Double

  ' Form ToolTip
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  Private GenericUpdateObject As RenaissanceGlobals.RenaissanceTimerUpdateClass
  Private Date_ValueDate_ValueDateChanged As Boolean

  ' Form specific Permissioning variables
  Private THIS_FORM_PermissionArea As String
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  Private THIS_FORM_FormID As CTAFormID

  ' Form Status Flags

  Private FormIsValid As Boolean
  Private _FormOpenFailed As Boolean
  Private InPaint As Boolean
  Private _InUse As Boolean
  Private InFormUpdate_Tick As Boolean

  ' User Permission Flags

  Private HasReadPermission As Boolean
  Private HasUpdatePermission As Boolean
  Private HasInsertPermission As Boolean
  Private HasDeletePermission As Boolean

  ' Data Structures
  Private PertracInstruments As DataView = Nothing ' Manages Select List for Pertrac Instruments.

  ' "Charts" Management Arrays

  Private PriceChartArrayList As New ArrayList
  Private ReturnsChartArrayList As New ArrayList
  Private RollingReturnChartArrayList As New ArrayList
  Private ReturnScatterChartArrayList As New ArrayList
  Private StdDevChartArrayList As New ArrayList
  Private VARChartArrayList As New ArrayList
  Private OmegaChartArrayList As New ArrayList
  Private DrawdownChartArrayList As New ArrayList

  ' "Comparisons" Data Structures

  Private ComparisonStatsGridList As New ArrayList
  Private APRBarArrayList As New ArrayList
  Private VolatilityBarArrayList As New ArrayList

  '

  Private CustomGroupID_SelectedList As Integer
  Private CustomGroupID_WholeList As Integer

#End Region

#Region " Form 'Properties' "

  Public ReadOnly Property MainForm() As CTAMain Implements StandardCTAForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  Public Property IsOverCancelButton() As Boolean Implements StandardCTAForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return False
    End Get
    Set(ByVal Value As Boolean)
    End Set
  End Property

  Public ReadOnly Property IsInPaint() As Boolean Implements StandardCTAForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  Public ReadOnly Property InUse() As Boolean Implements StandardCTAForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardCTAForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property


  Private Property ChartsToUpdate() As Boolean
    Get
      Return _ChartsToUpdate
    End Get
    Set(ByVal value As Boolean)
      If (value) Then
        _ChartsToUpdateTime = Now()

        If (GenericUpdateObject IsNot Nothing) Then
          GenericUpdateObject.FormToUpdate = True
        End If
      End If

      _ChartsToUpdate = value
    End Set
  End Property

  Private ReadOnly Property ChartsToUpdateTime() As Date
    Get
      Return _ChartsToUpdateTime
    End Get
  End Property

#End Region

  Public Sub New(ByVal pMainForm As CTAMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    Try
      Me.Cursor = Cursors.WaitCursor

      _MainForm = pMainForm
      AddHandler _MainForm.CTAAutoUpdate, AddressOf Me.AutoUpdate
      AddHandler _MainForm.CTA_FontSizeChanged, AddressOf Me.FontSizeChanged

      _FormOpenFailed = False
      _InUse = True
      CustomGroupID_SelectedList = pMainForm.UniqueCTANumber
      CustomGroupID_WholeList = pMainForm.UniqueCTANumber

      ' ******************************************************
      ' Form Specific Settings :
      ' ******************************************************

      ' Form Permissioning :-

      THIS_FORM_PermissionArea = Me.Name
      THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

      ' 'This' form ID

      THIS_FORM_FormID = CTAFormID.frmSimulationBrowser

      ' Format Event Handlers for form controls

      AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

      AddHandler Combo_InstrumentSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_InstrumentSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_InstrumentSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_InstrumentSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      AddHandler Combo_InterestRateSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_InterestRateSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_InterestRateSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_InterestRateSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      'AddHandler Combo_Charts_CompareSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Charts_CompareSeriesPertrac.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      'AddHandler Combo_Charts_CompareSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      'AddHandler Combo_Charts_CompareSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType
      AddHandler Combo_Charts_CompareSeriesGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
      AddHandler Combo_Charts_CompareSeriesGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
      AddHandler Combo_Charts_CompareSeriesGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
      AddHandler Combo_Charts_CompareSeriesGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      AddHandler Grid_Performance_0.KeyDown, AddressOf Grid_Performance_KeyDown
      Grid_Performance_0.ContextMenuStrip = New ContextMenuStrip
      Grid_Performance_0.ContextMenuStrip.Tag = Grid_Performance_0
      AddHandler Grid_Performance_0.ContextMenuStrip.Opening, AddressOf PerformanceGrid_cms_Opening
      Grid_Performance_0.ContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))


      AddHandler Me.Menu_ManageFolders.Click, AddressOf MainForm.Generic_FormMenu_Click

      ' Form Control Changed events

      ' Set up the ToolTip
      MainForm.SetFormToolTip(Me, FormTooltip)

      ' ******************************************************
      ' End Form Specific.
      ' ******************************************************

      Grid_Performance_0.Styles.Add("Positive").ForeColor = Color.Blue
      Grid_Performance_0.Styles.Add("Negative").ForeColor = Color.Red
      Grid_Performance_0.Styles.Add("TopRow", Grid_Performance_0.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter
      Grid_Performance_0.Styles.Add("FirstCell", Grid_Performance_0.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter
      Grid_Performance_0.Rows(0).Style = Grid_Performance_0.Styles("TopRow")
      Grid_Performance_0.SetCellStyle(0, 0, "FirstCell")

      Dim thisStyle As C1.Win.C1FlexGrid.CellStyle

      thisStyle = Grid_SimpleStatistics.Styles.Add("PositivePercent")
      thisStyle.ForeColor = Color.Blue
      thisStyle.Format = "#,##0.00%"

      thisStyle = Grid_SimpleStatistics.Styles.Add("NegativePercent")
      thisStyle.ForeColor = Color.Red
      thisStyle.Format = "#,##0.00%"

      thisStyle = Grid_SimpleStatistics.Styles.Add("SharpeRatio")
      thisStyle.ForeColor = Color.Blue
      thisStyle.Format = "#,##0.00"

      Grid_SimpleStatistics.Item(1, 0) = "Return"
      Grid_SimpleStatistics.Item(2, 0) = "Annualised Return"
      Grid_SimpleStatistics.Item(3, 0) = "Volatility"
      Grid_SimpleStatistics.Item(4, 0) = "Sample Volatility"
      Grid_SimpleStatistics.Item(5, 0) = "Sharpe Ratio"
      Grid_SimpleStatistics.Item(6, 0) = "% Positive"

      ' Initialise Chart Arraylists

      PriceChartArrayList.Add(Me.Chart_Prices)
      PriceChartArrayList.Add(Me.Chart_FrontPage_Prices)
      ReturnsChartArrayList.Add(Me.Chart_Returns)
      RollingReturnChartArrayList.Add(Me.Chart_RollingReturn)
      RollingReturnChartArrayList.Add(Me.Chart_FrontPage_RollingReturn)
      ReturnScatterChartArrayList.Add(Me.Chart_ReturnScatter)
      StdDevChartArrayList.Add(Me.Chart_StdDev)
      VARChartArrayList.Add(Me.Chart_VAR)
      OmegaChartArrayList.Add(Me.Chart_Omega)
      DrawdownChartArrayList.Add(Me.Chart_DrawDown)

      MainForm.AddCopyMenuToChart(Me.Chart_Prices)
      MainForm.AddCopyMenuToChart(Me.Chart_FrontPage_Prices)
      MainForm.AddCopyMenuToChart(Me.Chart_Returns)
      MainForm.AddCopyMenuToChart(Me.Chart_RollingReturn)
      MainForm.AddCopyMenuToChart(Me.Chart_FrontPage_RollingReturn)
      MainForm.AddCopyMenuToChart(Me.Chart_ReturnScatter)
      MainForm.AddCopyMenuToChart(Me.Chart_StdDev)
      MainForm.AddCopyMenuToChart(Me.Chart_VAR)
      MainForm.AddCopyMenuToChart(Me.Chart_Omega)
      MainForm.AddCopyMenuToChart(Me.Chart_DrawDown)

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  Public Sub ResetForm() Implements StandardCTAForm.ResetForm
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      FormSimulationParameters.InstrumentID = 0
      FormSimulationParameters.InterestRateID = 0

      Call Form_Load(Me, New System.EventArgs)

    Catch ex As Exception
    End Try

  End Sub

  Public Sub CloseForm() Implements StandardCTAForm.CloseForm
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      ALWAYS_CLOSE_THIS_FORM = True
      Me.Close()

    Catch ex As Exception
    End Try
  End Sub

  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Me.Cursor = Cursors.WaitCursor

      FontSizeChanged(Me, New CTA.CTAMain.FontChangedEventArgs(MainForm.CTAFontSize))

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
      _FormOpenFailed = False
      _InUse = True
      _ZoomReset = True

      ' Initialise Data structures. Connection, Adaptor and Dataset.

      If Not (MainForm Is Nothing) Then
        FormIsValid = True
      Else
        MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      ' Initialse form

      InPaint = True
      IsOverCancelButton = False
      Try
        ' Initialise Timer

        Date_ValueDate_ValueDateChanged = False
        GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf FormUpdate_Tick)

        ' Check User permissions
        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

          FormIsValid = False
          _FormOpenFailed = True
          Exit Sub
        End If

        ' Display initial record.

        Date_DataStartDate.Value = Renaissance_BaseDate
        Check_AutoStart.Checked = False
        Radio_UnAdjustedAverage.Checked = True
        Radio_Trading_FirstTradeCrossing.Checked = True
        Radio_Trading_VolWeightByHighStDev.Checked = True
        Combo_DataPeriod.SelectedValue = CInt(DealingPeriod.Daily)

        Check_Trading_BandWeightings.Checked = False
        Panel_Trading_BandWeighting.Enabled = False
        Radio_Trading_ByIndexZScore.Checked = True

        Check_VAMI_ShowBands.Checked = True
        Check_VAMI_ShowPortfolio.Checked = True
        Check_Trading_AllowLong.Checked = True
        Check_Trading_AllowShort.Checked = True

        Me.TabControl_Charts.SelectedIndex = 0
        Me.Tab_InstrumentDetails.SelectedIndex = 0

        Me.Date_Charts_DateFrom.Value = Renaissance_BaseDate
        Me.Date_Charts_DateTo.Value = Renaissance_EndDate_Data
        Text_Chart_Lamda.Value = 1.0#
        Text_Chart_RollingPeriod.Value = 12.0#
        Text_ScalingFactor.Value = 1.0#
        Radio_SingleScalingFactor.Checked = True
        Radio_Charts_ComparePertrac.Checked = True
        Check_OmegaRatio.Checked = True
        Numeric_Trading_CrossingUnweightPoint.Value = 0.05#

      Catch ex As Exception
      Finally
        InPaint = False
      End Try

      '

      LastLoadedSimulationID = (-1)
      Menu_SaveSimulation.Enabled = False
      Menu_SaveSimulation.Text = "Save Simulation"

      ' Initialise Menu

      Build_LoadSimulation_Menu()

      ' Initialise Combos

      Call SetInvestmentSeriesCombo()

      Call SetComparePertracCombo()
      Call SetCompareGroupCombo()
      Call Set_DataPeriodCombo()

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

    ' Re-Set Start mode Flag


  End Sub

  Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim HideForm As Boolean

    Try

      ' Hide or Close this form ?
      ' All depends on how many of this form type are Open or in Cache...

      _InUse = False
      MainForm.RemoveFormUpdate(Me) ' Remove Form Update reference, will be re-established in Load() if this form is cached.

      'If (PaintTimer IsNot Nothing) Then
      '	Try
      '		PaintTimer.Stop()
      '	Catch ex As Exception
      '	Finally
      '		PaintTimer = Nothing
      '	End Try
      'End If

      FormSimulationParameters.InstrumentID = 0
      FormSimulationParameters.InterestRateID = 0

      If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
        HideForm = False
      Else

        HideForm = True
        If MainForm.CTAForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
          HideForm = False
        End If
      End If

    Catch ex As Exception
    End Try


    If HideForm = True Then
      Try

        MainForm.HideInFormsCollection(Me)
        Me.Hide() ' NPP Fix

      Catch ex As Exception
      Finally

        e.Cancel = True

      End Try
    Else
      Try
        Dim TabCounter As Integer
        Dim thisTab As TabPage

        For TabCounter = (TabControl_PerformanceTables.TabCount - 1) To 0 Step -1
          If (TabCounter > 0) Then
            thisTab = TabControl_PerformanceTables.TabPages(TabCounter)
            TabControl_PerformanceTables.TabPages.RemoveAt(TabCounter)
            Try
              RemoveHandler CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).KeyDown, AddressOf Grid_Performance_KeyDown
              RemoveHandler CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip.Opening, AddressOf PerformanceGrid_cms_Opening
            Catch ex As Exception
            End Try
            Try
              CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip.Tag = Nothing
              CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip = Nothing
            Catch ex As Exception
            End Try

            Try
              thisTab.Controls.Clear()
              thisTab.Dispose()
            Catch ex As Exception
            End Try
          End If
        Next
      Catch ex As Exception
      End Try

      Try
        MainForm.RemoveFromFormsCollection(Me)
        Call MainForm.PertracData.GetActivePertracInstruments(True, -999)

        RemoveHandler _MainForm.CTAAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

        RemoveHandler Combo_InstrumentSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InstrumentSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InstrumentSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InstrumentSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_InterestRateSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_InterestRateSeries.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_InterestRateSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_InterestRateSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        'RemoveHandler Combo_Charts_CompareSeries.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Charts_CompareSeriesPertrac.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        'RemoveHandler Combo_Charts_CompareSeries.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        'RemoveHandler Combo_Charts_CompareSeries.KeyUp, AddressOf MainForm.ComboSelectAsYouType
        RemoveHandler Combo_Charts_CompareSeriesGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_Charts_CompareSeriesGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_Charts_CompareSeriesGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_Charts_CompareSeriesGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

      Catch ex As Exception
      End Try
    End If

  End Sub

  Private Function FormUpdate_Tick() As Boolean
    ' *******************************************************************************
    '
    ' Callback process for the Generic Form Update Process.
    '
    ' Function MUST return True / False on Update.
    ' True will clear the update request. False will not.
    '
    ' Motivation :
    ' Sometimes, when charting parameters may be quickly updated, one wants to defer
    ' the chart update until the update has finished.
    '
    ' *******************************************************************************
    Dim RVal As Boolean = False
    Dim SomeFalse As Boolean = False

    Try

      If (Me.IsDisposed) Then
        Return True
      End If

      If (InFormUpdate_Tick) OrElse (Not _InUse) Then
        Return False
        Exit Function
      End If

    Catch ex As Exception
    End Try

    Try
      InFormUpdate_Tick = True
      Me.UseWaitCursor = True

      ' Update Form :

      If (ChartsToUpdate) Then

        Try
          If (Now().Subtract(ChartsToUpdateTime).TotalSeconds >= PAINT_TIMER_DELAY) Then
            Try

              Call RunTrandingSimulation()

              Call UpdateTradingResultsGrid(FormSimulationParameters, Grid_PortfolioHistory)

              Call Me.PaintAllCharts(_ZoomReset)

              _ZoomReset = False

            Catch ex As Exception
            Finally
              ChartsToUpdate = False
            End Try

          Else
            ' Not Yet ready to update
            ' Return False.

            SomeFalse = True
          End If
        Catch ex As Exception
        End Try
      End If

      ' Update After ValueDate.Value Changed

      If (Date_ValueDate_ValueDateChanged) Then
        Date_ValueDate_ValueDateChanged = False

      End If

      RVal = Not SomeFalse

    Catch ex As Exception
      RVal = False
    Finally
      InFormUpdate_Tick = False
      Me.UseWaitCursor = False
    End Try

    Return RVal

  End Function

#End Region

  Private Sub FontSizeChanged(ByVal sender As System.Object, ByVal e As CTAMain.FontChangedEventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      MainForm.SetControlFont(Me, e.NewFontSize)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    ' *****************************************************************************************
    ' Routine to handle changes / updates to tables by this and other windows.
    ' If this, or any other, form posts a change to a table, then it will invoke an update event 
    ' detailing what tables have been altered.
    ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
    ' the 'VeniceAutoUpdate' event of the main Venice form.
    ' Each form may them react as appropriate to changes in any table that might impact it.
    '
    ' *****************************************************************************************

    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    Try

      InPaint = True
      KnowledgeDateChanged = False
      SetButtonStatus_Flag = False

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
        KnowledgeDateChanged = True
        RefreshForm = True
      End If

      ' ****************************************************************
      ' Check for changes relevant to this form
      ' ****************************************************************

      ' Changes to the KnowledgeDate :-
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        SetButtonStatus_Flag = True
      End If

      ' Changes to the tblUserPermissions table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          Me.Close()
          Exit Sub
        End If

        SetButtonStatus_Flag = True

      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTA_Simulation) = True) Or KnowledgeDateChanged Then

        Build_LoadSimulation_Menu()

      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Performance)) Then

        ' Re-Set cache.
        ' Me.MainForm.StatFunctions.ClearCache() -- Performed in MainForm.
        ' Me.MainForm.PertracData.ClearDataCache() -- Performed in MainForm.

      End If

      ' Build_LoadSimulation_Menu

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTA_Folders)) OrElse (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTA_Simulation)) Then

        Build_LoadSimulation_Menu()

      End If

    Catch ex As Exception
    Finally
      InPaint = OrgInPaint
    End Try

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RefreshForm) OrElse (SetButtonStatus_Flag) Then
      Call SetButtonStatus()
    End If

  End Sub

  Public Function GetSelectedIDs(ByVal List_SelectItems As ListBox) As Integer()
    ' ****************************************************************
    '
    '
    ' ****************************************************************

    Dim RVal(-1) As Integer

    Try

      If (List_SelectItems IsNot Nothing) AndAlso (List_SelectItems.SelectedItems.Count > 0) Then
        ReDim RVal(List_SelectItems.SelectedItems.Count - 1)

        Dim ItemCounter As Integer

        Try
          For ItemCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
            RVal(ItemCounter) = List_SelectItems.SelectedItems(ItemCounter)(List_SelectItems.ValueMember)
          Next
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception

    End Try

    Return RVal
  End Function

  Public Sub SetSelectedIDs(ByVal List_SelectItems As ListBox, ByVal SelectItems() As Integer)
    ' ****************************************************************
    '
    '
    ' ****************************************************************

    Dim ListCounter As Integer
    Dim ItemCounter As Integer

    Try
      InPaint = True

      If (List_SelectItems.SelectedItems.Count > 0) Then
        List_SelectItems.SelectedItems.Clear()
      End If

      If (SelectItems IsNot Nothing) AndAlso (SelectItems.Length > 0) Then
        For ItemCounter = 0 To (SelectItems.Length - 1)
          For ListCounter = 0 To (List_SelectItems.Items.Count - 1)
            If CInt(List_SelectItems.Items(ListCounter)(List_SelectItems.ValueMember)) = SelectItems(ItemCounter) Then
              If List_SelectItems.SelectedItems.Contains(List_SelectItems.Items(ListCounter)) = False Then
                List_SelectItems.SelectedItems.Add(List_SelectItems.Items(ListCounter))
              End If

              Exit For
            End If
          Next
        Next

      End If
    Catch ex As Exception
    Finally
      InPaint = False
    End Try
  End Sub


#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "


  ' Check User permissions
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

  Private Sub SetComparePertracCombo()

    Combo_Charts_CompareSeriesPertrac.MasternameCollection = MainForm.MasternameDictionary
    Combo_Charts_CompareSeriesPertrac.SelectNoMatch = False

  End Sub

  Private Sub SetCompareGroupCombo()

    Dim TempTable As New DataTable
    Dim TempTableRow As DataRow
    Dim GroupsTable As DSGroupList.tblGroupListDataTable
    Dim SelectedGroups() As DSGroupList.tblGroupListRow
    Dim GroupRow As DSGroupList.tblGroupListRow

    Try

      TempTable.Columns.Add(New DataColumn("DM", GetType(String)))
      TempTable.Columns.Add(New DataColumn("VM", GetType(Integer)))

      GroupsTable = CType(MainForm.Load_Table(RenaissanceStandardDatasets.tblGroupList), DSGroupList).tblGroupList
      SelectedGroups = GroupsTable.Select("True", "GroupListName")

      ' Dynamic Group

      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of Currently selected Items (Average Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_SelectedList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Mean))
      TempTable.Rows.Add(TempTableRow)
      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of Currently selected Items (Median Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_SelectedList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Median))
      TempTable.Rows.Add(TempTableRow)
      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of All List Items (Average Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_WholeList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Mean))
      TempTable.Rows.Add(TempTableRow)
      TempTableRow = TempTable.NewRow
      TempTableRow("DM") = ".Group of ALL List Items (Median Return)"
      TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CustomGroupID_WholeList, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.DrillDown_Group_Median))
      TempTable.Rows.Add(TempTableRow)

      ' Normal Groups.

      For Each GroupRow In SelectedGroups

        TempTableRow = TempTable.NewRow
        TempTableRow("DM") = GroupRow("GroupListName").ToString & " (Average Return)"
        TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(GroupRow("GroupListID")), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Mean))
        TempTable.Rows.Add(TempTableRow)
        TempTableRow = TempTable.NewRow
        TempTableRow("DM") = GroupRow("GroupListName").ToString & " (Median Return)"
        TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(GroupRow("GroupListID")), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Median))
        TempTable.Rows.Add(TempTableRow)
        TempTableRow = TempTable.NewRow
        TempTableRow("DM") = GroupRow("GroupListName").ToString & " (Weighted Return)"
        TempTableRow("VM") = CInt(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(CUInt(GroupRow("GroupListID")), RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Weighted))
        TempTable.Rows.Add(TempTableRow)

      Next

    Catch ex As Exception
    End Try

    Call MainForm.SetTblGenericCombo( _
       Me.Combo_Charts_CompareSeriesGroup, _
       TempTable.Select, _
       "DM", _
       "VM", _
       "True", False, True, True, 0, "")     ' 

    ' 


  End Sub


  Private Sub Set_DataPeriodCombo()

    Call MainForm.SetTblGenericCombo( _
      Me.Combo_DataPeriod, _
      GetType(DealingPeriod), _
      False)

  End Sub

  Private Sub SetInvestmentSeriesCombo()
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************
    Dim thisCommand As New SqlCommand
    Dim tmpTable As New DataTable

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        thisCommand.CommandText = "SELECT ID, Mastername FROM dbo.fn_tblMastername() WHERE [DataPeriod] = " & CInt(DealingPeriod.Daily).ToString
        thisCommand.CommandType = CommandType.Text
        thisCommand.CommandTimeout = 600
        thisCommand.Connection = MainForm.GetCTAConnection

        tmpTable.Load(thisCommand.ExecuteReader)

        Call MainForm.SetTblGenericCombo(Combo_InstrumentSeries, tmpTable.Select(), "Mastername", "ID", "", False, True, True, 0, "")
        Call MainForm.SetTblGenericCombo(Combo_InterestRateSeries, tmpTable.Select(), "Mastername", "ID", "", False, True, True, 0, "")

      End If
    Catch ex As Exception
    Finally
      Try
        If (thisCommand.Connection IsNot Nothing) Then
          thisCommand.Connection.Close()
        End If
      Catch ex As Exception
      Finally
        thisCommand.Connection = Nothing
      End Try
    End Try


  End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

  End Sub

  Private Sub SaveSimulation(ByVal pSimID As Integer, ByVal pSimName As String, ByVal pConfirm As Boolean, ByRef pSimDetails As SimulationParameterClass, ByVal FolderID As Integer, ByVal pSaveArrays As Boolean)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      Dim SimDS As DSCTA_Simulation
      Dim SimTbl As DSCTA_Simulation.tblCTA_SimulationDataTable
      Dim SimRows() As DSCTA_Simulation.tblCTA_SimulationRow
      Dim SimRow As DSCTA_Simulation.tblCTA_SimulationRow
      'Dim ms As New System.IO.MemoryStream
      'Dim Formatter As New BinaryFormatter
      'Dim DefinitionString As String

      SimDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblCTA_Simulation)

      If (SimDS IsNot Nothing) Then
        SimTbl = SimDS.tblCTA_Simulation

        If pSimID > 0 Then

          SimRows = SimDS.tblCTA_Simulation.Select("SimulationID=" & pSimID.ToString)

          If (SimRows IsNot Nothing) AndAlso (SimRows.Length > 0) Then
            If (pConfirm) AndAlso (MessageBox.Show("Overwite existing Simulation ?", "Save Simulation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No) Then
              Exit Sub
            End If

            SimRow = SimRows(0)
          Else
            SimRow = SimTbl.NewtblCTA_SimulationRow
            SimRow.SimulationID = 0
          End If

        Else

          SimRows = SimDS.tblCTA_Simulation.Select("SimulationName='" & pSimName & "'")

          If (SimRows IsNot Nothing) AndAlso (SimRows.Length > 0) Then
            If (pConfirm) AndAlso (MessageBox.Show("Overwite existing Simulation named '" & pSimName & "'?", "Save Simulation", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No) Then
              Exit Sub
            End If

            SimRow = SimRows(0)
          Else
            SimRow = SimTbl.NewtblCTA_SimulationRow
            SimRow.SimulationID = 0
          End If

        End If

        ' Set Record details

        If (pSimID <= 0) OrElse (pSimName.Length > 0) OrElse (SimRow.RowState And DataRowState.Detached) Then
          SimRow.SimulationName = pSimName
        End If

        LastLoadedSimulationName = SimRow.SimulationName

        If (FolderID >= 0) OrElse (SimRow.RowState And DataRowState.Detached) Then
          SimRow.SimulationFolder = Math.Max(0, FolderID)
        End If

        SimRow.InstrumentID = pSimDetails.InstrumentID
        SimRow.InterestRateID = pSimDetails.InterestRateID
        SimRow.SimulationDataPeriod = pSimDetails.StatsDatePeriod
        SimRow.SimulationDefinition = CTASimulationFunctions.SimulationParameterClass.SerialiseParameterClass(pSimDetails) '  DefinitionString

        If SimRow.RowState And DataRowState.Detached Then
          SimTbl.Rows.Add(SimRow)
        End If

        MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCTA_Simulation, New DataRow() {SimRow})

        LastLoadedSimulationID = SimRow.SimulationID
        Menu_SaveSimulation.Enabled = True
        Menu_SaveSimulation.Text = "Save Simulation (" & SimRow.SimulationName & ", " & SimRow.SimulationID.ToString & ")"

        Dim UpdateString As String = ""

        Try

          If (SimRow IsNot Nothing) AndAlso (Not SimRow.IsSimulationIDNull) AndAlso (SimRow.SimulationID > 0) Then
            UpdateString = SimRow.SimulationID.ToString
          End If

        Catch ex As Exception

          UpdateString = ""

        Finally

          Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblCTA_Simulation.ChangeID, UpdateString))

        End Try

      End If

    Catch ex As Exception

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Saving Simulation details", ex.Message, ex.StackTrace, True)

    End Try

  End Sub

  Private Function LoadSimulation(ByVal pSimID As Integer) As SimulationParameterClass
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim RVal As SimulationParameterClass = Nothing
    ' Dim ms As System.IO.MemoryStream = Nothing

    LastLoadedSimulationID = (-1)
    Menu_SaveSimulation.Enabled = False
    Menu_SaveSimulation.Text = "Save Simulation"

    Try

      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Return RVal
      End If

      Dim SimRow As DSCTA_Simulation.tblCTA_SimulationRow
      Dim Formatter As New BinaryFormatter

      Dim thisCommand As New SqlCommand
      Dim tmpTable As New DSCTA_Simulation.tblCTA_SimulationDataTable

      Try
        If (Me.Created) AndAlso (Not Me.IsDisposed) Then

          thisCommand.CommandText = "adp_tblCTA_Simulation_SelectCommand"
          thisCommand.CommandType = CommandType.StoredProcedure
          thisCommand.CommandTimeout = 600
          thisCommand.Parameters.Add(New SqlParameter("@SimulationID", SqlDbType.Int)).Value = pSimID
          thisCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate
          thisCommand.Connection = MainForm.GetCTAConnection

          tmpTable.Load(thisCommand.ExecuteReader)

        End If
      Catch ex As Exception
      Finally
        Try
          If (thisCommand.Connection IsNot Nothing) Then
            thisCommand.Connection.Close()
          End If
        Catch ex As Exception
        Finally
          thisCommand.Connection = Nothing
        End Try
      End Try

      If (tmpTable.Rows.Count > 0) AndAlso (CType(tmpTable.Rows(0), DSCTA_Simulation.tblCTA_SimulationRow).SimulationID = pSimID) Then
        SimRow = CType(tmpTable.Rows(0), DSCTA_Simulation.tblCTA_SimulationRow)
      Else
        SimRow = Nothing
      End If

      If (SimRow IsNot Nothing) AndAlso (SimRow.SimulationDefinition.Length > 0) Then

        'ms = New System.IO.MemoryStream(Convert.FromBase64String(SimRow.SimulationDefinition))
        'ms.Position = 0
        'RVal = CType(Formatter.Deserialize(ms), CTASimulationFunctions.SimulationParameterClass)

        RVal = CTASimulationFunctions.SimulationParameterClass.DeSerialiseParameterClass(SimRow.SimulationDefinition)
        LastLoadedSimulationName = SimRow.SimulationName
        LastLoadedSimulationID = pSimID
        Menu_SaveSimulation.Enabled = True
        Menu_SaveSimulation.Text = "Save Simulation (" & SimRow.SimulationName & ", " & SimRow.SimulationID.ToString & ")"

      End If

    Catch ex As Exception

      RVal = Nothing

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Loading Simulation details", ex.Message, ex.StackTrace, True)

      'Finally

      '  If (ms IsNot Nothing) Then

      '    Try

      '      ms.Close()

      '    Catch ex As Exception
      '    End Try

      '  End If
    End Try

    Return RVal

  End Function

#End Region

#Region " Menu Code"

  Private Sub Menu_ClearSimulation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_ClearSimulation.Click
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      SetFormFromSimulationParameters(New SimulationParameterClass)

      LastLoadedSimulationID = (-1)
      Menu_SaveSimulation.Enabled = False
      Menu_SaveSimulation.Text = "Save Simulation"

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Menu_SaveSimulation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_SaveSimulation.Click
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      If Me.LastLoadedSimulationID > 0 Then

        SetSimulationParametersFromForm(FormSimulationParameters)

        SaveSimulation(LastLoadedSimulationID, "", False, FormSimulationParameters, -1, False)

      Else
        Menu_SaveSimulationAs_Click(sender, e)
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Menu_SaveSimulationAs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_SaveSimulationAs.Click
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      Dim SimName As String
      Dim FolderID As Integer = 0

      SimName = InputBox("Please enter name for this Simulation", "Save Simulation", "")

      If SimName.Length > 0 Then

        If (TypeOf (sender) Is ToolStripMenuItem) AndAlso (CType(sender, ToolStripMenuItem).Tag IsNot Nothing) Then
          If (IsNumeric(CType(sender, ToolStripMenuItem).Tag)) Then
            FolderID = CInt(CType(sender, ToolStripMenuItem).Tag)
          End If
        End If

        SetSimulationParametersFromForm(FormSimulationParameters)

        SaveSimulation(0, SimName, True, FormSimulationParameters, FolderID, False)

      End If

    Catch ex As Exception

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Saving Simulation details", ex.Message, ex.StackTrace, True)

    End Try


  End Sub

  Private Sub Menu_LoadSimulation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      Dim SimID As Integer
      Dim TempSim As SimulationParameterClass

      SimID = CType(sender, ToolStripMenuItem).Tag

      TempSim = LoadSimulation(SimID)

      SetFormFromSimulationParameters(TempSim)

      FormSimulationParameters = TempSim

      FormSimulationParameters.SeriesToUpdate = True
      FormSimulationParameters.InterestRateToUpdate = True

      ChartsToUpdate = True

    Catch ex As Exception

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Loading Simulation details", ex.Message, ex.StackTrace, True)

    End Try


  End Sub

  Private Sub Menu_DeleteSimulation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      Dim SimID As Integer
      Dim SimRow As DSCTA_Simulation.tblCTA_SimulationRow

      SimID = CType(sender, ToolStripMenuItem).Tag

      SimRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCTA_Simulation, SimID), DSCTA_Simulation.tblCTA_SimulationRow)

      If (SimRow IsNot Nothing) AndAlso (SimRow.SimulationDefinition.Length > 0) Then
        Dim UpdateString As String = ""

        Try
          If (SimRow IsNot Nothing) AndAlso (Not SimRow.IsSimulationIDNull) AndAlso (SimRow.SimulationID > 0) Then
            UpdateString = SimRow.SimulationID.ToString
          End If

          SimRow.Delete()

          MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCTA_Simulation, New DataRow() {SimRow})

        Catch ex As Exception

          MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Deleting Simulation details", ex.Message, ex.StackTrace, True)
          UpdateString = ""

        Finally

          Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblCTA_Simulation.ChangeID, UpdateString))

        End Try

      End If

    Catch ex As Exception

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error Deleting Simulation details", ex.Message, ex.StackTrace, True)

    End Try


  End Sub

  Private Sub Build_LoadSimulation_Menu()
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try

      Menu_LoadSimulation.DropDownItems.Clear()
      Menu_DeleteSimulation.DropDownItems.Clear()

      BuildSubMenu(Menu_LoadSimulation, Menu_DeleteSimulation, 0)

    Catch ex As Exception

    End Try

    'Try

    '  If (Not Me.Created) OrElse (Me.IsDisposed) Then
    '    Exit Sub
    '  End If

    '  Dim SimDS As DSCTA_Simulation
    '  Dim SelectedRows() As DSCTA_Simulation.tblCTA_SimulationRow
    '  Dim thisRow As DSCTA_Simulation.tblCTA_SimulationRow
    '  Dim ThisMenuItem As ToolStripMenuItem

    '  SimDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblCTA_Simulation)

    '  Menu_LoadSimulation.DropDownItems.Clear()
    '  Menu_DeleteSimulation.DropDownItems.Clear()

    '  If (SimDS IsNot Nothing) Then

    '    SelectedRows = SimDS.tblCTA_Simulation.Select("True", "SimulationName")

    '    For Index As Integer = 0 To (SelectedRows.Length - 1)

    '      thisRow = SelectedRows(Index)

    '      ' Load Menu item

    '      ThisMenuItem = New ToolStripMenuItem(thisRow.SimulationName, Nothing, AddressOf Menu_LoadSimulation_Click)
    '      ThisMenuItem.Tag = thisRow.SimulationID

    '      Menu_LoadSimulation.DropDownItems.Add(ThisMenuItem)

    '      ' Delete menu item 

    '      ThisMenuItem = New ToolStripMenuItem(thisRow.SimulationName, Nothing, AddressOf Menu_DeleteSimulation_Click)
    '      ThisMenuItem.Tag = thisRow.SimulationID

    '      Menu_DeleteSimulation.DropDownItems.Add(ThisMenuItem)


    '    Next

    '  End If

    'Catch ex As Exception

    '  MainForm.LogError(Me.Name, LOG_LEVELS.Error, "Error building `Load Simulation` menu.", ex.Message, ex.StackTrace, True)

    'End Try
  End Sub

  Private Sub BuildSubMenu(ByRef pParentLoadMenu As ToolStripMenuItem, ByRef pParentDeleteMenu As ToolStripMenuItem, ByVal pFolderID As Integer)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      ' Add Sub Menus

      Try

        Dim FolderDS As DSCTA_Folders
        Dim SelectedRows() As DSCTA_Folders.tblCTA_FoldersRow
        Dim thisRow As DSCTA_Folders.tblCTA_FoldersRow
        Dim ThisLoadMenuItem As ToolStripMenuItem
        Dim ThisDeleteMenuItem As ToolStripMenuItem

        FolderDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblCTA_Folders)

        If (FolderDS IsNot Nothing) Then

          SelectedRows = FolderDS.tblCTA_Folders.Select("ParentFolderId = " & pFolderID.ToString, "FolderName")

          For Index As Integer = 0 To (SelectedRows.Length - 1)

            thisRow = SelectedRows(Index)

            ' Load Menu item

            ThisLoadMenuItem = New ToolStripMenuItem(thisRow.FolderName)
            ThisLoadMenuItem.Font = New Font(ThisLoadMenuItem.Font, FontStyle.Bold)

            pParentLoadMenu.DropDownItems.Add(ThisLoadMenuItem)

            ' Delete menu item 

            ThisDeleteMenuItem = New ToolStripMenuItem(thisRow.FolderName)
            ThisDeleteMenuItem.Font = New Font(ThisDeleteMenuItem.Font, FontStyle.Bold)

            pParentDeleteMenu.DropDownItems.Add(ThisDeleteMenuItem)

            ' Build Sub-Menus

            BuildSubMenu(ThisLoadMenuItem, ThisDeleteMenuItem, thisRow.FolderID)

          Next

        End If

      Catch ex As Exception
      End Try

      ' Add Simulations

      Try

        Dim SimDS As DSCTA_Simulation
        Dim SelectedRows() As DSCTA_Simulation.tblCTA_SimulationRow
        Dim thisRow As DSCTA_Simulation.tblCTA_SimulationRow
        Dim ThisMenuItem As ToolStripMenuItem

        SimDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblCTA_Simulation)

        If (SimDS IsNot Nothing) Then

          SelectedRows = SimDS.tblCTA_Simulation.Select("SimulationFolder = " & pFolderID.ToString, "SimulationName")

          For Index As Integer = 0 To (SelectedRows.Length - 1)

            thisRow = SelectedRows(Index)

            ' Load Menu item

            ThisMenuItem = New ToolStripMenuItem(thisRow.SimulationName, Nothing, AddressOf Menu_LoadSimulation_Click)
            ThisMenuItem.Tag = thisRow.SimulationID
            ThisMenuItem.ForeColor = Color.Blue

            pParentLoadMenu.DropDownItems.Add(ThisMenuItem)

            ' Delete menu item 

            ThisMenuItem = New ToolStripMenuItem(thisRow.SimulationName, Nothing, AddressOf Menu_DeleteSimulation_Click)
            ThisMenuItem.Tag = thisRow.SimulationID
            ThisMenuItem.ForeColor = Color.Blue

            pParentDeleteMenu.DropDownItems.Add(ThisMenuItem)

          Next

        End If

      Catch ex As Exception

      End Try


      ' Add Save Menu

      ' Menu_SaveSimulation_Click

      Try

        Dim ThisMenuItem As ToolStripMenuItem

        ThisMenuItem = New ToolStripMenuItem("Save Simulation As ...", Nothing, AddressOf Menu_SaveSimulationAs_Click)
        ThisMenuItem.Tag = pFolderID
        pParentLoadMenu.DropDownItems.Add(ThisMenuItem)

      Catch ex As Exception
      End Try

    Catch ex As Exception

    End Try

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

#Region " Parameters Controls Change Events"

  Private Sub Combo_InstrumentSeries_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_InstrumentSeries.SelectedIndexChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
      _ZoomReset = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_InstrumentSeries_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Combo_InterestRateSeries_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_InterestRateSeries.SelectedIndexChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_InterestRateSeries_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Date_DataStartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_DataStartDate.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_InterestRateSeries_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub



  Private Sub Combo_DataPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_DataPeriod.SelectedIndexChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_DataPeriod_SelectedIndexChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_MovingAverageDays_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_MovingAverageDays.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_InterestRateSeries_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Radio_UnAdjustedAverage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_UnAdjustedAverage.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_UnAdjustedAverage_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Check_ExponentialPriceAverage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ExponentialPriceAverage.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_ExponentialPriceAverage_CheckedChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Radio_ExponentialWeightedAverage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExponentialWeightedAverage.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_ExponentialWeightedAverage_CheckedChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Radio_LinearWeightedAverage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_LinearWeightedAverage.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_LinearWeightedAverage_CheckedChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Radio_VolatilityWeightedDuration_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_VolatilityWeightedDuration.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_VolatilityWeightedDuration_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_ExponentialPrice_Lambda_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_ExponentialPrice_Lambda.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Check_ExponentialPriceAverage.Checked) Then
        ChartsToUpdate = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_ExponentialPrice_Lambda_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_ExponentialAverage_Lambda_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_ExponentialAverage_Lambda.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Radio_ExponentialWeightedAverage.Checked) Then
        ChartsToUpdate = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_ExponentialAverage_Lambda_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_LinearAverage_Lambda_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_LinearAverage_Lambda.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Radio_LinearWeightedAverage.Checked) Then
        ChartsToUpdate = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_LinearAverage_Lambda_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_VolatilityAverage_Lambda_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_VolatilityAverage_Lambda.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Radio_VolatilityWeightedDuration.Checked) Then
        ChartsToUpdate = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_VolatilityAverage_Lambda_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_Trading_VolWeightDuration_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_VolWeightDuration.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Radio_Trading_VolWeight_VsSecondDuration.Checked) Then
        ChartsToUpdate = True
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_VolWeightDuration_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub DateTime_Trading_StartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTime_Trading_StartDate.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in DateTime_Trading_StartDate_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Radio_Trading_FirstTradeStart_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_FirstTradeStart.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_Trading_FirstTradeStart_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Radio_Trading_VolWeightNone_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_VolWeightNone.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_Trading_VolWeightNone_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Radio_Trading_VolWeightByHighStDev_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_VolWeightByHighStDev.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_Trading_VolWeightByHighStDev_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub


  Private Sub Radio_Trading_VolWeight_VsSecondDuration_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_VolWeight_VsSecondDuration.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_Trading_VolWeight_VsSecondDuration_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Radio_Trading_FirstTradeCrossing_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_FirstTradeCrossing.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_Trading_FirstTradeCrossing_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Check_Trading_AllowLong_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Trading_AllowLong.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_Trading_AllowLong_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Check_Trading_AllowShort_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Trading_AllowShort.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_Trading_AllowShort_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_Trading_MaxDrawdown_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_MaxDrawdown.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        ChartsToUpdate = True

        If (Numeric_Trading_MaxDrawdown.Value <> Math.Min(Math.Max(Numeric_Trading_MaxDrawdown.Value, Numeric_Shadow_MaxDrawdown.Minimum), Numeric_Shadow_MaxDrawdown.Maximum)) Then
          Numeric_Trading_MaxDrawdown.Value = Math.Min(Math.Max(Numeric_Trading_MaxDrawdown.Value, Numeric_Shadow_MaxDrawdown.Minimum), Numeric_Shadow_MaxDrawdown.Maximum)
        Else
          Numeric_Shadow_MaxDrawdown.Value = Numeric_Trading_MaxDrawdown.Value
        End If

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_MaxDrawdown_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_Shadow_MaxDrawdown_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Shadow_MaxDrawdown.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        If (Numeric_Trading_MaxDrawdown.Value <> Numeric_Shadow_MaxDrawdown.Value) Then
          Numeric_Trading_MaxDrawdown.Value = Numeric_Shadow_MaxDrawdown.Value
        End If
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Shadow_MaxDrawdown_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Check_Trading_MonthlyDrawdownReset_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Trading_MonthlyDrawdownReset.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Numeric_Trading_MonthlyDrawdownResetCount.Enabled = Check_Trading_MonthlyDrawdownReset.Checked
        ChartsToUpdate = True
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Numeric_Trading_MonthlyDrawdownResetCount_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Numeric_Trading_MonthlyDrawdownResetCount.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Numeric_Trading_MonthlyDrawdownResetCount.Enabled) AndAlso (Me.Created) AndAlso (Not Me.IsDisposed) Then
        ChartsToUpdate = True
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Numeric_TrandingCosts_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_TrandingCosts.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_TrandingCosts_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_FundingSpread_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_FundingSpread.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_FundingSpread_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_Trading_BaselineGearing_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_BaselineGearing.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

      Numeric_Shadow_BaselineGearing.Value = Numeric_Trading_BaselineGearing.Value
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_BaselineGearing_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Numeric_Shadow_BaselineGearing_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Shadow_BaselineGearing.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Numeric_Trading_BaselineGearing.Value <> Numeric_Shadow_BaselineGearing.Value) Then
        Numeric_Trading_BaselineGearing.Value = Numeric_Shadow_BaselineGearing.Value
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Shadow_BaselineGearing_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Numeric_Trading_VolExponent_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_VolExponent.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_VolExponent_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Numeric_Trading_VolWeightCap_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_VolWeightCap.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_VolWeightCap_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Numeric_Trading_RebalanceThreshold_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_RebalanceThreshold.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

      Numeric_Shadow_RebalanceThreshold.Value = Numeric_Trading_RebalanceThreshold.Value
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_RebalanceThreshold_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Numeric_Shadow_RebalanceThreshold_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Shadow_RebalanceThreshold.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Numeric_Trading_RebalanceThreshold.Value <> Numeric_Shadow_RebalanceThreshold.Value) Then
        Numeric_Trading_RebalanceThreshold.Value = Numeric_Shadow_RebalanceThreshold.Value
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Shadow_RebalanceThreshold_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Numeric_Trading_PercentBand_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_PercentBand.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Radio_Trading_ByPercentageBand.Checked) Then
        ChartsToUpdate = True

      End If

      Numeric_Shadow_PercentBand.Value = Numeric_Trading_PercentBand.Value
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_PercentBand_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Numeric_Shadow_PercentBand_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Shadow_PercentBand.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Numeric_Trading_PercentBand.Value <> Numeric_Shadow_PercentBand.Value) Then
        Numeric_Trading_PercentBand.Value = Numeric_Shadow_PercentBand.Value
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Shadow_PercentBand_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Numeric_Trading_CrossingUnweightPoint_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_CrossingUnweightPoint.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Check_Trading_PhaseOutAtCrossing.Checked) Then

        ChartsToUpdate = True

      End If

      If (Numeric_Trading_CrossingUnweightPoint.Value <> Math.Min(Math.Max(Numeric_Trading_CrossingUnweightPoint.Value, Numeric_Trading_ShadowCrossingUnweightPoint.Minimum), Numeric_Trading_ShadowCrossingUnweightPoint.Maximum)) Then
        Numeric_Trading_CrossingUnweightPoint.Value = Math.Min(Math.Max(Numeric_Trading_CrossingUnweightPoint.Value, Numeric_Trading_ShadowCrossingUnweightPoint.Minimum), Numeric_Trading_ShadowCrossingUnweightPoint.Maximum)
      Else
        Numeric_Trading_ShadowCrossingUnweightPoint.Value = Numeric_Trading_CrossingUnweightPoint.Value
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_CrossingUnweightPoint_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Numeric_Trading_ShadowCrossingUnweightPoint_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_ShadowCrossingUnweightPoint.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Numeric_Trading_CrossingUnweightPoint.Value <> Numeric_Trading_ShadowCrossingUnweightPoint.Value) Then
        Numeric_Trading_CrossingUnweightPoint.Value = Numeric_Trading_ShadowCrossingUnweightPoint.Value
      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_ShadowCrossingUnweightPoint_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Check_Trading_BandWeightings_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Trading_BandWeightings.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

      Panel_Trading_BandWeighting.Enabled = Check_Trading_BandWeightings.Checked
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_Trading_BandWeightings_CheckedChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Radio_Trading_ByIndexZScore_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_ByIndexZScore.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_Trading_ByIndexZScore_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Radio_Trading_ByAverageZScore_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_ByAverageZScore.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_Trading_ByAverageZScore_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Radio_Trading_ByPercentageBand_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_ByPercentageBand.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Radio_Trading_ByPercentageBand_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Check_Trading_ShaseOutAtCrossing_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Trading_PhaseOutAtCrossing.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_Trading_ShaseOutAtCrossing_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_Trading_IndexZScore_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_IndexZScore.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Radio_Trading_ByIndexZScore.Checked) Then
        ChartsToUpdate = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_IndexZScore_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_Trading_AverageZScore_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_AverageZScore.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Radio_Trading_ByAverageZScore.Checked) Then
        ChartsToUpdate = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_AverageZScore_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_Trading_ZScore_Period_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_ZScore_Period.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Check_Trading_BandWeightings.Checked) Then
        ChartsToUpdate = True

      End If
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_ZScore_Period_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_Trading_WeightingDecayFactor_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_WeightingDecayFactor.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_WeightingDecayFactor_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_Trading_ReInvestmentEvaluationPeriod_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_ReInvestmentEvaluationPeriod.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_ReInvestmentEvaluationPeriod_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_Trading_BandDeltaLimit_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_Trading_BandDeltaLimit.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_Trading_BandDeltaLimit_ValueChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Numeric_LocalHighExclusionPeriod_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Numeric_LocalHighExclusionPeriod.ValueChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Numeric_LocalHighExclusionPeriod_ValueChanged()", ex.StackTrace, True)
    End Try
  End Sub

#End Region

#Region " 'Charts' Tab Control events"

  Private Sub Check_VAMI_ShowPortfolio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_VAMI_ShowPortfolio.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_VAMI_ShowPortfolio_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Check_VAMI_ShowBands_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_VAMI_ShowBands.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Check_VAMI_ShowBands_CheckedChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Combo_Charts_CompareSeriesPertrac_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Charts_CompareSeriesPertrac.SelectedValueChanged  ' SelectedIndexChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_Charts_CompareSeriesPertrac_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Combo_Charts_CompareSeriesGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Charts_CompareSeriesGroup.SelectedIndexChanged   ' 
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_Charts_CompareSeriesGroup_SelectedIndexChanged()", ex.StackTrace, True)
    End Try
  End Sub

  Private Sub Charts_Parameter_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
   Handles Date_Charts_DateFrom.ValueChanged, Date_Charts_DateTo.ValueChanged, Text_Chart_Lamda.ValueChanged, Text_Chart_RollingPeriod.ValueChanged, Text_ScalingFactor.ValueChanged

    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      ChartsToUpdate = True
    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Combo_Charts_CompareSeries_SelectedIndexChanged()", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub Radio_SingleScalingFactor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_SingleScalingFactor.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Radio_SingleScalingFactor.Checked) Then

        Me.Text_ScalingFactor.Enabled = True

        If (Not InPaint) Then

          ChartsToUpdate = True

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_DynamicScalingFactor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_DynamicScalingFactor.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try
      If (Radio_DynamicScalingFactor.Checked) Then

        Me.Text_ScalingFactor.Enabled = False

        If (Not InPaint) Then

          ChartsToUpdate = True

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Button_DefaultDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_DefaultDate.Click
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      Me.Date_Charts_DateFrom.Value = Renaissance_BaseDate
      Me.Date_Charts_DateTo.Value = Renaissance_EndDate_Data
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Text_Chart_VARPeriod_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Chart_VARPeriod.ValueChanged
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (InPaint = False) Then

          PaintVARCharts()

        End If

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Chart_VARConfidence_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Text_Chart_VARConfidence.ValueChanged
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (InPaint = False) Then

          PaintVARCharts()

        End If

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Combo_Charts_VARSeries_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Charts_VARSeries.SelectedIndexChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (InPaint = False) Then

          PaintVARCharts()

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Edit_OmegaReturn_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Edit_OmegaReturn.ValueChanged
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (InPaint = False) Then

          PaintOmegaCharts()

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_OmegaRatio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_OmegaRatio.CheckedChanged
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        If (InPaint = False) Then

          PaintOmegaCharts()

        End If

      End If

    Catch ex As Exception
    End Try

  End Sub


  Private Sub Radio_Charts_ComparePertrac_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Charts_ComparePertrac.CheckedChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        Combo_Charts_CompareSeriesPertrac.Visible = True
        Combo_Charts_CompareSeriesGroup.Visible = False

        ChartsToUpdate = True
      End If

    Catch ex As Exception
    End Try

  End Sub


  Private Sub Radio_Charts_CompareGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Charts_CompareGroup.CheckedChanged
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If (Radio_Charts_CompareGroup.Checked) Then
        Combo_Charts_CompareSeriesPertrac.Visible = False
        Combo_Charts_CompareSeriesGroup.Visible = True

        ChartsToUpdate = True
      End If

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " 'Statistics' Tab Control Events"

  Private Sub Combo_Statistics_HighlightSeries_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_Statistics_HighlightSeries.SelectedIndexChanged
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    If (Me.Created) And (InPaint = False) Then
      If (Combo_Statistics_HighlightSeries.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_Statistics_HighlightSeries.SelectedValue)) Then
        DrawStatisticsTabGrids(CInt(Combo_Statistics_HighlightSeries.SelectedValue), Text_Statistics_RiskFree.Value)
      Else
        DrawStatisticsTabGrids(0, 0)
      End If
    End If

  End Sub

  Private Sub Text_Statistics_RiskFree_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Text_Statistics_RiskFree.ValueChanged
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    If (Me.Created) And (InPaint = False) Then
      If (Combo_Statistics_HighlightSeries.SelectedIndex >= 0) AndAlso (IsNumeric(Combo_Statistics_HighlightSeries.SelectedValue)) Then
        DrawStatisticsTabGrids(CInt(Combo_Statistics_HighlightSeries.SelectedValue), Text_Statistics_RiskFree.Value)
      Else
        DrawStatisticsTabGrids(0, 0)
      End If
    End If
  End Sub

  Private Sub DrawStatisticsTabGrids(ByVal pPertracID As Integer, ByVal pRiskFree As Double)
    ' *******************************************************************************
    ' Refresh the Statistics and DrawDown Grids.
    '
    ' *******************************************************************************

    Dim DateSeries() As Date = FormSimulationParameters.PortfolioDateSeries
    Dim NAVSeries() As Double = FormSimulationParameters.PortfolioNAVSeries

    ' 1) Get Simple statistics & DrawDown array.
    '  DrawDownDetails are pre sorted by DrawDown percentage in descending order.

    Dim FundStats As StatFunctions.SeriesStatsClass = MainForm.StatFunctions.GetSimpleStats(MainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), False, pRiskFree, 1.0#)
    Dim DrawDownDetails() As StatFunctions.DrawDownInstanceClass = MainForm.StatFunctions.GetDrawDownDetails(MainForm.PertracData.GetPertracDataPeriod(pPertracID), CULng(pPertracID), False, Renaissance_BaseDate, Renaissance_EndDate_Data, 1.0#)

    ' Paint Stats Grid

    Try
      Grid_SimpleStatistics.Item(1, 1) = FundStats.SimpleReturn.M12
      Grid_SimpleStatistics.Item(1, 2) = FundStats.SimpleReturn.M36
      Grid_SimpleStatistics.Item(1, 3) = FundStats.SimpleReturn.M60
      Grid_SimpleStatistics.Item(1, 4) = FundStats.SimpleReturn.ITD

      Grid_SimpleStatistics.Item(2, 1) = FundStats.AnnualisedReturn.M12
      Grid_SimpleStatistics.Item(2, 2) = FundStats.AnnualisedReturn.M36
      Grid_SimpleStatistics.Item(2, 3) = FundStats.AnnualisedReturn.M60
      Grid_SimpleStatistics.Item(2, 4) = FundStats.AnnualisedReturn.ITD

      Grid_SimpleStatistics.Item(3, 1) = FundStats.Volatility.M12
      Grid_SimpleStatistics.Item(3, 2) = FundStats.Volatility.M36
      Grid_SimpleStatistics.Item(3, 3) = FundStats.Volatility.M60
      Grid_SimpleStatistics.Item(3, 4) = FundStats.Volatility.ITD

      Grid_SimpleStatistics.Item(4, 1) = FundStats.Volatility_Series.M12
      Grid_SimpleStatistics.Item(4, 2) = FundStats.Volatility_Series.M36
      Grid_SimpleStatistics.Item(4, 3) = FundStats.Volatility_Series.M60
      Grid_SimpleStatistics.Item(4, 4) = FundStats.Volatility_Series.ITD

      Grid_SimpleStatistics.Item(5, 1) = FundStats.SharpeRatio.M12
      Grid_SimpleStatistics.Item(5, 2) = FundStats.SharpeRatio.M36
      Grid_SimpleStatistics.Item(5, 3) = FundStats.SharpeRatio.M60
      Grid_SimpleStatistics.Item(5, 4) = FundStats.SharpeRatio.ITD

      Grid_SimpleStatistics.Item(6, 1) = FundStats.PercentPositive.M12
      Grid_SimpleStatistics.Item(6, 2) = FundStats.PercentPositive.M36
      Grid_SimpleStatistics.Item(6, 3) = FundStats.PercentPositive.M60
      Grid_SimpleStatistics.Item(6, 4) = FundStats.PercentPositive.ITD
    Catch ex As Exception
    End Try

    ' Format Stats Grid.

    Dim RowCount As Integer
    Dim ColCount As Integer
    Dim PositiveFormat As String
    Dim NegativeFormat As String

    Try
      For RowCount = 1 To 6
        If RowCount = 5 Then
          PositiveFormat = "SharpeRatio"
          NegativeFormat = "SharpeRatio"
        Else
          PositiveFormat = "PositivePercent"
          NegativeFormat = "NegativePercent"
        End If

        For ColCount = 1 To 4
          If CDbl(Grid_SimpleStatistics.Item(RowCount, ColCount)) < 0 Then
            Grid_SimpleStatistics.SetCellStyle(RowCount, ColCount, NegativeFormat)
          Else
            Grid_SimpleStatistics.SetCellStyle(RowCount, ColCount, PositiveFormat)
          End If
        Next
      Next
    Catch ex As Exception
    End Try

    ' Populate DrawDown Grid :-

    Try
      Me.Grid_Stats_Drawdowns.Rows.Count = DrawDownDetails.Length + 1

      For RowCount = 0 To (DrawDownDetails.Length - 1)
        Grid_Stats_Drawdowns.Item(RowCount + 1, 1) = DrawDownDetails(RowCount).DateFrom
        Grid_Stats_Drawdowns.Item(RowCount + 1, 2) = DrawDownDetails(RowCount).DateTrough
        Grid_Stats_Drawdowns.Item(RowCount + 1, 3) = DrawDownDetails(RowCount).DateTo
        Grid_Stats_Drawdowns.Item(RowCount + 1, 4) = DrawDownDetails(RowCount).DrawDown
        Grid_Stats_Drawdowns.Item(RowCount + 1, 5) = GetPeriodCount(MainForm.PertracData.GetPertracDataPeriod(pPertracID), DrawDownDetails(RowCount).DateFrom, DrawDownDetails(RowCount).DateTrough) & " " & PeriodName(MainForm.PertracData.GetPertracDataPeriod(pPertracID))
        Grid_Stats_Drawdowns.Item(RowCount + 1, 6) = (GetPeriodCount(MainForm.PertracData.GetPertracDataPeriod(pPertracID), DrawDownDetails(RowCount).DateTrough, DrawDownDetails(RowCount).DateTo) - 1).ToString & " " & PeriodName(MainForm.PertracData.GetPertracDataPeriod(pPertracID))
        Grid_Stats_Drawdowns.Item(RowCount + 1, 7) = GetPeriodCount(MainForm.PertracData.GetPertracDataPeriod(pPertracID), DrawDownDetails(RowCount).DateFrom, DrawDownDetails(RowCount).DateTo) & " " & PeriodName(MainForm.PertracData.GetPertracDataPeriod(pPertracID))

        If DrawDownDetails(RowCount).RecoveryOngoing Then
          Grid_Stats_Drawdowns.Item(RowCount + 1, 8) = "and counting"
        Else
          Grid_Stats_Drawdowns.Item(RowCount + 1, 8) = ""
        End If
      Next
    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Charts Menu Events"

  Private Sub ToolStripCombo_Periods_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *********************************************************************************
    ' Event handler for 'Date Period' Menu items.
    '
    '
    ' *********************************************************************************

    If InPaint = False Then
      Try
        Dim thisPeriodID As Integer
        Dim SelectedMenuItem As ToolStripMenuItem
        Dim StatsDatePeriod As DealingPeriod = CType(Me.Combo_DataPeriod.SelectedValue, DealingPeriod)

        Dim PeriodDS As RenaissanceDataClass.DSPeriod
        Dim PeriodTbl As RenaissanceDataClass.DSPeriod.tblPeriodDataTable
        Dim SelectedRows() As RenaissanceDataClass.DSPeriod.tblPeriodRow

        SelectedMenuItem = CType(sender, System.Windows.Forms.ToolStripMenuItem)
        If (SelectedMenuItem IsNot Nothing) AndAlso (IsNumeric(SelectedMenuItem.Tag)) Then
          If (SelectedMenuItem.Text = SelectedMenuItem.Tag.ToString) Then
            ' Year

            Dim SelectedYear As Integer
            Try
              SelectedYear = CInt(SelectedMenuItem.Tag)

              If (SelectedYear >= Renaissance_BaseDate.Year) And (SelectedYear <= Renaissance_EndDate_Data.Year) Then
                ' Charts
                Date_Charts_DateFrom.Value = New Date(SelectedYear - 1, 12, 31)
                Date_Charts_DateTo.Value = New Date(SelectedYear, 12, 31)

                ' Trigger immediate re-paint
                _ChartsToUpdateTime = Now.AddHours(-1)
                Call FormUpdate_Tick()

              End If
            Catch ex As Exception

            End Try
          Else
            ' tblPeriod

            thisPeriodID = CInt(SelectedMenuItem.Tag)

            If (thisPeriodID > 0) Then
              PeriodDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPeriod)
              PeriodTbl = PeriodDS.tblPeriod
              SelectedRows = PeriodTbl.Select("PeriodID=" & thisPeriodID.ToString)

              If (SelectedRows.Length > 0) Then
                Date_Charts_DateFrom.Value = SelectedRows(0).PeriodDateFrom
                Date_Charts_DateTo.Value = SelectedRows(0).PeriodDateTo

                ' Trigger immediate re-paint
                _ChartsToUpdateTime = Now.AddHours(-1)
                Call FormUpdate_Tick()

              End If
            End If
          End If
        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", ToolStripCombo_Periods_Click()", LOG_LEVELS.Error, ex.Message, "Error imposing Period Dates", ex.StackTrace, True)
      End Try

    End If
  End Sub

#End Region

#Region " Investment Functions"

  Private Sub SetSimulationParametersFromForm(ByRef pSimulationParameter As SimulationParameterClass)
    ' *********************************************************************************
    '
    '
    ' *********************************************************************************

    Try

      If (Combo_InstrumentSeries.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_InstrumentSeries.SelectedValue)) Then
        pSimulationParameter.InstrumentID = CInt(Combo_InstrumentSeries.SelectedValue)
      Else
        pSimulationParameter.InstrumentID = 0
      End If

      If (Combo_InterestRateSeries.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_InterestRateSeries.SelectedValue)) Then
        pSimulationParameter.InterestRateID = CInt(Combo_InterestRateSeries.SelectedValue)
      Else
        pSimulationParameter.InterestRateID = 0
      End If

      pSimulationParameter.DataStartDate = Date_DataStartDate.Value
      pSimulationParameter.MovingAverageDays = Numeric_MovingAverageDays.Value

      If IsNumeric(Combo_DataPeriod.SelectedValue) Then
        pSimulationParameter.StatsDatePeriod = CType(Combo_DataPeriod.SelectedValue, DealingPeriod)
      Else
        pSimulationParameter.StatsDatePeriod = DealingPeriod.Daily
      End If

      pSimulationParameter.MovingAverage_ExponentialPricingAverage = Check_ExponentialPriceAverage.Checked
      pSimulationParameter.MovingAverage_ExponentialPricingLambda = Numeric_ExponentialPrice_Lambda.Value
      pSimulationParameter.MovingAverage_ExponentialWeightedAverage = Radio_ExponentialWeightedAverage.Checked
      pSimulationParameter.MovingAverage_ExponentialWeightedAverageLambda = Numeric_ExponentialAverage_Lambda.Value
      pSimulationParameter.MovingAverage_LinearWeightedAverage = Radio_LinearWeightedAverage.Checked
      pSimulationParameter.MovingAverage_LinearWeightedAverageLambda = Numeric_LinearAverage_Lambda.Value
      pSimulationParameter.MovingAverage_VolatilityWeightedDuration = Radio_VolatilityWeightedDuration.Checked
      pSimulationParameter.MovingAverage_VolatilityWeightedDurationLambda = Numeric_VolatilityAverage_Lambda.Value

      pSimulationParameter.Trading_RebalanceThreshold = Numeric_Trading_RebalanceThreshold.Value
      pSimulationParameter.Trading_BaselineGearing = Numeric_Trading_BaselineGearing.Value
      pSimulationParameter.Trading_MaxDrawdown = Numeric_Trading_MaxDrawdown.Value
      pSimulationParameter.Trading_DrawdownMonthlyReset = Check_Trading_MonthlyDrawdownReset.Checked
      If (Check_Trading_MonthlyDrawdownReset.Checked) Then
        pSimulationParameter.Trading_DrawdownResetMonths = Math.Max(1, Math.Abs(Numeric_Trading_MonthlyDrawdownResetCount.Value))
      Else
        pSimulationParameter.Trading_DrawdownResetMonths = 1
      End If

      pSimulationParameter.Trading_AllowLongPosition = Check_Trading_AllowLong.Checked
      pSimulationParameter.Trading_AllowShortPosition = Check_Trading_AllowShort.Checked
      pSimulationParameter.Trading_TradingCosts = Numeric_TrandingCosts.Value
      pSimulationParameter.Trading_FundingSpread = Numeric_FundingSpread.Value
      pSimulationParameter.Trading_StartDate = DateTime_Trading_StartDate.Value
      pSimulationParameter.Trading_FirstTradeAtStart = Radio_Trading_FirstTradeStart.Checked
      pSimulationParameter.Trading_VolatilityWeighting_StdDevHighVsAverage = Radio_Trading_VolWeightByHighStDev.Checked
      pSimulationParameter.Trading_VolatilityWeighting_StdDevVsGivenDuration = Radio_Trading_VolWeight_VsSecondDuration.Checked
      pSimulationParameter.Trading_VolatilityWeighting_VolWeightDuration = Numeric_Trading_VolWeightDuration.Value
      pSimulationParameter.Trading_VolatilityWeighting_VolWeightCap = Numeric_Trading_VolWeightCap.Value
      pSimulationParameter.Trading_VolatilityWeighting_Exponent = Numeric_Trading_VolExponent.Value


      If Check_Trading_BandWeightings.Checked Then
        pSimulationParameter.Trading_BandWeighting_ByIndexZScore = Radio_Trading_ByIndexZScore.Checked
        pSimulationParameter.Trading_BandWeighting_ByAverageZScore = Radio_Trading_ByAverageZScore.Checked
        pSimulationParameter.Trading_BandWeighting_ByPercentageBand = Radio_Trading_ByPercentageBand.Checked
        pSimulationParameter.Trading_BandWeighting_PhaseOutAtCrossing = Check_Trading_PhaseOutAtCrossing.Checked

      Else
        pSimulationParameter.Trading_BandWeighting_ByIndexZScore = False
        pSimulationParameter.Trading_BandWeighting_ByAverageZScore = False
        pSimulationParameter.Trading_BandWeighting_ByPercentageBand = False
        pSimulationParameter.Trading_BandWeighting_PhaseOutAtCrossing = False
      End If

      pSimulationParameter.Trading_CrossingUnweightPoint = Numeric_Trading_CrossingUnweightPoint.Value
      pSimulationParameter.Trading_BandWeighting_IndexZScoreThreshold = Numeric_Trading_IndexZScore.Value
      pSimulationParameter.Trading_BandWeighting_AverageZScoreThreshold = Numeric_Trading_AverageZScore.Value
      pSimulationParameter.Trading_BandWeighting_ZScoreStdDevPeriod = Numeric_Trading_ZScore_Period.Value
      pSimulationParameter.Trading_BandWeighting_PercentageBandThreshold = Numeric_Trading_PercentBand.Value
      pSimulationParameter.Trading_BandWeighting_WeightingDecayFactor = Numeric_Trading_WeightingDecayFactor.Value
      pSimulationParameter.Trading_BandWeighting_ReInvestmentEvaluationPeriod = Numeric_Trading_ReInvestmentEvaluationPeriod.Value
      pSimulationParameter.Trading_BandWeighting_DailyWeightDeltaLimit = Numeric_Trading_BandDeltaLimit.Value
      pSimulationParameter.Trading_BandWeighting_LocalHighExclusionPeriod = Numeric_LocalHighExclusionPeriod.Value

    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetFormFromSimulationParameters(ByRef pSimulationParameter As SimulationParameterClass)
    ' *********************************************************************************
    '
    '
    ' *********************************************************************************

    Try
      If Combo_InstrumentSeries.Items.Count > 0 Then
        If (pSimulationParameter.InstrumentID > 0) Then
          Combo_InstrumentSeries.SelectedValue = pSimulationParameter.InstrumentID
        Else
          Combo_InstrumentSeries.SelectedIndex = 0
        End If
      End If

      If Combo_InterestRateSeries.Items.Count > 0 Then
        If (pSimulationParameter.InterestRateID > 0) Then
          Combo_InterestRateSeries.SelectedValue = pSimulationParameter.InterestRateID
        Else
          Combo_InterestRateSeries.SelectedIndex = 0
        End If
      End If

      Date_DataStartDate.Value = pSimulationParameter.DataStartDate
      Combo_DataPeriod.SelectedValue = CInt(pSimulationParameter.StatsDatePeriod)
      Numeric_MovingAverageDays.Value = pSimulationParameter.MovingAverageDays

      Check_ExponentialPriceAverage.Checked = pSimulationParameter.MovingAverage_ExponentialPricingAverage
      Numeric_ExponentialPrice_Lambda.Value = pSimulationParameter.MovingAverage_ExponentialPricingLambda
      Radio_ExponentialWeightedAverage.Checked = pSimulationParameter.MovingAverage_ExponentialWeightedAverage
      Numeric_ExponentialAverage_Lambda.Value = pSimulationParameter.MovingAverage_ExponentialWeightedAverageLambda
      Radio_LinearWeightedAverage.Checked = pSimulationParameter.MovingAverage_LinearWeightedAverage
      Numeric_LinearAverage_Lambda.Value = pSimulationParameter.MovingAverage_LinearWeightedAverageLambda
      Radio_VolatilityWeightedDuration.Checked = pSimulationParameter.MovingAverage_VolatilityWeightedDuration
      Numeric_VolatilityAverage_Lambda.Value = pSimulationParameter.MovingAverage_VolatilityWeightedDurationLambda

      Check_Trading_AllowLong.Checked = pSimulationParameter.Trading_AllowLongPosition
      Check_Trading_AllowShort.Checked = pSimulationParameter.Trading_AllowShortPosition

      Numeric_Trading_RebalanceThreshold.Value = pSimulationParameter.Trading_RebalanceThreshold
      Numeric_Trading_BaselineGearing.Value = pSimulationParameter.Trading_BaselineGearing
      Numeric_Trading_MaxDrawdown.Value = pSimulationParameter.Trading_MaxDrawdown
      Check_Trading_MonthlyDrawdownReset.Checked = pSimulationParameter.Trading_DrawdownMonthlyReset
      Numeric_Trading_MonthlyDrawdownResetCount.Enabled = Check_Trading_MonthlyDrawdownReset.Checked
      Numeric_Trading_MonthlyDrawdownResetCount.Value = pSimulationParameter.Trading_DrawdownResetMonths
      Numeric_TrandingCosts.Value = pSimulationParameter.Trading_TradingCosts
      Numeric_FundingSpread.Value = pSimulationParameter.Trading_FundingSpread
      DateTime_Trading_StartDate.Value = pSimulationParameter.Trading_StartDate

      If (pSimulationParameter.Trading_FirstTradeAtStart) Then
        Radio_Trading_FirstTradeStart.Checked = True
      Else
        Radio_Trading_FirstTradeCrossing.Checked = True
      End If

      If (pSimulationParameter.Trading_VolatilityWeighting_StdDevHighVsAverage) Then
        Radio_Trading_VolWeightByHighStDev.Checked = True
      ElseIf (pSimulationParameter.Trading_VolatilityWeighting_StdDevVsGivenDuration) Then
        Radio_Trading_VolWeight_VsSecondDuration.Checked = True
      Else
        Radio_Trading_VolWeightNone.Checked = True
      End If

      Numeric_Trading_VolWeightDuration.Value = pSimulationParameter.Trading_VolatilityWeighting_VolWeightDuration
      Numeric_Trading_VolWeightCap.Value = pSimulationParameter.Trading_VolatilityWeighting_VolWeightCap
      Numeric_Trading_VolExponent.Value = pSimulationParameter.Trading_VolatilityWeighting_Exponent

      If (pSimulationParameter.Trading_BandWeighting) Then
        Check_Trading_BandWeightings.Checked = True
        Radio_Trading_ByIndexZScore.Checked = pSimulationParameter.Trading_BandWeighting_ByIndexZScore
        Radio_Trading_ByAverageZScore.Checked = pSimulationParameter.Trading_BandWeighting_ByAverageZScore
        Radio_Trading_ByPercentageBand.Checked = pSimulationParameter.Trading_BandWeighting_ByPercentageBand
        Check_Trading_PhaseOutAtCrossing.Checked = pSimulationParameter.Trading_BandWeighting_PhaseOutAtCrossing
      Else
        Check_Trading_BandWeightings.Checked = False
        Radio_Trading_ByIndexZScore.Checked = False
        Radio_Trading_ByAverageZScore.Checked = False
        Radio_Trading_ByPercentageBand.Checked = False
        Check_Trading_PhaseOutAtCrossing.Checked = False
      End If

      Numeric_Trading_CrossingUnweightPoint.Value = pSimulationParameter.Trading_CrossingUnweightPoint
      Numeric_Trading_IndexZScore.Value = pSimulationParameter.Trading_BandWeighting_IndexZScoreThreshold
      Numeric_Trading_AverageZScore.Value = pSimulationParameter.Trading_BandWeighting_AverageZScoreThreshold
      Numeric_Trading_ZScore_Period.Value = pSimulationParameter.Trading_BandWeighting_ZScoreStdDevPeriod
      Numeric_Trading_PercentBand.Value = pSimulationParameter.Trading_BandWeighting_PercentageBandThreshold
      Numeric_Trading_WeightingDecayFactor.Value = pSimulationParameter.Trading_BandWeighting_WeightingDecayFactor
      Numeric_Trading_ReInvestmentEvaluationPeriod.Value = pSimulationParameter.Trading_BandWeighting_ReInvestmentEvaluationPeriod
      Numeric_Trading_BandDeltaLimit.Value = pSimulationParameter.Trading_BandWeighting_DailyWeightDeltaLimit
      Numeric_LocalHighExclusionPeriod.Value = pSimulationParameter.Trading_BandWeighting_LocalHighExclusionPeriod

    Catch ex As Exception
    End Try

  End Sub

  Private Sub RunTrandingSimulation()
    ' *********************************************************************************
    '
    '
    ' *********************************************************************************

    Try
      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      SetSimulationParametersFromForm(FormSimulationParameters)

      If FormTradeSimulator.RunTrandingSimulation(MainForm.StatFunctions, FormSimulationParameters, 0UL, Nothing) Then
        Label_MaxDrawdown.Text = FormSimulationParameters.Result_MaxDrawdown.ToString("#,##0.00%")
        Label_IRR.Text = FormSimulationParameters.Result_IRR.ToString("#,##0.#00%")
        Label_ITDReturn.Text = FormSimulationParameters.Result_ITDReturn.ToString("#,##0.#00%")
        Label_FirstDate.Text = FormSimulationParameters.Result_FirstDate.ToString("dd MMM yyyy")
        Label_LastDate.Text = FormSimulationParameters.Result_LastDate.ToString("dd MMM yyyy")
      Else
        Label_MaxDrawdown.Text = ""
        Label_IRR.Text = ""
        Label_ITDReturn.Text = ""
        Label_FirstDate.Text = ""
        Label_LastDate.Text = ""
      End If

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " TradingResultsGrid Code"

  Private Sub UpdateTradingResultsGrid(ByVal ThisSimulationParameters As CTASimulationFunctions.SimulationParameterClass, ByVal thisResultsGrid As C1FlexGrid)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If

      If (ThisSimulationParameters.PortfolioHistory Is Nothing) OrElse (ThisSimulationParameters.PortfolioHistory.Length <= 0) Then
        thisResultsGrid.Rows.Count = 1
        Exit Sub
      End If

      Dim DateIndex As Integer
      Dim StartDateIndex As Integer
      Dim PortfolioCount As Integer = 0
      Dim ThisPortfolioClass As DailyPortfolioClass
      Dim ThisGridRow As C1.Win.C1FlexGrid.Row

      Dim Col_PortfolioDate As Integer = thisResultsGrid.Cols.IndexOf("Col_PortfolioDate")
      Dim Col_StockPrice As Integer = thisResultsGrid.Cols.IndexOf("Col_StockPrice")
      Dim Col_AverageLevel As Integer = thisResultsGrid.Cols.IndexOf("Col_AverageLevel")
      Dim Col_InterestRate As Integer = thisResultsGrid.Cols.IndexOf("Col_InterestRate")
      Dim Col_IsCrossing As Integer = thisResultsGrid.Cols.IndexOf("Col_IsCrossing")
      Dim Col_CashOpen As Integer = thisResultsGrid.Cols.IndexOf("Col_CashOpen")
      Dim Col_CashClose As Integer = thisResultsGrid.Cols.IndexOf("Col_CashClose")
      Dim Col_AssetOpen As Integer = thisResultsGrid.Cols.IndexOf("Col_AssetOpen")
      Dim Col_AssetClose As Integer = thisResultsGrid.Cols.IndexOf("Col_AssetClose")
      Dim Col_ModelAssetOpen As Integer = thisResultsGrid.Cols.IndexOf("Col_ModelAssetOpen")
      Dim Col_ModelAssetClose As Integer = thisResultsGrid.Cols.IndexOf("Col_ModelAssetClose")
      Dim Col_NAVOpen As Integer = thisResultsGrid.Cols.IndexOf("Col_NAVOpen")
      Dim Col_NAVClose As Integer = thisResultsGrid.Cols.IndexOf("Col_NAVClose")
      Dim Col_DrawDown As Integer = thisResultsGrid.Cols.IndexOf("Col_DrawDown")
      Dim Col_MonthDrawDown As Integer = thisResultsGrid.Cols.IndexOf("Col_MonthDrawDown")
      Dim Col_MonthDrawDownCount As Integer = thisResultsGrid.Cols.IndexOf("Col_MonthDrawDownCount")
      'Dim Col_DrawDownCount As Integer = thisResultsGrid.Cols.IndexOf("")
      Dim Col_CompoundTargetGearing As Integer = thisResultsGrid.Cols.IndexOf("Col_CompoundTargetGearing")
      Dim Col_VolatilityGearingModifier As Integer = thisResultsGrid.Cols.IndexOf("Col_VolatilityGearingModifier")
      Dim Col_BandGearingModifier As Integer = thisResultsGrid.Cols.IndexOf("Col_BandGearingModifier")
      Dim Col_CumulativeTradingFees As Integer = thisResultsGrid.Cols.IndexOf("Col_CumulativeTradingFees")
      Dim Col_CumulativeInterest As Integer = thisResultsGrid.Cols.IndexOf("Col_CumulativeInterest")
      Dim Col_IRR As Integer = thisResultsGrid.Cols.IndexOf("Col_IRR")
      Dim MonthStartNAV As Double = 0.0#

      ' PortfolioHistory

      For DateIndex = 0 To (ThisSimulationParameters.PortfolioHistory.Length - 1)
        If (ThisSimulationParameters.PortfolioHistory(DateIndex) IsNot Nothing) Then
          StartDateIndex = DateIndex
          Exit For
        End If
      Next

      ' Size Grid

      thisResultsGrid.Rows.Count = (ThisSimulationParameters.PortfolioHistory.Length - StartDateIndex) + 1

      MonthStartNAV = ThisSimulationParameters.PortfolioHistory(StartDateIndex).NAVOpen

      For DateIndex = StartDateIndex To (ThisSimulationParameters.PortfolioHistory.Length - 1)
        If (ThisSimulationParameters.PortfolioHistory(DateIndex) IsNot Nothing) Then
          ThisPortfolioClass = ThisSimulationParameters.PortfolioHistory(DateIndex)

          If (DateIndex > StartDateIndex) AndAlso ((ThisPortfolioClass.PortfolioDate.Month <> ThisSimulationParameters.PortfolioHistory(DateIndex - 1).PortfolioDate.Month) Or (ThisPortfolioClass.PortfolioDate.Year <> ThisSimulationParameters.PortfolioHistory(DateIndex - 1).PortfolioDate.Year)) Then
            MonthStartNAV = ThisSimulationParameters.PortfolioHistory(DateIndex - 1).NAVClose
          End If

          ThisGridRow = thisResultsGrid.Rows((DateIndex - StartDateIndex) + 1)

          ThisGridRow(Col_PortfolioDate) = ThisPortfolioClass.PortfolioDate
          ThisGridRow(Col_StockPrice) = ThisPortfolioClass.StockPrice
          ThisGridRow(Col_AverageLevel) = ThisPortfolioClass.AverageLevel
          ThisGridRow(Col_InterestRate) = ThisPortfolioClass.InterestRate / 100.0#
          ThisGridRow(Col_IsCrossing) = ThisPortfolioClass.IsCrossing
          ThisGridRow(Col_CashOpen) = ThisPortfolioClass.CashOpen
          ThisGridRow(Col_CashClose) = ThisPortfolioClass.CashClose
          ThisGridRow(Col_AssetOpen) = ThisPortfolioClass.AssetOpen
          ThisGridRow(Col_AssetClose) = ThisPortfolioClass.AssetClose
          ThisGridRow(Col_ModelAssetOpen) = ThisPortfolioClass.ModelAssetOpen
          ThisGridRow(Col_ModelAssetClose) = ThisPortfolioClass.ModelAssetClose
          ThisGridRow(Col_NAVOpen) = ThisPortfolioClass.NAVOpen
          ThisGridRow(Col_NAVClose) = ThisPortfolioClass.NAVClose
          ThisGridRow(Col_DrawDown) = ThisPortfolioClass.DrawDown
          ThisGridRow(Col_MonthDrawDown) = ThisPortfolioClass.OneMonthDrawDown ' Math.Max(0, 1.0# - (ThisPortfolioClass.NAVClose / MonthStartNAV))
          ThisGridRow(Col_MonthDrawDownCount) = ThisPortfolioClass.MonthsSinceDrawDownLimitBreach
          ThisGridRow(Col_CompoundTargetGearing) = ThisPortfolioClass.CompoundTargetGearing
          ThisGridRow(Col_VolatilityGearingModifier) = ThisPortfolioClass.VolatilityGearingModifier
          ThisGridRow(Col_BandGearingModifier) = ThisPortfolioClass.TradingBandGearingModifier
          ThisGridRow(Col_CumulativeTradingFees) = ThisPortfolioClass.CumulativeTradingFees
          ThisGridRow(Col_CumulativeInterest) = ThisPortfolioClass.CumulativeInterest

          If (DateIndex > StartDateIndex) Then
            ThisGridRow(Col_IRR) = ((ThisSimulationParameters.PortfolioHistory(DateIndex).NAVClose / ThisSimulationParameters.PortfolioHistory(StartDateIndex).NAVOpen) ^ ((1.0# / ((ThisSimulationParameters.PortfolioHistory(DateIndex).PortfolioDate - ThisSimulationParameters.PortfolioHistory(StartDateIndex).PortfolioDate).Days / 365)))) - 1.0#
          End If

        End If
      Next

    Catch ex As Exception

    End Try

  End Sub

#End Region

#Region " Chart Paint Functions"

  Private Sub PaintAllCharts(Optional ByVal pZoomReset As Boolean = True)
    ' *********************************************************************************
    ' Does what it says.
    '
    ' Calls all of the chart management routines to ensure all charts are updated.
    ' *********************************************************************************

    If (Me.Created) Then

      PaintVAMICharts(Nothing, Nothing, pZoomReset)

      PaintMonthlyCharts(Nothing, Nothing, pZoomReset)

      PaintRollingReturnCharts(Nothing, Nothing, pZoomReset)

      PaintReturnScatterCharts()

      PaintStdDevCharts(Nothing, Nothing, pZoomReset)

      PaintVARCharts(Nothing, Nothing, pZoomReset)

      PaintOmegaCharts(Nothing, Nothing, pZoomReset)

      PaintDrawdownCharts(Nothing, Nothing, pZoomReset)

    End If

  End Sub

  Private Sub PaintVAMICharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing, Optional ByVal pZoomReset As Boolean = True)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim DateSeries() As Date = FormSimulationParameters.PortfolioDateSeries
    Dim NAVSeries() As Double = FormSimulationParameters.PortfolioNAVSeries

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Dim DateIndex As Integer
    Dim BandGearingUpper(-1) As Double
    Dim BandGearingLower(-1) As Double
    Dim BandGearing_BoundaryValue As Double

    If (Check_Trading_BandWeightings.Checked) Then
      Dim BandGearing_BandWidth As Double

      BandGearingUpper = Array.CreateInstance(GetType(Double), FormSimulationParameters.AverageSeriesNAV.Length)
      BandGearingLower = Array.CreateInstance(GetType(Double), FormSimulationParameters.AverageSeriesNAV.Length)


      If (Radio_Trading_ByIndexZScore.Checked) OrElse (Radio_Trading_ByAverageZScore.Checked) Then
        If (Radio_Trading_ByAverageZScore.Checked) Then
          BandGearing_BoundaryValue = Numeric_Trading_AverageZScore.Value
        Else
          BandGearing_BoundaryValue = Numeric_Trading_IndexZScore.Value
        End If

        For DateIndex = 0 To (FormSimulationParameters.BandGearing_StDevSeries.Length - 1)

          BandGearing_BandWidth = (FormSimulationParameters.BandGearing_StDevSeries(DateIndex) * BandGearing_BoundaryValue)

          BandGearingUpper(DateIndex) = FormSimulationParameters.AverageSeriesNAV(DateIndex) + BandGearing_BandWidth
          BandGearingLower(DateIndex) = FormSimulationParameters.AverageSeriesNAV(DateIndex) - BandGearing_BandWidth

        Next

      ElseIf (Radio_Trading_ByPercentageBand.Checked) Then
        BandGearing_BoundaryValue = Numeric_Trading_PercentBand.Value

        For DateIndex = 0 To (FormSimulationParameters.AverageSeriesNAV.Length - 1)

          BandGearingUpper(DateIndex) = FormSimulationParameters.AverageSeriesNAV(DateIndex) * (1.0# + BandGearing_BoundaryValue)
          BandGearingLower(DateIndex) = FormSimulationParameters.AverageSeriesNAV(DateIndex) * (1.0# - BandGearing_BoundaryValue)

        Next

      End If

    End If

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines
        Try

          Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 0, "Instrument", FormSimulationParameters.InstrumentDate, FormSimulationParameters.InstrumentNAV, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
          Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 1, "Trigger", FormSimulationParameters.AverageSeriesDate, FormSimulationParameters.AverageSeriesNAV, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)

          If (Check_VAMI_ShowPortfolio.Checked) Then
            Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 2, "Portfolio", DateSeries, NAVSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
          Else
            Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 2, "Portfolio", Nothing, Nothing, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
          End If

          If (Check_VAMI_ShowBands.Checked) AndAlso (Check_Trading_BandWeightings.Checked) Then
            Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 3, "", FormSimulationParameters.AverageSeriesDate, BandGearingUpper, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
            Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 4, "", FormSimulationParameters.AverageSeriesDate, BandGearingLower, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
          Else
            Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 3, "", Nothing, Nothing, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
            Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 4, "", Nothing, Nothing, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
          End If

        Catch ex As Exception
        End Try

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock PriceChartArrayList
        For Each thisChart In Me.PriceChartArrayList
          If (thisChart.Visible) Then

            ' Set Instrument Lines

            Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 0, "Instrument", FormSimulationParameters.InstrumentDate, FormSimulationParameters.InstrumentNAV, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
            Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 1, "Trigger", FormSimulationParameters.AverageSeriesDate, FormSimulationParameters.AverageSeriesNAV, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)

            If (Check_VAMI_ShowPortfolio.Checked) Then
              Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 2, "Portfolio", DateSeries, NAVSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
            Else
              Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 2, "Portfolio", Nothing, Nothing, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
            End If

            If (Check_VAMI_ShowBands.Checked) AndAlso (Check_Trading_BandWeightings.Checked) Then
              Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 3, "", FormSimulationParameters.AverageSeriesDate, BandGearingUpper, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
              Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 4, "", FormSimulationParameters.AverageSeriesDate, BandGearingLower, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
            Else
              Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 3, "", Nothing, Nothing, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
              Set_LineChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 4, "", Nothing, Nothing, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
            End If

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintMonthlyCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing, Optional ByVal pZoomReset As Boolean = True)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim FirstMonthDate As Date
    Dim LastMonthDate As Date
    Dim ThisDate As Date
    Dim MonthPeriodCount As Integer
    Dim StartIndex As Integer
    Dim DateIndex As Integer
    Dim PeriodIndex As Integer

    Dim TempDateSeries(-1) As Date
    Dim DateSeries(-1) As Date
    Dim NAVSeries(-1) As Double
    Dim InstrumentPriceSeries(-1) As Double
    Dim ChartDateSeries(-1) As Date
    Dim ChartNAVReturnSeries(-1) As Double
    Dim ChartInstrumentReturnSeries(-1) As Double

    If (FormSimulationParameters.PortfolioHistory IsNot Nothing) AndAlso (FormSimulationParameters.PortfolioHistory.Length > 1) Then

      ' *****************************************************************************
      ' Build Monthly NAV and Returns series for the Portfolio and the Underlying Instrument.
      ' *****************************************************************************

      ' Series Dates :-

      For DateIndex = 0 To (FormSimulationParameters.PortfolioHistory.Length - 1)
        If (FormSimulationParameters.PortfolioHistory(DateIndex) IsNot Nothing) Then
          StartIndex = DateIndex
          Exit For
        End If
      Next

      FirstMonthDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Monthly, FormSimulationParameters.PortfolioHistory(StartIndex).PortfolioDate, False)
      LastMonthDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Monthly, FormSimulationParameters.PortfolioHistory(FormSimulationParameters.PortfolioHistory.Length - 1).PortfolioDate, True)

      MonthPeriodCount = RenaissanceUtilities.DatePeriodFunctions.GetPeriodCount(DealingPeriod.Monthly, FirstMonthDate, LastMonthDate)

      ' Initialise Arrays.

      ReDim TempDateSeries(MonthPeriodCount)
      ReDim DateSeries(MonthPeriodCount)
      ReDim NAVSeries(MonthPeriodCount)
      ReDim InstrumentPriceSeries(MonthPeriodCount)


      DateSeries(0) = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Monthly, RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Monthly, FirstMonthDate, -1), False)
      NAVSeries(0) = FormSimulationParameters.PortfolioHistory(StartIndex).NAVOpen

      ' Initialise Date Series.

      For DateIndex = 1 To (MonthPeriodCount)
        DateSeries(DateIndex) = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Monthly, RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Monthly, DateSeries(DateIndex - 1), 1), False)
        TempDateSeries(DateIndex) = Renaissance_BaseDate
        NAVSeries(DateIndex) = 0
      Next

      ' Transform Portfolio results to a monthly NAV series

      For DateIndex = StartIndex To (FormSimulationParameters.PortfolioHistory.Length - 1)

        ThisDate = FormSimulationParameters.PortfolioHistory(DateIndex).PortfolioDate
        PeriodIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(DealingPeriod.Monthly, FirstMonthDate, ThisDate) + 1

        If (ThisDate > TempDateSeries(PeriodIndex)) Then
          NAVSeries(PeriodIndex) = FormSimulationParameters.PortfolioHistory(DateIndex).NAVClose
          TempDateSeries(PeriodIndex) = ThisDate
        End If

      Next

      ' Instrument NAVs to a monthly NAV series

      For DateIndex = 1 To (FormSimulationParameters.InstrumentDate.Length - 1)
        PeriodIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(DealingPeriod.Monthly, FirstMonthDate, FormSimulationParameters.InstrumentDate(DateIndex)) + 1

        If (PeriodIndex >= 0) AndAlso (PeriodIndex < InstrumentPriceSeries.Length) Then
          InstrumentPriceSeries(PeriodIndex) = FormSimulationParameters.InstrumentNAV(DateIndex)
        End If
      Next

      ' Forward Fill price and NAV series.

      For DateIndex = 1 To (NAVSeries.Length - 1)
        If (NAVSeries(DateIndex) = 0) Then
          NAVSeries(DateIndex) = NAVSeries(DateIndex - 1)
        End If
        If (InstrumentPriceSeries(DateIndex) = 0) Then
          InstrumentPriceSeries(DateIndex) = InstrumentPriceSeries(DateIndex - 1)
        End If
      Next

      ' Returns Series

      ReDim ChartDateSeries(NAVSeries.Length - 2)
      ReDim ChartNAVReturnSeries(NAVSeries.Length - 2)
      ReDim ChartInstrumentReturnSeries(NAVSeries.Length - 2)

      Array.Copy(DateSeries, 1, ChartDateSeries, 0, ChartDateSeries.Length)

      For DateIndex = 1 To (NAVSeries.Length - 1)

        If (NAVSeries(DateIndex) <> 0) AndAlso (NAVSeries(DateIndex - 1) <> 0) Then
          ChartNAVReturnSeries(DateIndex - 1) = (NAVSeries(DateIndex) / NAVSeries(DateIndex - 1)) - 1.0#
        End If
        If (InstrumentPriceSeries(DateIndex) <> 0) AndAlso (InstrumentPriceSeries(DateIndex - 1) <> 0) Then
          ChartInstrumentReturnSeries(DateIndex - 1) = (InstrumentPriceSeries(DateIndex) / InstrumentPriceSeries(DateIndex - 1)) - 1.0#
        End If
      Next

    End If

    ' Call Charting functions.

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Try

          Set_ReturnsBarChart(MainForm, ThisChartOnly, 0, "Portfolio", ChartDateSeries, ChartNAVReturnSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, False, pZoomReset)
          Set_ReturnsBarChart(MainForm, ThisChartOnly, 1, "Instrument", ChartDateSeries, ChartInstrumentReturnSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, False, pZoomReset)

        Catch ex As Exception
        End Try

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock ReturnsChartArrayList
        For Each thisChart In Me.ReturnsChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Set_ReturnsBarChart(MainForm, thisChart, 0, "Portfolio", ChartDateSeries, ChartNAVReturnSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, False, pZoomReset)
            Set_ReturnsBarChart(MainForm, thisChart, 1, "Instrument", ChartDateSeries, ChartInstrumentReturnSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, False, pZoomReset)

          End If
        Next
      End SyncLock
    End If

  End Sub

  Private Sub PaintRollingReturnCharts(ByVal pThisChartOnly As Object, ByVal ParameterList() As String)
    Call PaintRollingReturnCharts(pThisChartOnly, ParameterList, True)
  End Sub

  Private Sub PaintRollingReturnCharts(ByVal pThisChartOnly As Object, ByVal ParameterList() As String, ByVal pZoomReset As Boolean)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim DateSeries() As Date = FormSimulationParameters.PortfolioDateSeries
    Dim NAVSeries() As Double = FormSimulationParameters.PortfolioNAVSeries
    Dim RollingPeriod As Integer = (MainForm.StatFunctions.AnnualPeriodCount(FormSimulationParameters.StatsDatePeriod) * Text_Chart_RollingPeriod.Value / 12)
    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Set_RollingReturnChart(MainForm, ThisChartOnly, 0, "Portfolio", DateSeries, NAVSeries, RollingPeriod, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
        Set_RollingReturnChart(MainForm, ThisChartOnly, 1, "Instrument", FormSimulationParameters.InstrumentDate, FormSimulationParameters.InstrumentNAV, RollingPeriod, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock RollingReturnChartArrayList
        For Each thisChart In Me.RollingReturnChartArrayList
          If (thisChart.Visible) Then

            Set_RollingReturnChart(MainForm, thisChart, 0, "Portfolio", DateSeries, NAVSeries, RollingPeriod, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
            Set_RollingReturnChart(MainForm, thisChart, 1, "Instrument", FormSimulationParameters.InstrumentDate, FormSimulationParameters.InstrumentNAV, RollingPeriod, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintReturnScatterCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing, Optional ByVal pZoomReset As Boolean = True)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Try
          Set_Chart_FromList(MainForm, _IDsToChart, CTAChartTypes.ReturnScatter, ThisChartOnly, ComparisonSeries, StatFunctions.ContingentSelect.ConditionAll, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock ReturnScatterChartArrayList
        For Each thisChart In Me.ReturnScatterChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Try
              Set_Chart_FromList(MainForm, _IDsToChart, CTAChartTypes.ReturnScatter, thisChart, ComparisonSeries, StatFunctions.ContingentSelect.ConditionAll, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintStdDevCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing, Optional ByVal pZoomReset As Boolean = True)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim DateSeries() As Date = FormSimulationParameters.PortfolioDateSeries
    Dim NAVSeries() As Double = FormSimulationParameters.PortfolioNAVSeries
    Dim StdDevSeries(-1) As Double
    Dim DateIndex As Integer
    Dim RollingPeriod As Integer = (MainForm.StatFunctions.AnnualPeriodCount(FormSimulationParameters.StatsDatePeriod) * Text_Chart_RollingPeriod.Value / 12)

    If (DateSeries IsNot Nothing) AndAlso (DateSeries.Length > 0) Then

      Dim ReturnsSeries(DateSeries.Length - 1) As Double
      Dim AvgSeries(-1) As Double

      For DateIndex = 1 To (DateSeries.Length - 1)
        ReturnsSeries(DateIndex) = (NAVSeries(DateIndex) / NAVSeries(DateIndex - 1)) - 1.0#
      Next

      MainForm.StatFunctions.CalcAvgStDevSeries(ReturnsSeries, AvgSeries, StdDevSeries, FormSimulationParameters.MovingAverageDays, 1.0#)

    End If

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    Dim thisChart As Dundas.Charting.WinControl.Chart

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines
        Try
          Set_StdDevChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 0, "Portfolio", RollingPeriod, DateSeries, StdDevSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)

          ' Set_Chart_FromList(MainForm, _IDsToChart, CTAChartTypes.StdDev, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
        Catch ex As Exception
        End Try

        ' Set Comparison Instrument Line
        If (ComparisonSeries > 0) Then
          Set_StdDevChart(MainForm, ComparisonSeries, ThisChartOnly, -1, ComparisonSeriesText, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
        End If

      End If

    Else

      SyncLock StdDevChartArrayList
        For Each thisChart In Me.StdDevChartArrayList
          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Try
              Set_StdDevChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 0, "Portfolio", RollingPeriod, DateSeries, StdDevSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)

              ' Set_Chart_FromList(MainForm, _IDsToChart, CTAChartTypes.StdDev, thisChart, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, False)
            Catch ex As Exception
            End Try

            If (ComparisonSeries > 0) Then
              Set_StdDevChart(MainForm, ComparisonSeries, thisChart, -1, ComparisonSeriesText, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
            End If

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintVARCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing, Optional ByVal pZoomReset As Boolean = True)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************
    Dim DateSeries() As Date = FormSimulationParameters.PortfolioDateSeries
    Dim NAVSeries() As Double = FormSimulationParameters.PortfolioNAVSeries
    Dim StdDevSeries(-1) As Double
    Dim RollingReturnSeries(-1) As Double
    Dim DateIndex As Integer
    Dim RollingPeriod As Integer = (MainForm.StatFunctions.AnnualPeriodCount(FormSimulationParameters.StatsDatePeriod) * Text_Chart_RollingPeriod.Value / 12)
    Dim VARPeriodsPeriod As Integer = (MainForm.StatFunctions.AnnualPeriodCount(FormSimulationParameters.StatsDatePeriod) * Text_Chart_VARPeriod.Value / 12)

    If (DateSeries IsNot Nothing) AndAlso (DateSeries.Length > 0) Then

      Dim ReturnsSeries(DateSeries.Length - 1) As Double
      Dim AvgSeries(-1) As Double
      ReDim RollingReturnSeries(DateSeries.Length - 1)

      For DateIndex = 1 To (DateSeries.Length - 1)
        ReturnsSeries(DateIndex) = (NAVSeries(DateIndex) / NAVSeries(DateIndex - 1)) - 1.0#

        If (DateIndex >= RollingPeriod) Then
          RollingReturnSeries(DateIndex) = (NAVSeries(DateIndex) / NAVSeries(DateIndex - RollingPeriod)) - 1.0#
        End If
      Next

      MainForm.StatFunctions.CalcAvgStDevSeries(ReturnsSeries, AvgSeries, StdDevSeries, RollingPeriod, 1.0#)

    End If

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      Set_VARChartDundas(MainForm, FormSimulationParameters.StatsDatePeriod, pThisChartOnly, "Portfolio", DateSeries, StdDevSeries, RollingReturnSeries, Text_Chart_VARConfidence.Value, RollingPeriod, VARPeriodsPeriod, False, True, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)

      ' Set_VARChart(MainForm, CInt(Combo_Charts_VARSeries.SelectedValue), pThisChartOnly, Combo_Charts_VARSeries.Text, Text_Chart_VARConfidence.Value, Text_Chart_RollingPeriod.Value, VARPeriodsPeriod, Text_Chart_Lamda.Value, GetScalingFactor(MainForm, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, CULng(Combo_Charts_VARSeries.SelectedValue), CULng(ComparisonSeries)), Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value) ' Text_ScalingFactor.Value

    Else

      Dim thisChart As Object

      SyncLock VARChartArrayList

        For Each thisChart In Me.VARChartArrayList

          If (thisChart.Visible) Then
            ' Set Prices Chart Series Count

            Set_VARChartDundas(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, "Portfolio", DateSeries, StdDevSeries, RollingReturnSeries, Text_Chart_VARConfidence.Value, RollingPeriod, VARPeriodsPeriod, False, True, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)

            ' Set_VARChart(MainForm, CInt(Combo_Charts_VARSeries.SelectedValue), thisChart, Combo_Charts_VARSeries.Text, Text_Chart_VARConfidence.Value, Text_Chart_RollingPeriod.Value, Text_Chart_VARPeriod.Value, Text_Chart_Lamda.Value, GetScalingFactor(MainForm, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, CULng(Combo_Charts_VARSeries.SelectedValue), CULng(ComparisonSeries)), Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)

          End If

        Next

      End SyncLock

    End If

  End Sub

  Private Sub PaintOmegaCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing, Optional ByVal pZoomReset As Boolean = True)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim DateSeries() As Date = FormSimulationParameters.PortfolioDateSeries
    Dim NAVSeries() As Double = FormSimulationParameters.PortfolioNAVSeries
    Dim RollingPeriod As Integer = (MainForm.StatFunctions.AnnualPeriodCount(FormSimulationParameters.StatsDatePeriod) * Edit_OmegaReturn.Value / 12)

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines
        Try
          Set_OmegaChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 0, "Portfolio", RollingPeriod, DateSeries, NAVSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Check_OmegaRatio.Checked, "TopLeft", pZoomReset)

          'Set_Chart_FromList(MainForm, _IDsToChart, CTAChartTypes.Omega, ThisChartOnly, ComparisonSeries, Combo_Chart_Conditional.SelectedValue, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Edit_OmegaReturn.Value, Text_Chart_Lamda.Value, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, Check_OmegaRatio.Checked)
        Catch ex As Exception
        End Try

        ' Set Comparison Instrument Line
        If (ComparisonSeries > 0) Then
          Set_OmegaChart(MainForm, ComparisonSeries, ThisChartOnly, -1, ComparisonSeriesText, Edit_OmegaReturn.Value, Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Check_OmegaRatio.Checked)
        End If

      End If

    Else

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock OmegaChartArrayList
        For Each thisChart In Me.OmegaChartArrayList
          If (thisChart.Visible) Then

            ' Set Instrument Lines
            Try

              Set_OmegaChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 0, "Portfolio", RollingPeriod, DateSeries, NAVSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Check_OmegaRatio.Checked, "TopLeft", pZoomReset)

              ' Set_Chart_FromList(MainForm, _IDsToChart, CTAChartTypes.Omega, thisChart, ComparisonSeries, Nz(Combo_Chart_Conditional.SelectedValue, StatFunctions.ContingentSelect.ConditionAll), Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Edit_OmegaReturn.Value, Text_Chart_Lamda.Value, Radio_DynamicScalingFactor.Checked, Text_ScalingFactor.Value, 1.0#, Check_OmegaRatio.Checked)
            Catch ex As Exception
            End Try

            ' Set Comparison Instrument Line
            If (ComparisonSeries > 0) Then
              Set_OmegaChart(MainForm, ComparisonSeries, thisChart, -1, ComparisonSeriesText, Edit_OmegaReturn.Value, Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, Check_OmegaRatio.Checked)
            End If

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintDrawdownCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing, Optional ByVal pZoomReset As Boolean = True)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim DateSeries() As Date = FormSimulationParameters.PortfolioDateSeries
    Dim NAVSeries() As Double = FormSimulationParameters.PortfolioNAVSeries

    Dim ComparisonSeries As Integer = (0)
    Dim ComparisonSeriesText As String = ""

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesPertrac.Text
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
          ComparisonSeriesText = Combo_Charts_CompareSeriesGroup.Text
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
      ComparisonSeriesText = ""
    End Try

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        ' Set Instrument Lines
        Try
          Set_DrawDownChart(MainForm, FormSimulationParameters.StatsDatePeriod, ThisChartOnly, 0, "Portfolio", DateSeries, NAVSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
        Catch ex As Exception
        End Try

        ' Set Comparison Instrument Line
        If (ComparisonSeries > 0) Then
          Set_DrawDownChart(MainForm, ComparisonSeries, ThisChartOnly, -1, ComparisonSeriesText, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
        End If

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock DrawdownChartArrayList
        For Each thisChart In Me.DrawdownChartArrayList
          If (thisChart.Visible) Then

            ' Set Instrument Lines
            Try
              Set_DrawDownChart(MainForm, FormSimulationParameters.StatsDatePeriod, thisChart, 0, "Portfolio", DateSeries, NAVSeries, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value, "TopLeft", pZoomReset)
            Catch ex As Exception
            End Try

            ' Set Comparison Instrument Line
            If (ComparisonSeries > 0) Then
              Set_DrawDownChart(MainForm, ComparisonSeries, thisChart, -1, ComparisonSeriesText, Text_Chart_RollingPeriod.Value, Text_Chart_Lamda.Value, 1.0#, Date_Charts_DateFrom.Value, Date_Charts_DateTo.Value)
            End If

          End If
        Next
      End SyncLock

    End If

  End Sub

  Private Sub Chart_Prices_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Prices.VisibleChanged, Chart_FrontPage_Prices.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintVAMICharts(thisChart, Nothing, True)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_Returns_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Returns.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintMonthlyCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_RollingReturn_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_RollingReturn.VisibleChanged, Chart_FrontPage_RollingReturn.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintRollingReturnCharts(thisChart, Nothing, True)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_ReturnScatter_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_ReturnScatter.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintReturnScatterCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_StdDev_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_StdDev.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintStdDevCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub C1Chart_VAR_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As C1.Win.C1Chart.C1Chart

      If (TypeOf sender Is C1.Win.C1Chart.C1Chart) Then
        thisChart = CType(sender, C1.Win.C1Chart.C1Chart)

        If thisChart.Visible Then
          PaintVARCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_VAR_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_VAR.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintVARCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_Omega_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Omega.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintOmegaCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Chart_DrawDown_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_DrawDown.VisibleChanged
    ' *****************************************************************************
    ' Routine to re-paint chart if it becomes visible.
    '
    ' The Tab Control sets the 'Visible' property to false for non-visible charts
    ' thus they are not painted when the instrument is selected, and they must be
    ' painted when they become visible.
    ' *****************************************************************************

    Try
      Dim thisChart As Dundas.Charting.WinControl.Chart

      If (TypeOf sender Is Dundas.Charting.WinControl.Chart) Then
        thisChart = CType(sender, Dundas.Charting.WinControl.Chart)

        If thisChart.Visible Then
          PaintDrawdownCharts(thisChart)
        End If

      End If
    Catch ex As Exception
    End Try
  End Sub


#End Region

#Region " Performance Chart creation"

  Private Sub SetPerformanceDataGrid()
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Dim TabCounter As Integer
    Dim InstrumentCounter As Integer
    Dim PertracName As String

    Dim thisTab As TabPage
    Dim thisGrid As C1.Win.C1FlexGrid.C1FlexGrid
    Dim thislabel As Label
    Dim ColCounter As Integer

    ' Initialise Tab Control
    If Me.TabControl_PerformanceTables.TabCount < 1 Then
      Dim FirstTab As TabPage
      Dim FirstGrid As C1.Win.C1FlexGrid.C1FlexGrid
      Dim Firstlabel As Label

      FirstTab = TabControl_PerformanceTables.TabPages(0)
      FirstGrid = Me.Grid_Performance_0
      Firstlabel = Me.Label_PT_0

      For TabCounter = TabControl_PerformanceTables.TabCount To (0)
        TabControl_PerformanceTables.TabPages.Add("TabPage" & (TabCounter + 1).ToString, "Stock" & (TabCounter + 1).ToString)

        thisTab = TabControl_PerformanceTables.TabPages(TabControl_PerformanceTables.TabCount - 1)

        thisTab.AutoScroll = FirstTab.AutoScroll
        thisTab.Location = New System.Drawing.Point(FirstTab.Left, FirstTab.Top)
        thisTab.Padding = New System.Windows.Forms.Padding(FirstTab.Padding.Top)
        thisTab.Size = New System.Drawing.Size(FirstTab.Width, FirstTab.Height)
        thisTab.UseVisualStyleBackColor = FirstTab.UseVisualStyleBackColor

        thislabel = New Label()
        thislabel.BackColor = System.Drawing.SystemColors.Window
        thislabel.BorderStyle = Firstlabel.BorderStyle
        thislabel.Location = New System.Drawing.Point(Firstlabel.Left, Firstlabel.Top)
        thislabel.Name = "Label_PT_" & TabCounter.ToString
        thislabel.Size = New System.Drawing.Size(Firstlabel.Width, Firstlabel.Height)
        thislabel.Anchor = AnchorStyles.Top Or AnchorStyles.Left Or AnchorStyles.Right
        thislabel.Text = "<none>"

        thisGrid = New C1.Win.C1FlexGrid.C1FlexGrid
        Try
          thisGrid.BeginInit()

          thisGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
           Or System.Windows.Forms.AnchorStyles.Left) _
           Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
          thisGrid.Cursor = System.Windows.Forms.Cursors.Default
          thisGrid.Location = New System.Drawing.Point(FirstGrid.Left, FirstGrid.Top)
          thisGrid.Name = "Grid_Performance_" & TabCounter.ToString
          thisGrid.Rows.DefaultSize = FirstGrid.Rows.DefaultSize
          thisGrid.Size = New System.Drawing.Size(FirstGrid.Width, FirstGrid.Height)
          thisGrid.TabIndex = 0
          thisGrid.Cols.Count = FirstGrid.Cols.Count
          thisGrid.Rows.Count = 2
          thisGrid.AutoClipboard = False
          '  Private Sub Grid_Performance_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Performance_0.KeyDown

          AddHandler thisGrid.KeyDown, AddressOf Grid_Performance_KeyDown
          thisGrid.ContextMenuStrip = New ContextMenuStrip
          thisGrid.ContextMenuStrip.Tag = thisGrid
          AddHandler thisGrid.ContextMenuStrip.Opening, AddressOf PerformanceGrid_cms_Opening
          thisGrid.ContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

          Try

            For ColCounter = 0 To (FirstGrid.Cols.Count - 1)
              thisGrid.Cols(ColCounter).AllowDragging = FirstGrid.Cols(ColCounter).AllowDragging
              thisGrid.Cols(ColCounter).AllowEditing = FirstGrid.Cols(ColCounter).AllowEditing
              thisGrid.Cols(ColCounter).AllowResizing = FirstGrid.Cols(ColCounter).AllowResizing
              thisGrid.Cols(ColCounter).AllowSorting = FirstGrid.Cols(ColCounter).AllowSorting
              thisGrid.Cols(ColCounter).Caption = FirstGrid.Cols(ColCounter).Caption
              thisGrid.Cols(ColCounter).DataType = FirstGrid.Cols(ColCounter).DataType
              thisGrid.Cols(ColCounter).Format = FirstGrid.Cols(ColCounter).Format
              thisGrid.Cols(ColCounter).TextAlign = FirstGrid.Cols(ColCounter).TextAlign
              thisGrid.Cols(ColCounter).Visible = FirstGrid.Cols(ColCounter).Visible
              thisGrid.Cols(ColCounter).Width = FirstGrid.Cols(ColCounter).Width
            Next
          Catch ex As Exception
          End Try

          thisGrid.Styles.Add("Positive").ForeColor = Color.Blue
          thisGrid.Styles.Add("Negative").ForeColor = Color.Red
          thisGrid.Styles.Add("TopRow", Grid_Performance_0.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.RightCenter
          thisGrid.Styles.Add("FirstCell", Grid_Performance_0.Rows(0).Style).TextAlign = C1.Win.C1FlexGrid.TextAlignEnum.LeftCenter

          thisGrid.Rows(0).Style = Grid_Performance_0.Styles("TopRow")
          thisGrid.SetCellStyle(0, 0, "FirstCell")

        Catch ex As Exception
        Finally
          thisGrid.EndInit()
        End Try

        thisTab.Controls.Add(thislabel)
        thisTab.Controls.Add(thisGrid)

      Next
    End If

    ' Discard Unused TabPages, it does not appear possible just to make them invisible

    If Me.TabControl_PerformanceTables.TabCount > 1 Then
      For TabCounter = (TabControl_PerformanceTables.TabCount - 1) To 1 Step -1
        If (TabCounter > 0) Then
          thisTab = TabControl_PerformanceTables.TabPages(TabCounter)
          TabControl_PerformanceTables.TabPages.RemoveAt(TabCounter)
          Try
            RemoveHandler CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).KeyDown, AddressOf Grid_Performance_KeyDown
            RemoveHandler CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip.Opening, AddressOf PerformanceGrid_cms_Opening
          Catch ex As Exception
          End Try
          Try
            CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip.Tag = Nothing
            CType(thisTab.Controls(1), C1.Win.C1FlexGrid.C1FlexGrid).ContextMenuStrip = Nothing
          Catch ex As Exception
          End Try

          Try
            thisTab.Controls.Clear()
            thisTab.Dispose()
          Catch ex As Exception
          End Try
        End If
      Next
    End If

    ' Show all remaining TabPages

    TabControl_PerformanceTables.TabPages(0).Show()

    ' Set Label and Chart Data

    PertracName = "Portfolio"

    Try
      MainForm.CTAStatusLabel.Text = "Loading " & PertracName
      MainForm.CTAStatusStrip.Refresh()
    Catch ex As Exception
    End Try

    Dim StartYear As Integer
    Dim ThisReturn As Double
    Dim RowCounter As Integer
    Dim YTD_Sum As Double
    Dim DateIndex As Integer

    Try
      thisTab = TabControl_PerformanceTables.TabPages(InstrumentCounter)
      thislabel = thisTab.Controls("Label_PT_" & InstrumentCounter.ToString)
      thisGrid = thisTab.Controls("Grid_Performance_" & InstrumentCounter.ToString)

      If (thisGrid IsNot Nothing) Then
        thisGrid.Tag = 0
      End If

      thisGrid.Rows.Count = 1
      thisTab.Text = PertracName
      thisTab.ToolTipText = PertracName
      thislabel.Text = PertracName

      Dim DateSeries() As Date = FormSimulationParameters.PortfolioDateSeries
      Dim NAVSeries() As Double = FormSimulationParameters.PortfolioNAVSeries

      If (DateSeries IsNot Nothing) AndAlso (DateSeries.Length > 1) Then

        StartYear = DateSeries(1).Year

        thisGrid.Rows.Count = (DateSeries(DateSeries.Length - 1).Year - StartYear) + 2

        For DateIndex = 1 To (DateSeries.Length - 1)
          ThisReturn = (NAVSeries(DateIndex) / NAVSeries(DateIndex - 1)) - 1.0#
          thisGrid.Item((DateSeries(DateIndex).Year - StartYear) + 1, DateSeries(DateIndex).Month) = ThisReturn

          If (ThisReturn < 0) Then
            thisGrid.SetCellStyle((DateSeries(DateIndex).Year - StartYear) + 1, DateSeries(DateIndex).Month, "Negative")
          Else
            thisGrid.SetCellStyle((DateSeries(DateIndex).Year - StartYear) + 1, DateSeries(DateIndex).Month, "Positive")
          End If
        Next

        ' Set YTD
        For RowCounter = 1 To (thisGrid.Rows.Count - 1)
          YTD_Sum = 1
          thisGrid.Item(RowCounter, 0) = (StartYear + RowCounter) - 1

          For ColCounter = 1 To 12
            Try
              YTD_Sum *= (1.0 + CDbl(thisGrid.Item(RowCounter, ColCounter)))
            Catch ex As Exception
            End Try
          Next

          thisGrid.Item(RowCounter, 13) = (YTD_Sum - 1)
          If ((YTD_Sum - 1) < 0) Then
            thisGrid.SetCellStyle(RowCounter, 13, "Negative")
          Else
            thisGrid.SetCellStyle(RowCounter, 13, "Positive")
          End If
        Next
      End If
    Catch ex As Exception
    End Try

    MainForm.CTAStatusLabel.Text = ""
    MainForm.CTAStatusStrip.Refresh()

  End Sub

#End Region

#Region " Chart DoubleClick Events"

  Private Sub Chart_Prices_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Prices.DoubleClick, Chart_FrontPage_Prices.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_CTAForm(CTAFormID.frmViewChart).Form
    newChartForm.CTAParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintVAMICharts
    newChartForm.FormChart = sender
    newChartForm.ParentChartList = PriceChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock PriceChartArrayList
      PriceChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_Returns_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Returns.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_CTAForm(CTAFormID.frmViewChart).Form
    newChartForm.CTAParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintMonthlyCharts
    newChartForm.FormChart = Chart_Returns
    newChartForm.ParentChartList = ReturnsChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock ReturnsChartArrayList
      ReturnsChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_RollingReturn_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_RollingReturn.DoubleClick, Chart_FrontPage_RollingReturn.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_CTAForm(CTAFormID.frmViewChart).Form
    newChartForm.CTAParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintRollingReturnCharts
    newChartForm.FormChart = Chart_RollingReturn
    newChartForm.ParentChartList = RollingReturnChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock RollingReturnChartArrayList
      RollingReturnChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_ReturnScatter_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_ReturnScatter.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_CTAForm(CTAFormID.frmViewChart).Form
    newChartForm.CTAParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintReturnScatterCharts
    newChartForm.FormChart = Chart_ReturnScatter
    newChartForm.ParentChartList = ReturnScatterChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock ReturnScatterChartArrayList
      ReturnScatterChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_StdDev_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_StdDev.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_CTAForm(CTAFormID.frmViewChart).Form
    newChartForm.CTAParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintStdDevCharts
    newChartForm.FormChart = Chart_StdDev
    newChartForm.ParentChartList = StdDevChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock StdDevChartArrayList
      StdDevChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub C1Chart_VAR_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_CTAForm(CTAFormID.frmViewChart).Form
    newChartForm.CTAParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintVARCharts
    newChartForm.FormChart = sender
    newChartForm.ParentChartList = VARChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock VARChartArrayList
      VARChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_VAR_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_VAR.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_CTAForm(CTAFormID.frmViewChart).Form
    newChartForm.CTAParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintVARCharts
    newChartForm.FormChart = sender
    newChartForm.ParentChartList = VARChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock VARChartArrayList
      VARChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_Omega_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Omega.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_CTAForm(CTAFormID.frmViewChart).Form
    newChartForm.CTAParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintOmegaCharts
    newChartForm.FormChart = sender
    newChartForm.ParentChartList = OmegaChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock OmegaChartArrayList
      OmegaChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_Drawdown_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_DrawDown.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_CTAForm(CTAFormID.frmViewChart).Form
    newChartForm.CTAParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintDrawdownCharts
    newChartForm.FormChart = Chart_DrawDown
    newChartForm.ParentChartList = DrawdownChartArrayList
    newChartForm.Text = "Chart " & TabControl_Charts.SelectedTab.Text

    SyncLock DrawdownChartArrayList
      DrawdownChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

#End Region

#Region " Grid Context Menu Code / Grid Ctrl-C Code. Also 'Copy-All-Grids-to-Clipboard' Code. "

  Sub PerformanceGrid_cms_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim CustomFieldID As Integer = 0
    Dim PertracID As Integer = 0
    Dim GroupID As Integer = 0
    Dim ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid
    Dim ThisMenuStrip As ContextMenuStrip

    If Not (TypeOf (CType(sender, ContextMenuStrip).Tag) Is C1FlexGrid) Then
      Exit Sub
    End If
    ThisGrid = CType(CType(sender, ContextMenuStrip).Tag, C1FlexGrid)
    ThisMenuStrip = ThisGrid.ContextMenuStrip
    If (ThisMenuStrip Is Nothing) Then
      Exit Sub
    End If

    Try

      ' Clear the ContextMenuStrip control's 
      ' Items collection.
      ThisMenuStrip.Items.Clear()

      ThisMenuStrip.Items.Add("Copy Selected prices", Nothing, AddressOf Grid_Menu_Copy)
      ThisMenuStrip.Items.Add("Copy All Prices, This Instrument only", Nothing, AddressOf Grid_Menu_CopyAllRows)
      ThisMenuStrip.Items.Add(New ToolStripSeparator)
      ThisMenuStrip.Items.Add("Copy All Prices, All Selected Instruments as a Grid", Nothing, AddressOf Grid_Menu_CopyAllGridsAsGrid)
      ThisMenuStrip.Items.Add("Copy All Prices, All Selected Instruments as a Long List", Nothing, AddressOf Grid_Menu_CopyAllGridsAsList)


    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Menu_Copy(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      If Not (TypeOf CType(sender, ToolStripMenuItem).Owner Is ContextMenuStrip) Then
        Exit Sub
      End If
      If Not (TypeOf (CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag) Is C1FlexGrid) Then
        Exit Sub
      End If

      Call MainForm.CopyGridSelection(CType(CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag, C1.Win.C1FlexGrid.C1FlexGrid), True, True)
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Menu_CopyAllRows(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      If Not (TypeOf CType(sender, ToolStripMenuItem).Owner Is ContextMenuStrip) Then
        Exit Sub
      End If
      If Not (TypeOf (CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag) Is C1FlexGrid) Then
        Exit Sub
      End If

      Call MainForm.CopyGridSelection(CType(CType(CType(sender, ToolStripMenuItem).Owner, ContextMenuStrip).Tag, C1.Win.C1FlexGrid.C1FlexGrid), True, True, True)
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Menu_CopyAllGridsAsGrid(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      Call CopyAllPerformanceGrids(True)
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Menu_CopyAllGridsAsList(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *********************************************************************************************************
    '
    ' *********************************************************************************************************

    Try
      Call CopyAllPerformanceGrids(False)
    Catch ex As Exception
    End Try
  End Sub

  Public Sub CopyAllPerformanceGrids(ByVal PresentAsAGrid As Boolean)
    '' *********************************************************************************************************
    ''
    '' *********************************************************************************************************
    'Dim ClipString As String = ""
    'Dim HeaderString As String = ""

    'Dim InstrumentCounter As Integer
    'Dim DateCounter As Integer
    'Dim ReturnCounter As Integer
    'Dim PertracID As Integer
    'Dim ThisInstrumentID As ULong
    'Dim PertracName As String
    'Dim MinDate As Date = RenaissanceGlobals.Globals.Renaissance_EndDate_Data
    'Dim MaxDate As Date = RenaissanceGlobals.Globals.Renaissance_BaseDate
    'Dim PeriodCount As Integer
    'Dim TempDateSeries() As Date
    'Dim DateArray() As Date
    'Dim IDArray() As Integer
    'Dim ReturnsArray(-1, -1) As Double
    'Dim TempReturnSeries() As Double

    'Try
    '  If (List_SelectItems.SelectedItems.Count <= 0) Then
    '    Exit Sub
    '  End If

    '  If (PresentAsAGrid) Then

    '    ' Iterate through selected instruments in order to get Max / Min dates and thus allow the result Arrays to be dimensioned.

    '    For InstrumentCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)

    '      Try
    '        PertracID = CInt(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.ValueMember))
    '        PertracName = CStr(Nz(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.DisplayMember), "<Missing>"))
    '        ThisInstrumentID = MainForm.StatFunctions.CombinedStatsID(PertracID, False)


    '        Try
    '          MainForm.CTAStatusLabel.Text = "Loading " & PertracName
    '          MainForm.CTAStatusStrip.Refresh()
    '        Catch ex As Exception
    '        End Try

    '      Catch ex As Exception
    '        PertracID = 0
    '        PertracName = "<Error>"
    '      End Try

    '      If (PertracID > 0) Then
    '        TempDateSeries = MainForm.StatFunctions.DateSeries(ThisInstrumentID, False, 12, 1, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)

    '        If (TempDateSeries IsNot Nothing) AndAlso (TempDateSeries.Length > 0) Then
    '          If (TempDateSeries(0) < MinDate) Then MinDate = TempDateSeries(0)
    '          If (TempDateSeries(TempDateSeries.Length - 1) > MaxDate) Then MaxDate = TempDateSeries(TempDateSeries.Length - 1)
    '        End If
    '      End If

    '    Next

    '    ' Dimension Arrays

    '    PeriodCount = RenaissanceUtilities.DatePeriodFunctions.GetPeriodCount(MainForm.StatFunctions.StatsDatePeriod, MinDate, MaxDate)
    '    ReDim DateArray(PeriodCount - 1)
    '    ReDim IDArray(List_SelectItems.SelectedItems.Count - 1)
    '    ReDim ReturnsArray(PeriodCount - 1, List_SelectItems.SelectedItems.Count - 1)

    '    DateArray(0) = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(MainForm.StatFunctions.StatsDatePeriod, MinDate, True)
    '    DateCounter = 1
    '    While (DateCounter < DateArray.Length)
    '      DateArray(DateCounter) = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(MainForm.StatFunctions.StatsDatePeriod, DateArray(DateCounter - 1), 1)
    '      DateCounter += 1
    '    End While

    '    ' All data returned in a wide grid

    '    HeaderString = "Date" & Chr(9)
    '    For InstrumentCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)

    '      ' Resolve Instrument ID.
    '      ' Am using the Stats code as it returns data in a standardised format. One value per period only.

    '      Try
    '        PertracID = CInt(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.ValueMember))
    '        PertracName = CStr(Nz(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.DisplayMember), "<Missing>"))
    '        ThisInstrumentID = MainForm.StatFunctions.CombinedStatsID(PertracID, False)
    '      Catch ex As Exception
    '        PertracID = 0
    '        PertracName = "<Error>"
    '      End Try

    '      Try
    '        MainForm.CTAStatusLabel.Text = "Loading " & PertracName
    '        MainForm.CTAStatusStrip.Refresh()
    '      Catch ex As Exception
    '      End Try

    '      HeaderString &= "(" & PertracID & ") " & PertracName & Chr(9)

    '      ' Populate the ReturnsArray with the correct data.
    '      ' Assume that the date ranges for each instrument are likely to differ.

    '      If (PertracID > 0) Then
    '        TempDateSeries = MainForm.StatFunctions.DateSeries(ThisInstrumentID, False, 12, 1, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)
    '        TempReturnSeries = MainForm.StatFunctions.ReturnSeries(ThisInstrumentID, False, 12, 1, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)

    '        If (TempDateSeries IsNot Nothing) AndAlso (TempDateSeries.Length > 0) Then
    '          Dim ReturnIndex As Integer

    '          ReturnIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(MainForm.StatFunctions.StatsDatePeriod, MinDate, TempDateSeries(0))

    '          For ReturnCounter = 0 To (TempDateSeries.Length - 1)
    '            ReturnsArray(ReturnIndex, InstrumentCounter) = TempReturnSeries(ReturnCounter)
    '            ReturnIndex += 1
    '          Next

    '        End If
    '      End If

    '    Next

    '    HeaderString &= Chr(13)

    '    ' Build the ClipString 
    '    ' ReturnsArray(PeriodCount - 1, List_SelectItems.SelectedItems.Count - 1)

    '    ClipString = ""

    '    For DateCounter = 0 To (PeriodCount - 1)
    '      ClipString &= DateArray(DateCounter).ToString(QUERY_SHORTDATEFORMAT) & Chr(9)

    '      For ReturnCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
    '        ClipString &= ReturnsArray(DateCounter, ReturnCounter).ToString("###0.00##%") & Chr(9)
    '      Next

    '      ClipString &= Chr(13)
    '    Next

    '  Else
    '    ' All data returned in a long Column...

    '    HeaderString = "ID" & Chr(9) & "Date" & Chr(9) & "Return" & Chr(13)
    '    ClipString = ""

    '    Dim PertracIDString As String

    '    For InstrumentCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)

    '      Try
    '        PertracID = CInt(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.ValueMember))
    '        PertracName = CStr(Nz(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.DisplayMember), "<Missing>"))
    '        ThisInstrumentID = MainForm.StatFunctions.CombinedStatsID(PertracID, False)
    '      Catch ex As Exception
    '        PertracID = 0
    '        PertracName = "<Error>"
    '      End Try
    '      PertracIDString = PertracID.ToString

    '      Try
    '        MainForm.CTAStatusLabel.Text = "Loading " & PertracName
    '        MainForm.CTAStatusStrip.Refresh()
    '      Catch ex As Exception
    '      End Try

    '      If (PertracID > 0) Then
    '        TempDateSeries = MainForm.StatFunctions.DateSeries(ThisInstrumentID, False, 12, 1, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)
    '        TempReturnSeries = MainForm.StatFunctions.ReturnSeries(ThisInstrumentID, False, 12, 1, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)


    '        If (TempDateSeries IsNot Nothing) AndAlso (TempDateSeries.Length > 0) Then

    '          For ReturnCounter = 0 To (TempDateSeries.Length - 1)

    '            ClipString &= PertracIDString & Chr(9) & FitDateToPeriod(MainForm.StatFunctions.StatsDatePeriod, TempDateSeries(ReturnCounter), True).ToString(QUERY_SHORTDATEFORMAT) & Chr(9) & TempReturnSeries(ReturnCounter).ToString("###0.00##%") & Chr(13)

    '          Next

    '        End If

    '      End If

    '    Next

    '  End If

    'Catch ex As Exception
    'Finally
    '  Try
    '    MainForm.CTAStatusLabel.Text = ""
    '    MainForm.CTAStatusStrip.Refresh()
    '  Catch ex As Exception
    '  End Try

    '  Try
    '    Clipboard.Clear()
    '    Clipboard.SetData(System.Windows.Forms.DataFormats.Text, (HeaderString & ClipString))
    '  Catch ex As Exception
    '  End Try
    'End Try


  End Sub

  Private Sub Grid_Performance_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) ' Handles Grid_Performance_0.KeyDown
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, True)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Comp_Statistics_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Stats_Drawdowns_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Stats_Drawdowns.KeyDown
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_SimpleStatistics_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_SimpleStatistics.KeyDown
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub List_SelectItems_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *************************************************************************************
    ' Clipboard 'Copy' functionality
    '
    '
    ' *************************************************************************************

    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyListSelection(CType(sender, ListBox))
        e.SuppressKeyPress = True
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private _validData As Boolean

  Private Sub List_SelectItems_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs)
    'get the data
    Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

    'no empty data
    If DragString Is Nothing OrElse DragString.Length = 0 Then
      _validData = False
      Return
    End If

    If (DragString IsNot Nothing) AndAlso (DragString.StartsWith(CTA_DRAG_IDs_HEADER)) Then
      If DragString.Split(",").Length > 1 Then
        _validData = True
        Return
      End If
    End If

    _validData = False

  End Sub

  Private Sub List_SelectItems_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs)
    _validData = False
  End Sub

  Private Sub List_SelectItems_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs)
    If _validData Then
      e.Effect = DragDropEffects.Copy
    Else
      e.Effect = DragDropEffects.None
    End If

  End Sub

#End Region

  Private Function GetIDsFromList(ByVal pList As ListBox) As Integer()
    ' *************************************************************************************
    '
    '
    ' *************************************************************************************

    Dim RVal(-1) As Integer
    Dim InstrumentCounter As Integer

    Try
      If (pList.SelectedItems.Count > 0) Then

        ReDim RVal(pList.SelectedItems.Count - 1)

        For InstrumentCounter = 0 To (pList.SelectedItems.Count - 1)
          RVal(InstrumentCounter) = CInt(pList.SelectedItems(InstrumentCounter)(pList.ValueMember))
        Next

      End If
    Catch ex As Exception
      ReDim RVal(-1)
    End Try

    Return RVal

  End Function

  Private Function GetLatestStartDateFromList(ByVal pList As ListBox) As Date
    ' *************************************************************************************
    '
    '
    ' *************************************************************************************

    Dim RVal As Date = Renaissance_BaseDate
    Dim PertracIDs() As Integer = GetIDsFromList(pList)
    Dim InstrumentCounter As Integer
    Dim ThisDate As Date
    Dim EarliestDate As Date = Renaissance_BaseDate

    If (PertracIDs IsNot Nothing) AndAlso (PertracIDs.Count > 0) Then

      For InstrumentCounter = 0 To (PertracIDs.Count - 1)

        ThisDate = MainForm.StatFunctions.GetStartDate(PertracIDs(InstrumentCounter))

        If (ThisDate > EarliestDate) Then
          EarliestDate = ThisDate
        End If

      Next

    End If

    ' Add in the Comparison Series

    Dim ComparisonSeries As Integer = (0)

    Try
      If (Radio_Charts_ComparePertrac.Checked) Then
        If (Combo_Charts_CompareSeriesPertrac.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesPertrac.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesPertrac.SelectedValue)
        End If
      Else
        If (Combo_Charts_CompareSeriesGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Charts_CompareSeriesGroup.SelectedValue)) Then
          ComparisonSeries = CInt(Combo_Charts_CompareSeriesGroup.SelectedValue)
        End If
      End If
    Catch ex As Exception
      ComparisonSeries = 0
    End Try

    If (ComparisonSeries > 0) Then
      ThisDate = MainForm.StatFunctions.GetStartDate(ComparisonSeries)

      If (ThisDate > EarliestDate) Then
        EarliestDate = ThisDate
      End If
    End If

    ' Return date as appropriate 

    If (EarliestDate < Renaissance_EndDate_Data) Then
      Return EarliestDate
    Else
      Return Renaissance_BaseDate
    End If

  End Function

  Private Sub ShowChartsReport(ByRef pPageImageArray As ArrayList)
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************

    Try

      Dim newForm As frmViewReport

      newForm = MainForm.New_CTAForm(CTAFormID.frmViewReport).Form

      ' Display Report Form.

      Try

        If Not (newForm Is Nothing) Then

          newForm.Opacity = 0

          newForm.ReportPreview.Document = pPageImageArray

          Dim OCount As Integer
          newForm.Show()

          If MainForm.EnableReportFadeIn Then

            Try
              Dim TStart As Date = Now()
              Dim TEnd As Date

              For OCount = 0 To 100 Step 5
                newForm.Opacity = OCount / 100
                Threading.Thread.Sleep(10)
              Next

              TEnd = Now()
              If (TEnd - TStart).TotalMilliseconds >= 1000 Then
                MainForm.DisableReportFadeIn()
              End If

            Catch ex As Exception
            End Try

          Else
            newForm.Opacity = 1.0#
          End If
        End If

      Catch ex As Exception
      End Try

    Catch ex As Exception
    End Try


  End Sub








End Class
