Public NotInheritable Class CTASplash


  Private Sub CTASplash_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

  End Sub

  Public EndTime As Date

  Private Sub Splash_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    Try
      ClosingTimer.Stop()
    Catch ex As Exception
    End Try
  End Sub

  Public Sub StartTimer(ByVal pEndTime As Date)
    EndTime = pEndTime

    Me.ClosingTimer.Start()

  End Sub

  Private Sub ClosingTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClosingTimer.Tick
    If (Now > EndTime) Then
      Me.ClosingTimer.Stop()
      Me.Close()
    End If
  End Sub

End Class
