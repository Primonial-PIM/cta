Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals


Public Class frmManageFolders

  Inherits System.Windows.Forms.Form
  Implements StandardCTAForm

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents btnSave As System.Windows.Forms.Button
  Friend WithEvents btnClose As System.Windows.Forms.Button
  Friend WithEvents TreeView_Instruments As System.Windows.Forms.TreeView
  Friend WithEvents btnDeleteFolder As System.Windows.Forms.Button
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Label_MovingAverage As System.Windows.Forms.Label
  Friend WithEvents Label_DataPeriod As System.Windows.Forms.Label
  Friend WithEvents Label_InterestRate As System.Windows.Forms.Label
  Friend WithEvents Label_Instrument As System.Windows.Forms.Label
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Check_ExponentialPriceAverage As System.Windows.Forms.CheckBox
  Friend WithEvents Radio_UnAdjustedAverage As System.Windows.Forms.RadioButton
  Friend WithEvents Label8 As System.Windows.Forms.Label
  Friend WithEvents Radio_ExponentialWeightedAverage As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_VolatilityWeightedDuration As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_LinearWeightedAverage As System.Windows.Forms.RadioButton
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
  Friend WithEvents Label7 As System.Windows.Forms.Label
  Friend WithEvents Label10 As System.Windows.Forms.Label
  Friend WithEvents Label33 As System.Windows.Forms.Label
  Friend WithEvents Check_Trading_AllowShort As System.Windows.Forms.CheckBox
  Friend WithEvents Check_Trading_AllowLong As System.Windows.Forms.CheckBox
  Friend WithEvents Label32 As System.Windows.Forms.Label
  Friend WithEvents Check_Trading_MonthlyDrawdownReset As System.Windows.Forms.CheckBox
  Friend WithEvents Label31 As System.Windows.Forms.Label
  Friend WithEvents Panel_Trading_BandWeighting As System.Windows.Forms.Panel
  Friend WithEvents Check_Trading_PhaseOutAtCrossing As System.Windows.Forms.CheckBox
  Friend WithEvents Label28 As System.Windows.Forms.Label
  Friend WithEvents Label26 As System.Windows.Forms.Label
  Friend WithEvents Label25 As System.Windows.Forms.Label
  Friend WithEvents Label9 As System.Windows.Forms.Label
  Friend WithEvents Label11 As System.Windows.Forms.Label
  Friend WithEvents Radio_Trading_ByPercentageBand As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Trading_ByAverageZScore As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Trading_ByIndexZScore As System.Windows.Forms.RadioButton
  Friend WithEvents Check_Trading_BandWeightings As System.Windows.Forms.CheckBox
  Friend WithEvents Panel7 As System.Windows.Forms.Panel
  Friend WithEvents Label27 As System.Windows.Forms.Label
  Friend WithEvents Label22 As System.Windows.Forms.Label
  Friend WithEvents Radio_Trading_VolWeight_VsSecondDuration As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Trading_VolWeightNone As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Trading_VolWeightByHighStDev As System.Windows.Forms.RadioButton
  Friend WithEvents Label12 As System.Windows.Forms.Label
  Friend WithEvents Label15 As System.Windows.Forms.Label
  Friend WithEvents Label13 As System.Windows.Forms.Label
  Friend WithEvents Group_FirstTrade As System.Windows.Forms.Panel
  Friend WithEvents Radio_Trading_FirstTradeStart As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Trading_FirstTradeCrossing As System.Windows.Forms.RadioButton
  Friend WithEvents Label14 As System.Windows.Forms.Label
  Friend WithEvents Label16 As System.Windows.Forms.Label
  Friend WithEvents Label17 As System.Windows.Forms.Label
  Friend WithEvents Text_FundingSpread As System.Windows.Forms.TextBox
  Friend WithEvents Text_TrandingCosts As System.Windows.Forms.TextBox
  Friend WithEvents Text_VolatilityAverage_Lambda As System.Windows.Forms.TextBox
  Friend WithEvents Text_LinearAverage_Lambda As System.Windows.Forms.TextBox
  Friend WithEvents Text_ExponentialAverage_Lambda As System.Windows.Forms.TextBox
  Friend WithEvents Text_ExponentialPrice_Lambda As System.Windows.Forms.TextBox
  Friend WithEvents Text_MovingAverageDays As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_BaselineGearing As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_RebalanceThreshold As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_MonthlyDrawdownResetCount As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_MaxDrawdown As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_StartDate As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_CrossingUnweightPoint As System.Windows.Forms.TextBox
  Friend WithEvents Text_LocalHighExclusionPeriod As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_BandDeltaLimit As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_ReInvestmentEvaluationPeriod As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_WeightingDecayFactor As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_PercentBand As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_ZScore_Period As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_AverageZScore As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_IndexZScore As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_VolWeightDuration As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_VolWeightCap As System.Windows.Forms.TextBox
  Friend WithEvents Text_Trading_VolExponent As System.Windows.Forms.TextBox
  Friend WithEvents btnAddFolder As System.Windows.Forms.Button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.btnClose = New System.Windows.Forms.Button
    Me.TreeView_Instruments = New System.Windows.Forms.TreeView
    Me.btnAddFolder = New System.Windows.Forms.Button
    Me.btnDeleteFolder = New System.Windows.Forms.Button
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.GroupBox2 = New System.Windows.Forms.GroupBox
    Me.Text_Trading_BaselineGearing = New System.Windows.Forms.TextBox
    Me.Text_Trading_RebalanceThreshold = New System.Windows.Forms.TextBox
    Me.Text_Trading_MonthlyDrawdownResetCount = New System.Windows.Forms.TextBox
    Me.Text_Trading_MaxDrawdown = New System.Windows.Forms.TextBox
    Me.Text_Trading_StartDate = New System.Windows.Forms.TextBox
    Me.Label33 = New System.Windows.Forms.Label
    Me.Check_Trading_AllowShort = New System.Windows.Forms.CheckBox
    Me.Check_Trading_AllowLong = New System.Windows.Forms.CheckBox
    Me.Label32 = New System.Windows.Forms.Label
    Me.Check_Trading_MonthlyDrawdownReset = New System.Windows.Forms.CheckBox
    Me.Label31 = New System.Windows.Forms.Label
    Me.Panel_Trading_BandWeighting = New System.Windows.Forms.Panel
    Me.Text_Trading_CrossingUnweightPoint = New System.Windows.Forms.TextBox
    Me.Text_LocalHighExclusionPeriod = New System.Windows.Forms.TextBox
    Me.Text_Trading_BandDeltaLimit = New System.Windows.Forms.TextBox
    Me.Text_Trading_ReInvestmentEvaluationPeriod = New System.Windows.Forms.TextBox
    Me.Text_Trading_WeightingDecayFactor = New System.Windows.Forms.TextBox
    Me.Text_Trading_PercentBand = New System.Windows.Forms.TextBox
    Me.Text_Trading_ZScore_Period = New System.Windows.Forms.TextBox
    Me.Text_Trading_AverageZScore = New System.Windows.Forms.TextBox
    Me.Text_Trading_IndexZScore = New System.Windows.Forms.TextBox
    Me.Check_Trading_PhaseOutAtCrossing = New System.Windows.Forms.CheckBox
    Me.Label28 = New System.Windows.Forms.Label
    Me.Label26 = New System.Windows.Forms.Label
    Me.Label25 = New System.Windows.Forms.Label
    Me.Label9 = New System.Windows.Forms.Label
    Me.Label11 = New System.Windows.Forms.Label
    Me.Radio_Trading_ByPercentageBand = New System.Windows.Forms.RadioButton
    Me.Radio_Trading_ByAverageZScore = New System.Windows.Forms.RadioButton
    Me.Radio_Trading_ByIndexZScore = New System.Windows.Forms.RadioButton
    Me.Check_Trading_BandWeightings = New System.Windows.Forms.CheckBox
    Me.Panel7 = New System.Windows.Forms.Panel
    Me.Text_Trading_VolWeightDuration = New System.Windows.Forms.TextBox
    Me.Text_Trading_VolWeightCap = New System.Windows.Forms.TextBox
    Me.Text_Trading_VolExponent = New System.Windows.Forms.TextBox
    Me.Label27 = New System.Windows.Forms.Label
    Me.Label22 = New System.Windows.Forms.Label
    Me.Radio_Trading_VolWeight_VsSecondDuration = New System.Windows.Forms.RadioButton
    Me.Radio_Trading_VolWeightNone = New System.Windows.Forms.RadioButton
    Me.Radio_Trading_VolWeightByHighStDev = New System.Windows.Forms.RadioButton
    Me.Label12 = New System.Windows.Forms.Label
    Me.Label15 = New System.Windows.Forms.Label
    Me.Label13 = New System.Windows.Forms.Label
    Me.Group_FirstTrade = New System.Windows.Forms.Panel
    Me.Radio_Trading_FirstTradeStart = New System.Windows.Forms.RadioButton
    Me.Radio_Trading_FirstTradeCrossing = New System.Windows.Forms.RadioButton
    Me.Label14 = New System.Windows.Forms.Label
    Me.Label16 = New System.Windows.Forms.Label
    Me.Label17 = New System.Windows.Forms.Label
    Me.GroupBox3 = New System.Windows.Forms.GroupBox
    Me.Text_FundingSpread = New System.Windows.Forms.TextBox
    Me.Text_TrandingCosts = New System.Windows.Forms.TextBox
    Me.Label7 = New System.Windows.Forms.Label
    Me.Label10 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.Text_VolatilityAverage_Lambda = New System.Windows.Forms.TextBox
    Me.Text_LinearAverage_Lambda = New System.Windows.Forms.TextBox
    Me.Text_ExponentialAverage_Lambda = New System.Windows.Forms.TextBox
    Me.Text_ExponentialPrice_Lambda = New System.Windows.Forms.TextBox
    Me.Text_MovingAverageDays = New System.Windows.Forms.TextBox
    Me.Label6 = New System.Windows.Forms.Label
    Me.Check_ExponentialPriceAverage = New System.Windows.Forms.CheckBox
    Me.Radio_UnAdjustedAverage = New System.Windows.Forms.RadioButton
    Me.Label8 = New System.Windows.Forms.Label
    Me.Radio_ExponentialWeightedAverage = New System.Windows.Forms.RadioButton
    Me.Radio_VolatilityWeightedDuration = New System.Windows.Forms.RadioButton
    Me.Radio_LinearWeightedAverage = New System.Windows.Forms.RadioButton
    Me.Label_MovingAverage = New System.Windows.Forms.Label
    Me.Label_DataPeriod = New System.Windows.Forms.Label
    Me.Label_InterestRate = New System.Windows.Forms.Label
    Me.Label_Instrument = New System.Windows.Forms.Label
    Me.Label4 = New System.Windows.Forms.Label
    Me.Label5 = New System.Windows.Forms.Label
    Me.Label3 = New System.Windows.Forms.Label
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label1 = New System.Windows.Forms.Label
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    Me.Panel_Trading_BandWeighting.SuspendLayout()
    Me.Panel7.SuspendLayout()
    Me.Group_FirstTrade.SuspendLayout()
    Me.GroupBox3.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(470, 539)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 1
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(353, 539)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 4
    Me.btnSave.Text = "&Save"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(551, 539)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 2
    Me.btnClose.Text = "&Close"
    '
    'TreeView_Instruments
    '
    Me.TreeView_Instruments.AllowDrop = True
    Me.TreeView_Instruments.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TreeView_Instruments.LabelEdit = True
    Me.TreeView_Instruments.Location = New System.Drawing.Point(2, 2)
    Me.TreeView_Instruments.Name = "TreeView_Instruments"
    Me.TreeView_Instruments.Size = New System.Drawing.Size(401, 514)
    Me.TreeView_Instruments.Sorted = True
    Me.TreeView_Instruments.TabIndex = 0
    '
    'btnAddFolder
    '
    Me.btnAddFolder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnAddFolder.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAddFolder.Location = New System.Drawing.Point(124, 539)
    Me.btnAddFolder.Name = "btnAddFolder"
    Me.btnAddFolder.Size = New System.Drawing.Size(76, 28)
    Me.btnAddFolder.TabIndex = 5
    Me.btnAddFolder.Text = "Add Folder"
    '
    'btnDeleteFolder
    '
    Me.btnDeleteFolder.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnDeleteFolder.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDeleteFolder.Location = New System.Drawing.Point(224, 539)
    Me.btnDeleteFolder.Name = "btnDeleteFolder"
    Me.btnDeleteFolder.Size = New System.Drawing.Size(76, 28)
    Me.btnDeleteFolder.TabIndex = 6
    Me.btnDeleteFolder.Text = "Delete Folder"
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.SplitContainer1.Location = New System.Drawing.Point(2, 2)
    Me.SplitContainer1.Name = "SplitContainer1"
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.TreeView_Instruments)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.Panel1)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label_MovingAverage)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label_DataPeriod)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label_InterestRate)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label_Instrument)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label4)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label5)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
    Me.SplitContainer1.Panel2.Controls.Add(Me.Label1)
    Me.SplitContainer1.Size = New System.Drawing.Size(806, 518)
    Me.SplitContainer1.SplitterDistance = 405
    Me.SplitContainer1.TabIndex = 7
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel1.AutoScroll = True
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel1.Controls.Add(Me.GroupBox2)
    Me.Panel1.Controls.Add(Me.GroupBox3)
    Me.Panel1.Controls.Add(Me.GroupBox1)
    Me.Panel1.Location = New System.Drawing.Point(27, 124)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(360, 381)
    Me.Panel1.TabIndex = 9
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.Text_Trading_BaselineGearing)
    Me.GroupBox2.Controls.Add(Me.Text_Trading_RebalanceThreshold)
    Me.GroupBox2.Controls.Add(Me.Text_Trading_MonthlyDrawdownResetCount)
    Me.GroupBox2.Controls.Add(Me.Text_Trading_MaxDrawdown)
    Me.GroupBox2.Controls.Add(Me.Text_Trading_StartDate)
    Me.GroupBox2.Controls.Add(Me.Label33)
    Me.GroupBox2.Controls.Add(Me.Check_Trading_AllowShort)
    Me.GroupBox2.Controls.Add(Me.Check_Trading_AllowLong)
    Me.GroupBox2.Controls.Add(Me.Label32)
    Me.GroupBox2.Controls.Add(Me.Check_Trading_MonthlyDrawdownReset)
    Me.GroupBox2.Controls.Add(Me.Label31)
    Me.GroupBox2.Controls.Add(Me.Panel_Trading_BandWeighting)
    Me.GroupBox2.Controls.Add(Me.Check_Trading_BandWeightings)
    Me.GroupBox2.Controls.Add(Me.Panel7)
    Me.GroupBox2.Controls.Add(Me.Label12)
    Me.GroupBox2.Controls.Add(Me.Label15)
    Me.GroupBox2.Controls.Add(Me.Label13)
    Me.GroupBox2.Controls.Add(Me.Group_FirstTrade)
    Me.GroupBox2.Controls.Add(Me.Label14)
    Me.GroupBox2.Controls.Add(Me.Label16)
    Me.GroupBox2.Controls.Add(Me.Label17)
    Me.GroupBox2.Location = New System.Drawing.Point(7, 271)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(329, 530)
    Me.GroupBox2.TabIndex = 3
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "Trading Style"
    '
    'Text_Trading_BaselineGearing
    '
    Me.Text_Trading_BaselineGearing.Location = New System.Drawing.Point(144, 159)
    Me.Text_Trading_BaselineGearing.Name = "Text_Trading_BaselineGearing"
    Me.Text_Trading_BaselineGearing.Size = New System.Drawing.Size(78, 20)
    Me.Text_Trading_BaselineGearing.TabIndex = 65
    '
    'Text_Trading_RebalanceThreshold
    '
    Me.Text_Trading_RebalanceThreshold.Location = New System.Drawing.Point(144, 133)
    Me.Text_Trading_RebalanceThreshold.Name = "Text_Trading_RebalanceThreshold"
    Me.Text_Trading_RebalanceThreshold.Size = New System.Drawing.Size(78, 20)
    Me.Text_Trading_RebalanceThreshold.TabIndex = 64
    '
    'Text_Trading_MonthlyDrawdownResetCount
    '
    Me.Text_Trading_MonthlyDrawdownResetCount.Location = New System.Drawing.Point(188, 107)
    Me.Text_Trading_MonthlyDrawdownResetCount.Name = "Text_Trading_MonthlyDrawdownResetCount"
    Me.Text_Trading_MonthlyDrawdownResetCount.Size = New System.Drawing.Size(51, 20)
    Me.Text_Trading_MonthlyDrawdownResetCount.TabIndex = 63
    '
    'Text_Trading_MaxDrawdown
    '
    Me.Text_Trading_MaxDrawdown.Location = New System.Drawing.Point(144, 83)
    Me.Text_Trading_MaxDrawdown.Name = "Text_Trading_MaxDrawdown"
    Me.Text_Trading_MaxDrawdown.Size = New System.Drawing.Size(78, 20)
    Me.Text_Trading_MaxDrawdown.TabIndex = 62
    '
    'Text_Trading_StartDate
    '
    Me.Text_Trading_StartDate.Location = New System.Drawing.Point(144, 14)
    Me.Text_Trading_StartDate.Name = "Text_Trading_StartDate"
    Me.Text_Trading_StartDate.Size = New System.Drawing.Size(95, 20)
    Me.Text_Trading_StartDate.TabIndex = 61
    '
    'Label33
    '
    Me.Label33.AutoSize = True
    Me.Label33.Location = New System.Drawing.Point(7, 64)
    Me.Label33.Name = "Label33"
    Me.Label33.Size = New System.Drawing.Size(97, 13)
    Me.Label33.TabIndex = 60
    Me.Label33.Text = "Strategy can go ...."
    '
    'Check_Trading_AllowShort
    '
    Me.Check_Trading_AllowShort.Location = New System.Drawing.Point(221, 62)
    Me.Check_Trading_AllowShort.Name = "Check_Trading_AllowShort"
    Me.Check_Trading_AllowShort.Size = New System.Drawing.Size(62, 18)
    Me.Check_Trading_AllowShort.TabIndex = 59
    Me.Check_Trading_AllowShort.Text = "Short"
    Me.Check_Trading_AllowShort.UseVisualStyleBackColor = True
    '
    'Check_Trading_AllowLong
    '
    Me.Check_Trading_AllowLong.Location = New System.Drawing.Point(145, 62)
    Me.Check_Trading_AllowLong.Name = "Check_Trading_AllowLong"
    Me.Check_Trading_AllowLong.Size = New System.Drawing.Size(62, 18)
    Me.Check_Trading_AllowLong.TabIndex = 58
    Me.Check_Trading_AllowLong.Text = "Long"
    Me.Check_Trading_AllowLong.UseVisualStyleBackColor = True
    '
    'Label32
    '
    Me.Label32.AutoSize = True
    Me.Label32.Location = New System.Drawing.Point(242, 110)
    Me.Label32.Name = "Label32"
    Me.Label32.Size = New System.Drawing.Size(42, 13)
    Me.Label32.TabIndex = 42
    Me.Label32.Text = "Months"
    '
    'Check_Trading_MonthlyDrawdownReset
    '
    Me.Check_Trading_MonthlyDrawdownReset.Location = New System.Drawing.Point(145, 109)
    Me.Check_Trading_MonthlyDrawdownReset.Name = "Check_Trading_MonthlyDrawdownReset"
    Me.Check_Trading_MonthlyDrawdownReset.Size = New System.Drawing.Size(17, 16)
    Me.Check_Trading_MonthlyDrawdownReset.TabIndex = 40
    Me.Check_Trading_MonthlyDrawdownReset.UseVisualStyleBackColor = True
    '
    'Label31
    '
    Me.Label31.Location = New System.Drawing.Point(7, 109)
    Me.Label31.Name = "Label31"
    Me.Label31.Size = New System.Drawing.Size(138, 13)
    Me.Label31.TabIndex = 54
    Me.Label31.Text = "Monthly Drawdown reset"
    '
    'Panel_Trading_BandWeighting
    '
    Me.Panel_Trading_BandWeighting.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_Trading_BandWeighting.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Text_Trading_CrossingUnweightPoint)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Text_LocalHighExclusionPeriod)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Text_Trading_BandDeltaLimit)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Text_Trading_ReInvestmentEvaluationPeriod)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Text_Trading_WeightingDecayFactor)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Text_Trading_PercentBand)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Text_Trading_ZScore_Period)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Text_Trading_AverageZScore)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Text_Trading_IndexZScore)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Check_Trading_PhaseOutAtCrossing)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Label28)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Label26)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Label25)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Label9)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Label11)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Radio_Trading_ByPercentageBand)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Radio_Trading_ByAverageZScore)
    Me.Panel_Trading_BandWeighting.Controls.Add(Me.Radio_Trading_ByIndexZScore)
    Me.Panel_Trading_BandWeighting.Location = New System.Drawing.Point(94, 294)
    Me.Panel_Trading_BandWeighting.Name = "Panel_Trading_BandWeighting"
    Me.Panel_Trading_BandWeighting.Size = New System.Drawing.Size(229, 226)
    Me.Panel_Trading_BandWeighting.TabIndex = 48
    '
    'Text_Trading_CrossingUnweightPoint
    '
    Me.Text_Trading_CrossingUnweightPoint.Location = New System.Drawing.Point(161, 196)
    Me.Text_Trading_CrossingUnweightPoint.Name = "Text_Trading_CrossingUnweightPoint"
    Me.Text_Trading_CrossingUnweightPoint.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_CrossingUnweightPoint.TabIndex = 77
    '
    'Text_LocalHighExclusionPeriod
    '
    Me.Text_LocalHighExclusionPeriod.Location = New System.Drawing.Point(161, 171)
    Me.Text_LocalHighExclusionPeriod.Name = "Text_LocalHighExclusionPeriod"
    Me.Text_LocalHighExclusionPeriod.Size = New System.Drawing.Size(60, 20)
    Me.Text_LocalHighExclusionPeriod.TabIndex = 76
    '
    'Text_Trading_BandDeltaLimit
    '
    Me.Text_Trading_BandDeltaLimit.Location = New System.Drawing.Point(161, 147)
    Me.Text_Trading_BandDeltaLimit.Name = "Text_Trading_BandDeltaLimit"
    Me.Text_Trading_BandDeltaLimit.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_BandDeltaLimit.TabIndex = 75
    '
    'Text_Trading_ReInvestmentEvaluationPeriod
    '
    Me.Text_Trading_ReInvestmentEvaluationPeriod.Location = New System.Drawing.Point(161, 123)
    Me.Text_Trading_ReInvestmentEvaluationPeriod.Name = "Text_Trading_ReInvestmentEvaluationPeriod"
    Me.Text_Trading_ReInvestmentEvaluationPeriod.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_ReInvestmentEvaluationPeriod.TabIndex = 74
    '
    'Text_Trading_WeightingDecayFactor
    '
    Me.Text_Trading_WeightingDecayFactor.Location = New System.Drawing.Point(161, 99)
    Me.Text_Trading_WeightingDecayFactor.Name = "Text_Trading_WeightingDecayFactor"
    Me.Text_Trading_WeightingDecayFactor.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_WeightingDecayFactor.TabIndex = 73
    '
    'Text_Trading_PercentBand
    '
    Me.Text_Trading_PercentBand.Location = New System.Drawing.Point(161, 76)
    Me.Text_Trading_PercentBand.Name = "Text_Trading_PercentBand"
    Me.Text_Trading_PercentBand.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_PercentBand.TabIndex = 72
    '
    'Text_Trading_ZScore_Period
    '
    Me.Text_Trading_ZScore_Period.Location = New System.Drawing.Point(161, 51)
    Me.Text_Trading_ZScore_Period.Name = "Text_Trading_ZScore_Period"
    Me.Text_Trading_ZScore_Period.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_ZScore_Period.TabIndex = 71
    '
    'Text_Trading_AverageZScore
    '
    Me.Text_Trading_AverageZScore.Location = New System.Drawing.Point(161, 28)
    Me.Text_Trading_AverageZScore.Name = "Text_Trading_AverageZScore"
    Me.Text_Trading_AverageZScore.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_AverageZScore.TabIndex = 70
    '
    'Text_Trading_IndexZScore
    '
    Me.Text_Trading_IndexZScore.Location = New System.Drawing.Point(161, 3)
    Me.Text_Trading_IndexZScore.Name = "Text_Trading_IndexZScore"
    Me.Text_Trading_IndexZScore.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_IndexZScore.TabIndex = 69
    '
    'Check_Trading_PhaseOutAtCrossing
    '
    Me.Check_Trading_PhaseOutAtCrossing.Location = New System.Drawing.Point(3, 197)
    Me.Check_Trading_PhaseOutAtCrossing.Name = "Check_Trading_PhaseOutAtCrossing"
    Me.Check_Trading_PhaseOutAtCrossing.Size = New System.Drawing.Size(149, 18)
    Me.Check_Trading_PhaseOutAtCrossing.TabIndex = 17
    Me.Check_Trading_PhaseOutAtCrossing.Text = "UnWeight at Crossings"
    Me.Check_Trading_PhaseOutAtCrossing.UseVisualStyleBackColor = True
    '
    'Label28
    '
    Me.Label28.AutoSize = True
    Me.Label28.Location = New System.Drawing.Point(3, 174)
    Me.Label28.Name = "Label28"
    Me.Label28.Size = New System.Drawing.Size(137, 13)
    Me.Label28.TabIndex = 15
    Me.Label28.Text = "Local High exclusion period"
    '
    'Label26
    '
    Me.Label26.AutoSize = True
    Me.Label26.Location = New System.Drawing.Point(20, 54)
    Me.Label26.Name = "Label26"
    Me.Label26.Size = New System.Drawing.Size(108, 13)
    Me.Label26.TabIndex = 4
    Me.Label26.Text = "ZScore StDev Period"
    '
    'Label25
    '
    Me.Label25.AutoSize = True
    Me.Label25.Location = New System.Drawing.Point(3, 150)
    Me.Label25.Name = "Label25"
    Me.Label25.Size = New System.Drawing.Size(119, 13)
    Me.Label25.TabIndex = 13
    Me.Label25.Text = "Daily Weight Delta Limit"
    '
    'Label9
    '
    Me.Label9.AutoSize = True
    Me.Label9.Location = New System.Drawing.Point(3, 126)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(160, 13)
    Me.Label9.TabIndex = 11
    Me.Label9.Text = "Re-Investment evaluation period"
    '
    'Label11
    '
    Me.Label11.AutoSize = True
    Me.Label11.Location = New System.Drawing.Point(3, 102)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(119, 13)
    Me.Label11.TabIndex = 9
    Me.Label11.Text = "Weighting Decay factor"
    '
    'Radio_Trading_ByPercentageBand
    '
    Me.Radio_Trading_ByPercentageBand.AutoSize = True
    Me.Radio_Trading_ByPercentageBand.Location = New System.Drawing.Point(3, 77)
    Me.Radio_Trading_ByPercentageBand.Name = "Radio_Trading_ByPercentageBand"
    Me.Radio_Trading_ByPercentageBand.Size = New System.Drawing.Size(108, 17)
    Me.Radio_Trading_ByPercentageBand.TabIndex = 6
    Me.Radio_Trading_ByPercentageBand.TabStop = True
    Me.Radio_Trading_ByPercentageBand.Text = "Percentage Band"
    Me.Radio_Trading_ByPercentageBand.UseVisualStyleBackColor = True
    '
    'Radio_Trading_ByAverageZScore
    '
    Me.Radio_Trading_ByAverageZScore.AutoSize = True
    Me.Radio_Trading_ByAverageZScore.Location = New System.Drawing.Point(3, 29)
    Me.Radio_Trading_ByAverageZScore.Name = "Radio_Trading_ByAverageZScore"
    Me.Radio_Trading_ByAverageZScore.Size = New System.Drawing.Size(103, 17)
    Me.Radio_Trading_ByAverageZScore.TabIndex = 2
    Me.Radio_Trading_ByAverageZScore.TabStop = True
    Me.Radio_Trading_ByAverageZScore.Text = "Average ZScore"
    Me.Radio_Trading_ByAverageZScore.UseVisualStyleBackColor = True
    '
    'Radio_Trading_ByIndexZScore
    '
    Me.Radio_Trading_ByIndexZScore.AutoSize = True
    Me.Radio_Trading_ByIndexZScore.Location = New System.Drawing.Point(3, 4)
    Me.Radio_Trading_ByIndexZScore.Name = "Radio_Trading_ByIndexZScore"
    Me.Radio_Trading_ByIndexZScore.Size = New System.Drawing.Size(89, 17)
    Me.Radio_Trading_ByIndexZScore.TabIndex = 0
    Me.Radio_Trading_ByIndexZScore.TabStop = True
    Me.Radio_Trading_ByIndexZScore.Text = "Index ZScore"
    Me.Radio_Trading_ByIndexZScore.UseVisualStyleBackColor = True
    '
    'Check_Trading_BandWeightings
    '
    Me.Check_Trading_BandWeightings.Location = New System.Drawing.Point(7, 294)
    Me.Check_Trading_BandWeightings.Name = "Check_Trading_BandWeightings"
    Me.Check_Trading_BandWeightings.Size = New System.Drawing.Size(84, 36)
    Me.Check_Trading_BandWeightings.TabIndex = 50
    Me.Check_Trading_BandWeightings.Text = "Band Weighting"
    Me.Check_Trading_BandWeightings.UseVisualStyleBackColor = True
    '
    'Panel7
    '
    Me.Panel7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel7.Controls.Add(Me.Text_Trading_VolWeightDuration)
    Me.Panel7.Controls.Add(Me.Text_Trading_VolWeightCap)
    Me.Panel7.Controls.Add(Me.Text_Trading_VolExponent)
    Me.Panel7.Controls.Add(Me.Label27)
    Me.Panel7.Controls.Add(Me.Label22)
    Me.Panel7.Controls.Add(Me.Radio_Trading_VolWeight_VsSecondDuration)
    Me.Panel7.Controls.Add(Me.Radio_Trading_VolWeightNone)
    Me.Panel7.Controls.Add(Me.Radio_Trading_VolWeightByHighStDev)
    Me.Panel7.Location = New System.Drawing.Point(94, 184)
    Me.Panel7.Name = "Panel7"
    Me.Panel7.Size = New System.Drawing.Size(229, 104)
    Me.Panel7.TabIndex = 47
    '
    'Text_Trading_VolWeightDuration
    '
    Me.Text_Trading_VolWeightDuration.Location = New System.Drawing.Point(161, 71)
    Me.Text_Trading_VolWeightDuration.Name = "Text_Trading_VolWeightDuration"
    Me.Text_Trading_VolWeightDuration.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_VolWeightDuration.TabIndex = 68
    '
    'Text_Trading_VolWeightCap
    '
    Me.Text_Trading_VolWeightCap.Location = New System.Drawing.Point(161, 2)
    Me.Text_Trading_VolWeightCap.Name = "Text_Trading_VolWeightCap"
    Me.Text_Trading_VolWeightCap.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_VolWeightCap.TabIndex = 67
    '
    'Text_Trading_VolExponent
    '
    Me.Text_Trading_VolExponent.Location = New System.Drawing.Point(62, 2)
    Me.Text_Trading_VolExponent.Name = "Text_Trading_VolExponent"
    Me.Text_Trading_VolExponent.Size = New System.Drawing.Size(60, 20)
    Me.Text_Trading_VolExponent.TabIndex = 66
    '
    'Label27
    '
    Me.Label27.AutoSize = True
    Me.Label27.Location = New System.Drawing.Point(129, 5)
    Me.Label27.Name = "Label27"
    Me.Label27.Size = New System.Drawing.Size(26, 13)
    Me.Label27.TabIndex = 2
    Me.Label27.Text = "Cap"
    '
    'Label22
    '
    Me.Label22.AutoSize = True
    Me.Label22.Location = New System.Drawing.Point(3, 5)
    Me.Label22.Name = "Label22"
    Me.Label22.Size = New System.Drawing.Size(52, 13)
    Me.Label22.TabIndex = 0
    Me.Label22.Text = "Exponent"
    '
    'Radio_Trading_VolWeight_VsSecondDuration
    '
    Me.Radio_Trading_VolWeight_VsSecondDuration.AutoSize = True
    Me.Radio_Trading_VolWeight_VsSecondDuration.Location = New System.Drawing.Point(3, 72)
    Me.Radio_Trading_VolWeight_VsSecondDuration.Name = "Radio_Trading_VolWeight_VsSecondDuration"
    Me.Radio_Trading_VolWeight_VsSecondDuration.Size = New System.Drawing.Size(124, 17)
    Me.Radio_Trading_VolWeight_VsSecondDuration.TabIndex = 6
    Me.Radio_Trading_VolWeight_VsSecondDuration.TabStop = True
    Me.Radio_Trading_VolWeight_VsSecondDuration.Text = "StdDev : Average vs"
    Me.Radio_Trading_VolWeight_VsSecondDuration.UseVisualStyleBackColor = True
    '
    'Radio_Trading_VolWeightNone
    '
    Me.Radio_Trading_VolWeightNone.AutoSize = True
    Me.Radio_Trading_VolWeightNone.Location = New System.Drawing.Point(3, 32)
    Me.Radio_Trading_VolWeightNone.Name = "Radio_Trading_VolWeightNone"
    Me.Radio_Trading_VolWeightNone.Size = New System.Drawing.Size(51, 17)
    Me.Radio_Trading_VolWeightNone.TabIndex = 4
    Me.Radio_Trading_VolWeightNone.TabStop = True
    Me.Radio_Trading_VolWeightNone.Text = "None"
    Me.Radio_Trading_VolWeightNone.UseVisualStyleBackColor = True
    '
    'Radio_Trading_VolWeightByHighStDev
    '
    Me.Radio_Trading_VolWeightByHighStDev.AutoSize = True
    Me.Radio_Trading_VolWeightByHighStDev.Location = New System.Drawing.Point(3, 52)
    Me.Radio_Trading_VolWeightByHighStDev.Name = "Radio_Trading_VolWeightByHighStDev"
    Me.Radio_Trading_VolWeightByHighStDev.Size = New System.Drawing.Size(149, 17)
    Me.Radio_Trading_VolWeightByHighStDev.TabIndex = 5
    Me.Radio_Trading_VolWeightByHighStDev.TabStop = True
    Me.Radio_Trading_VolWeightByHighStDev.Text = "StdDev : High vs Average"
    Me.Radio_Trading_VolWeightByHighStDev.UseVisualStyleBackColor = True
    '
    'Label12
    '
    Me.Label12.AutoSize = True
    Me.Label12.Location = New System.Drawing.Point(7, 17)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(94, 13)
    Me.Label12.TabIndex = 51
    Me.Label12.Text = "Trading Start Date"
    '
    'Label15
    '
    Me.Label15.Location = New System.Drawing.Point(7, 189)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(65, 39)
    Me.Label15.TabIndex = 49
    Me.Label15.Text = "Volatility Weighting"
    '
    'Label13
    '
    Me.Label13.AutoSize = True
    Me.Label13.Location = New System.Drawing.Point(7, 41)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(75, 13)
    Me.Label13.TabIndex = 52
    Me.Label13.Text = "First Trade at :"
    '
    'Group_FirstTrade
    '
    Me.Group_FirstTrade.Controls.Add(Me.Radio_Trading_FirstTradeStart)
    Me.Group_FirstTrade.Controls.Add(Me.Radio_Trading_FirstTradeCrossing)
    Me.Group_FirstTrade.Location = New System.Drawing.Point(144, 37)
    Me.Group_FirstTrade.Name = "Group_FirstTrade"
    Me.Group_FirstTrade.Size = New System.Drawing.Size(151, 21)
    Me.Group_FirstTrade.TabIndex = 57
    '
    'Radio_Trading_FirstTradeStart
    '
    Me.Radio_Trading_FirstTradeStart.AutoSize = True
    Me.Radio_Trading_FirstTradeStart.Location = New System.Drawing.Point(2, 2)
    Me.Radio_Trading_FirstTradeStart.Name = "Radio_Trading_FirstTradeStart"
    Me.Radio_Trading_FirstTradeStart.Size = New System.Drawing.Size(47, 17)
    Me.Radio_Trading_FirstTradeStart.TabIndex = 0
    Me.Radio_Trading_FirstTradeStart.TabStop = True
    Me.Radio_Trading_FirstTradeStart.Text = "Start"
    Me.Radio_Trading_FirstTradeStart.UseVisualStyleBackColor = True
    '
    'Radio_Trading_FirstTradeCrossing
    '
    Me.Radio_Trading_FirstTradeCrossing.AutoSize = True
    Me.Radio_Trading_FirstTradeCrossing.Location = New System.Drawing.Point(55, 2)
    Me.Radio_Trading_FirstTradeCrossing.Name = "Radio_Trading_FirstTradeCrossing"
    Me.Radio_Trading_FirstTradeCrossing.Size = New System.Drawing.Size(87, 17)
    Me.Radio_Trading_FirstTradeCrossing.TabIndex = 1
    Me.Radio_Trading_FirstTradeCrossing.TabStop = True
    Me.Radio_Trading_FirstTradeCrossing.Text = "First Crossing"
    Me.Radio_Trading_FirstTradeCrossing.UseVisualStyleBackColor = True
    '
    'Label14
    '
    Me.Label14.AutoSize = True
    Me.Label14.Location = New System.Drawing.Point(7, 162)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(87, 13)
    Me.Label14.TabIndex = 56
    Me.Label14.Text = "Baseline Gearing"
    '
    'Label16
    '
    Me.Label16.AutoSize = True
    Me.Label16.Location = New System.Drawing.Point(7, 86)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(89, 13)
    Me.Label16.TabIndex = 53
    Me.Label16.Text = "Drawdown Cutoff"
    '
    'Label17
    '
    Me.Label17.AutoSize = True
    Me.Label17.Location = New System.Drawing.Point(7, 136)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(109, 13)
    Me.Label17.TabIndex = 55
    Me.Label17.Text = "Rebalance Threshold"
    '
    'GroupBox3
    '
    Me.GroupBox3.Controls.Add(Me.Text_FundingSpread)
    Me.GroupBox3.Controls.Add(Me.Text_TrandingCosts)
    Me.GroupBox3.Controls.Add(Me.Label7)
    Me.GroupBox3.Controls.Add(Me.Label10)
    Me.GroupBox3.Location = New System.Drawing.Point(6, 194)
    Me.GroupBox3.Name = "GroupBox3"
    Me.GroupBox3.Size = New System.Drawing.Size(285, 71)
    Me.GroupBox3.TabIndex = 2
    Me.GroupBox3.TabStop = False
    Me.GroupBox3.Text = "Costs"
    '
    'Text_FundingSpread
    '
    Me.Text_FundingSpread.Location = New System.Drawing.Point(189, 36)
    Me.Text_FundingSpread.Name = "Text_FundingSpread"
    Me.Text_FundingSpread.Size = New System.Drawing.Size(77, 20)
    Me.Text_FundingSpread.TabIndex = 45
    '
    'Text_TrandingCosts
    '
    Me.Text_TrandingCosts.Location = New System.Drawing.Point(189, 13)
    Me.Text_TrandingCosts.Name = "Text_TrandingCosts"
    Me.Text_TrandingCosts.Size = New System.Drawing.Size(77, 20)
    Me.Text_TrandingCosts.TabIndex = 44
    '
    'Label7
    '
    Me.Label7.AutoSize = True
    Me.Label7.Location = New System.Drawing.Point(17, 39)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(82, 13)
    Me.Label7.TabIndex = 29
    Me.Label7.Text = "Funding Spread"
    '
    'Label10
    '
    Me.Label10.AutoSize = True
    Me.Label10.Location = New System.Drawing.Point(17, 16)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(131, 13)
    Me.Label10.TabIndex = 28
    Me.Label10.Text = "Trading Costs, % per trade"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.Text_VolatilityAverage_Lambda)
    Me.GroupBox1.Controls.Add(Me.Text_LinearAverage_Lambda)
    Me.GroupBox1.Controls.Add(Me.Text_ExponentialAverage_Lambda)
    Me.GroupBox1.Controls.Add(Me.Text_ExponentialPrice_Lambda)
    Me.GroupBox1.Controls.Add(Me.Text_MovingAverageDays)
    Me.GroupBox1.Controls.Add(Me.Label6)
    Me.GroupBox1.Controls.Add(Me.Check_ExponentialPriceAverage)
    Me.GroupBox1.Controls.Add(Me.Radio_UnAdjustedAverage)
    Me.GroupBox1.Controls.Add(Me.Label8)
    Me.GroupBox1.Controls.Add(Me.Radio_ExponentialWeightedAverage)
    Me.GroupBox1.Controls.Add(Me.Radio_VolatilityWeightedDuration)
    Me.GroupBox1.Controls.Add(Me.Radio_LinearWeightedAverage)
    Me.GroupBox1.Location = New System.Drawing.Point(6, 5)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(285, 183)
    Me.GroupBox1.TabIndex = 0
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Moving Average"
    '
    'Text_VolatilityAverage_Lambda
    '
    Me.Text_VolatilityAverage_Lambda.Location = New System.Drawing.Point(189, 140)
    Me.Text_VolatilityAverage_Lambda.Name = "Text_VolatilityAverage_Lambda"
    Me.Text_VolatilityAverage_Lambda.Size = New System.Drawing.Size(77, 20)
    Me.Text_VolatilityAverage_Lambda.TabIndex = 43
    '
    'Text_LinearAverage_Lambda
    '
    Me.Text_LinearAverage_Lambda.Location = New System.Drawing.Point(189, 116)
    Me.Text_LinearAverage_Lambda.Name = "Text_LinearAverage_Lambda"
    Me.Text_LinearAverage_Lambda.Size = New System.Drawing.Size(77, 20)
    Me.Text_LinearAverage_Lambda.TabIndex = 42
    '
    'Text_ExponentialAverage_Lambda
    '
    Me.Text_ExponentialAverage_Lambda.Location = New System.Drawing.Point(189, 93)
    Me.Text_ExponentialAverage_Lambda.Name = "Text_ExponentialAverage_Lambda"
    Me.Text_ExponentialAverage_Lambda.Size = New System.Drawing.Size(77, 20)
    Me.Text_ExponentialAverage_Lambda.TabIndex = 41
    '
    'Text_ExponentialPrice_Lambda
    '
    Me.Text_ExponentialPrice_Lambda.Location = New System.Drawing.Point(189, 48)
    Me.Text_ExponentialPrice_Lambda.Name = "Text_ExponentialPrice_Lambda"
    Me.Text_ExponentialPrice_Lambda.Size = New System.Drawing.Size(77, 20)
    Me.Text_ExponentialPrice_Lambda.TabIndex = 40
    '
    'Text_MovingAverageDays
    '
    Me.Text_MovingAverageDays.Location = New System.Drawing.Point(67, 22)
    Me.Text_MovingAverageDays.Name = "Text_MovingAverageDays"
    Me.Text_MovingAverageDays.Size = New System.Drawing.Size(59, 20)
    Me.Text_MovingAverageDays.TabIndex = 39
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Location = New System.Drawing.Point(17, 25)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(31, 13)
    Me.Label6.TabIndex = 28
    Me.Label6.Text = "Days"
    '
    'Check_ExponentialPriceAverage
    '
    Me.Check_ExponentialPriceAverage.Location = New System.Drawing.Point(20, 50)
    Me.Check_ExponentialPriceAverage.Name = "Check_ExponentialPriceAverage"
    Me.Check_ExponentialPriceAverage.Size = New System.Drawing.Size(17, 16)
    Me.Check_ExponentialPriceAverage.TabIndex = 34
    Me.Check_ExponentialPriceAverage.UseVisualStyleBackColor = True
    '
    'Radio_UnAdjustedAverage
    '
    Me.Radio_UnAdjustedAverage.AutoSize = True
    Me.Radio_UnAdjustedAverage.Location = New System.Drawing.Point(20, 71)
    Me.Radio_UnAdjustedAverage.Name = "Radio_UnAdjustedAverage"
    Me.Radio_UnAdjustedAverage.Size = New System.Drawing.Size(99, 17)
    Me.Radio_UnAdjustedAverage.TabIndex = 29
    Me.Radio_UnAdjustedAverage.TabStop = True
    Me.Radio_UnAdjustedAverage.Text = "Simple Average"
    Me.Radio_UnAdjustedAverage.UseVisualStyleBackColor = True
    '
    'Label8
    '
    Me.Label8.Location = New System.Drawing.Point(37, 50)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(138, 13)
    Me.Label8.TabIndex = 30
    Me.Label8.Text = "Exponential Price Average"
    '
    'Radio_ExponentialWeightedAverage
    '
    Me.Radio_ExponentialWeightedAverage.AutoSize = True
    Me.Radio_ExponentialWeightedAverage.Location = New System.Drawing.Point(20, 94)
    Me.Radio_ExponentialWeightedAverage.Name = "Radio_ExponentialWeightedAverage"
    Me.Radio_ExponentialWeightedAverage.Size = New System.Drawing.Size(136, 17)
    Me.Radio_ExponentialWeightedAverage.TabIndex = 31
    Me.Radio_ExponentialWeightedAverage.TabStop = True
    Me.Radio_ExponentialWeightedAverage.Text = "Exponentially Weighted"
    Me.Radio_ExponentialWeightedAverage.UseVisualStyleBackColor = True
    '
    'Radio_VolatilityWeightedDuration
    '
    Me.Radio_VolatilityWeightedDuration.AutoSize = True
    Me.Radio_VolatilityWeightedDuration.Location = New System.Drawing.Point(20, 140)
    Me.Radio_VolatilityWeightedDuration.Name = "Radio_VolatilityWeightedDuration"
    Me.Radio_VolatilityWeightedDuration.Size = New System.Drawing.Size(155, 17)
    Me.Radio_VolatilityWeightedDuration.TabIndex = 33
    Me.Radio_VolatilityWeightedDuration.TabStop = True
    Me.Radio_VolatilityWeightedDuration.Text = "Volatility Weighted Duration"
    Me.Radio_VolatilityWeightedDuration.UseVisualStyleBackColor = True
    '
    'Radio_LinearWeightedAverage
    '
    Me.Radio_LinearWeightedAverage.AutoSize = True
    Me.Radio_LinearWeightedAverage.Location = New System.Drawing.Point(20, 117)
    Me.Radio_LinearWeightedAverage.Name = "Radio_LinearWeightedAverage"
    Me.Radio_LinearWeightedAverage.Size = New System.Drawing.Size(103, 17)
    Me.Radio_LinearWeightedAverage.TabIndex = 32
    Me.Radio_LinearWeightedAverage.TabStop = True
    Me.Radio_LinearWeightedAverage.Text = "Linear Weighted"
    Me.Radio_LinearWeightedAverage.UseVisualStyleBackColor = True
    '
    'Label_MovingAverage
    '
    Me.Label_MovingAverage.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_MovingAverage.Location = New System.Drawing.Point(150, 98)
    Me.Label_MovingAverage.Name = "Label_MovingAverage"
    Me.Label_MovingAverage.Size = New System.Drawing.Size(240, 13)
    Me.Label_MovingAverage.TabIndex = 8
    Me.Label_MovingAverage.Text = "."
    '
    'Label_DataPeriod
    '
    Me.Label_DataPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_DataPeriod.Location = New System.Drawing.Point(150, 76)
    Me.Label_DataPeriod.Name = "Label_DataPeriod"
    Me.Label_DataPeriod.Size = New System.Drawing.Size(240, 13)
    Me.Label_DataPeriod.TabIndex = 7
    Me.Label_DataPeriod.Text = "."
    '
    'Label_InterestRate
    '
    Me.Label_InterestRate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_InterestRate.Location = New System.Drawing.Point(150, 54)
    Me.Label_InterestRate.Name = "Label_InterestRate"
    Me.Label_InterestRate.Size = New System.Drawing.Size(240, 13)
    Me.Label_InterestRate.TabIndex = 6
    Me.Label_InterestRate.Text = "."
    '
    'Label_Instrument
    '
    Me.Label_Instrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_Instrument.Location = New System.Drawing.Point(150, 32)
    Me.Label_Instrument.Name = "Label_Instrument"
    Me.Label_Instrument.Size = New System.Drawing.Size(240, 13)
    Me.Label_Instrument.TabIndex = 5
    Me.Label_Instrument.Text = "."
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Location = New System.Drawing.Point(33, 98)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(85, 13)
    Me.Label4.TabIndex = 4
    Me.Label4.Text = "Moving Average"
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Location = New System.Drawing.Point(33, 76)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(63, 13)
    Me.Label5.TabIndex = 3
    Me.Label5.Text = "Data Period"
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(33, 54)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(68, 13)
    Me.Label3.TabIndex = 2
    Me.Label3.Text = "Interest Rate"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(33, 32)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(56, 13)
    Me.Label2.TabIndex = 1
    Me.Label2.Text = "Instrument"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(10, 8)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(90, 13)
    Me.Label1.TabIndex = 0
    Me.Label1.Text = "Simulation Details"
    '
    'frmManageFolders
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(810, 579)
    Me.Controls.Add(Me.SplitContainer1)
    Me.Controls.Add(Me.btnDeleteFolder)
    Me.Controls.Add(Me.btnAddFolder)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.btnCancel)
    Me.Name = "frmManageFolders"
    Me.Text = "Manage Folders"
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.Panel2.PerformLayout()
    Me.SplitContainer1.ResumeLayout(False)
    Me.Panel1.ResumeLayout(False)
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.Panel_Trading_BandWeighting.ResumeLayout(False)
    Me.Panel_Trading_BandWeighting.PerformLayout()
    Me.Panel7.ResumeLayout(False)
    Me.Panel7.PerformLayout()
    Me.Group_FirstTrade.ResumeLayout(False)
    Me.Group_FirstTrade.PerformLayout()
    Me.GroupBox3.ResumeLayout(False)
    Me.GroupBox3.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub

#End Region

#Region " Form Locals and Constants "

  ' Form 'Parent', the Main CTA form.
  ' Generally only accessed through the 'MainForm' property.
  Private WithEvents _MainForm As CTAMain

  ' Form ToolTip
  Private FormTooltip As New ToolTip()

  ' Form Menu


  ' Form Constants, specific to the table being updated.

  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
  Private THIS_TABLENAME As String
  Private THIS_ADAPTORNAME As String
  Private THIS_DATASETNAME As String


  ' The standard ChangeID for this form. e.g. tblInstrument
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  ' Form Specific Order fields
  Private THIS_FORM_SelectBy As String
  Private THIS_FORM_OrderBy As String

  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
  Private THIS_FORM_PermissionArea As String
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  Private THIS_FORM_FormID As CTAFormID

  ' Data Structures

  Private myDataset As RenaissanceDataClass.DSCTA_Simulation  ' Form Specific !!!!
  Private myTable As RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationDataTable
  Private myConnection As SqlConnection
  Private myAdaptor As SqlDataAdapter

  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


  ' Active Element.

  Private thisDataRow As RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationRow    ' Form Specific !!!!
  Private thisAuditID As Integer
  Private thisPosition As Integer
  Private __IsOverCancelButton As Boolean
  Private _InUse As Boolean

  ' Form Status Flags

  Private FormIsValid As Boolean
  Private FormChanged As Boolean
  Private _FormOpenFailed As Boolean
  Private InPaint As Boolean
  Private AddNewRecord As Boolean

  ' User Permission Flags

  Private HasReadPermission As Boolean
  Private HasUpdatePermission As Boolean
  Private HasInsertPermission As Boolean
  Private HasDeletePermission As Boolean

  Private FOLDER_COLOUR As System.Drawing.Color = Color.Black
  Private SIMULATION_COLOUR As System.Drawing.Color = Color.Blue
  Private ITEM_ALTERED_COLOUR As System.Drawing.Color = Color.Red

  Private NodesToDelete As New List(Of TreeNode)

#End Region

#Region " Form 'Properties' "

  Public ReadOnly Property MainForm() As CTAMain Implements StandardCTAForm.MainForm
    ' Public property to return handle to the 'Main' CTA form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  Public Property IsOverCancelButton() As Boolean Implements StandardCTAForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return __IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      __IsOverCancelButton = Value
    End Set
  End Property

  Public ReadOnly Property IsInPaint() As Boolean Implements StandardCTAForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  Public ReadOnly Property InUse() As Boolean Implements StandardCTAForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardCTAForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  Private Class ID_Struct
    Public AuditID As Integer
    Public FolderParentID As Integer
    Public FolderID As Integer
    Public SimulationID As Integer
    Public ItemName As String
    Private _IsSimulation As Boolean

    Public SimulationDataPeriod As DealingPeriod  ' Only Used when Copying Simulations.
    Public SimulationDefinition As String
    Private _ParametersChanged As Boolean
    Private _ChangedParameters As ArrayList

    Public Sub New(ByVal pAuditID As Integer, ByVal pFolderID As Integer, ByVal pSimulationID As Integer, ByVal pFolderParentID As Integer, ByVal pItemName As String, ByVal pIsSimulation As Boolean)
      Me.AuditID = pAuditID
      Me.FolderID = pFolderID
      Me.SimulationID = pSimulationID
      Me.FolderParentID = pFolderParentID
      Me.ItemName = pItemName
      Me._IsSimulation = pIsSimulation

      SimulationDataPeriod = DealingPeriod.Monthly
      SimulationDefinition = ""
      ParametersChanged = False
      _ChangedParameters = Nothing

      ' Item Can't be a Simulation and a Folder

      If Me.FolderID > 0 Then
        Me.SimulationID = 0
      End If

    End Sub

    Public Sub New(ByVal pAuditID As Integer, ByVal pFolderID As Integer, ByVal pSimulationID As Integer, ByVal pFolderParentID As Integer, ByVal pItemName As String, ByVal pIsSimulation As Boolean, ByVal pSimulationDataPeriod As DealingPeriod, ByVal pSimulationDefinition As String)

      Me.New(pAuditID, pFolderID, pSimulationID, pFolderParentID, pItemName, pIsSimulation)

      SimulationDataPeriod = pSimulationDataPeriod
      SimulationDefinition = pSimulationDefinition

    End Sub

    Public Property IsFolder() As Boolean
      Get
        Return (Not _IsSimulation)
      End Get
      Set(ByVal value As Boolean)
        _IsSimulation = (Not value)
      End Set
    End Property

    Public Property IsSimulation() As Boolean
      Get
        Return (_IsSimulation)
      End Get
      Set(ByVal value As Boolean)
        _IsSimulation = value
      End Set
    End Property

    Public Property ParametersChanged() As Boolean
      Get
        Return _ParametersChanged
      End Get
      Set(ByVal value As Boolean)
        If (value <> _ParametersChanged) Then
          If (value) Then
            If (_ChangedParameters Is Nothing) Then
              _ChangedParameters = New ArrayList
            End If
          Else
            If (_ChangedParameters IsNot Nothing) Then
              _ChangedParameters.Clear()
            End If
          End If
        ElseIf (Not value) Then
          If (_ChangedParameters IsNot Nothing) Then
            _ChangedParameters.Clear()
          End If
        End If
      End Set
    End Property

    Public Function ChangedParameters() As SingleSimulationParameterClass()
      If (_ChangedParameters Is Nothing) OrElse (_ChangedParameters.Count <= 0) Then
        Return Array.CreateInstance(GetType(SingleSimulationParameterClass), 0)
      Else
        Return _ChangedParameters.ToArray(GetType(SingleSimulationParameterClass))
      End If
    End Function

    Public Function ChangedParameters(ByVal pExistingParameters() As SingleSimulationParameterClass) As SingleSimulationParameterClass()
      If (pExistingParameters Is Nothing) OrElse (pExistingParameters.Length <= 0) Then
        Return Me.ChangedParameters
      End If

      If (_ChangedParameters Is Nothing) OrElse (_ChangedParameters.Count <= 0) Then
        Return pExistingParameters
      Else
        Dim Rval() As SingleSimulationParameterClass

        Rval = Array.CreateInstance(GetType(SingleSimulationParameterClass), (pExistingParameters.Length + _ChangedParameters.Count))

        Try

          Array.Copy(pExistingParameters, 0, Rval, 0, pExistingParameters.Length)

          Array.Copy(_ChangedParameters.ToArray(GetType(SingleSimulationParameterClass)), 0, Rval, pExistingParameters.Length, _ChangedParameters.Count)

        Catch ex As Exception
        End Try

        Return Rval
      End If
    End Function

    Public Sub AddChangedSimulationParameter(ByVal pNewParameter As SingleSimulationParameterClass)

      Try
        If (pNewParameter IsNot Nothing) Then
          Dim ParamIndex As Integer

          ParametersChanged = True

          If (_ChangedParameters Is Nothing) Then
            _ChangedParameters = New ArrayList
          End If

          For ParamIndex = 0 To (_ChangedParameters.Count - 1)
            If CType(_ChangedParameters(ParamIndex), SingleSimulationParameterClass).ParameterID = pNewParameter.ParameterID Then
              CType(_ChangedParameters(ParamIndex), SingleSimulationParameterClass).ParameterValue = pNewParameter.ParameterValue
              Exit Sub
            End If
          Next

          _ChangedParameters.Add(New SingleSimulationParameterClass(pNewParameter.ParameterID, pNewParameter.ParameterValue))

        End If

      Catch ex As Exception
      End Try

    End Sub

  End Class

  Private Class SingleSimulationParameterClass
    Public ParameterID As CTASimulationFunctions.SimulationParameterEnum
    Public ParameterValue As Object

    Public Sub New()
      Me.ParameterID = CTASimulationFunctions.SimulationParameterEnum.None
      Me.ParameterValue = Nothing
    End Sub

    Public Sub New(ByVal pID As CTASimulationFunctions.SimulationParameterEnum, ByVal pValue As Object)
      Me.ParameterID = pID
      Me.ParameterValue = pValue
    End Sub
  End Class

  Public Sub New(ByVal pMainForm As CTAMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.CTAAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************


    ' Default Select and Order fields.

    THIS_FORM_SelectBy = "SimulationName"
    THIS_FORM_OrderBy = "SimulationName"

    THIS_FORM_ValueMember = "SimulationID"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = "frmManageFolders"
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = CTAFormID.frmManageFolders

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.tblCTA_Simulation  ' This Defines the Form Data !!! 

    ' Form Control Changed events

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(CTA_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, CTA_CONNECTION, THIS_TABLENAME)
    myDataset = MainForm.Load_Table(ThisStandardDataset, False)
    myTable = myDataset.Tables(0)

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  Public Sub ResetForm() Implements StandardCTAForm.ResetForm
    THIS_FORM_SelectBy = "SimulationName"
    THIS_FORM_OrderBy = "SimulationName"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  Public Sub CloseForm() Implements StandardCTAForm.CloseForm
    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub

  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' ************************************************************************************
    '
    '
    ' ************************************************************************************

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myDataset Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True

      Exit Sub
    End If

    ' Display initial record.
    Call GetInitialData()

    InPaint = False


  End Sub

  Private Sub frmManageFolders_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' ************************************************************************************
    '
    '
    ' ************************************************************************************
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      HideForm = True
      If MainForm.CTAForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.CTAAutoUpdate, AddressOf Me.AutoUpdate
      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    ' ************************************************************************************
    ' Routine to handle changes / updates to tables by this and other windows.
    ' If this, or any other, form posts a change to a table, then it will invoke an update event 
    ' detailing what tables have been altered.
    ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
    ' the 'CTAAutoUpdate' event of the main CTA form.
    ' Each form may them react as appropriate to changes in any table that might impact it.
    '
    '
    '
    ' ************************************************************************************
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    InPaint = True
    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************


    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If


    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then

      ' Re-Set Controls etc.

      Dim NodeArray() As TreeNode
      Dim NodeMatchedArray() As Boolean
      Dim thisDataRow As DataRow
      Dim MissingNodes As Integer
      Dim ParentIDsChanged As Boolean
      Dim NodeCounter As Integer

      MissingNodes = 0
      ParentIDsChanged = False

      ' Check each Simulation in the table against it's respective node (if it exists)

      Dim ThisTable As DataTable = GetData()
      Dim ProcessedFolders As New List(Of Integer)

      NodeArray = GetNodeArray(Me.TreeView_Instruments)
      ReDim NodeMatchedArray(NodeArray.Length - 1)

      ' Check Folders

      Dim ThisStruct As ID_Struct

      For Each thisDataRow In ThisTable.Rows

        If (CInt(thisDataRow("FolderID")) > 0) Then

          If (Not (ProcessedFolders.Contains(CInt(thisDataRow("FolderID"))))) Then
            ProcessedFolders.Add(CInt(thisDataRow("FolderID")))
            MissingNodes += 1

            For NodeCounter = 0 To (NodeArray.GetLength(0) - 1)
              If (NodeArray(NodeCounter) IsNot Nothing) Then

                ThisStruct = CType(NodeArray(NodeCounter).Tag, ID_Struct)

                If (ThisStruct.IsFolder) AndAlso (ThisStruct.FolderID = CInt(thisDataRow("FolderID"))) Then

                  ' OK, this Folder exists as a node...

                  MissingNodes -= 1

                  ' Has it changed ?

                  If (ThisStruct.FolderParentID <> CInt(thisDataRow("ParentFolderID"))) Then
                    ' Folder Moved...

                    If (FormChanged) Then
                      MessageBox.Show("The Saved Simulation heirarchy has been changed by another user." & vbCrLf & "Structure will be re-loaded, any unsaved changed will be lost.", "Warning : External Instrument Change", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    End If

                    GetInitialData()
                    GoTo finish

                  End If

                  If (ThisStruct.ItemName <> thisDataRow("FolderName").ToString) Then
                    ThisStruct.ItemName = thisDataRow("FolderName").ToString
                    NodeArray(NodeCounter).Text = ThisStruct.ItemName
                  End If

                End If

              End If
            Next

          End If
        End If

        If (CInt(thisDataRow("SimulationID")) > 0) Then
          MissingNodes += 1

          For NodeCounter = 0 To (NodeArray.GetLength(0) - 1)
            If (NodeArray(NodeCounter) IsNot Nothing) Then

              ThisStruct = CType(NodeArray(NodeCounter).Tag, ID_Struct)

              If (ThisStruct.IsSimulation) AndAlso (ThisStruct.SimulationID = CInt(thisDataRow("SimulationID"))) Then

                ' OK, this Simulation exists as a node...

                MissingNodes -= 1

                ' Has it changed ?

                If (ThisStruct.FolderParentID <> CInt(thisDataRow("FolderID"))) Then

                  ' Simulation Moved.

                  If (FormChanged) Then
                    MessageBox.Show("The Saved Simulation heirarchy has been changed by another user." & vbCrLf & "Structure will be re-loaded, any unsaved changed will be lost.", "Warning : External Instrument Change", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                  End If

                  GetInitialData()
                  GoTo finish

                End If

                If (ThisStruct.ItemName <> thisDataRow("SimulationName").ToString) Then
                  ThisStruct.ItemName = thisDataRow("SimulationName").ToString
                  NodeArray(NodeCounter).Text = ThisStruct.ItemName
                End If

              End If

            End If
          Next

        End If

      Next

      If (MissingNodes > 0) Then
        If (FormChanged) Then
          MessageBox.Show("The new Simulations have been added by another user." & vbCrLf & "Structure will be re-loaded, any unsaved changed will be lost.", "Warning : External Instrument Change", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
        End If
        GetInitialData()
      End If

    End If

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************
finish:

    InPaint = OrgInPaint
    If SetButtonStatus_Flag Then
      Call SetButtonStatus()
    End If

  End Sub

  Private Function GetNodeArray(ByRef pTreeView As TreeView) As TreeNode()
    ' ****************************************************************
    ' Header function to retrieve an array of ALL TreeNodes from the 
    ' Given TreeView.
    ' ****************************************************************
    Dim ReturnArray() As TreeNode
    Dim NodeCount As Integer

    Try
      Dim ThisNode As TreeNode
      NodeCount = 0
      For Each ThisNode In pTreeView.Nodes
        NodeCount += CountNodeArray(ThisNode)
      Next

      ReDim ReturnArray(NodeCount)

      NodeCount = 0
      For Each ThisNode In pTreeView.Nodes
        PopulateNodeArray(ThisNode, ReturnArray, NodeCount)
      Next

    Catch ex As Exception
      Erase ReturnArray
    End Try

    Return ReturnArray

  End Function

  Private Function CountNodeArray(ByRef pTreeNode As TreeNode) As Integer
    ' ****************************************************************
    ' Recursive function used to count the number of TreeNodes in a TreeView.
    ' ****************************************************************
    Dim Rval As Integer

    Dim ThisNode As TreeNode
    Rval = 1
    For Each ThisNode In pTreeNode.Nodes
      Rval += CountNodeArray(ThisNode)
    Next

    Return Rval
  End Function

  Private Sub PopulateNodeArray(ByRef pTreeNode As TreeNode, ByRef ReturnArray() As TreeNode, ByRef RVal As Integer)
    ' ****************************************************************
    ' Recursive function to retrieve an array of ALL TreeNodes from the 
    ' Given TreeView.
    ' ****************************************************************

    Dim ThisNode As TreeNode
    ReturnArray(RVal) = pTreeNode
    RVal += 1
    For Each ThisNode In pTreeNode.Nodes
      PopulateNodeArray(ThisNode, ReturnArray, RVal)
    Next

  End Sub

  Private Sub ClearParametersPane()

    Try
      Text_MovingAverageDays.Text = ""
      Text_MovingAverageDays.ForeColor = FOLDER_COLOUR
      Check_ExponentialPriceAverage.Checked = False
      Check_ExponentialPriceAverage.ForeColor = FOLDER_COLOUR
      Text_ExponentialPrice_Lambda.Text = ""
      Text_ExponentialPrice_Lambda.ForeColor = FOLDER_COLOUR

      Radio_UnAdjustedAverage.Checked = False
      Radio_UnAdjustedAverage.ForeColor = FOLDER_COLOUR
      Radio_ExponentialWeightedAverage.Checked = False
      Radio_ExponentialWeightedAverage.ForeColor = FOLDER_COLOUR
      Text_ExponentialAverage_Lambda.Text = ""
      Text_ExponentialAverage_Lambda.ForeColor = FOLDER_COLOUR

      Radio_LinearWeightedAverage.Checked = False
      Radio_LinearWeightedAverage.ForeColor = FOLDER_COLOUR
      Text_LinearAverage_Lambda.Text = ""
      Text_LinearAverage_Lambda.ForeColor = FOLDER_COLOUR

      Radio_VolatilityWeightedDuration.Checked = False
      Radio_VolatilityWeightedDuration.ForeColor = FOLDER_COLOUR
      Text_VolatilityAverage_Lambda.Text = ""
      Text_VolatilityAverage_Lambda.ForeColor = FOLDER_COLOUR

      Text_TrandingCosts.Text = ""
      Text_TrandingCosts.ForeColor = FOLDER_COLOUR
      Text_FundingSpread.Text = ""
      Text_FundingSpread.ForeColor = FOLDER_COLOUR

      Text_Trading_StartDate.Text = ""
      Text_Trading_StartDate.ForeColor = FOLDER_COLOUR

      Radio_Trading_FirstTradeStart.Checked = False
      Radio_Trading_FirstTradeStart.ForeColor = FOLDER_COLOUR
      Radio_Trading_FirstTradeCrossing.Checked = False
      Radio_Trading_FirstTradeCrossing.ForeColor = FOLDER_COLOUR
      Check_Trading_AllowLong.Checked = False
      Check_Trading_AllowLong.ForeColor = FOLDER_COLOUR
      Check_Trading_AllowShort.Checked = False
      Check_Trading_AllowShort.ForeColor = FOLDER_COLOUR
      Text_Trading_MaxDrawdown.Text = ""
      Text_Trading_MaxDrawdown.ForeColor = FOLDER_COLOUR

      Check_Trading_MonthlyDrawdownReset.Checked = False
      Check_Trading_MonthlyDrawdownReset.ForeColor = FOLDER_COLOUR
      Text_Trading_MonthlyDrawdownResetCount.Text = ""
      Text_Trading_MonthlyDrawdownResetCount.ForeColor = FOLDER_COLOUR

      Text_Trading_RebalanceThreshold.Text = ""
      Text_Trading_RebalanceThreshold.ForeColor = FOLDER_COLOUR

      Text_Trading_BaselineGearing.Text = ""
      Text_Trading_BaselineGearing.ForeColor = FOLDER_COLOUR

      Text_Trading_VolExponent.Text = ""
      Text_Trading_VolExponent.ForeColor = FOLDER_COLOUR

      Text_Trading_VolWeightCap.Text = ""
      Text_Trading_VolWeightCap.ForeColor = FOLDER_COLOUR

      Radio_Trading_VolWeightNone.Checked = False
      Radio_Trading_VolWeightNone.ForeColor = FOLDER_COLOUR
      Radio_Trading_VolWeightByHighStDev.Checked = False
      Radio_Trading_VolWeightByHighStDev.ForeColor = FOLDER_COLOUR
      Radio_Trading_VolWeight_VsSecondDuration.Checked = False
      Radio_Trading_VolWeight_VsSecondDuration.ForeColor = FOLDER_COLOUR
      Text_Trading_VolWeightDuration.Text = ""
      Text_Trading_VolWeightDuration.ForeColor = FOLDER_COLOUR

      Check_Trading_BandWeightings.Checked = False
      Check_Trading_BandWeightings.ForeColor = FOLDER_COLOUR
      Radio_Trading_ByIndexZScore.Checked = False
      Radio_Trading_ByIndexZScore.ForeColor = FOLDER_COLOUR
      Text_Trading_IndexZScore.Text = ""
      Text_Trading_IndexZScore.ForeColor = FOLDER_COLOUR

      Radio_Trading_ByAverageZScore.Checked = False
      Radio_Trading_ByAverageZScore.ForeColor = FOLDER_COLOUR
      Text_Trading_AverageZScore.Text = ""
      Text_Trading_AverageZScore.ForeColor = FOLDER_COLOUR

      Text_Trading_ZScore_Period.Text = ""
      Text_Trading_ZScore_Period.ForeColor = FOLDER_COLOUR

      Radio_Trading_ByPercentageBand.Checked = False
      Radio_Trading_ByPercentageBand.ForeColor = FOLDER_COLOUR
      Text_Trading_PercentBand.Text = ""
      Text_Trading_PercentBand.ForeColor = FOLDER_COLOUR

      Text_Trading_WeightingDecayFactor.Text = ""
      Text_Trading_WeightingDecayFactor.ForeColor = FOLDER_COLOUR

      Text_Trading_ReInvestmentEvaluationPeriod.Text = ""
      Text_Trading_ReInvestmentEvaluationPeriod.ForeColor = FOLDER_COLOUR

      Text_Trading_BandDeltaLimit.Text = ""
      Text_Trading_BandDeltaLimit.ForeColor = FOLDER_COLOUR

      Text_LocalHighExclusionPeriod.Text = ""
      Text_LocalHighExclusionPeriod.ForeColor = FOLDER_COLOUR

      Check_Trading_PhaseOutAtCrossing.Checked = False
      Check_Trading_PhaseOutAtCrossing.ForeColor = FOLDER_COLOUR
      Text_Trading_CrossingUnweightPoint.Text = ""
      Text_Trading_CrossingUnweightPoint.ForeColor = FOLDER_COLOUR


    Catch ex As Exception
    End Try
  End Sub

  Private Sub PopulateParameterPane(ByVal ThisSimulation As CTASimulationFunctions.SimulationParameterClass)

    Try
      If (ThisSimulation IsNot Nothing) Then

        Text_MovingAverageDays.Text = ThisSimulation.MovingAverageDays.ToString

        Check_ExponentialPriceAverage.Checked = ThisSimulation.MovingAverage_ExponentialPricingAverage
        Text_ExponentialPrice_Lambda.Text = ThisSimulation.MovingAverage_ExponentialPricingLambda.ToString
        Radio_ExponentialWeightedAverage.Checked = ThisSimulation.MovingAverage_ExponentialWeightedAverage
        Text_ExponentialAverage_Lambda.Text = ThisSimulation.MovingAverage_ExponentialWeightedAverageLambda.ToString
        Radio_LinearWeightedAverage.Checked = ThisSimulation.MovingAverage_LinearWeightedAverage
        Text_LinearAverage_Lambda.Text = ThisSimulation.MovingAverage_LinearWeightedAverageLambda.ToString
        Radio_VolatilityWeightedDuration.Checked = ThisSimulation.MovingAverage_VolatilityWeightedDuration
        Text_VolatilityAverage_Lambda.Text = ThisSimulation.MovingAverage_VolatilityWeightedDurationLambda.ToString
        Radio_UnAdjustedAverage.Checked = Not (Radio_ExponentialWeightedAverage.Checked Or Radio_LinearWeightedAverage.Checked Or Radio_VolatilityWeightedDuration.Checked)

        Check_Trading_AllowLong.Checked = ThisSimulation.Trading_AllowLongPosition
        Check_Trading_AllowShort.Checked = ThisSimulation.Trading_AllowShortPosition

        Text_Trading_RebalanceThreshold.Text = ThisSimulation.Trading_RebalanceThreshold.ToString
        Text_Trading_BaselineGearing.Text = ThisSimulation.Trading_BaselineGearing.ToString
        Text_Trading_MaxDrawdown.Text = ThisSimulation.Trading_MaxDrawdown.ToString
        Check_Trading_MonthlyDrawdownReset.Checked = ThisSimulation.Trading_DrawdownMonthlyReset
        Text_Trading_MonthlyDrawdownResetCount.Enabled = Check_Trading_MonthlyDrawdownReset.Checked
        Text_Trading_MonthlyDrawdownResetCount.Text = ThisSimulation.Trading_DrawdownResetMonths.ToString
        Text_TrandingCosts.Text = ThisSimulation.Trading_TradingCosts.ToString
        Text_FundingSpread.Text = ThisSimulation.Trading_FundingSpread.ToString
        Text_Trading_StartDate.Text = ThisSimulation.Trading_StartDate.ToString(DISPLAYMEMBER_DATEFORMAT)

        If (ThisSimulation.Trading_FirstTradeAtStart) Then
          Radio_Trading_FirstTradeStart.Checked = True
        Else
          Radio_Trading_FirstTradeCrossing.Checked = True
        End If

        If (ThisSimulation.Trading_VolatilityWeighting_StdDevHighVsAverage) Then
          Radio_Trading_VolWeightByHighStDev.Checked = True
        ElseIf (ThisSimulation.Trading_VolatilityWeighting_StdDevVsGivenDuration) Then
          Radio_Trading_VolWeight_VsSecondDuration.Checked = True
        Else
          Radio_Trading_VolWeightNone.Checked = True
        End If

        Text_Trading_VolWeightDuration.Text = ThisSimulation.Trading_VolatilityWeighting_VolWeightDuration.ToString
        Text_Trading_VolWeightCap.Text = ThisSimulation.Trading_VolatilityWeighting_VolWeightCap.ToString
        Text_Trading_VolExponent.Text = ThisSimulation.Trading_VolatilityWeighting_Exponent.ToString

        If (ThisSimulation.Trading_BandWeighting) Then
          Check_Trading_BandWeightings.Checked = True
          Radio_Trading_ByIndexZScore.Checked = ThisSimulation.Trading_BandWeighting_ByIndexZScore
          Radio_Trading_ByAverageZScore.Checked = ThisSimulation.Trading_BandWeighting_ByAverageZScore
          Radio_Trading_ByPercentageBand.Checked = ThisSimulation.Trading_BandWeighting_ByPercentageBand
          Check_Trading_PhaseOutAtCrossing.Checked = ThisSimulation.Trading_BandWeighting_PhaseOutAtCrossing
        Else
          Check_Trading_BandWeightings.Checked = False
          Radio_Trading_ByIndexZScore.Checked = False
          Radio_Trading_ByAverageZScore.Checked = False
          Radio_Trading_ByPercentageBand.Checked = False
          Check_Trading_PhaseOutAtCrossing.Checked = False
        End If

        Text_Trading_CrossingUnweightPoint.Text = ThisSimulation.Trading_CrossingUnweightPoint.ToString
        Text_Trading_IndexZScore.Text = ThisSimulation.Trading_BandWeighting_IndexZScoreThreshold.ToString
        Text_Trading_AverageZScore.Text = ThisSimulation.Trading_BandWeighting_AverageZScoreThreshold.ToString
        Text_Trading_ZScore_Period.Text = ThisSimulation.Trading_BandWeighting_ZScoreStdDevPeriod.ToString
        Text_Trading_PercentBand.Text = ThisSimulation.Trading_BandWeighting_PercentageBandThreshold.ToString
        Text_Trading_WeightingDecayFactor.Text = ThisSimulation.Trading_BandWeighting_WeightingDecayFactor.ToString
        Text_Trading_ReInvestmentEvaluationPeriod.Text = ThisSimulation.Trading_BandWeighting_ReInvestmentEvaluationPeriod.ToString
        Text_Trading_BandDeltaLimit.Text = ThisSimulation.Trading_BandWeighting_DailyWeightDeltaLimit.ToString
        Text_LocalHighExclusionPeriod.Text = ThisSimulation.Trading_BandWeighting_LocalHighExclusionPeriod.ToString

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub PopulateParameterPane(ByVal TheseParameters() As SingleSimulationParameterClass)
    ' ********************************************************************************
    '
    ' ********************************************************************************

    Try
      Dim ThisParameter As SingleSimulationParameterClass
      Dim ThisIsNumeric As Boolean
      Dim ThisIsDate As Boolean
      Dim ThisIsNothing As Boolean

      For Each ThisParameter In TheseParameters

        Try
          If (ThisParameter.ParameterValue Is Nothing) Then
            ThisIsNothing = True
            ThisIsNumeric = False
            ThisIsDate = False
          Else
            ThisIsNothing = False
            ThisIsNumeric = IsNumeric(ThisParameter.ParameterValue)
            ThisIsDate = IsDate(ThisParameter.ParameterValue)
          End If

          Select Case ThisParameter.ParameterID

            Case CTASimulationFunctions.SimulationParameterEnum.InstrumentID

            Case CTASimulationFunctions.SimulationParameterEnum.InterestRateID

            Case CTASimulationFunctions.SimulationParameterEnum.DataStartDate

            Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_Days
              If (ThisIsNothing) Then
                Text_MovingAverageDays.Text = ""
                Text_MovingAverageDays.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_MovingAverageDays.Text = CInt(ThisParameter.ParameterValue).ToString
                Text_MovingAverageDays.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialPricingAverage
              If (ThisIsNothing) Then
                Check_ExponentialPriceAverage.Checked = False
                Check_ExponentialPriceAverage.ForeColor = FOLDER_COLOUR
              Else
                Check_ExponentialPriceAverage.Checked = CBool(ThisParameter.ParameterValue)
                Check_ExponentialPriceAverage.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialPricingLambda
              If (ThisIsNothing) Then
                Text_ExponentialPrice_Lambda.Text = ""
                Text_ExponentialPrice_Lambda.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_ExponentialPrice_Lambda.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_ExponentialPrice_Lambda.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialWeightedAverage
              If (ThisIsNothing) Then
                Radio_ExponentialWeightedAverage.Checked = False
                Radio_ExponentialWeightedAverage.ForeColor = FOLDER_COLOUR
              Else
                Radio_ExponentialWeightedAverage.Checked = CBool(ThisParameter.ParameterValue)
                Radio_ExponentialWeightedAverage.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialWeightedAverageLambda
              If (ThisIsNothing) Then
                Text_ExponentialAverage_Lambda.Text = ""
                Text_ExponentialAverage_Lambda.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_ExponentialAverage_Lambda.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_ExponentialAverage_Lambda.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_LinearWeightedAverage
              If (ThisIsNothing) Then
                Radio_LinearWeightedAverage.Checked = False
                Radio_LinearWeightedAverage.ForeColor = FOLDER_COLOUR
              Else
                Radio_LinearWeightedAverage.Checked = CBool(ThisParameter.ParameterValue)
                Radio_LinearWeightedAverage.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_LinearWeightedAverageLambda
              If (ThisIsNothing) Then
                Text_LinearAverage_Lambda.Text = ""
                Text_LinearAverage_Lambda.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_LinearAverage_Lambda.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_LinearAverage_Lambda.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_VolatilityWeightedDuration
              If (ThisIsNothing) Then
                Radio_VolatilityWeightedDuration.Checked = False
                Radio_VolatilityWeightedDuration.ForeColor = FOLDER_COLOUR
              Else
                Radio_VolatilityWeightedDuration.Checked = CBool(ThisParameter.ParameterValue)
                Radio_VolatilityWeightedDuration.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_VolatilityWeightedDurationLambda
              If (ThisIsNothing) Then
                Text_LinearAverage_Lambda.Text = ""
                Text_LinearAverage_Lambda.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_VolatilityAverage_Lambda.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_LinearAverage_Lambda.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.InitialNAV  ' Notional Starting NAV

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_AllowLongPosition
              If (ThisIsNothing) Then
                Check_Trading_AllowLong.Checked = False
                Check_Trading_AllowLong.ForeColor = FOLDER_COLOUR
              Else
                Check_Trading_AllowLong.Checked = CBool(ThisParameter.ParameterValue)
                Check_Trading_AllowLong.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_AllowShortPosition
              If (ThisIsNothing) Then
                Check_Trading_AllowShort.Checked = False
                Check_Trading_AllowShort.ForeColor = FOLDER_COLOUR
              Else
                Check_Trading_AllowShort.Checked = CBool(ThisParameter.ParameterValue)
                Check_Trading_AllowShort.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_RebalanceThreshold
              If (ThisIsNothing) Then
                Text_Trading_RebalanceThreshold.Text = ""
                Text_Trading_RebalanceThreshold.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_RebalanceThreshold.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_RebalanceThreshold.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BaselineGearing
              If (ThisIsNothing) Then
                Text_Trading_BaselineGearing.Text = ""
                Text_Trading_BaselineGearing.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_BaselineGearing.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_BaselineGearing.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_MaxDrawdown
              If (ThisIsNothing) Then
                Text_Trading_MaxDrawdown.Text = ""
                Text_Trading_MaxDrawdown.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_MaxDrawdown.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_MaxDrawdown.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_DrawdownMonthlyReset
              If (ThisIsNothing) Then
                Check_Trading_MonthlyDrawdownReset.Checked = False
                Check_Trading_MonthlyDrawdownReset.ForeColor = FOLDER_COLOUR
              Else
                Check_Trading_MonthlyDrawdownReset.Checked = CBool(ThisParameter.ParameterValue)
                Check_Trading_MonthlyDrawdownReset.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_DrawdownResetMonths
              If (ThisIsNothing) Then
                Text_Trading_MonthlyDrawdownResetCount.Text = ""
                Text_Trading_MonthlyDrawdownResetCount.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_MonthlyDrawdownResetCount.Text = CInt(ThisParameter.ParameterValue).ToString
                Text_Trading_MonthlyDrawdownResetCount.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_TradingCosts
              If (ThisIsNothing) Then
                Text_TrandingCosts.Text = ""
                Text_TrandingCosts.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_TrandingCosts.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_TrandingCosts.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_FundingSpread
              If (ThisIsNothing) Then
                Text_FundingSpread.Text = ""
                Text_FundingSpread.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_FundingSpread.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_FundingSpread.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_StartDate
              If (ThisIsNothing) Then
                Text_Trading_StartDate.Text = ""
                Text_Trading_StartDate.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsDate) Then
                Text_Trading_StartDate.Text = CDate(ThisParameter.ParameterValue).ToString(DISPLAYMEMBER_DATEFORMAT)
                Text_Trading_StartDate.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_FirstTradeAtStart
              If (ThisIsNothing) Then
                Radio_Trading_FirstTradeStart.Checked = False
                Radio_Trading_FirstTradeCrossing.Checked = False
              Else
                Radio_Trading_FirstTradeStart.Checked = CBool(ThisParameter.ParameterValue)
                Radio_Trading_FirstTradeCrossing.Checked = Not Radio_Trading_FirstTradeStart.Checked
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_StdDevHighVsAverage
              If (ThisIsNothing) Then
                Radio_Trading_VolWeightByHighStDev.Checked = False
                Radio_Trading_VolWeightByHighStDev.ForeColor = FOLDER_COLOUR
              Else
                Radio_Trading_VolWeightByHighStDev.Checked = CBool(ThisParameter.ParameterValue)
                Radio_Trading_VolWeightByHighStDev.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_StdDevVsGivenDuration
              If (ThisIsNothing) Then
                Radio_Trading_VolWeight_VsSecondDuration.Checked = False
                Radio_Trading_VolWeight_VsSecondDuration.ForeColor = FOLDER_COLOUR
              Else
                Radio_Trading_VolWeight_VsSecondDuration.Checked = CBool(ThisParameter.ParameterValue)
                Radio_Trading_VolWeight_VsSecondDuration.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_VolWeightDuration
              If (ThisIsNothing) Then
                Text_Trading_VolWeightDuration.Text = ""
                Text_Trading_VolWeightDuration.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_VolWeightDuration.Text = CInt(ThisParameter.ParameterValue).ToString
                Text_Trading_VolWeightDuration.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_VolWeightCap
              If (ThisIsNothing) Then
                Text_Trading_VolWeightCap.Text = ""
                Text_Trading_VolWeightCap.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_VolWeightCap.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_VolWeightCap.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_Exponent
              If (ThisIsNothing) Then
                Text_Trading_VolExponent.Text = ""
                Text_Trading_VolExponent.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_VolExponent.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_VolExponent.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByIndexZScore
              If (ThisIsNothing) Then
                Radio_Trading_ByIndexZScore.Checked = False
                Radio_Trading_ByIndexZScore.ForeColor = FOLDER_COLOUR
              Else
                Radio_Trading_ByIndexZScore.Checked = CBool(ThisParameter.ParameterValue)
                Radio_Trading_ByIndexZScore.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_IndexZScoreThreshold
              If (ThisIsNothing) Then
                Text_Trading_IndexZScore.Text = ""
                Text_Trading_IndexZScore.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_IndexZScore.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_IndexZScore.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByAverageZScore
              If (ThisIsNothing) Then
                Radio_Trading_ByAverageZScore.Checked = False
                Radio_Trading_ByAverageZScore.ForeColor = FOLDER_COLOUR
              Else
                Radio_Trading_ByAverageZScore.Checked = CBool(ThisParameter.ParameterValue)
                Radio_Trading_ByAverageZScore.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_AverageZScoreThreshold
              If (ThisIsNothing) Then
                Text_Trading_AverageZScore.Text = ""
                Text_Trading_AverageZScore.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_AverageZScore.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_AverageZScore.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ZScoreStdDevPeriod
              If (ThisIsNothing) Then
                Text_Trading_ZScore_Period.Text = ""
                Text_Trading_ZScore_Period.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_ZScore_Period.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_ZScore_Period.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByPercentageBand
              If (ThisIsNothing) Then
                Radio_Trading_ByPercentageBand.Checked = False
                Radio_Trading_ByPercentageBand.ForeColor = FOLDER_COLOUR
              Else
                Radio_Trading_ByPercentageBand.Checked = CBool(ThisParameter.ParameterValue)
                Radio_Trading_ByPercentageBand.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_PercentageBandThreshold
              If (ThisIsNothing) Then
                Text_Trading_PercentBand.Text = ""
                Text_Trading_PercentBand.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_PercentBand.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_PercentBand.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_WeightingDecayFactor
              If (ThisIsNothing) Then
                Text_Trading_WeightingDecayFactor.Text = ""
                Text_Trading_WeightingDecayFactor.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_WeightingDecayFactor.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_WeightingDecayFactor.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ReInvestmentEvaluationPeriod
              If (ThisIsNothing) Then
                Text_Trading_ReInvestmentEvaluationPeriod.Text = ""
                Text_Trading_ReInvestmentEvaluationPeriod.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_ReInvestmentEvaluationPeriod.Text = CInt(ThisParameter.ParameterValue).ToString
                Text_Trading_ReInvestmentEvaluationPeriod.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_DailyWeightDeltaLimit
              If (ThisIsNothing) Then
                Text_Trading_BandDeltaLimit.Text = ""
                Text_Trading_BandDeltaLimit.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_BandDeltaLimit.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_BandDeltaLimit.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_LocalHighExclusionPeriod
              If (ThisIsNothing) Then
                Text_LocalHighExclusionPeriod.Text = ""
                Text_LocalHighExclusionPeriod.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_LocalHighExclusionPeriod.Text = CInt(ThisParameter.ParameterValue).ToString
                Text_LocalHighExclusionPeriod.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_PhaseOutAtCrossing
              If (ThisIsNothing) Then
                Check_Trading_PhaseOutAtCrossing.Checked = False
                Check_Trading_PhaseOutAtCrossing.ForeColor = FOLDER_COLOUR
              Else
                Check_Trading_PhaseOutAtCrossing.Checked = CBool(ThisParameter.ParameterValue)
                Check_Trading_PhaseOutAtCrossing.ForeColor = ITEM_ALTERED_COLOUR
              End If

            Case CTASimulationFunctions.SimulationParameterEnum.Trading_CrossingUnweightPoint
              If (ThisIsNothing) Then
                Text_Trading_CrossingUnweightPoint.Text = ""
                Text_Trading_CrossingUnweightPoint.ForeColor = FOLDER_COLOUR
              ElseIf (ThisIsNumeric) Then
                Text_Trading_CrossingUnweightPoint.Text = CDbl(ThisParameter.ParameterValue).ToString
                Text_Trading_CrossingUnweightPoint.ForeColor = ITEM_ALTERED_COLOUR
              End If

          End Select

        Catch ex As Exception
        End Try

      Next

    Catch ex As Exception
    End Try

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Check User permissions
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If

  End Sub


#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  Private Function GetData() As DataTable
    ' ********************************************************************************
    '
    ' ********************************************************************************

    Dim tmpCommand As New SqlCommand
    Dim ThisTable As New DataTable

    Try
      tmpCommand.CommandType = CommandType.Text
      tmpCommand.CommandText = "SELECT ISNULL(Folders.FolderID, 0) AS FolderID, ISNULL(Folders.ParentFolderId, 0) AS ParentFolderId, ISNULL(Folders.FolderName, '') AS FolderName, ISNULL(Sims.SimulationID, 0) AS SimulationID, ISNULL(Sims.SimulationName, '') AS SimulationName, ISNULL(Sims.SimulationDataPeriod, 4) AS SimulationDataPeriod FROM fn_tblCTA_Folders_SelectKD(@KnowledgeDate) Folders FULL OUTER JOIN fn_tblCTA_Simulation_SelectKD(@KnowledgeDate) Sims ON Folders.FolderID = Sims.SimulationFolder"
      tmpCommand.Connection = MainForm.GetCTAConnection
      tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
      tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      ThisTable.Load(tmpCommand.ExecuteReader)

    Catch ex As Exception

    Finally
      Try
        If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
          tmpCommand.Connection.Close()
          tmpCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    Return ThisTable

  End Function

  Private Sub GetInitialData()
    ' ********************************************************************************
    '
    ' ********************************************************************************

    Dim GetRows() As DataRow
    Dim thisRow As DataRow

    Dim ThisTable As DataTable = GetData()

    NodesToDelete.Clear()

    While (Me.TreeView_Instruments.Nodes.Count > 0)
      Me.TreeView_Instruments.Nodes.Remove(TreeView_Instruments.Nodes(TreeView_Instruments.Nodes.Count - 1))
    End While
    Me.TreeView_Instruments.Nodes.Clear()

    If (TreeView_Instruments.SelectedNode IsNot Nothing) Then
      TreeView_Instruments.SelectedNode.Remove()
      TreeView_Instruments.SelectedNode = Nothing
    End If

    If (TreeView_Instruments.TopNode IsNot Nothing) Then
      TreeView_Instruments.TopNode.Remove()
      TreeView_Instruments.TopNode = Nothing
    End If

    Dim ProcessedFolders As New List(Of Integer)

    Try

      Me.TreeView_Instruments.BeginUpdate()

      Dim RootNode As New TreeNode(">")
      RootNode.Tag = New ID_Struct((-1), (-1), (-1), (-1), ">", False)
      Me.TreeView_Instruments.Nodes.Add(RootNode)

      GetRows = ThisTable.Select("ParentFolderID <= 0", "FolderName, SimulationName")

      For Each thisRow In GetRows
        If (CInt(thisRow("FolderID")) > 0) Then
          ' Folder Entry

          If (Not (ProcessedFolders.Contains(CInt(thisRow("FolderID"))))) Then

            ProcessedFolders.Add(CInt(thisRow("FolderID")))

            Dim NewNode As New TreeNode(thisRow("FolderName"))
            NewNode.Tag = New ID_Struct(thisRow("FolderID"), thisRow("FolderID"), 0, thisRow("ParentFolderID"), thisRow("FolderName").ToString, False)
            NewNode.ForeColor = FOLDER_COLOUR
            NewNode.NodeFont = New System.Drawing.Font(TreeView_Instruments.Font, FontStyle.Bold)

            TreeView_Instruments.LabelEdit = True

            FillNode(NewNode, ThisTable)
            RootNode.Nodes.Add(NewNode)

          End If

        End If
      Next

      GetRows = ThisTable.Select("(FolderID <= 0) AND (SimulationID > 0)", "FolderName, SimulationName")

      For Each thisRow In GetRows
        If (CInt(thisRow("FolderID")) <= 0) AndAlso (CInt(thisRow("SimulationID")) > 0) Then
          ' Simulation Entry

          Dim NewNode As New TreeNode(thisRow("SimulationName"))
          NewNode.Tag = New ID_Struct(thisRow("SimulationID"), 0, thisRow("SimulationID"), thisRow("FolderID"), thisRow("SimulationName").ToString, True, CType(CInt(thisRow("SimulationDataPeriod")), DealingPeriod), "")
          NewNode.ForeColor = SIMULATION_COLOUR
          RootNode.Nodes.Add(NewNode)

        End If
      Next

      Me.TreeView_Instruments.EndUpdate()
      RootNode.Expand()
      TreeView_Instruments.SelectedNode = RootNode

      ProcessedFolders.Clear()

    Catch ex As Exception
    Finally
      ClearParametersPane()
    End Try

    FormChanged = False

    Me.btnCancel.Enabled = False
    Me.btnSave.Enabled = False

  End Sub

  Private Sub FillNode(ByVal InstrumentNode As TreeNode, ByVal SourceTable As DataTable)
    ' ********************************************************************************
    '
    ' ********************************************************************************

    Dim FolderID As Integer

    Try
      FolderID = CType(InstrumentNode.Tag, ID_Struct).FolderID
    Catch ex As Exception
      Exit Sub
    End Try

    Dim GetRows() As DataRow
    Dim thisRow As DataRow

    GetRows = SourceTable.Select("(ParentFolderID = " & FolderID & ")", "FolderName, SimulationName")

    InstrumentNode.Nodes.Clear()

    Dim ProcessedFolders As New List(Of Integer)

    Try
      Me.TreeView_Instruments.BeginUpdate()

      For Each thisRow In GetRows
        If (CInt(thisRow("FolderID")) > 0) Then
          ' Folder Entry

          If (Not (ProcessedFolders.Contains(CInt(thisRow("FolderID"))))) Then

            ProcessedFolders.Add(CInt(thisRow("FolderID")))

            Dim NewNode As New TreeNode(thisRow("FolderName"))
            NewNode.Tag = New ID_Struct(thisRow("FolderID"), thisRow("FolderID"), 0, thisRow("ParentFolderID"), thisRow("FolderName").ToString, False)
            NewNode.ForeColor = FOLDER_COLOUR
            NewNode.NodeFont = New System.Drawing.Font(TreeView_Instruments.Font, FontStyle.Bold)
            FillNode(NewNode, SourceTable)
            InstrumentNode.Nodes.Add(NewNode)

          End If

        End If
      Next

      GetRows = SourceTable.Select("(FolderID = " & FolderID & ") AND (SimulationID > 0)", "FolderName, SimulationName")

      For Each thisRow In GetRows
        If (CInt(thisRow("FolderID")) = FolderID) AndAlso (CInt(thisRow("SimulationID")) > 0) Then
          ' Simulation Entry

          Dim NewNode As New TreeNode(thisRow("SimulationName"))
          NewNode.Tag = New ID_Struct(thisRow("SimulationID"), 0, thisRow("SimulationID"), thisRow("FolderID"), thisRow("SimulationName").ToString, True)
          NewNode.ForeColor = SIMULATION_COLOUR
          InstrumentNode.Nodes.Add(NewNode)

        End If
      Next

      Me.TreeView_Instruments.EndUpdate()
      ProcessedFolders.Clear()

    Catch ex As Exception
      Exit Sub
    End Try


  End Sub

  Private Sub TreeView_Instruments_AfterExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView_Instruments.AfterExpand

  End Sub

  Private Sub TreeView_Instruments_BeforeExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewCancelEventArgs) Handles TreeView_Instruments.BeforeExpand
    'FillNode(e.Node)
  End Sub

  Private Sub TreeView_Instruments_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles TreeView_Instruments.ItemDrag
    ' ********************************************************************************
    '
    ' ********************************************************************************

    'Set the drag node and initiate the DragDrop 

    Try
      If Not (CType(e.Item, TreeNode) Is TreeView_Instruments.TopNode) Then
        DoDragDrop(e.Item, DragDropEffects.Move)
      End If
    Catch ex As Exception

    End Try

  End Sub

  Private Sub TreeView_Instruments_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_Instruments.DragEnter
    ' ********************************************************************************
    '
    ' ********************************************************************************

    'See if there is a TreeNode being dragged
    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", _
        True) Then
      'TreeNode found allow move effect
      e.Effect = DragDropEffects.Move
    Else
      'No TreeNode found, prevent move
      e.Effect = DragDropEffects.None
    End If
  End Sub

  Private Sub TreeView_Instruments_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeView_Instruments.DragLeave

  End Sub

  Private Sub TreeView_Instruments_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_Instruments.DragOver
    ' ********************************************************************************
    '
    ' ********************************************************************************

    'Check that there is a TreeNode being dragged 
    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", _
           True) = False Then Exit Sub

    'Get the TreeView raising the event (incase multiple on form)

    Dim selectedTreeview As TreeView = CType(sender, TreeView)

    'As the mouse moves over nodes, provide feedback to the user by highlighting the node that is the 
    'current drop target

    Dim pt As Point = _
        CType(sender, TreeView).PointToClient(New Point(e.X, e.Y))
    Dim targetNode As TreeNode = selectedTreeview.GetNodeAt(pt)

    If (targetNode Is Nothing) Then

      e.Effect = DragDropEffects.None
      Exit Sub
    End If

    ' Can't Drop on Simulations.

    If (CType(targetNode.Tag, ID_Struct).IsSimulation) Then

      e.Effect = DragDropEffects.None
      Exit Sub
    End If

    'See if the targetNode is currently selected, if so no need to validate again

    If Not (selectedTreeview.SelectedNode Is targetNode) Then
      'Select the    node currently under the cursor
      selectedTreeview.SelectedNode = targetNode

      'Check that the selected node is not the dropNode and
      'also that it is not a child of the dropNode and 
      'therefore an invalid target
      Dim dropNode As TreeNode = _
          CType(e.Data.GetData("System.Windows.Forms.TreeNode"),  _
          TreeNode)

      Do Until targetNode Is Nothing
        If targetNode Is dropNode Then
          e.Effect = DragDropEffects.None

          Exit Sub
        End If
        targetNode = targetNode.Parent
      Loop

      'Currently selected node is a suitable target
      e.Effect = DragDropEffects.Move

    End If
  End Sub

  Private Sub TreeView_Instruments_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView_Instruments.DragDrop
    ' ********************************************************************************
    '
    ' ********************************************************************************

    'Check that there is a TreeNode being dragged
    If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", _
          True) = False Then Exit Sub

    'Get the TreeView raising the event (incase multiple on form)
    Dim selectedTreeview As TreeView = CType(sender, TreeView)

    'Get the TreeNode being dragged
    Dim dropNode As TreeNode = _
          CType(e.Data.GetData("System.Windows.Forms.TreeNode"),  _
          TreeNode)

    'The target node should be selected from the DragOver event
    Dim targetNode As TreeNode = selectedTreeview.SelectedNode

    If (e.KeyState And 8) Then ' Ctrl Key is pressed

      CopyNodeData(dropNode, targetNode)

      Call FormControlChanged(Me, New System.EventArgs)

      'Ensure the newley created node is visible to
      'the user and select it
      targetNode.EnsureVisible()
      targetNode.ExpandAll()
      selectedTreeview.SelectedNode = targetNode

    Else

      If (CType(targetNode.Tag, ID_Struct).FolderID = CType(dropNode.Tag, ID_Struct).FolderParentID) Or ((CType(targetNode.Tag, ID_Struct).FolderID <= 0) And (CType(dropNode.Tag, ID_Struct).FolderID = CType(dropNode.Tag, ID_Struct).FolderParentID)) Then
        If (CType(dropNode.Tag, ID_Struct).IsFolder) Then
          dropNode.ForeColor = FOLDER_COLOUR
        Else
          dropNode.ForeColor = SIMULATION_COLOUR
        End If
      Else
        dropNode.ForeColor = ITEM_ALTERED_COLOUR
        Call FormControlChanged(Me, New System.EventArgs)
      End If

      'Remove the drop node from its current location
      ' dropNode.Remove()

      dropNode.Parent.Nodes.Remove(dropNode)

      'If there is no targetNode add dropNode to the bottom of
      'the TreeView root nodes, otherwise add it to the end of
      'the dropNode child nodes
      If targetNode Is Nothing Then
        selectedTreeview.Nodes.Add(dropNode)
      Else
        targetNode.Nodes.Add(dropNode)
      End If

      'Ensure the newley created node is visible to
      'the user and select it
      dropNode.EnsureVisible()
      selectedTreeview.SelectedNode = dropNode

    End If

  End Sub

  Private Sub TreeView_Instruments_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView_Instruments.AfterSelect

    Try
      Dim ThisNode As TreeNode = e.Node

      If (ThisNode IsNot Nothing) AndAlso (ThisNode.Tag IsNot Nothing) AndAlso (TypeOf ThisNode.Tag Is ID_Struct) Then
        Dim ThisStruct As ID_Struct = CType(ThisNode.Tag, ID_Struct)

        If ThisStruct.IsSimulation Then
          Dim ThisSimulation As CTASimulationFunctions.SimulationParameterClass = Nothing

          Try
            ThisSimulation = CTASimulationFunctions.SimulationParameterClass.DeSerialiseParameterClass(GetSimulationDefinition(ThisStruct.SimulationID))
          Catch ex As Exception
            ThisSimulation = Nothing
          End Try

          Label_Instrument.Text = MainForm.PertracData.GetInformationValue(ThisSimulation.InstrumentID, PertracInformationFields.Mastername)
          Label_InterestRate.Text = MainForm.PertracData.GetInformationValue(ThisSimulation.InterestRateID, PertracInformationFields.Mastername)
          Label_DataPeriod.Text = ThisSimulation.StatsDatePeriod.ToString
          Label_MovingAverage.Text = ThisSimulation.MovingAverageDays.ToString

          ClearParametersPane()
          PopulateParameterPane(ThisSimulation)
          PopulateParameterPane(GetAllChangedParameters(ThisNode))

        Else

          Label_Instrument.Text = ""
          Label_InterestRate.Text = ""
          Label_DataPeriod.Text = ""
          Label_MovingAverage.Text = ""

          ClearParametersPane()
          PopulateParameterPane(GetAllChangedParameters(ThisNode))
        End If

      Else
        Label_Instrument.Text = ""
        Label_InterestRate.Text = ""
        Label_DataPeriod.Text = ""
        Label_MovingAverage.Text = ""
        ClearParametersPane()
      End If

    Catch ex As Exception
      Label_Instrument.Text = ""
      Label_InterestRate.Text = ""
      Label_DataPeriod.Text = ""
      Label_MovingAverage.Text = ""
      ClearParametersPane()
    End Try

  End Sub

  Private Sub CopyNodeData(ByVal SourceNode As TreeNode, ByVal DestinationNode As TreeNode)
    ' ********************************************************************************
    '
    ' ********************************************************************************

    Dim NewNode As TreeNode
    Dim SourceStruct As ID_Struct

    ' First Copy the Top-Source Node

    SourceStruct = CType(SourceNode.Tag, ID_Struct)

    NewNode = New TreeNode("Copy of " & SourceNode.Text)
    NewNode.Tag = New ID_Struct(0, 0, 0, SourceStruct.FolderID, NewNode.Text, SourceStruct.IsSimulation, SourceStruct.SimulationDataPeriod, SourceStruct.SimulationDefinition)

    If (SourceStruct.SimulationID > 0) AndAlso (SourceStruct.SimulationDefinition.Length = 0) Then
      CType(NewNode.Tag, ID_Struct).SimulationDataPeriod = GetSimulationDataPeriod(SourceStruct.SimulationID)
      CType(NewNode.Tag, ID_Struct).SimulationDefinition = GetSimulationDefinition(SourceStruct.SimulationID)
    End If

    NewNode.NodeFont = SourceNode.NodeFont
    NewNode.ForeColor = ITEM_ALTERED_COLOUR

    DestinationNode.Nodes.Add(NewNode)

    If (SourceStruct.IsFolder) AndAlso (SourceNode.Nodes.Count > 0) Then
      ' Copy Childern

      Dim NewSimulationNode As TreeNode

      For Each ThisNode As TreeNode In SourceNode.Nodes

        SourceStruct = CType(ThisNode.Tag, ID_Struct)

        If (SourceStruct.IsFolder) Then

          CopyNodeData(ThisNode, NewNode)

        ElseIf (SourceStruct.IsSimulation) Then

          NewSimulationNode = New TreeNode("Copy of " & ThisNode.Text)
          NewSimulationNode.Tag = New ID_Struct(0, 0, 0, SourceStruct.FolderID, NewSimulationNode.Text, SourceStruct.IsSimulation, SourceStruct.SimulationDataPeriod, SourceStruct.SimulationDefinition)

          If (SourceStruct.SimulationID > 0) AndAlso (SourceStruct.SimulationDefinition.Length = 0) Then
            CType(NewSimulationNode.Tag, ID_Struct).SimulationDefinition = GetSimulationDefinition(SourceStruct.SimulationID)
          End If

          NewSimulationNode.NodeFont = ThisNode.NodeFont
          NewSimulationNode.ForeColor = ITEM_ALTERED_COLOUR

          NewNode.Nodes.Add(NewSimulationNode)

        End If

      Next

    End If

  End Sub

  Private Function GetSimulationDefinition(ByVal SimulationID As Integer) As String
    ' ********************************************************************************
    '
    ' ********************************************************************************

    Dim RVal As String = ""

    Try

      Dim tmpCommand As New SqlCommand
      Dim ThisTable As New RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationDataTable
      Dim ThisRow As RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationRow

      Try
        tmpCommand.CommandType = CommandType.Text
        tmpCommand.CommandText = "SELECT * FROM fn_tblCTA_Simulation_SelectKD(@KnowledgeDate) WHERE SimulationID = " & SimulationID.ToString
        tmpCommand.Connection = MainForm.GetCTAConnection
        tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
        tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

        ThisTable.Load(tmpCommand.ExecuteReader)

      Catch ex As Exception
      Finally
        Try
          If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
            tmpCommand.Connection.Close()
            tmpCommand.Connection = Nothing
          End If
        Catch ex As Exception
        End Try
      End Try

      If (ThisTable IsNot Nothing) AndAlso (ThisTable.Rows.Count > 0) Then
        ThisRow = ThisTable.Rows(0)
        RVal = ThisRow.SimulationDefinition
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Simulation definition", ex.StackTrace, True)
      RVal = ""
    End Try

    Return RVal

  End Function

  Private Function GetSimulationDataPeriod(ByVal SimulationID As Integer) As DealingPeriod
    ' ********************************************************************************
    '
    ' ********************************************************************************

    Dim RVal As DealingPeriod = DealingPeriod.Monthly

    Try

      Dim tmpCommand As New SqlCommand
      Dim ThisTable As New RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationDataTable
      Dim ThisRow As RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationRow

      Try
        tmpCommand.CommandType = CommandType.Text
        tmpCommand.CommandText = "SELECT * FROM fn_tblCTA_Simulation_SelectKD(@KnowledgeDate) WHERE SimulationID = " & SimulationID.ToString
        tmpCommand.Connection = MainForm.GetCTAConnection
        tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
        tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

        ThisTable.Load(tmpCommand.ExecuteReader)

      Catch ex As Exception
      Finally
        Try
          If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
            tmpCommand.Connection.Close()
            tmpCommand.Connection = Nothing
          End If
        Catch ex As Exception
        End Try
      End Try

      If (ThisTable IsNot Nothing) AndAlso (ThisTable.Rows.Count > 0) Then
        ThisRow = ThisTable.Rows(0)
        RVal = CType(ThisRow.SimulationDataPeriod, DealingPeriod)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error getting Simulation Data Period", ex.StackTrace, True)
      RVal = ""
    End Try

    Return RVal

  End Function

  Private Sub TreeView_Instruments_AfterLabelEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.NodeLabelEditEventArgs) Handles TreeView_Instruments.AfterLabelEdit
    ' ********************************************************************************
    '
    ' ********************************************************************************

    If (e.Node Is Nothing) OrElse (e.Node.Tag Is Nothing) Then
      Exit Sub
    End If

    Dim EditNode As TreeNode = e.Node

    EditNode.ForeColor = ITEM_ALTERED_COLOUR
    Call FormControlChanged(Me, New System.EventArgs)

  End Sub

  Private Sub btnDeleteFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteFolder.Click
    ' ********************************************************************************
    '
    ' ********************************************************************************

    ' Exit if no node is selected
    If Me.TreeView_Instruments.SelectedNode Is Nothing Then
      Exit Sub
    End If

    ' Get Current Node to see if it is a Simulation.

    Dim CurrentNode As TreeNode
    CurrentNode = Me.TreeView_Instruments.SelectedNode

    ' Can't Delete Root

    If (CurrentNode Is TreeView_Instruments.TopNode) Then
      Exit Sub
    End If

    ' Mark As Deleted

    If MessageBox.Show("Are you sure that you want to delete the chosen Folders ?" & vbCrLf & "The Folders and any Simulations that they contain will be permanently deleted.", "Delete Folders", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.OK Then

      NodesToDelete.Add(CurrentNode)

      CurrentNode.Parent.Nodes.Remove(CurrentNode)

      Call FormControlChanged(Me, New System.EventArgs)

    End If

  End Sub

  Private Sub btnAddFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddFolder.Click
    ' ********************************************************************************
    '
    ' ********************************************************************************

    ' Exit if no node is selected
    If Me.TreeView_Instruments.SelectedNode Is Nothing Then
      Exit Sub
    End If

    Dim CurrentNode As TreeNode
    CurrentNode = Me.TreeView_Instruments.SelectedNode
    If CurrentNode Is Nothing Then
      CurrentNode = Me.TreeView_Instruments.TopNode
    Else
      If (CType(CurrentNode.Tag, ID_Struct).IsSimulation) Then
        Exit Sub
      End If
    End If

    Dim NewNode As New TreeNode("New Folder")

    NewNode.Tag = New ID_Struct(0, 0, 0, Math.Max(0, CType(CurrentNode.Tag, ID_Struct).FolderID), "New Folder", False)
    NewNode.NodeFont = New System.Drawing.Font(TreeView_Instruments.Font, FontStyle.Bold)
    NewNode.ForeColor = ITEM_ALTERED_COLOUR

    CurrentNode.Nodes.Add(NewNode)
    CurrentNode.Expand()
    NewNode.BeginEdit()

  End Sub

  Private Sub Button_MoveUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    'Dim dropnode As TreeNode
    'Dim targetNode As TreeNode
    'Dim selectedTreeview As TreeView = TreeView_Instruments

    '' Exit if no node is selected
    'If Me.TreeView_Instruments.SelectedNode Is Nothing Then
    '  Exit Sub
    'End If

    'dropnode = Me.TreeView_Instruments.SelectedNode

    '' Exit if this is the root node
    'If (dropnode.Parent Is Nothing) Then
    '  Exit Sub
    'End If

    'targetNode = dropnode.Parent

    '' Exit if This node's parent is the root node
    'If (CType(targetNode.Tag, ID_Struct).FolderID <= 0) Then
    '  Exit Sub
    'End If
    'If (targetNode.Parent Is Nothing) Then
    '  Exit Sub
    'End If
    'targetNode = targetNode.Parent

    'If (CType(targetNode.Tag, ID_Struct).FolderID = CType(dropnode.Tag, ID_Struct).FolderParentID) Or ((CType(targetNode.Tag, ID_Struct).FolderID <= 0) And (CType(dropnode.Tag, ID_Struct).FolderID = CType(dropnode.Tag, ID_Struct).FolderParentID)) Then
    '  If (CType(dropnode.Tag, ID_Struct).IsFolder) Then
    '    dropnode.ForeColor = FOLDER_COLOUR
    '  Else
    '    dropnode.ForeColor = SIMULATION_COLOUR
    '  End If
    'Else
    '  dropnode.ForeColor = ITEM_ALTERED_COLOUR
    '  Call FormControlChanged(Me, New System.EventArgs)
    'End If


    ''Remove the drop node from its current location
    'dropnode.Remove()

    ''If there is no targetNode add dropNode to the bottom of
    ''the TreeView root nodes, otherwise add it to the end of
    ''the dropNode child nodes
    'If targetNode Is Nothing Then
    '  selectedTreeview.Nodes.Add(dropnode)
    'Else
    '  targetNode.Nodes.Add(dropnode)
    'End If

    ''Ensure the newley created node is visible to
    ''the user and select it
    'dropnode.EnsureVisible()
    'selectedTreeview.SelectedNode = dropnode


  End Sub

  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' *************************************************************
    Dim ProtectedItem As Boolean = False

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If Me.btnSave.Enabled = False Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to CTA are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim UpdateRows(0) As DataRow

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs()

    Try
      ' Set 'Paint' flag.
      InPaint = True

      ' Initiate Edit,
      Call SaveChangedNodes(Me.TreeView_Instruments.TopNode, UpdateMessage, CType(TreeView_Instruments.TopNode.Tag, ID_Struct).ChangedParameters)

      ' Delete Nodes

      If (NodesToDelete.Count > 0) Then
        Try

          For Each ThisNode As TreeNode In NodesToDelete

            DeleteNodeData(ThisNode, UpdateMessage)

          Next

        Catch ex As Exception
        Finally
          If (NodesToDelete IsNot Nothing) AndAlso (NodesToDelete.Count > 0) Then
            NodesToDelete.Clear()
          End If
        End Try

      End If

    Catch ex As Exception
    Finally
      InPaint = False
    End Try

    ' Finish off

    AddNewRecord = False
    FormChanged = False
    Me.btnCancel.Enabled = False
    Me.btnSave.Enabled = False

    ' Propogate changes

    Dim UpdateID As RenaissanceChangeID

    For Each UpdateID In UpdateMessage.TablesChanged
      If (UpdateID <> RenaissanceChangeID.None) Then
        MainForm.ReloadTable(UpdateID, False)
      End If
    Next

    Call MainForm.Main_RaiseEvent(UpdateMessage)

  End Function

  Private Sub SaveChangedNodes(ByRef pParentNode As TreeNode, ByVal UpdateMessage As RenaissanceGlobals.RenaissanceUpdateEventArgs, ByVal SimulationChanges() As SingleSimulationParameterClass)
    Dim ChildNode As TreeNode
    Dim AuditID As Integer
    Dim FolderID As Integer
    Dim CurrentParentFolderID As Integer
    Dim NewParentID As Integer
    Dim UpdateRows(0) As DataRow
    Dim ChildStruct As ID_Struct

    Try

      For Each ChildNode In pParentNode.Nodes

        ChildStruct = CType(ChildNode.Tag, ID_Struct)
        If (ChildStruct.FolderID = 0) AndAlso (ChildStruct.SimulationID = 0) AndAlso (ChildStruct.IsFolder) Then

          ' New Folder.
          ' Must Add New Folders first in case there is a new folder below it ( need accurate Parent Folder ID ).

          Try

            Dim ThisFolderTable As New RenaissanceDataClass.DSCTA_Folders.tblCTA_FoldersDataTable
            Dim ThisFolderRow As RenaissanceDataClass.DSCTA_Folders.tblCTA_FoldersRow = ThisFolderTable.NewtblCTA_FoldersRow

            ThisFolderRow.FolderID = 0
            ThisFolderRow.ParentFolderId = Math.Max(CType(pParentNode.Tag, ID_Struct).FolderID, 0)
            ThisFolderRow.FolderName = ChildNode.Text

            ThisFolderTable.AddtblCTA_FoldersRow(ThisFolderRow)

            MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCTA_Folders, New DataRow() {ThisFolderRow})
            UpdateMessage.TableChanged(RenaissanceChangeID.tblCTA_Folders) = True

            ChildStruct.FolderID = ThisFolderRow.FolderID
            ChildStruct.FolderParentID = ThisFolderRow.ParentFolderId
            ChildStruct.ItemName = ThisFolderRow.FolderName

          Catch ex As Exception

          End Try

        End If

        ' Process Children Nodes

        If (ChildNode.Nodes IsNot Nothing) AndAlso (ChildNode.Nodes.Count > 0) Then
          Call SaveChangedNodes(ChildNode, UpdateMessage, CType(ChildNode.Tag, ID_Struct).ChangedParameters(SimulationChanges))
        End If

        Try
          ' Get Information on this node and it's parent
          ChildStruct = CType(ChildNode.Tag, ID_Struct)

          AuditID = ChildStruct.AuditID
          FolderID = ChildStruct.FolderID
          CurrentParentFolderID = ChildStruct.FolderParentID
          NewParentID = Math.Max(CType(pParentNode.Tag, ID_Struct).FolderID, 0)

          If (ChildStruct.IsFolder) Then

            If (NewParentID <> CurrentParentFolderID) OrElse (ChildStruct.ItemName <> ChildNode.Text) Then

              Try

                Dim ThisFolderRow As RenaissanceDataClass.DSCTA_Folders.tblCTA_FoldersRow

                ThisFolderRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCTA_Folders, FolderID)

                ThisFolderRow.ParentFolderId = NewParentID
                ThisFolderRow.FolderName = ChildNode.Text

                MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCTA_Folders, New DataRow() {ThisFolderRow})
                UpdateMessage.TableChanged(RenaissanceChangeID.tblCTA_Folders) = True

              Catch ex As Exception

              End Try

              ChildStruct.FolderParentID = NewParentID
              ChildStruct.ItemName = ChildNode.Text

            End If

          ElseIf (ChildStruct.IsSimulation) Then

            Dim ThisSimulationTable As RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationDataTable = Nothing
            Dim ThisSimulationRow As RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationRow
            Dim ThisParametersChanged() As SingleSimulationParameterClass = ChildStruct.ChangedParameters(SimulationChanges)
            Dim ThisSim As CTASimulationFunctions.SimulationParameterClass = Nothing

            If (ChildStruct.SimulationDefinition Is Nothing) OrElse (ChildStruct.SimulationDefinition.Length <= 0) Then
              ThisSim = Get_UpdatedSimulation(GetSimulationDefinition(ChildStruct.SimulationID), ThisParametersChanged)
            Else
              ThisSim = Get_UpdatedSimulation(ChildStruct.SimulationDefinition, ThisParametersChanged)
            End If

            If (ChildStruct.SimulationID = 0) Then ' New Simulation

              If (ThisSimulationTable Is Nothing) Then
                ThisSimulationTable = New RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationDataTable
              End If

              ThisSimulationRow = ThisSimulationTable.NewtblCTA_SimulationRow

              ThisSimulationRow.SimulationID = 0
              ThisSimulationRow.SimulationName = ChildNode.Text
              ThisSimulationRow.SimulationFolder = NewParentID
              ThisSimulationRow.SimulationDataPeriod = ChildStruct.SimulationDataPeriod
              ThisSimulationRow.InstrumentID = ThisSim.InstrumentID
              ThisSimulationRow.InterestRateID = ThisSim.InterestRateID

              ThisSimulationRow.SimulationDefinition = CTASimulationFunctions.SimulationParameterClass.SerialiseParameterClass(ThisSim)

              ThisSimulationTable.Rows.Add(ThisSimulationRow)

              MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCTA_Simulation, New DataRow() {ThisSimulationRow})
              UpdateMessage.TableChanged(RenaissanceChangeID.tblCTA_Simulation) = True

              ' ChildStruct.FolderID = NewParentID ' Folder i sZero for Simulation entries.

              ChildStruct.SimulationID = ThisSimulationRow.SimulationID
              ChildStruct.FolderParentID = NewParentID
              ChildStruct.ItemName = ChildNode.Text

            ElseIf (NewParentID <> CurrentParentFolderID) OrElse (ChildStruct.ItemName <> ChildNode.Text) OrElse ((ThisParametersChanged IsNot Nothing) AndAlso (ThisParametersChanged.Length > 0)) Then

              ThisSimulationRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCTA_Simulation, ChildStruct.SimulationID)

              ThisSimulationRow.SimulationFolder = NewParentID
              ThisSimulationRow.SimulationName = ChildNode.Text
              ThisSimulationRow.InstrumentID = ThisSim.InstrumentID
              ThisSimulationRow.InterestRateID = ThisSim.InterestRateID

              If (ThisParametersChanged IsNot Nothing) AndAlso (ThisParametersChanged.Length > 0) Then
                ThisSimulationRow.SimulationDefinition = CTASimulationFunctions.SimulationParameterClass.SerialiseParameterClass(ThisSim)
              End If

              MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCTA_Simulation, New DataRow() {ThisSimulationRow})
              UpdateMessage.TableChanged(RenaissanceChangeID.tblCTA_Simulation) = True

              ' ChildStruct.FolderID = NewParentID ' Folder is Zero for Simulation entries.

              ChildStruct.FolderParentID = NewParentID
              ChildStruct.ItemName = ChildNode.Text

            End If

          End If

          ' Reset Parameter changes

          CType(ChildNode.Tag, ID_Struct).ParametersChanged = False

          ' Reset Colour

          If (CType(ChildNode.Tag, ID_Struct).IsFolder) Then
            ChildNode.ForeColor = FOLDER_COLOUR
          Else
            ChildNode.ForeColor = SIMULATION_COLOUR
          End If

        Catch ex As Exception
          AuditID = (-1)
          thisDataRow = Nothing
        End Try

      Next

    Catch ex As Exception
    End Try

  End Sub

  Private Function Get_UpdatedSimulation(ByVal SimulationDefinition As String, ByVal SimulationParameters() As SingleSimulationParameterClass) As CTASimulationFunctions.SimulationParameterClass
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************
    Dim ThisSim As CTASimulationFunctions.SimulationParameterClass = Nothing

    Try
      Dim ThisParameter As SingleSimulationParameterClass
      Dim ThisIsNumeric As Boolean
      Dim ThisIsDate As Boolean

      ThisSim = CTASimulationFunctions.SimulationParameterClass.DeSerialiseParameterClass(SimulationDefinition)

      If (SimulationParameters Is Nothing) OrElse (SimulationParameters.Length <= 0) Then
        Return ThisSim
      End If

      For ParamIndex As Integer = 0 To (SimulationParameters.Length - 1)

        ThisParameter = SimulationParameters(ParamIndex)

        ' Find any Later updates to this Parameter

        For LaterIndex As Integer = (ParamIndex + 1) To (SimulationParameters.Length - 1)
          If (SimulationParameters(LaterIndex).ParameterID = ThisParameter.ParameterID) Then
            ThisParameter = SimulationParameters(LaterIndex)
          End If
        Next

        ' Update Sim Parameter.

        If (ThisParameter.ParameterValue IsNot Nothing) Then

          ThisIsNumeric = IsNumeric(ThisParameter.ParameterValue)
          ThisIsDate = IsDate(ThisParameter.ParameterValue)

          Try

            Select Case ThisParameter.ParameterID

              Case CTASimulationFunctions.SimulationParameterEnum.InstrumentID
                If (ThisIsNumeric) Then ThisSim.InstrumentID = CInt(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.InterestRateID
                If (ThisIsNumeric) Then ThisSim.InterestRateID = CInt(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.DataStartDate
                If (ThisIsDate) Then ThisSim.DataStartDate = CDate(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_Days
                If (ThisIsNumeric) Then ThisSim.MovingAverageDays = CInt(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialPricingAverage
                ThisSim.MovingAverage_ExponentialPricingAverage = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialPricingLambda
                If (ThisIsNumeric) Then ThisSim.MovingAverage_ExponentialPricingLambda = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialWeightedAverage
                ThisSim.MovingAverage_ExponentialWeightedAverage = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialWeightedAverageLambda
                If (ThisIsNumeric) Then ThisSim.MovingAverage_ExponentialWeightedAverageLambda = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_LinearWeightedAverage
                ThisSim.MovingAverage_LinearWeightedAverage = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_LinearWeightedAverageLambda
                If (ThisIsNumeric) Then ThisSim.MovingAverage_LinearWeightedAverageLambda = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_VolatilityWeightedDuration
                ThisSim.MovingAverage_VolatilityWeightedDuration = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.MovingAverage_VolatilityWeightedDurationLambda
                If (ThisIsNumeric) Then ThisSim.MovingAverage_VolatilityWeightedDurationLambda = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.InitialNAV  ' Notional Starting NAV
                If (ThisIsNumeric) Then ThisSim.InitialNAV = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_AllowLongPosition
                ThisSim.Trading_AllowLongPosition = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_AllowShortPosition
                ThisSim.Trading_AllowShortPosition = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_RebalanceThreshold
                If (ThisIsNumeric) Then ThisSim.Trading_RebalanceThreshold = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BaselineGearing
                If (ThisIsNumeric) Then ThisSim.Trading_BaselineGearing = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_MaxDrawdown
                If (ThisIsNumeric) Then ThisSim.Trading_MaxDrawdown = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_DrawdownMonthlyReset
                If (ThisIsNumeric) Then ThisSim.Trading_DrawdownMonthlyReset = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_DrawdownResetMonths
                If (ThisIsNumeric) Then ThisSim.Trading_DrawdownResetMonths = CInt(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_TradingCosts
                If (ThisIsNumeric) Then ThisSim.Trading_TradingCosts = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_FundingSpread
                If (ThisIsNumeric) Then ThisSim.Trading_FundingSpread = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_StartDate
                If (ThisIsDate) Then ThisSim.Trading_StartDate = CDate(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_FirstTradeAtStart
                ThisSim.Trading_FirstTradeAtStart = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_StdDevHighVsAverage
                ThisSim.Trading_VolatilityWeighting_StdDevHighVsAverage = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_StdDevVsGivenDuration
                ThisSim.Trading_VolatilityWeighting_StdDevVsGivenDuration = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_VolWeightDuration
                If (ThisIsNumeric) Then ThisSim.Trading_VolatilityWeighting_VolWeightDuration = CInt(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_VolWeightCap
                If (ThisIsNumeric) Then ThisSim.Trading_VolatilityWeighting_VolWeightCap = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_Exponent
                If (ThisIsNumeric) Then ThisSim.Trading_VolatilityWeighting_Exponent = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByIndexZScore
                ThisSim.Trading_BandWeighting_ByIndexZScore = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_IndexZScoreThreshold
                If (ThisIsNumeric) Then ThisSim.Trading_BandWeighting_IndexZScoreThreshold = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByAverageZScore
                ThisSim.Trading_BandWeighting_ByAverageZScore = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_AverageZScoreThreshold
                If (ThisIsNumeric) Then ThisSim.Trading_BandWeighting_AverageZScoreThreshold = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ZScoreStdDevPeriod
                If (ThisIsNumeric) Then ThisSim.Trading_BandWeighting_ZScoreStdDevPeriod = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByPercentageBand
                ThisSim.Trading_BandWeighting_ByPercentageBand = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_PercentageBandThreshold
                If (ThisIsNumeric) Then ThisSim.Trading_BandWeighting_PercentageBandThreshold = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_WeightingDecayFactor
                If (ThisIsNumeric) Then ThisSim.Trading_BandWeighting_WeightingDecayFactor = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ReInvestmentEvaluationPeriod
                If (ThisIsNumeric) Then ThisSim.Trading_BandWeighting_ReInvestmentEvaluationPeriod = CInt(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_DailyWeightDeltaLimit
                If (ThisIsNumeric) Then ThisSim.Trading_BandWeighting_DailyWeightDeltaLimit = CDbl(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_LocalHighExclusionPeriod
                If (ThisIsNumeric) Then ThisSim.Trading_BandWeighting_LocalHighExclusionPeriod = CInt(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_PhaseOutAtCrossing
                ThisSim.Trading_BandWeighting_PhaseOutAtCrossing = CBool(ThisParameter.ParameterValue)

              Case CTASimulationFunctions.SimulationParameterEnum.Trading_CrossingUnweightPoint
                If (ThisIsNumeric) Then ThisSim.Trading_CrossingUnweightPoint = CDbl(ThisParameter.ParameterValue)

            End Select

          Catch ex As Exception
          End Try

        End If

      Next

      Return ThisSim

    Catch ex As Exception
    End Try

    Return ThisSim

  End Function

  Private Sub DeleteNodeData(ByRef pParentNode As TreeNode, ByVal UpdateMessage As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    If (pParentNode Is Nothing) Then
      Exit Sub
    End If

    Dim ChildNode As TreeNode
    Dim ThisStruct As ID_Struct

    For Each ChildNode In pParentNode.Nodes

      ' Delete Children

      DeleteNodeData(ChildNode, UpdateMessage)

    Next

    ' Delete This Node

    ThisStruct = CType(pParentNode.Tag, ID_Struct)

    If (ThisStruct.IsFolder) AndAlso (ThisStruct.FolderID > 0) Then

      Dim ThisFolderRow As RenaissanceDataClass.DSCTA_Folders.tblCTA_FoldersRow

      ThisFolderRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCTA_Folders, ThisStruct.FolderID)

      If (ThisFolderRow IsNot Nothing) AndAlso (Not (ThisFolderRow.RowState And DataRowState.Deleted)) Then
        ThisFolderRow.Delete()
        MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCTA_Folders, New DataRow() {ThisFolderRow})
        UpdateMessage.TableChanged(RenaissanceChangeID.tblCTA_Folders) = True
      End If

    ElseIf (ThisStruct.IsSimulation) AndAlso (ThisStruct.SimulationID > 0) Then

      Dim ThisSimulationRow As RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationRow

      ThisSimulationRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCTA_Simulation, ThisStruct.SimulationID)

      If (ThisSimulationRow IsNot Nothing) AndAlso (Not (ThisSimulationRow.RowState And DataRowState.Deleted)) Then
        ThisSimulationRow.Delete()
        MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCTA_Simulation, New DataRow() {ThisSimulationRow})
        UpdateMessage.TableChanged(RenaissanceChangeID.tblCTA_Simulation) = True
      End If

    End If

    If (pParentNode.Parent IsNot Nothing) Then
      pParentNode.Parent.Nodes.Remove(pParentNode)
    End If

  End Sub

  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
      ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

      Me.TreeView_Instruments.Enabled = True

    Else

      Me.TreeView_Instruments.Enabled = False

    End If

  End Sub

  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""

    ' Validate 

    Return RVal

  End Function

  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub

#End Region

  Private Function GetAllChangedParameters(ByVal ThisNode As TreeNode) As SingleSimulationParameterClass()

    Dim RVal() As SingleSimulationParameterClass = Nothing

    Try

      If (ThisNode.Parent IsNot Nothing) AndAlso (TypeOf ThisNode.Parent Is TreeNode) Then
        RVal = GetAllChangedParameters(ThisNode.Parent)
      End If

      RVal = CType(ThisNode.Tag, ID_Struct).ChangedParameters(RVal)

    Catch ex As Exception
    End Try

    Return rval

  End Function


#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    GetInitialData()

  End Sub

  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    Try

      If (FormChanged = True) Then
        Call SetFormData(False)

        ClearParametersPane()

        If (TreeView_Instruments.SelectedNode IsNot Nothing) Then
          TreeView_Instruments_AfterSelect(TreeView_Instruments.SelectedNode, New TreeViewEventArgs(TreeView_Instruments.SelectedNode))
        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' Close Form

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Control Event Code"

  Private Sub Check_Trading_MonthlyDrawdownReset_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Trading_MonthlyDrawdownReset.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Text_Trading_MonthlyDrawdownResetCount.Enabled = Check_Trading_MonthlyDrawdownReset.Checked

        Dim ThisControl As CheckBox = CType(sender, CheckBox)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_DrawdownMonthlyReset, ThisValue))
          Check_Trading_BandWeightings.ForeColor = ITEM_ALTERED_COLOUR

          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_Trading_BandWeightings_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Trading_BandWeightings.CheckedChanged
    ' *****************************************************************************
    '
    '
    '
    ' *****************************************************************************


    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Panel_Trading_BandWeighting.Enabled = Check_Trading_BandWeightings.Checked

        Dim ThisControl As CheckBox = CType(sender, CheckBox)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          If (Radio_Trading_ByIndexZScore.Checked) OrElse (Radio_Trading_ByAverageZScore.Checked) OrElse (Radio_Trading_ByPercentageBand.Checked) Then
            If (ThisValue) Then
              ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByIndexZScore, Radio_Trading_ByIndexZScore.Checked))
              ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByAverageZScore, Radio_Trading_ByAverageZScore.Checked))
              ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByPercentageBand, Radio_Trading_ByPercentageBand.Checked))

              Radio_Trading_ByIndexZScore.ForeColor = ITEM_ALTERED_COLOUR
              Radio_Trading_ByAverageZScore.ForeColor = ITEM_ALTERED_COLOUR
              Radio_Trading_ByPercentageBand.ForeColor = ITEM_ALTERED_COLOUR
            Else
              ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByIndexZScore, False))
              ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByAverageZScore, False))
              ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByPercentageBand, False))
            End If

            TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR

          End If

          Check_Trading_BandWeightings.ForeColor = ITEM_ALTERED_COLOUR

          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetSimParameter_Double(ByVal sender As System.Object, ByVal ControlText As String, ByVal MinLimit As Double, ByVal SimID As CTASimulationFunctions.SimulationParameterEnum)

    Try

      Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)

      If (ConvertIsNumeric(ControlText)) Then

        Dim ThisValue As Double = ConvertValue(ControlText, GetType(Double))

        If (ThisValue >= MinLimit) Then
          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(SimID, ThisValue))

          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          If (TypeOf sender Is TextBox) Then
            CType(sender, TextBox).ForeColor = ITEM_ALTERED_COLOUR
          End If
          Call FormControlChanged(sender, New System.EventArgs)

        Else
          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(SimID, Nothing))

        End If

      Else
        ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(SimID, Nothing))

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetSimParameter_Date(ByVal sender As System.Object, ByVal ControlText As String, ByVal MinLimit As Date, ByVal SimID As CTASimulationFunctions.SimulationParameterEnum)

    Try

      Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)

      If (IsDate(ControlText)) Then

        Dim ThisValue As Date = CDate(ControlText)

        If (ThisValue >= MinLimit) Then
          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(SimID, ThisValue))

          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          If (TypeOf sender Is TextBox) Then
            CType(sender, TextBox).ForeColor = ITEM_ALTERED_COLOUR
          End If
          Call FormControlChanged(sender, New System.EventArgs)

        Else
          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(SimID, Nothing))

        End If

      Else
        ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(SimID, Nothing))

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_MovingAverageDays_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_MovingAverageDays.TextChanged

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 1.0#, CTASimulationFunctions.SimulationParameterEnum.MovingAverage_Days)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_ExponentialPrice_Lambda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_ExponentialPrice_Lambda.TextChanged

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.01#, CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialPricingLambda)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_ExponentialAverage_Lambda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_ExponentialAverage_Lambda.TextChanged

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.01#, CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialWeightedAverageLambda)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_LinearAverage_Lambda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_LinearAverage_Lambda.TextChanged

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.01#, CTASimulationFunctions.SimulationParameterEnum.MovingAverage_LinearWeightedAverageLambda)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_VolatilityAverage_Lambda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_VolatilityAverage_Lambda.TextChanged

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.01#, CTASimulationFunctions.SimulationParameterEnum.MovingAverage_VolatilityWeightedDurationLambda)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_TrandingCosts_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_TrandingCosts.TextChanged

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_TradingCosts)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_FundingSpread_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_FundingSpread.TextChanged

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_FundingSpread)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_StartDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_StartDate.TextChanged

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Date(sender, ThisControl.Text, Renaissance_BaseDate, CTASimulationFunctions.SimulationParameterEnum.MovingAverage_VolatilityWeightedDurationLambda)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_MaxDrawdown_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_MaxDrawdown.TextChanged

    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_MaxDrawdown)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_MonthlyDrawdownResetCount_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_MonthlyDrawdownResetCount.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_DrawdownResetMonths)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_RebalanceThreshold_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_RebalanceThreshold.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_RebalanceThreshold)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_BaselineGearing_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_BaselineGearing.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_BaselineGearing)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_VolExponent_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_VolExponent.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_Exponent)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_VolWeightCap_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_VolWeightCap.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_VolWeightCap)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_VolWeightDuration_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_VolWeightDuration.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_VolWeightDuration)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_IndexZScore_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_IndexZScore.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_IndexZScoreThreshold)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_AverageZScore_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_AverageZScore.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_AverageZScoreThreshold)

        End If
      End If
    Catch ex As Exception
    End Try


  End Sub

  Private Sub Text_Trading_ZScore_Period_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_ZScore_Period.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ZScoreStdDevPeriod)

        End If
      End If
    Catch ex As Exception
    End Try


  End Sub

  Private Sub Text_Trading_PercentBand_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_PercentBand.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_PercentageBandThreshold)

        End If
      End If
    Catch ex As Exception
    End Try


  End Sub

  Private Sub Text_Trading_WeightingDecayFactor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_WeightingDecayFactor.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_WeightingDecayFactor)

        End If
      End If
    Catch ex As Exception
    End Try


  End Sub

  Private Sub Text_Trading_ReInvestmentEvaluationPeriod_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_ReInvestmentEvaluationPeriod.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ReInvestmentEvaluationPeriod)

        End If
      End If
    Catch ex As Exception
    End Try


  End Sub

  Private Sub Text_Trading_BandDeltaLimit_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_BandDeltaLimit.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_DailyWeightDeltaLimit)

        End If
      End If
    Catch ex As Exception
    End Try


  End Sub

  Private Sub Text_LocalHighExclusionPeriod_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_LocalHighExclusionPeriod.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_LocalHighExclusionPeriod)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Text_Trading_CrossingUnweightPoint_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Text_Trading_CrossingUnweightPoint.TextChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then

        Dim ThisControl As TextBox = CType(sender, TextBox)

        If (ThisControl.Focused) Then

          SetSimParameter_Double(sender, ThisControl.Text, 0.0#, CTASimulationFunctions.SimulationParameterEnum.Trading_CrossingUnweightPoint)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_ExponentialPriceAverage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ExponentialPriceAverage.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As CheckBox = CType(sender, CheckBox)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialPricingAverage, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_UnAdjustedAverage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_UnAdjustedAverage.CheckedChanged
    ' Not Required
  End Sub

  Private Sub Radio_ExponentialWeightedAverage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExponentialWeightedAverage.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As RadioButton = CType(sender, RadioButton)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.MovingAverage_ExponentialWeightedAverage, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_LinearWeightedAverage_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_LinearWeightedAverage.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As RadioButton = CType(sender, RadioButton)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.MovingAverage_LinearWeightedAverage, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_VolatilityWeightedDuration_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_VolatilityWeightedDuration.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As RadioButton = CType(sender, RadioButton)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.MovingAverage_VolatilityWeightedDuration, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_Trading_FirstTradeStart_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_FirstTradeStart.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As RadioButton = CType(sender, RadioButton)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_FirstTradeAtStart, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_Trading_FirstTradeCrossing_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_FirstTradeCrossing.CheckedChanged
    ' Not Required
  End Sub

  Private Sub Check_Trading_AllowLong_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Trading_AllowLong.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As CheckBox = CType(sender, CheckBox)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_AllowLongPosition, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Check_Trading_AllowShort_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Trading_AllowShort.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As CheckBox = CType(sender, CheckBox)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_AllowShortPosition, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Radio_Trading_VolWeightNone_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_VolWeightNone.CheckedChanged
    ' Not Needed
  End Sub

  Private Sub Radio_Trading_VolWeightByHighStDev_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_VolWeightByHighStDev.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As RadioButton = CType(sender, RadioButton)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_StdDevHighVsAverage, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Radio_Trading_VolWeight_VsSecondDuration_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_VolWeight_VsSecondDuration.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As RadioButton = CType(sender, RadioButton)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_VolatilityWeighting_StdDevVsGivenDuration, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Radio_Trading_ByIndexZScore_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_ByIndexZScore.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As RadioButton = CType(sender, RadioButton)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByIndexZScore, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Radio_Trading_ByAverageZScore_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_ByAverageZScore.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As RadioButton = CType(sender, RadioButton)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByAverageZScore, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Radio_Trading_ByPercentageBand_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Trading_ByPercentageBand.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As RadioButton = CType(sender, RadioButton)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_ByPercentageBand, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Check_Trading_PhaseOutAtCrossing_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Trading_PhaseOutAtCrossing.CheckedChanged
    Try
      If (Me.Created) AndAlso (Not Me.IsDisposed) Then
        Dim ThisControl As CheckBox = CType(sender, CheckBox)

        If (ThisControl.Focused) Then
          Dim ThisIDStruct As ID_Struct = CType(TreeView_Instruments.SelectedNode.Tag, ID_Struct)
          Dim ThisValue As Boolean = ThisControl.Checked

          ThisIDStruct.AddChangedSimulationParameter(New SingleSimulationParameterClass(CTASimulationFunctions.SimulationParameterEnum.Trading_BandWeighting_PhaseOutAtCrossing, ThisValue))

          ThisControl.ForeColor = ITEM_ALTERED_COLOUR
          TreeView_Instruments.SelectedNode.ForeColor = ITEM_ALTERED_COLOUR
          Call FormControlChanged(sender, New System.EventArgs)

        End If
      End If
    Catch ex As Exception
    End Try
  End Sub


#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region







End Class
