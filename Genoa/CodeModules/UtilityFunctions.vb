Imports System.Data.SqlClient
Imports System.Security.Principal


Module UtilityFunctions

  Function GetUserName() As String
    Dim RVal As String

    RVal = WindowsIdentity.GetCurrent.Name

    If RVal.Contains("\") Then
      Dim parts() As String = Split(RVal, "\")
      Dim username As String = parts(1)
      Return username
    End If

    Return RVal
  End Function

  Friend Function Nz(ByVal FirstObject As Object, ByVal SecondObject As Object) As Object
		' *************************************************************************************
		'
		'
		' *************************************************************************************

		If (FirstObject Is Nothing) OrElse (FirstObject Is DBNull.Value) Then
			Return SecondObject
		Else
			Return FirstObject
		End If
  End Function

	Friend Function Nz(ByVal FirstObject As Object, ByVal ReturnType As System.Type) As Object
		' *************************************************************************************
		'
		'
		' *************************************************************************************

		Try

			If (FirstObject Is Nothing) OrElse (FirstObject Is DBNull.Value) Then
        If (ReturnType.Name = "String") Then
          Return ""
        ElseIf (ReturnType.Name.StartsWith("Int")) Then
          Return 0
        ElseIf (ReturnType.Name = "Double") Then
          Return 0
        ElseIf (ReturnType.Name = "Date") Then
          Return New Date
        ElseIf (ReturnType.Name = "Boolean") Then
          Return False
        Else
          Return ReturnType.Assembly.CreateInstance(ReturnType.Name)
        End If

      Else
        Return FirstObject
			End If
		Catch ex As Exception
			Return Nothing
		End Try
	End Function

  Public Function CompareValue(ByVal val1 As IComparable, _
  ByVal val2 As IComparable) As Integer
		' *************************************************************************************
		'
		'
		' *************************************************************************************


    Try
      Return val1.CompareTo(val2)
    Catch ex As Exception
    End Try

    If IsNumeric(val1) AndAlso IsNumeric(val2) Then
      Try
        Return CDbl(val1).CompareTo(CDbl(val2))
      Catch ex As Exception
      End Try
    End If

    If IsDate(val1) AndAlso IsDate(val2) Then
      Try
        Return CDate(val1).CompareTo(CDate(val2))
      Catch ex As Exception
      End Try
    End If

    Return CStr(val1).ToUpper.CompareTo(CStr(val2).ToUpper)

  End Function

	Public Class MaxFunctions

		Public Overloads Shared Function MAX(ByVal V1 As Double, ByVal V2 As Double) As Double
			Try
				If (V1 > V2) Then
					Return V1
				Else
					Return V2
				End If
			Catch ex As Exception
			End Try

			Return V2
		End Function

		Public Overloads Shared Function MAX(ByVal V1 As Integer, ByVal V2 As Integer) As Integer
			Try
				If (V1 > V2) Then
					Return V1
				Else
					Return V2
				End If
			Catch ex As Exception
			End Try

			Return V2
		End Function

		Public Overloads Shared Function MAX(ByVal V1 As Date, ByVal V2 As Date) As Date
			Try
				If (V1 > V2) Then
					Return V1
				Else
					Return V2
				End If
			Catch ex As Exception
			End Try

			Return V2
		End Function

		Public Overloads Shared Function MIN(ByVal V1 As Double, ByVal V2 As Double) As Double
			Try
				If (V1 < V2) Then
					Return V1
				Else
					Return V2
				End If
			Catch ex As Exception
			End Try

			Return V2
		End Function

		Public Overloads Shared Function MIN(ByVal V1 As Integer, ByVal V2 As Integer) As Integer
			Try
				If (V1 < V2) Then
					Return V1
				Else
					Return V2
				End If
			Catch ex As Exception
			End Try

			Return V2
		End Function

		Public Overloads Shared Function MIN(ByVal V1 As Date, ByVal V2 As Date) As Date
			Try
				If (V1 < V2) Then
					Return V1
				Else
					Return V2
				End If
			Catch ex As Exception
			End Try

			Return V2
		End Function

	End Class

	Public Function DefaultPrecision(ByVal pNumber As Double) As Integer
		' ***********************************************************************
		'
		'
		' ***********************************************************************

		Dim RValPrecision As Integer = 0
		Dim LogValue As Integer

    Try
      If (Math.Abs(pNumber) >= 9.9) Then
        LogValue = CInt(Math.Log10(Math.Abs(pNumber)))
      End If

      If (LogValue > 0) Then
        RValPrecision = 6 - LogValue
      End If

      If (RValPrecision < 0) Then
        RValPrecision = 0
      End If

    Catch ex As Exception
    End Try

		Return (RValPrecision)

	End Function

	Public Function DefaultNumericFormat(ByVal pNumber As Double) As String
		' ***********************************************************************
		'
		'
		' ***********************************************************************

		Dim PrecisionToShow As Integer = 0
		Dim FormatString As String = "#,##0"

		Try
			PrecisionToShow = DefaultPrecision(pNumber)

			If (PrecisionToShow > 0) Then
				FormatString &= "." & New String(CType("0", Char), PrecisionToShow)
			End If
		Catch ex As Exception
		End Try

		Return FormatString
	End Function

	Public Function DefaultPercentageFormat(ByVal pNumber As Double) As String
		' ***********************************************************************
		'
		'
		' ***********************************************************************

		Return DefaultNumericFormat(pNumber) & "%"

	End Function

	Friend Function ConvertValue(ByVal FirstObject As Object, ByVal ReturnType As System.Type) As Object
		' *************************************************************************************
		'
		'
		' *************************************************************************************

		' Return Default Value if FirstObject is Nothing
		Try
			If (FirstObject Is Nothing) Then
				Return Nz(Nothing, ReturnType)
			End If
		Catch ex As Exception
		End Try

		Try
			Dim Multiplier As Double = 1.0#
			Dim TempString As String

			If (ReturnType.Name = "String") Then
				Return CStr(FirstObject)
			ElseIf (ReturnType.Name = "Double") Then
				If IsNumeric(FirstObject) Then
					Return CDbl(FirstObject)
				Else
					TempString = FirstObject.ToString.ToUpper

					While (TempString.Length > 0) AndAlso (IsNumeric(TempString.Substring(TempString.Length - 1)) = False)
						If (TempString.EndsWith("%")) Then
							Multiplier /= 100.0#
						ElseIf (TempString.EndsWith("K")) Then
							Multiplier *= 1000.0#
						ElseIf (TempString.EndsWith("M")) Then
							Multiplier *= 1000000.0#
						ElseIf (TempString.EndsWith("MIL")) Then
							Multiplier *= 1000000.0#
							TempString = TempString.Substring(0, TempString.Length - 2)
						End If

						TempString = TempString.Substring(0, TempString.Length - 1)
					End While

					If IsNumeric(TempString) Then
						Return CDbl(TempString) * Multiplier
					End If

					Return 0
				End If

				'If (FirstObject.ToString.EndsWith("%")) AndAlso (IsNumeric(FirstObject.ToString.Substring(0, FirstObject.ToString.Length - 1))) Then
				'	Return (CDbl(FirstObject.ToString.Substring(0, FirstObject.ToString.Length - 1)) / 100.0#)
				'Else
				'	Return 0
				'End If
			ElseIf (ReturnType.Name.StartsWith("Int")) Then
				If IsNumeric(FirstObject) Then
					Return CInt(FirstObject)
				Else
					TempString = FirstObject.ToString.ToUpper

					While (TempString.Length > 0) AndAlso (IsNumeric(TempString.Substring(TempString.Length - 1)) = False)
						If (TempString.EndsWith("%")) Then
							Multiplier /= 100.0#
						ElseIf (TempString.EndsWith("K")) Then
							Multiplier *= 1000.0#
						ElseIf (TempString.EndsWith("M")) Then
							Multiplier *= 1000000.0#
						ElseIf (TempString.EndsWith("MIL")) Then
							Multiplier *= 1000000.0#
							TempString = TempString.Substring(0, TempString.Length - 2)
						End If

						TempString = TempString.Substring(0, TempString.Length - 1)
					End While

					If IsNumeric(TempString) Then
						Return CInt(TempString) * Multiplier
					End If

					Return 0
				End If

				'If IsNumeric(FirstObject) Then
				'	Return CInt(FirstObject)
				'ElseIf (FirstObject.ToString.EndsWith("%")) AndAlso (IsNumeric(FirstObject.ToString.Substring(0, FirstObject.ToString.Length - 1))) Then
				'	Return (CInt(FirstObject.ToString.Substring(0, FirstObject.ToString.Length - 1)) / 100.0#)
				'Else
				'	Return 0
				'End If
			ElseIf (ReturnType.Name = "Date") Then
				If IsDate(FirstObject) Then
					Return CDate(FirstObject)
				Else
					Return 0
				End If
			ElseIf (ReturnType.Name = "Boolean") Then
				Return CBool(FirstObject)
			End If

			If (FirstObject Is Nothing) OrElse (FirstObject Is DBNull.Value) Then
				Return ReturnType.Assembly.CreateInstance(ReturnType.Name)
			Else
				Return FirstObject
			End If
		Catch ex As Exception
			Return Nothing
		End Try
	End Function

	Friend Function ConvertIsNumeric(ByVal FirstObject As Object) As Boolean
		' *************************************************************************************
		' Test to see if the given value is convertible to a numeric value.
		'
		' *************************************************************************************

		Try
			If (FirstObject Is Nothing) Then
				Return False
			End If
		Catch ex As Exception
		End Try

		Try
			Dim Multiplier As Double = 1.0#
			Dim TempString As String

			If IsNumeric(FirstObject) Then
				Return True
			Else
				TempString = FirstObject.ToString.ToUpper

				While (TempString.Length > 0) AndAlso (IsNumeric(TempString.Substring(TempString.Length - 1)) = False)
					If (TempString.EndsWith("%")) Then
						Multiplier /= 100.0#
					ElseIf (TempString.EndsWith("K")) Then
						Multiplier *= 1000.0#
					ElseIf (TempString.EndsWith("M")) Then
						Multiplier *= 1000000.0#
					ElseIf (TempString.EndsWith("MIL")) Then
						Multiplier *= 1000000.0#
						TempString = TempString.Substring(0, TempString.Length - 2)
					End If

					TempString = TempString.Substring(0, TempString.Length - 1)
				End While

				If IsNumeric(TempString) Then
					Return True
				End If

				Return False
			End If
		Catch ex As Exception
		End Try

		Return False

	End Function

	Friend Sub RemoveDataSetRows(ByRef pDataTable As DataTable, ByVal pSelectedRows() As DataRow)
		' *************************************************************************
		' Routine to remove given DataRows from the Given table.
		'
		' *************************************************************************

		Try
			Dim RowCount As Integer

			' Basic Validation :-

			If (pDataTable Is Nothing) OrElse (pDataTable.Rows.Count <= 0) Then
				Exit Sub
			End If

			If (pSelectedRows Is Nothing) OrElse (pSelectedRows.Length <= 0) Then
				Exit Sub
			End If

			' OK, here we go...

			For RowCount = 0 To (pSelectedRows.Length - 1)
				Try
					pDataTable.Rows.Remove(pSelectedRows(RowCount))
				Catch ex As Exception
				End Try
			Next

		Catch ex As Exception
		End Try

	End Sub


#Region " Normal Distribution functions :- "

	Public Function NORMSDIST(ByVal x As Double, ByVal cumulative As Boolean) As Double
		Return NORMDIST(x, 0, 1, cumulative)
	End Function

	Public Function NORMDIST(ByVal x As Double, ByVal mean As Double, ByVal std As Double, ByVal cumulative As Boolean) As Double

		If (cumulative) Then
			Return Phi(x, mean, std)
		Else
			Dim tmp As Double = 1 / ((Math.Sqrt(2 * Math.PI) * std))
			Return (tmp * Math.Exp(-0.5 * Math.Pow((x - mean) / std, 2)))
		End If

	End Function

	Public Function NORMSINV(ByVal Probability As Double) As Double
		Return NORMINV(Probability, 0.0#, 1.0#)
	End Function

	Public Function NORMINV(ByVal Probability As Double, ByVal mean As Double, ByVal std As Double) As Double
		' Target a value of x for which NORMDIST() = Probability

		Dim LBound As Double = (-5.0#)
		Dim LBoundNorm As Double
		Dim UBound As Double = 5.0#
		Dim UBoundNorm As Double
		Dim MidVal As Double
		Dim MidValNorm As Double
		Dim EPSILON As Double = 0.0000001
		Dim IterationCount As Integer = 0

		LBoundNorm = NORMDIST(LBound, mean, std, True)
		If (Probability <= LBoundNorm) Then
			Return LBound
		End If

		UBoundNorm = NORMDIST(UBound, mean, std, True)
		If (Probability >= UBoundNorm) Then
			Return UBound
		End If

		While 1
			MidVal = (LBound + UBound) / 2.0#
			MidValNorm = NORMDIST(MidVal, mean, std, True)

			If (Math.Abs(Probability - MidValNorm) <= EPSILON) Then
				Return MidVal
			ElseIf (Probability <= MidValNorm) Then
				UBound = MidVal
			Else
				LBound = MidVal
			End If

			If (IterationCount >= 100) OrElse ((UBound - LBound) <= EPSILON) Then
				Return ((LBound + UBound) / 2.0#)
			End If

			IterationCount += 1
		End While
	End Function

	'//from http://www.cs.princeton.edu/introcs/26function/MyMath.java.html
	'// fractional error less than 1.2 * 10 ^ -7.
	Public Function erf(ByVal z As Double) As Double

		Dim t As Double = 1.0 / (1.0 + 0.5 * Math.Abs(z))

		'// use Horner's method
		Dim ans As Double = 1 - t * Math.Exp(-z * z - 1.26551223 + _
		t * (1.00002368 + _
		t * (0.37409196 + _
		t * (0.09678418 + _
		t * (-0.18628806 + _
		t * (0.27886807 + _
		t * (-1.13520398 + _
		t * (1.48851587 + _
		t * (-0.82215223 + _
		t * (0.17087277))))))))))

		If (z >= 0) Then
			Return ans
		Else
			Return -ans
		End If
	End Function

	'// cumulative normal distribution
	Public Function Phi(ByVal z As Double) As Double
		Return 0.5 * (1.0 + erf(z / (Math.Sqrt(2.0))))
	End Function


	'// cumulative normal distribution with mean mu and std deviation sigma
	Public Function Phi(ByVal z As Double, ByVal mu As Double, ByVal sigma As Double) As Double
		Return Phi((z - mu) / sigma)
	End Function

#End Region

#Region "String Wildcard Match"

	Private Const WC_QUES As Char = "?"
	Private Const WC_STAR1 As Char = "*"
	Private Const WC_STAR2 As Char = "%"

	Public Enum WC_RESULT As Integer
		WC_MATCH = 0		 '/* char/string-match succeed */
		WC_MISMATCH			 '/* general char/string-match fail */
		WC_PAT_NULL_PTR	 '/* (char *) pattern == NULL */
		WC_CAN_NULL_PTR	 '/* (char *) candidate == NULL */
		WC_PAT_TOO_SHORT '/* too few pattern chars to satisfy count */
		WC_CAN_TOO_SHORT '/* too few candidate chars to satisy '?' in pattern */
	End Enum

	'/* ***************************************************************************
	' *
	' *          Copyright 1992-2005 by Pete Wilson All Rights Reserved
	' *           50 Staples Street : Lowell Massachusetts 01851 : USA
	' *       http://www.pwilson.net/   pete at pwilson dot net   +1 978-454-4547
	' *
	' * This item is free software; you can redistribute it and/or modify it under 
	' * the terms of the GNU General Public License as published by the 
	' * Free Software Foundation; either version 2.1 of the License, or (at your 
	' * option) any later version.
	' *
	' * Pete Wilson prepared this item in the hope that it might be useful, but it 
	' * has NO WARRANTY WHATEVER, nor even any implied warranty of MERCHANTABILITY 
	' * or FITNESS FOR A PARTICULAR PURPOSE. 
	' *
	' *************************************************************************** */

	'/* ***************************************************************************
	' *
	' *                          WC_STRNCMP.C
	' *
	' * Public function to match two strings with or without wildcards:
	' *    int wc_strncmp()  return zero if string match, else non-zero.
	' *
	' * Local private function to match two characters:
	' *    int ch_eq()       return zero if char match, else non-zero.       
	' *
	' * Revision History:
	' *
	' *     DATE      VER                     DESCRIPTION
	' * -----------  ------  ----------------------------------------------------
	' *  8-nov-2001   1.06   rewrite
	' * 20-jan-2002   1.07   rearrange wc test; general cleanup
	' * 14-feb-2002   1.08   fix bug (thanks, steph!): wc test outside ?,* tests
	' * 20-feb-2002   1.09   rearrange commentary as many suggested
	' * 25-feb-2002   1.10   wc_strlen() return size_t per dt, excise from here
	' * 08-May-2008   1.11   Re-Coded to VB, Nicholas Pennington.
	' *
	' *************************************************************************** */

	'/* ***************************************************************************
	' *
	' * Function:   wc_strncmp(
	' *               const char * pattern,   // match this string (can contain ?, *)
	' *               const char * candidate, //   against this one.
	' *               int count,              // require at least count chars in pattern
	' *               int do_case,            // 0 = no case, !0 = cased compare
	' *               int do_wildcards)       // 0 = no wc's, !0 = honor ? and *
	' *
	' * Action:     See if the string pointed by candidate meets the criteria    
	' *             given in the string pointed by pattern. The pattern string   
	' *             can contain the special wild-card characters:                
	' *             * -- meaning match any number of characters, including zero, 
	' *               in the candidate string;                                   
	' *             ? -- meaning match any single character, which must be present 
	' *               to satisfy the match, in the candidate string.
	' *
	' *             The int arg count tells the minimum length of the pattern
	' *             string, after '*' expansion, that will satisfy the string
	' *             compare in this call. If count is negative, then forbid
	' *             abbreviations: pattern and candidate strings must match exactly
	' *             in content and length to give a good compare. If count is 0, 
	' *             then an empty pattern string (pattern == "") returns success. 
	' *             If count is positive, then must match "count" characters
	' *             in the two strings to succeed, except we yield success if:
	' *             -- pattern and candidate strings are the same length; or
	' *             -- pattern string is shorter than "count"; or
	' *             -- do_wildcards > 0 and final pattern char == '*'. 
	' * 
	' *             If the integer argument do_case == 0, then ignore the case of 
	' *             each character: compare the tolower() of each character; if 
	' *             do_case != 0, then consider case in character compares.
	' *
	' *             If the int arg do_wildcards == 0, then treat the wildcard 
	' *             characters '*' and '?' just like any others: do a normal 
	' *             char-for-char string compare. But if do_wildcards is nonzero, 
	' *             then the string compare uses those wildcard characters as 
	' *             you'd expect.
	' *
	' * Returns:    WC_MATCH on successful match. If not WC_MATCH, then the
	' *             return val conveys a little more info: see wc_strncmp.h
	' *
	' * Note on Resynchronization: After a span of one or more pattern-string '*' 
	' *             found; and an immediately following span of zero [sic: 0] or 
	' *             more '?', we have to resynchronize the candidate string 
	' *             with the pattern string: need to find a place in the 
	' *             candidate to restart our compares. We've not a clue how many 
	' *             chars a span of '*' will match/absorb/represent until we find 
	' *             the next matching character in the candidate string. 
	' *             For example:
	' *       
	' * -------------------------------------------------------------------------
	' *   patt   cand   resync at 'A'           what's going on?
	' *  ------ ------- ------------- ------------------------------------------
	' *  "***A" "AbcdA"   "A----"     "***" matches zero characters
	' *  "**A"  "aAcdA"   "-A---"     "**" absorbs just one char: "a"
	' *  "*??A" "abcdA"   "----A"     "*" absorbs "ab", "??" skips "cd"
	' *  "**?A" "abAdA"   "--A--"     "**" absorbs "a", "?" skips "b" 
	' * -------------------------------------------------------------------------
	' *      
	' *             During any resync phase, we'll naturally be looking for the 
	' *             end of the candidate string and fail if we see it.
	' *   
	' *************************************************************************** */

	Public Function WC_CompareString(ByVal pPattern As String, ByVal pCandidate As String, Optional ByVal count As Integer = (-1), Optional ByVal do_case As Boolean = False, Optional ByVal do_wildcards As Boolean = True) As WC_RESULT

		Dim Patern() As Char
		Dim Candidate() As Char
		Dim PatternIndex As Integer
		Dim CandidateIndex As Integer

		Dim star_was_prev_char As Boolean
		Dim ques_was_prev_char As Boolean

		Dim RVal As WC_RESULT

		' Validate Parameters.

		If (pPattern Is Nothing) OrElse (pPattern.Length <= 0) Then
			Return WC_RESULT.WC_PAT_NULL_PTR
		End If

		If (pCandidate Is Nothing) OrElse (pCandidate.Length <= 0) Then
			Return WC_RESULT.WC_PAT_NULL_PTR
		End If

		' Initialise.

		Patern = pPattern.ToCharArray
		Candidate = pCandidate.ToCharArray
		PatternIndex = 0
		CandidateIndex = 0

		'/* match loop runs, comparing pattern and candidate strings char by char,
		'until (1) the end of the pattern string is found or (2) somebody found
		'some kind of mismatch condition. we deal with four cases in this loop: 
		'  -- pattern char is '?'; or 
		'  -- pattern char is '*'; or 
		'  -- candidate string is exhausted; or
		'  -- none of the above, pattern char is just a normal char.  */

		star_was_prev_char = False				'/* show previous character was not '*' */
		ques_was_prev_char = False				'/* and it wasn't '?', either */
		RVal = WC_RESULT.WC_MATCH					'/* assume success */

		While (RVal = WC_RESULT.WC_MATCH) AndAlso (PatternIndex < Patern.Length)


			If (do_wildcards) Then					'/* honoring wildcards? */

				'/* first: pattern-string character == '?' */

				If (Patern(PatternIndex) = WC_QUES) Then	 '/* better be another char in candidate */

					ques_was_prev_char = True		'/* but we don't care what it is , second: pattern-string character == '*' */

				ElseIf (Patern(PatternIndex) = WC_STAR1) OrElse (Patern(PatternIndex) = WC_STAR2) Then

					star_was_prev_char = True		'/* we'll need to resync later */
					PatternIndex += 1

					Continue While
				End If

			End If

			'/* third: any more characters in candidate string? */

			If CandidateIndex >= Candidate.Length Then ' (*candidate == '\0') 

				RVal = WC_RESULT.WC_CAN_TOO_SHORT

			ElseIf (ques_was_prev_char) Then '/* all set if we just saw ques */

				ques_was_prev_char = False				'/* reset and go check next pair */

				'/* fourth: plain old char compare; but resync first if necessary */

			Else

				If (star_was_prev_char) Then
					star_was_prev_char = False

					'/* resynchronize (see note in header) candidate with pattern */

					While (ch_eq(Patern(PatternIndex), Candidate(CandidateIndex), do_case) = WC_RESULT.WC_MISMATCH)

						CandidateIndex += 1

						If CandidateIndex >= Candidate.Length Then ' (*candidate++ == '\0') then
							RVal = WC_RESULT.WC_CAN_TOO_SHORT
							Exit While
						End If

					End While

					'/* end of re-sync, resume normal-type scan */
					'/* finally, after all that rigamarole upstairs: the main char compare */

				Else	'/* if star or ques was not last previous character */

					RVal = ch_eq(Patern(PatternIndex), Candidate(CandidateIndex), do_case)

				End If

			End If

			CandidateIndex += 1	 '/* point next chars in candidate and pattern strings */
			PatternIndex += 1		 '/* and go for next compare */

		End While	'/* while (retval == WC_MATCH && *pattern != '\0') */


		If (RVal = WC_RESULT.WC_MATCH) Then


			'/* we found end of pattern string, so we've a match to this point; now make
			'   sure "count" arg is satisfied. we'll deem it so and succeed the call if: 
			'   - we matched at least "count" chars; or
			'   - we matched fewer than "count" chars and:
			'     - pattern is same length as candidate; or
			'     - we're honoring wildcards and the final pattern char was star. 
			'   spell these tests right out /completely/ in the code */

			Dim MinMatchCount As Integer
			Dim NumberMatched As Integer

			If count < 0 Then					'/* if count negative, no abbrev: */
				MinMatchCount = Candidate.Length	'/*   must match two entire strings */
			Else
				MinMatchCount = count							'/* but count >= 0, can abbreviate */
			End If

			NumberMatched = CandidateIndex	' candidate - can_start;      '/* n chars we did in fact match */

			If (NumberMatched >= MinMatchCount) Then										'/* matched enough? */
				'/* yes; retval == WC_MATCH here */


			ElseIf (CandidateIndex >= Candidate.Length) Then ' == '\0')         '/* or cand same length as pattern? */
				'/* yes, all set */


			ElseIf (star_was_prev_char) Then		'/* or final char was star? */
				'/* yes, succeed that case, too */

			Else																 '/* otherwise, fail */
				RVal = WC_RESULT.WC_PAT_TOO_SHORT
			End If

		End If

		Return RVal

	End Function

	'/* ***************************************************************************
	' *
	' * Function:   int ch_eq(int c1, int c2, int do_case)                  
	' *
	' * Action:     Compare two characters, c1 and c2, and return WC_MATCH if     
	' *             chars are equal, WC_MISMATCH if chars are unequal. If the 
	' *             integer arg do_case == 0, treat upper-case same as lower-case.
	' *
	' * Returns:    WC_MATCH or WC_MISMATCH
	' *
	' *************************************************************************** */

	Private Function ch_eq(ByVal c1 As Char, ByVal c2 As Char, ByVal do_case As Boolean) As WC_RESULT

		If do_case Then

			If c1 = c2 Then
				Return WC_RESULT.WC_MATCH
			Else
				Return WC_RESULT.WC_MISMATCH
			End If

		Else

			If Char.ToLower(c1) = Char.ToLower(c2) Then
				Return WC_RESULT.WC_MATCH
			Else
				Return WC_RESULT.WC_MISMATCH
			End If

		End If

	End Function

#End Region

End Module
