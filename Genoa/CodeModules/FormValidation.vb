Imports System.Data.SqlClient
Imports RenaissanceGlobals

Module FormValidation

  Public Function CheckReferentialIntegrity(ByVal pMainForm As CTAMain, ByVal pDataRow As DataRow) As Boolean
    ' **********************************************************************
    ' Function to check Referential integrity violations for the deletion
    ' of the given DataRow.
    '
    ' Referential Relationships are defined in the 'tblReferentialIntegrity' table.
    ' 
    ' Returns TRUE if no conflict exists,
    ' Returns FALSE if an Integrity relationship would be violated.
    '
    ' **********************************************************************

    Dim RefIntegrityStdDataset As RenaissanceGlobals.StandardDataset
    Dim UpdatedTableName As String

    Dim ReferentialDataset As RenaissanceDataClass.DSReferentialIntegrity
    Dim ReferentialTable As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable

    Dim IntegrityChecks() As DataRow
    Dim ThisIntegrityCheck As DataRow

    RefIntegrityStdDataset = RenaissanceGlobals.RenaissanceStandardDatasets.GetStandardDataset(RenaissanceChangeID.tblReferentialIntegrity)

    UpdatedTableName = pDataRow.Table.TableName

    ' **********************************************************************
    ' Get Referential relationships, Read from 'tblReferentialIntegrity'
    ' **********************************************************************

    ' Establish Adaptor, Dataset objects and load the table using standard process.

    ReferentialDataset = pMainForm.Load_Table(RefIntegrityStdDataset, False)
    ReferentialTable = ReferentialDataset.tblReferentialIntegrity

    ' Select those rows relating to the table being updated.

    IntegrityChecks = ReferentialTable.Select("(TableName = '" & UpdatedTableName & "' OR TableName = 'qry" & UpdatedTableName & "') AND (IsDescriptiveReferenceOnly=0)")
    If IntegrityChecks.GetLength(0) > 0 Then

      ' If there are relevant checks, then ...

      For Each ThisIntegrityCheck In IntegrityChecks
        Dim TestTableName As String
        Dim TestDataSet As DataSet = Nothing
        Dim TestStdDataset As StandardDataset
        Dim TestTableChangeID As RenaissanceChangeID
        Dim MatchingRows() As DataRow
        Dim SearchString As String

        ' Load the dependent table ...

        TestTableName = ThisIntegrityCheck.Item("FeedsTable").ToString
        If (TestTableName.StartsWith("qry")) Then
          TestTableName = TestTableName.Substring(3)
        End If

        If System.Enum.IsDefined(GetType(RenaissanceChangeID), TestTableName) Then

          TestTableChangeID = CType(System.Enum.Parse(GetType(RenaissanceChangeID), TestTableName), RenaissanceChangeID)
          TestStdDataset = RenaissanceGlobals.RenaissanceStandardDatasets.GetStandardDataset(TestTableChangeID)

          If (TestStdDataset IsNot Nothing) Then
            TestDataSet = pMainForm.Load_Table(TestStdDataset, False)
          End If

          ' Select those rows where the relationship is active...

          If (TestDataSet IsNot Nothing) Then
            SearchString = ThisIntegrityCheck.Item("FeedsField").ToString & "="
            If pDataRow.Item(ThisIntegrityCheck.Item("TableField")).GetType Is GetType(Date) Then
              SearchString &= "'" & pDataRow.Item(ThisIntegrityCheck.Item("TableField")).ToString & "'"
            ElseIf pDataRow.Item(ThisIntegrityCheck.Item("TableField")).GetType Is GetType(String) Then
              SearchString &= "'" & pDataRow.Item(ThisIntegrityCheck.Item("TableField")).ToString & "'"
            Else
              SearchString &= pDataRow.Item(ThisIntegrityCheck.Item("TableField")).ToString
            End If

            MatchingRows = TestDataSet.Tables(0).Select(SearchString)

            ' If any such rows exist, then return FALSE.

            If MatchingRows.Length > 0 Then
              Return False
            End If
          End If

        End If

      Next
    End If

    Return True

  End Function


End Module
