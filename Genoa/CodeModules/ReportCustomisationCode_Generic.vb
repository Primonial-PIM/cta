Imports System.IO
Imports System.Data.SqlClient
Imports System.Reflection
Imports RenaissanceDataClass
Imports RenaissanceGlobals

Imports C1.C1Report

Module ReportCustomisationCode_Generic

  Public Function SetCustomReportDetails(ByRef pMainForm As CTAMain, ByVal pFundID As Integer, ByVal pReportFile As String, ByVal pReportName As String) As C1.C1Report.C1Report
    Return SetCustomReportDetails(pMainForm, pMainForm.MainAdaptorHandler, pMainForm.MainDataHandler, "CTA", pFundID, GetReportDefinition(pMainForm, pReportFile, pReportName))
  End Function

  Public Function SetCustomReportDetails(ByRef pMainForm As CTAMain, ByRef thisAdaptorHandler As AdaptorHandler, ByRef thisDataHandler As DataHandler, ByVal pApplicationName As String, ByVal pFundID As Integer, ByRef pReport As C1.C1Report.C1Report) As C1.C1Report.C1Report

    Dim thisAdaptor As New SqlDataAdapter

    Dim RC_DS As New RenaissanceDataClass.DSReportCustomisation

    Try
      If (pReport Is Nothing) Then
        Return pReport
      End If

      Call thisAdaptorHandler.Set_AdaptorCommands(thisDataHandler.Get_Connection(CTA_CONNECTION), thisAdaptor, "tblReportCustomisation")
      thisAdaptor.SelectCommand.Parameters("@ApplicationName").Value = pApplicationName
      thisAdaptor.SelectCommand.Parameters("@ReportName").Value = pReport.ReportName
      thisAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      thisAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = CDate("1 Jan 1900")

      thisAdaptor.Fill(RC_DS.tblReportCustomisation)

      Return SetCustomReportDetails(RC_DS.tblReportCustomisation, pReport)
    Catch ex As Exception
      pMainForm.LogError("", LOG_LEVELS.Error, ex.Message, "Error getting Report customisation details : " & pApplicationName.ToString & ", " & pReport.ReportName & ", " & pFundID.ToString, ex.StackTrace, True)
    End Try

    Return Nothing

  End Function

  Public Function SetCustomReportDetails(ByRef pRC_Table As RenaissanceDataClass.DSReportCustomisation.tblReportCustomisationDataTable, ByRef pReport As C1.C1Report.C1Report) As C1.C1Report.C1Report

    Dim SelectedRC_Rows As RenaissanceDataClass.DSReportCustomisation.tblReportCustomisationRow()
    Dim thisRC_Row As RenaissanceDataClass.DSReportCustomisation.tblReportCustomisationRow
    Dim RC_RowCount As Integer

    Try
      If (pRC_Table Is Nothing) Then
        Return pReport
      End If

      If (pReport Is Nothing) Then
        Return pReport
      End If
    Catch ex As Exception

    End Try

    Try
      SelectedRC_Rows = pRC_Table.Select("ReportName='" & pReport.ReportName & "'")

      If (SelectedRC_Rows Is Nothing) OrElse (SelectedRC_Rows.Length <= 0) Then
        Return pReport
      End If
    Catch ex As Exception
      Return Nothing
    End Try

    Dim TypeInfo As Type
    Dim thisProperty As PropertyInfo


    Try
      For RC_RowCount = 0 To (SelectedRC_Rows.Length - 1)
        thisRC_Row = SelectedRC_Rows(RC_RowCount)

        Try
          TypeInfo = pReport.Fields(thisRC_Row.FieldName).GetType
          thisProperty = TypeInfo.GetProperty(thisRC_Row.PropertyName)

          If thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.String) Then
            thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), thisRC_Row.PropertyValue, Nothing)
          ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.DateTime) Then
            thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), CDate(thisRC_Row.PropertyValue), Nothing)
          ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Boolean) Then
            thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), CBool(thisRC_Row.PropertyValue), Nothing)
          ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Single) Then
            thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), CSng(thisRC_Row.PropertyValue), Nothing)
          ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Double) Then
            thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), CDbl(thisRC_Row.PropertyValue), Nothing)
          ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int16) Then
            thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), CShort(thisRC_Row.PropertyValue), Nothing)
          ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int32) Then
            thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), CInt(thisRC_Row.PropertyValue), Nothing)
          ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Int64) Then
            thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), CLng(thisRC_Row.PropertyValue), Nothing)
          ElseIf thisProperty.PropertyType.UnderlyingSystemType Is GetType(System.Drawing.Color) Then
            If IsNumeric(thisRC_Row.PropertyValue) Then
              Dim IntValue As Integer
              IntValue = CInt(thisRC_Row.PropertyValue)
              thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), System.Drawing.Color.FromArgb((IntValue And 16777215) / 65536, (IntValue And 65535) / 256, (IntValue And 255)), Nothing)
            Else
              thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), System.Drawing.Color.FromName(CStr(thisRC_Row.PropertyValue)), Nothing)
            End If
          ElseIf thisProperty.PropertyType.UnderlyingSystemType.IsEnum Then
            Dim EnumObject As Object

            If IsNumeric(thisRC_Row.PropertyValue) Then
              EnumObject = System.Enum.ToObject(thisProperty.PropertyType.UnderlyingSystemType, thisRC_Row.PropertyValue)
            Else
              EnumObject = System.Enum.Parse(thisProperty.PropertyType.UnderlyingSystemType, thisRC_Row.PropertyValue, True)
            End If

            thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), EnumObject, Nothing)
          Else
            thisProperty.SetValue(pReport.Fields(thisRC_Row.FieldName), CDbl(thisRC_Row.PropertyValue), Nothing)
          End If
        Catch ex As Exception
        End Try
      Next
    Catch ex As Exception
      Return Nothing
    End Try

    Return pReport
  End Function

  Private Function CheckReportPermission(ByRef Mainform As CTAMain, ByRef thisReport As C1Report) As Boolean
    ' ***********************************************************************************
    '
    ' ***********************************************************************************

    Return CheckReportPermission(Mainform, thisReport.ReportName)
  End Function


  Private Function CheckReportPermission(ByRef Mainform As CTAMain, ByVal ReportName As String) As Boolean
    ' ***********************************************************************************
    '
    ' ***********************************************************************************

    If (Mainform.CheckPermissions(ReportName, RenaissanceGlobals.PermissionFeatureType.TypeReport) And RenaissanceGlobals.PermissionBitmap.PermRead) > 0 Then
      Return True
      Exit Function
    End If

    Return False
  End Function


  Private Function GetReportDefinition(ByRef Mainform As CTAMain, ByVal pReportFile As String, ByVal pReportName As String) As C1Report
    ' ******************************************************************************
    ' Return an instance of the given report name from the given report file.
    '
    ' This routine is designed to search upwards from the current executable directory
    ' for the given report file and the given report within it.
    ' In addition to the executable path, it will check for the 'ReportDefinitions'
    ' subdirectory on it's way up the directory tree.
    ' 
    ' ******************************************************************************

    Dim ReturnedReport As New C1Report

    Try
      If CheckReportPermission(Mainform, pReportName) = False Then
        Mainform.LogError("Display Report()", RenaissanceGlobals.LOG_LEVELS.Warning, "", "You do not have permission to view the " & pReportName & " report.", "", True)
        Return Nothing
        Exit Function
      End If
    Catch ex As Exception
      Return Nothing
      Exit Function
    End Try

    ' Validate parameters

    If (pReportFile.Length <= 0) Or (pReportName.Length <= 0) Then
      Return Nothing
    End If

    If (InStr(pReportFile, ".") = 0) Then
      pReportFile &= ".xml"
    End If

    ' Search for required file.

    Dim AppPath As String
    Dim RptDefinitionFile As String

    ' Start with the directory in which the executable resides.

    AppPath = Path.GetDirectoryName(Application.ExecutablePath)
    AppPath = Path.Combine(AppPath, "ReportDefinitions")

    RptDefinitionFile = Path.Combine(AppPath, pReportFile)

    ' Work up the directory tree...

    While (File.Exists(RptDefinitionFile) = False) And (AppPath.Length > 0)
      AppPath = Path.GetDirectoryName(AppPath)

      If AppPath Is Nothing Then AppPath = ""

      RptDefinitionFile = Path.Combine(AppPath, pReportFile)
      If (File.Exists(RptDefinitionFile) = False) Then
        RptDefinitionFile = Path.Combine(Path.Combine(AppPath, "ReportDefinitions"), pReportFile)
      End If
    End While

    If File.Exists(RptDefinitionFile) = False Then
      Return Nothing
      Exit Function
    End If

    ' Load Report

    Try
      ReturnedReport.Load(RptDefinitionFile, pReportName)
    Catch ex As Exception
      Return Nothing
    End Try

    Return ReturnedReport

  End Function


  Public Function Get_ReportCustomisationAttribute(ByRef thisAdaptorHandler As AdaptorHandler, ByRef thisDataHandler As DataHandler, ByVal pApplicationName As String, ByVal pFundID As Integer, ByVal pReportName As String, ByVal pAttributeName As String) As String

    Dim thisAdaptor As New SqlDataAdapter

    Dim RC_DS As New RenaissanceDataClass.DSReportCustomisation
    Dim RC_SelectedRows As RenaissanceDataClass.DSReportCustomisation.tblReportCustomisationRow()

    Try
      Call thisAdaptorHandler.Set_AdaptorCommands(thisDataHandler.Get_Connection(CTA_CONNECTION), thisAdaptor, "tblReportCustomisation")
      thisAdaptor.SelectCommand.Parameters("@ApplicationName").Value = pApplicationName
      thisAdaptor.SelectCommand.Parameters("@ReportName").Value = pReportName
      thisAdaptor.SelectCommand.Parameters("@FundID").Value = pFundID
      thisAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = CDate("1 Jan 1900")

      thisAdaptor.Fill(RC_DS.tblReportCustomisation)

    Catch ex As Exception
      Return ""
    End Try

    Try
      RC_SelectedRows = RC_DS.tblReportCustomisation.Select("FieldName='" & pAttributeName & "'")
      If (RC_SelectedRows Is Nothing) OrElse (RC_SelectedRows.Length <= 0) Then
        Return ""
      End If
      Return RC_SelectedRows(0).PropertyValue
    Catch ex As Exception
    End Try

    Return ""

  End Function

End Module
