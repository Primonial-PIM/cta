Option Strict On
Imports Microsoft.Win32
Imports System.IO
Imports System.Data.SqlClient
Imports System.Management
Imports System.Net
Imports System.Net.Sockets
Imports System.Windows.Forms.Clipboard
Imports CTA.CtaTCPClient
Imports TCPServer_Common
Imports System.Threading

Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissancePertracDataClass
Imports RenaissanceStatFunctions
Imports RenaissanceUtilities.DatePeriodFunctions

Public Class CTAMain
  Implements RenaissanceGlobals.StandardRenaissanceMainForm


#Region " Local Classes"

  <StructLayout(LayoutKind.Explicit, Size:=8)> Private Structure MACConverterClass
    <FieldOffset(0)> Private _ULongVal As ULong
    <FieldOffset(0)> Private _Byte1 As Byte
    <FieldOffset(1)> Private _Byte2 As Byte
    <FieldOffset(2)> Private _Byte3 As Byte
    <FieldOffset(3)> Private _Byte4 As Byte
    <FieldOffset(4)> Private _Byte5 As Byte
    <FieldOffset(5)> Private _Byte6 As Byte
    <FieldOffset(6)> Private _Byte7 As Byte
    <FieldOffset(7)> Private _Byte8 As Byte

    Public Sub New(ByVal pMACString As String)
      Me.MACString = pMACString
    End Sub

    Public Sub New(ByVal pMACLong As ULong)
      Me.MACLong = pMACLong
    End Sub

    Public Property MACLong() As ULong
      Get
        Return _ULongVal
      End Get
      Set(ByVal value As ULong)
        _ULongVal = value
      End Set
    End Property

    Public Property MACString() As String
      Get
        Dim RVal As String = ""

        Try

          RVal = Hex(_Byte1).PadLeft(2, CChar("0")) & ":" & Hex(_Byte2).PadLeft(2, CChar("0")) & ":" & Hex(_Byte4).PadLeft(2, CChar("0")) & ":" & Hex(_Byte5).PadLeft(2, CChar("0")) & ":" & Hex(_Byte6).PadLeft(2, CChar("0")) & ":" & Hex(_Byte7).PadLeft(2, CChar("0"))

        Catch ex As Exception
        End Try

        Return RVal
      End Get
      Set(ByVal value As String)
        Try

          Dim StringValues() As String = value.Split(New Char() {CChar(":"), CChar("-"), CChar(" ")})

          If (StringValues IsNot Nothing) AndAlso (StringValues.Length = 6) Then
            _Byte1 = Convert.ToByte(StringValues(0), 16)
            _Byte2 = Convert.ToByte(StringValues(1), 16)
            _Byte4 = Convert.ToByte(StringValues(2), 16)
            _Byte5 = Convert.ToByte(StringValues(3), 16)
            _Byte6 = Convert.ToByte(StringValues(4), 16)
            _Byte7 = Convert.ToByte(StringValues(5), 16)
            _Byte3 = CByte(Math.Min(Rnd() * 256, 255))
            _Byte8 = CByte(Math.Min(Rnd() * 256, 255))
          End If

        Catch ex As Exception
        End Try
      End Set
    End Property
  End Structure

  <StructLayout(LayoutKind.Explicit, Size:=8)> Private Structure DateConverterClass
    <FieldOffset(0)> Private _ULongVal As ULong
    <FieldOffset(0)> Public _Date1 As UInt16
    <FieldOffset(2)> Public _Date2 As UInt16
    <FieldOffset(4)> Public _Date3 As UInt16
    <FieldOffset(6)> Public _Date4 As UInt16

    Public Sub New(ByVal pDate As Date)
      Me._Date1 = CUShort(Math.Min(pDate.ToOADate, UInt16.MaxValue))
      Me._Date4 = CUShort(Math.Min(pDate.ToOADate, UInt16.MaxValue))
      Me._Date2 = CUShort(Math.Min(Rnd() * UInt16.MaxValue, UInt16.MaxValue))
      Me._Date3 = CUShort(Math.Min(Rnd() * UInt16.MaxValue, UInt16.MaxValue))
    End Sub

    Public Sub New(ByVal pLong As ULong)
      Me._ULongVal = pLong
    End Sub

    Public ReadOnly Property Date1() As Date
      Get
        If (_Date1 = _Date4) Then
          Return Date.FromOADate(CDbl(_Date1))
        Else
          Return CDate("1 Jan 1900")
        End If
      End Get
    End Property

    Public ReadOnly Property ULongVal() As ULong
      Get
        Return _ULongVal
      End Get
    End Property

  End Structure



#End Region

#Region " Class Globals and Declarations"

  ' Declaration of the AutoUpdate Event.
  ' Used to notify forms of changes.
  Public Event CTAAutoUpdate(ByVal sender As Object, ByVal e As RenaissanceUpdateEventArgs) Implements RenaissanceGlobals.StandardRenaissanceMainForm.RenaissanceAutoUpdate

  ' Collection of CTA Form Handles. One item for the Main-form and one item for each sub-Form.
  Private _CTAForms As New RenaissanceFormCollection

  ' Central Data Structures.
  Private WithEvents CTATCPClient As CtaTCPClient.CtaTCPClient
  Private UpdateEventControl As UpdateEventControlClass

  ' Generic Form-Update Timer
  Private WithEvents FormUpdateDataTimer As System.Windows.Forms.Timer
  Private FormUpdateObjects As New Dictionary(Of System.Windows.Forms.Form, RenaissanceTimerUpdateClass)

  ' Custom Field Data Cache
  ' Friend CustomField_DataCache As New Dictionary(Of ULong, Object)
  Private _CustomFieldDataCache As CustomFieldDataCacheClass ' New

  ' Application Knowledgedate
  Private _Main_Knowledgedate As Date

  ' Central Data and Report handlers
  Private _MainDataHandler As New RenaissanceDataClass.DataHandler
  Private _MainAdaptorHandler As New RenaissanceDataClass.AdaptorHandler
  Private _MainReportHandler As New ReportHandler(Me)
  Private _PertracData As New RenaissancePertracDataClass.PertracDataClass(Me)
  Private _StatFunctions As New StatFunctions(Me, _PertracData)
  Private _ThisInformationColumnCache As Dictionary(Of String, Integer())

  ' Central Pertrac Stuff for Grid editing
  Friend PertracLookupCollection As LookupCollection(Of Integer, String)

  ' Standard Message Server
  Private _MessageServerName As String = ""
  Private MessageServerAddress As IPAddress = IPAddress.None

  ' SQL Server
  Private SQLServerName As String = "<Unknown>"
  Private DatabaseName As String = ""
  Private _SQLTrustedConnection As Boolean = True
  Private _SQLUserName As String = ""
  Private _SQLPassword As String = ""
  Private _SQLConnectionString As String
  Private _SQLAsynchConnectString As String

  ' CTA Application Permission Tests
  Private _SecurityTest1 As Boolean = False
  Private _SecurityTest2 As Boolean = False
  Private _SecurityTest3 As Boolean = False
  Private _SecurityTest4 As Boolean = False
  Private Const Default_Licensed_MAC As ULong = 0
  Private Const Default_Licensed_Date As ULong = 0
  Private ApplicationLockout As Boolean = False

  Private _EntryForm_CacheCount As Integer = STANDARD_EntryForm_CACHE_COUNT
  Private _ReportForm_CacheCount As Integer = 0 ' STANDARD_ReportForm_CACHE_COUNT
  Private _SingleCache_CacheCount As Integer = 0 ' STANDARD_SingleForm_CACHE_COUNT
  Private _NoCache_CacheCount As Integer = 0
  Private _UniqueCounter As Integer = 0
  Private _UniqueCounterLock As New Object

  Private _SelectedFontSize As Single

  Public Const STANDARD_ReportForm_CACHE_COUNT As Integer = 1

  Dim Splash As New CTASplash
  Dim SplashTime As Date

  Friend _MasternameDictionary As New LookupCollection(Of Integer, String)   ' Dictionary(Of Integer, String)
  Friend _LocationsDictionary As LookupCollection(Of Integer, String) = Nothing  ' Dictionary(Of Integer, String)

#End Region

#Region " Form Properties"

  Public ReadOnly Property MainForm() As CTAMain
    ' *******************************************************************************
    '
    ' *******************************************************************************

    Get
      Return Me
    End Get
  End Property

  Public Property Main_Knowledgedate() As Date Implements StandardRenaissanceMainForm.Main_Knowledgedate
    ' *******************************************************************************
    ' Public property to Set the system Knowledgedate
    '
    ' The update event is automatically triggered.
    '
    ' *******************************************************************************
    Get
      Return _Main_Knowledgedate
    End Get
    Set(ByVal Value As Date)
      If Value <> _Main_Knowledgedate Then
        _Main_Knowledgedate = Value

        If Value.CompareTo(KNOWLEDGEDATE_NOW) <= 0 Then
          Me.CTAStatusLabel.Text = "KnowledgeDate is Live."
        Else
          If Value.TimeOfDay.TotalSeconds = 0 Then
            Me.CTAStatusLabel.Text = "KnowledgeDate is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_DATEFORMAT)
            Me.Menu_KD_Display.Text = "KD is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_DATEFORMAT)
          Else
            Me.CTAStatusLabel.Text = "KnowledgeDate is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
            Me.Menu_KD_Display.Text = "KD is " & _Main_Knowledgedate.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
          End If
        End If

        Call Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.KnowledgeDate))

      End If
    End Set
  End Property

  Public ReadOnly Property MainDataHandler() As RenaissanceDataClass.DataHandler Implements RenaissanceGlobals.StandardRenaissanceMainForm.MainDataHandler
    Get
      Return _MainDataHandler
    End Get
  End Property

  Public ReadOnly Property MainAdaptorHandler() As RenaissanceDataClass.AdaptorHandler Implements RenaissanceGlobals.StandardRenaissanceMainForm.MainAdaptorHandler
    Get
      Return _MainAdaptorHandler
    End Get
  End Property

  Public ReadOnly Property MainReportHandler() As ReportHandler
    Get
      Return _MainReportHandler
    End Get
  End Property

  Public ReadOnly Property PertracData() As RenaissancePertracDataClass.PertracDataClass
    Get
      Return _PertracData
    End Get
  End Property

  Public ReadOnly Property InformationColumnCache() As Dictionary(Of String, Integer())
    Get
      Return _ThisInformationColumnCache
    End Get
  End Property

  Friend ReadOnly Property CustomFieldDataCache() As CustomFieldDataCacheClass
    Get
      Return _CustomFieldDataCache
    End Get
  End Property

  Public ReadOnly Property StatFunctions() As StatFunctions
    Get
      Return _StatFunctions
    End Get
  End Property

  Public ReadOnly Property CTAForms() As RenaissanceFormCollection
    Get
      Return _CTAForms
    End Get
  End Property

  Public ReadOnly Property EntryForm_CacheCount() As Integer
    Get
      Return _EntryForm_CacheCount
    End Get
  End Property

  Public ReadOnly Property NoCache_CacheCount() As Integer
    Get
      Return _NoCache_CacheCount
    End Get
  End Property

  Public ReadOnly Property ReportForm_CacheCount() As Integer
    Get
      Return _ReportForm_CacheCount
    End Get
  End Property

  Public ReadOnly Property SingleCache_CacheCount() As Integer
    Get
      Return _SingleCache_CacheCount
    End Get
  End Property

  Public ReadOnly Property MessageServerName() As String
    Get
      Return _MessageServerName
    End Get
  End Property

  Private ReadOnly Property SQLTrustedConnection() As Boolean
    Get
      Return _SQLTrustedConnection
    End Get
  End Property

  Private ReadOnly Property SQLUserName() As String
    Get
      Return _SQLUserName
    End Get
  End Property

  Private ReadOnly Property SQLPassword() As String
    Get
      Return _SQLPassword
    End Get
  End Property

  Public ReadOnly Property SQLConnectString() As String
    Get
      Return Me._SQLConnectionString
    End Get
  End Property

  Public ReadOnly Property SQLAsynchConnectString() As String
    Get
      Return Me._SQLAsynchConnectString
    End Get
  End Property

  Public ReadOnly Property CTAFontSize() As Single
    Get
      Return _SelectedFontSize
    End Get
  End Property

  Public ReadOnly Property SecurityTestFlag1() As Boolean
    Get
      Return _SecurityTest1
    End Get
  End Property

  Public ReadOnly Property SecurityTestFlag2() As Boolean
    Get
      Return _SecurityTest2
    End Get
  End Property

  Public ReadOnly Property SecurityTestFlag3() As Boolean
    Get
      Return _SecurityTest3
    End Get
  End Property

  Public ReadOnly Property SecurityTestFlag4() As Boolean
    Get
      Return _SecurityTest4
    End Get
  End Property

  ReadOnly Property MasternameDictionary() As LookupCollection(Of Integer, String) Implements StandardRenaissanceMainForm.MasternameDictionary
    Get
      Return _MasternameDictionary
    End Get
  End Property

  ReadOnly Property LocationsDictionary() As LookupCollection(Of Integer, String) Implements StandardRenaissanceMainForm.LocationsDictionary
    Get
      Return _LocationsDictionary
    End Get
  End Property

  Public ReadOnly Property EnableReportFadeIn() As Boolean
    Get
      Return Menu_File_EnableReportFadeIn.Checked
    End Get
  End Property

  Public Sub DisableReportFadeIn()
    Try
      Menu_File_EnableReportFadeIn.Checked = False
    Catch ex As Exception
    End Try
  End Sub

  Public ReadOnly Property UniqueCTANumber() As Integer
    Get
      SyncLock _UniqueCounterLock

        _UniqueCounter += 1
        Return _UniqueCounter

      End SyncLock
    End Get
  End Property

#End Region

  Public Delegate Function Load_Table_Delegate(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet

#Region " Main, New, Load() and Closing() routines"

  Shared Sub Main(ByVal CmdArgs() As String)
    ' *******************************************************************************
    ' Start CTA
    ' 
    ' *******************************************************************************
    Dim AppForm As CTAMain

    Application.EnableVisualStyles()

    AppForm = New CTAMain(CmdArgs)

    Try
      If (AppForm.IsDisposed) Then
        AppForm = New CTAMain(CmdArgs)
      End If

      Application.Run(AppForm)
      AppForm.Close()
      AppForm = Nothing

    Catch ex As Exception
      'AppForm.LogError("Main()", LOG_LEVELS.Error, ex.Message, "Error in Main()", ex.StackTrace, True)
      If (AppForm IsNot Nothing) Then
        AppForm.CTAMain_Closing(AppForm, New System.ComponentModel.CancelEventArgs)
      End If
    End Try

  End Sub

  Private Sub New()

    ' This call is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call.

  End Sub

  Public Sub New(ByVal CmdArgs() As String)
    Me.New()

    If (Not (Splash Is Nothing)) Then
      SplashTime = Now
      Splash.Show()
    End If

    ' Get Computername

    Dim ComputerName As String
    Try
      ComputerName = Environment.GetEnvironmentVariable("COMPUTERNAME")
    Catch ex As Exception
      ComputerName = ""
    End Try

    ' Startup Parameters
    '
    ' MessageServer=<ServerName>

    Menu_Font_Small_Click(Me.Menu_Font_Medium, New EventArgs)

    _MessageServerName = ""
    _SQLTrustedConnection = True
    _SQLUserName = ""
    _SQLPassword = ""
    MessageServerAddress = IPAddress.None

    If Not (CmdArgs Is Nothing) Then
      If (CmdArgs.Length > 0) Then
        Dim ParameterString As String
        Dim ParameterName As String

        For Each ParameterString In CmdArgs
          If InStr(ParameterString, "=") > 0 Then
            ParameterName = ParameterString.Substring(0, InStr(ParameterString, "=") - 1)
            ParameterString = ParameterString.Substring(InStr(ParameterString, "="), (ParameterString.Length - InStr(ParameterString, "=")))

            Select Case ParameterName.ToUpper

              Case "MESSAGESERVER"
                _MessageServerName = ParameterString.Trim
                Try
                  MessageServerAddress = NetworkFunctions.GetServerAddress(_MessageServerName)
                Catch ex As Exception
                  _MessageServerName = ""
                  MessageServerAddress = IPAddress.None
                End Try

              Case "SQLSERVER"
                If ParameterString.Trim.Length > 0 Then
                  SQLServerName = ParameterString.Trim
                End If

              Case "DATABASE"
                If ParameterString.Trim.Length > 0 Then
                  DatabaseName = ParameterString.Trim
                End If

              Case "SQLUSERNAME"
                If ParameterString.Trim.Length > 0 Then
                  _SQLUserName = ParameterString.Trim
                  _SQLTrustedConnection = False
                End If

              Case "SQLPASSWORD"
                If ParameterString.Trim.Length > 0 Then
                  _SQLPassword = ParameterString.Trim
                End If

              Case "MDIFORM"
                If ParameterString.Trim.Length > 0 Then
                  If ParameterString.Trim.ToUpper = "TRUE" Then
                    Me.Menu_File_ToggleMDI.Checked = True
                    Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
                    Me.MaximizeBox = True
                    Me.IsMdiContainer = True
                  ElseIf IsNumeric(ParameterString.Trim) Then
                    If CInt(ParameterString.Trim) <> 0 Then
                      Me.Menu_File_ToggleMDI.Checked = False
                      Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
                      Me.MaximizeBox = True
                      Me.IsMdiContainer = True
                    End If
                  End If
                End If

              Case "FORMCACHE"
                If ParameterString.Trim.Length > 0 Then
                  Dim Flag As Boolean

                  Flag = False
                  Try
                    Flag = CBool(ParameterString.Trim)
                  Catch ex As Exception
                    Flag = True
                  End Try

                  If (Flag = False) Then
                    _EntryForm_CacheCount = 0
                    _ReportForm_CacheCount = 0
                    _SingleCache_CacheCount = 0
                    _NoCache_CacheCount = 0
                  Else
                    _EntryForm_CacheCount = STANDARD_EntryForm_CACHE_COUNT
                    _ReportForm_CacheCount = STANDARD_ReportForm_CACHE_COUNT
                    _SingleCache_CacheCount = STANDARD_SingleForm_CACHE_COUNT
                    _NoCache_CacheCount = 0
                  End If
                End If

            End Select
          End If
        Next
      End If
    End If

    ' Check Registry if IPAddress = None

    If (IPAddress.None.Equals(MessageServerAddress)) Then
      ' Try to resolve an address from the Registry

      _MessageServerName = CType(Get_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\MessageServer", "ServerName", ""), String)

      If (_MessageServerName = "") Then
        _MessageServerName = CType(Get_RegistryItem(Registry.LocalMachine, REGISTRY_BASE & "\MessageServer", "ServerName", System.Environment.MachineName), String)
      End If

      Try
        If (_MessageServerName.Length <= 0) Then
          _MessageServerName = System.Environment.MachineName
        End If
        MessageServerAddress = NetworkFunctions.GetServerAddress(_MessageServerName)
      Catch ex As Exception
        MessageServerAddress = IPAddress.None
        _MessageServerName = ""
      End Try
    End If

    ' Initiate TCP Client
    Try
      UpdateEventControl = New UpdateEventControlClass(Me)
      UpdateEventControl.Visible = False
      Me.Controls.Add(Me.UpdateEventControl)

      CTATCPClient = New CtaTCPClient.CtaTCPClient(_MessageServerName, MessageServerAddress)

      ' Add TCP Client Message Handler
      AddHandler CTATCPClient.MessageReceivedEvent, AddressOf Me.TCPMessageReceived

      CTATCPClient.Start()
    Catch ex As Exception
    End Try


    ' Process CTA Update requests
    AddHandler Me.CTAAutoUpdate, AddressOf Me.AutoUpdate

    ' Set up Menu Callbacks

    ' Add / Edit
    AddHandler Me.Menu_FundBrowser.Click, AddressOf Generic_FormMenu_Click
    AddHandler Me.Menu_ManageFolders.Click, AddressOf Generic_FormMenu_Click

    ' Reports

    ' KnowledgeDate
    AddHandler Me.Menu_KD_SetKD.Click, AddressOf Generic_FormMenu_Click

    ' System

    AddHandler Me.Menu_System_UserPermissions.Click, AddressOf Generic_FormMenu_Click
    AddHandler Me.Menu_System_ReviewCC.Click, AddressOf Generic_FormMenu_Click
    AddHandler Me.Menu_System_SelectCC.Click, AddressOf Generic_FormMenu_Click

    ' About

    AddHandler Me.Menu_About.Click, AddressOf Generic_FormMenu_Click

  End Sub

  Private Sub CTAMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' *******************************************************************************
    ' Initialise CTA
    ' 
    ' *******************************************************************************

    Dim myConnection As SqlConnection

    Dim ComputerName As String
    Dim ConnectString As String
    Dim ConnectionCount As Integer
    Dim CurrentConnection As Integer
    Dim ItemName As String

    ' Add the Main form to the CTA Forms Collection.
    CTAForms.Add(New RenaissanceGlobals.RenaissanceFormHandle(Me, CTAFormID.frmCTAMain, True))


    ' **********************************************************************
    ' Initialise Database Connection.
    ' 
    ' 1) Construct a reasonable default, then query the registry for a saved value.
    ' 2) Create a managed connection object 'cnnCTA'
    '
    ' **********************************************************************

    Me.Visible = True
    Application.DoEvents()
    Me.CTAStatusLabel.Text = "Initailising."
    Me.CTAStatusStrip.Refresh()

    ' Get initial connection string(s)
    ' Default value will be determined by what machine this is running on.
    ' Is it at home, my development machine or AN Other ?

    ' Establish default connection string.
    Me.CTAStatusLabel.Text = "Initailising, SQL Connection."
    Me.CTAStatusStrip.Refresh()

    If SQLServerName.StartsWith("<") Then
      ComputerName = Environment.GetEnvironmentVariable("COMPUTERNAME")

      If ComputerName Is Nothing Then
        ComputerName = "<Unknown>"
      End If

      If ComputerName.StartsWith("EXXP") Then
        If ComputerName.StartsWith("EXXP9731") Then
          If DatabaseName.Length <= 0 Then
            ConnectString = "SERVER=EXXP9731;DATABASE=InvestMaster_Test1;Trusted_Connection=Yes;Application Name=" & Application.ProductName
          Else
            ConnectString = "SERVER=EXXP9731;DATABASE=" & DatabaseName & ";Trusted_Connection=Yes;Application Name=" & Application.ProductName
          End If

          SQLServerName = "EXXP9731"
        Else
          If DatabaseName.Length <= 0 Then
            ConnectString = "SERVER=EXMSDB53;DATABASE=Renaissance;Trusted_Connection=Yes;Application Name=" & Application.ProductName
          Else
            ConnectString = "SERVER=EXMSDB53;DATABASE=" & DatabaseName & ";Trusted_Connection=Yes;Application Name=" & Application.ProductName
          End If
          SQLServerName = "EXMSDB53"
        End If
      Else
        If DatabaseName.Length <= 0 Then
          ConnectString = "SERVER=" & ComputerName & ";DATABASE=InvestMaster_Test1;Trusted_Connection=Yes;Application Name=" & Application.ProductName
        Else
          ConnectString = "SERVER=" & ComputerName & ";DATABASE=" & DatabaseName & ";Trusted_Connection=Yes;Application Name=" & Application.ProductName
        End If
      End If
    Else
      If (SQLTrustedConnection) Then
        If DatabaseName.Length <= 0 Then
          ConnectString = "SERVER=" & SQLServerName & ";DATABASE=Renaissance;Trusted_Connection=Yes;Application Name=" & Application.ProductName
        Else
          ConnectString = "SERVER=" & SQLServerName & ";DATABASE=" & DatabaseName & ";Trusted_Connection=Yes;Application Name=" & Application.ProductName
        End If

      Else
        If DatabaseName.Length <= 0 Then
          ConnectString = "SERVER=" & SQLServerName & ";DATABASE=Renaissance;Trusted_Connection=No;User ID=" & SQLUserName & ";Password=" & SQLPassword & ";Application Name=" & Application.ProductName
        Else
          ConnectString = "SERVER=" & SQLServerName & ";DATABASE=" & DatabaseName & ";Trusted_Connection=No;User ID=" & SQLUserName & ";Password=" & SQLPassword & ";Application Name=" & Application.ProductName
        End If
      End If
    End If

    ' Retrieve existing connection strings from the registry.

    ConnectionCount = CType(Get_RegistryItem(REGISTRY_BASE & "\Connections", "ConnectionCount", (-1)), Integer)
    If ConnectionCount > 0 Then
      CurrentConnection = CType(Get_RegistryItem(REGISTRY_BASE & "\Connections", "CurrentConnection", (-1)), Integer)

      If (CurrentConnection < ConnectionCount) And (CurrentConnection >= 0) Then
        ItemName = "C" & CurrentConnection.ToString

        ConnectString = CType(Get_RegistryItem(REGISTRY_BASE & "\Connections", ItemName, ConnectString), String)
      End If

    End If

    ' create connection object in the DataHandler object.

    myConnection = MainDataHandler.Add_Connection(CTA_CONNECTION, ConnectString)
    _SQLConnectionString = ConnectString
    _SQLAsynchConnectString = ConnectString & ";async=true"

    If (myConnection Is Nothing) Then
      If MainDataHandler.ErrorMessage.Length > 0 Then
        Me.LogError(Me.Name, LOG_LEVELS.Error, MainDataHandler.ErrorMessage, "Failed to open Connection.", "", True)
        Exit Sub
      Else
        Me.LogError(Me.Name, LOG_LEVELS.Error, "", "Failed to open Connection.", "", True)
        Exit Sub
      End If
    End If

    ' Ensure that it is opened.

    Try
      If myConnection.State = ConnectionState.Closed Then
        myConnection.Open()
      End If
    Catch ex As Exception
      Me.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Failed to open Connection.", ex.StackTrace, True)
      Exit Sub
    End Try

    ' **********************************************************************
    ' Check Security

    Dim SecurityOK As Boolean = True

    If (SecurityTestFlag1) Then
      SecurityOK = SecurityOK And SecutityTestOne()
    End If
    If (SecurityTestFlag2) Then
      SecurityOK = SecurityOK And SecutityTestTwo()
    End If
    If (SecurityTestFlag3) Then
      SecurityOK = SecurityOK And SecutityTestThree()
    End If
    If (SecurityTestFlag4) Then
      SecurityOK = SecurityOK And SecutityTestFour()
    End If

    If (Not SecurityOK) Then
      LogError(Me.Name, LOG_LEVELS.Warning, "", "CTA Security test failed, Please refer to F&C Partners LLP for application licensing.", "", True)

      ApplicationLockout = True
    End If

    ' **********************************************************************

    ' FormUpdateDataTimer
    ' FormUpdateObjects

    If (FormUpdateDataTimer IsNot Nothing) Then
      Try
        FormUpdateDataTimer.Stop()
      Catch ex As Exception
      Finally
        FormUpdateDataTimer = Nothing
      End Try
    End If
    Try
      FormUpdateDataTimer = New System.Windows.Forms.Timer
      FormUpdateDataTimer.Interval = 200
      AddHandler FormUpdateDataTimer.Tick, AddressOf FormUpdateDataTimer_Tick
      FormUpdateDataTimer.Start()
    Catch ex As Exception
    End Try

    ' **********************************************************************

    Main_Knowledgedate = KNOWLEDGEDATE_NOW

    Me.CTAStatusLabel.Text = ""
    Me.CTAStatusStrip.Refresh()
    Me.Menu_File_EnableReportFadeIn.Checked = True

    Call SetMenuBarEnabled()

    PertracData.StatFunctionsObject = StatFunctions

    ' Preload a couple of tables

    Call Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList)

    Call Refresh_MasternameDictionary()

    ' Call Refresh_LocationsDictionary()

    PertracLookupCollection = _MasternameDictionary ' GetTblLookupCollection(RenaissanceGlobals.RenaissanceStandardDatasets.Mastername, "Mastername", "ID", "", False, True, True, 0, "")

    _ThisInformationColumnCache = PertracData.Set_InformationColumnMap

    ' Initialise New Custom Data Cache.

    If (_CustomFieldDataCache Is Nothing) Then
      _CustomFieldDataCache = New CustomFieldDataCacheClass(Me)
    End If

    ' Refresh Splash if display time is not long enough.

    If (Not (Splash Is Nothing)) Then
      Try
        If (Now - SplashTime).TotalSeconds < SPLASH_DISPLAY Then
          Splash.StartTimer(SplashTime.AddSeconds(SPLASH_DISPLAY))
          Splash.Hide()
          Splash.ShowDialog()
        End If

        Splash.Hide()
        Splash.Close()
      Catch ex As Exception
      Finally
        If (Not (Splash Is Nothing)) Then
          If Splash.Visible Then Splash.Hide()
        End If
        Splash = Nothing
      End Try
    End If

  End Sub

  Private Sub CTAMain_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    ' **********************************************************************
    ' Close the application  neatly.
    ' **********************************************************************

    Dim TimeCounter As Integer

    ' Tidy Up :-

    ' Remove Handlers

    If (FormUpdateDataTimer IsNot Nothing) Then
      Try
        FormUpdateDataTimer.Stop()
      Catch ex As Exception
      Finally
        FormUpdateDataTimer = Nothing
      End Try
    End If

    ' ShutDown CTATCPClient

    Try
      If (Not (CTATCPClient Is Nothing)) Then
        CTATCPClient.CloseDown = True
        TimeCounter = 20
        While (TimeCounter > 0) And (CTATCPClient.IsCompleted = False)
          Threading.Thread.Sleep(10)
          TimeCounter -= 1
        End While
        If (CTATCPClient.IsCompleted = False) Then
          CTATCPClient.Stop()
        End If
      End If
    Catch ex As Exception
    Finally
      CTATCPClient = Nothing
    End Try

    ' **********************************************************************
    ' Clear Data Handler
    ' **********************************************************************

    Try

      If (MainDataHandler IsNot Nothing) Then
        SyncLock MainDataHandler
          If (MainDataHandler.Adaptors.Count > 0) Then
            MainDataHandler.Adaptors.Clear()
          End If

          If (MainDataHandler.Datasets.Count > 0) Then
            MainDataHandler.Datasets.Clear()
          End If

          If (MainDataHandler.DBConnections.Count > 0) Then
            Dim ThisConnection As SqlConnection

            For Each ThisConnection In MainDataHandler.DBConnections.Values
              If (ThisConnection IsNot Nothing) Then
                If (ThisConnection.State And ConnectionState.Open) = ConnectionState.Open Then
                  ThisConnection.Close()
                End If
              End If
            Next

            MainDataHandler.DBConnections.Clear()
          End If
        End SyncLock
      End If

    Catch ex As Exception
    Finally
      _MainDataHandler = Nothing
    End Try

  End Sub


#End Region

#Region " Update Event processes and Event Thread synchronisation classe"

  ' **************************************************************************************
  ' When a CTA Update is received from the TCPServer, the message should be de-coded and 
  ' appropriate Update events should be triggered and processed by the appropriate threads.
  ' And this is what I thought was happening, but I was wrong !
  '
  '
  ' When a MessageReceivedEvent is triggered by the TCPClient object, it is the TCPClient thread that
  ' executes the associated handlers.
  ' Thus the 'TCPMessageReceived' process is (always?) executed by the TCPclient thread.
  ' That being the case, the subsequent CTAUpdateEvents are also run within the TCPClient thread, which
  ' can cause problems when controls are updated on CTA forms, control update methods and properties not being 
  ' thread safe. 
  ' dotNet 1 did not seem to complain about this, though it is not thread safe and may have been the root cause
  ' of some strange bugs (Brian's mysterious hangings ?). Dot Net 2.0 does seem to complain vociferously
  ' about this which is what has brought it to my attention.
  '
  ' The simplest way of marshaling the update execution onto the main CTA UI thread seems to be to
  ' leverage the 'Invoke' method of the 'Control' class, which is designed to marshal execution onto the 
  ' Control's parent thread.
  ' 
  ' Thus the 'UpdateEventControlClass' exists for this sole purpose. It exposes a simple delegate which take parameters 
  ' identical to the ProcessTCPMessage() process. 
  '
  ' AddHandler CTATCPClient.MessageReceivedEvent, AddressOf Me.TCPMessageReceived
  ' **************************************************************************************

  Friend Class UpdateEventControlClass
    ' **************************************************************************************
    ' Simple Control Class designed to Marshal Update Event processing back to the main CTA
    ' UserInterface thread so that subsequent control updates are conducted in a thread safe fashion.
    ' **************************************************************************************

    Inherits Control

    Delegate Sub MessageReceivedDelegate(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)

    Private _MessageReceivedDelegate As MessageReceivedDelegate
    Private MainForm As CTAMain

    Public ReadOnly Property TCPMessageDelegate() As MessageReceivedDelegate
      Get
        Return _MessageReceivedDelegate
      End Get
    End Property

    Private Sub New()
      MyBase.New()
    End Sub

    Public Sub New(ByVal pMainForm As CTAMain)
      MyBase.New()

      MainForm = pMainForm
      _MessageReceivedDelegate = New MessageReceivedDelegate(AddressOf Me.TCPMessageReceived)
    End Sub

    Private Sub TCPMessageReceived(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)
      MainForm.ProcessTCPMessage(sender, e)
    End Sub

  End Class

  Public Sub Main_RaiseEvent(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs) Implements StandardRenaissanceMainForm.Main_RaiseEvent
    ' **********************************************************************
    ' Propagate the Changed Event, Globally.
    ' **********************************************************************
    Call Main_RaiseEvent(pEventArgs, True)
  End Sub

  Public Sub Main_RaiseEvent(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs, ByVal pGlobalUpdate As Boolean, Optional ByVal ApplicationName As FCP_Application = (FCP_Application.Genoa Or FCP_Application.Renaissance))
    ' **********************************************************************
    ' Propagate the local Changed Event
    '
    ' **********************************************************************
    Dim thisChangeID As RenaissanceGlobals.RenaissanceChangeID
    Dim thisUpdateDetail As String

    Dim ThisDataset As StandardDataset

    ' If this event includes the KnowledgeDate ID or the DB Connection ID, then re-load all the tables.
    ' If this is only a KnowledgeDate Update, then don't reload non-Knowledgedated tables.
    ' If this is a connection update, then re-load all standard tables.

    Dim IDsToReload As New ArrayList
    Dim UpdateID As RenaissanceChangeID

    Try
      Dim UpdateIsKnowledgeDate As Boolean = False
      Dim UpdateIsConnection As Boolean = False

      For Each thisChangeID In pEventArgs.TablesChanged
        thisUpdateDetail = pEventArgs.UpdateDetail(thisChangeID)

        If (thisChangeID = RenaissanceChangeID.KnowledgeDate) Then
          UpdateIsKnowledgeDate = True

          Try
            ' The CustomFieldDataCache object caches Custom Field Data and is shared between forms, thus
            ' it makes sense to clear the cache at this central point.

            If (CustomFieldDataCache IsNot Nothing) Then
              CustomFieldDataCache.ClearDataCache()
              CustomFieldDataCache.ClearDataCache(True)
            End If

          Catch ex As Exception
          End Try
        End If

        If (thisChangeID = RenaissanceChangeID.Connection) Then
          UpdateIsConnection = True
        End If

        ' Cycle through each standard dataset (via ChangeID enumeration)
        ' checking whether there should be a dependency based re-load.
        ' Do not bother re-loading the dataset if this is a Connection or KD Update
        ' as the DS will be re-loaded in the next step.

        For Each UpdateID In System.Enum.GetValues(GetType(RenaissanceChangeID))
          ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(UpdateID)

          If (Not (ThisDataset Is Nothing)) Then
            ThisDataset.ISChangeIDToNote(thisChangeID)

            If Not (UpdateIsConnection Or (UpdateIsKnowledgeDate And ThisDataset.IsKnowledgeDated)) Then
              If ThisDataset.IsChangeIDToTriggerUpdate(thisChangeID) Then
                If Not IDsToReload.Contains(UpdateID) Then
                  IDsToReload.Add(UpdateID)
                  ReloadTable(UpdateID, False)
                End If
              End If
            End If
          End If
        Next

        ' Post Table Refresh Actions

        Select Case thisChangeID

          Case RenaissanceChangeID.tblLocations

            Call Refresh_LocationsDictionary()

          Case RenaissanceChangeID.Mastername
            ' The PertracData object caches Instrument Returns Data and is shared between forms, thus
            ' it makes sense to clear the cache at this central point.

            Try

              'If (PertracLookupCollection IsNot Nothing) Then
              '	PertracLookupCollection.Clear()
              'End If

              'PertracLookupCollection = GetTblLookupCollection(RenaissanceGlobals.RenaissanceStandardDatasets.Mastername, "Mastername", "ID", "", False, True, True, 0, "")

              Call Refresh_MasternameDictionary()

              If (CustomFieldDataCache IsNot Nothing) Then
                CustomFieldDataCache.RefreshInstrumentNumerator()
              End If


            Catch ex As Exception
            End Try

          Case RenaissanceChangeID.Information

            Try
              ' The PertracData object caches Instrument Information Data and is shared between forms, thus
              ' it makes sense to clear the cache at this central point.

              SyncLock PertracData

                PertracData.ClearInformationCache()

                If (thisUpdateDetail Is Nothing) OrElse (thisUpdateDetail.Length <= 0) Then
                  CustomFieldDataCache.ClearDataCache(True)

                Else

                  CustomFieldDataCache.ClearStaticDataCache(thisUpdateDetail)
                End If

              End SyncLock
            Catch ex As Exception
            End Try

          Case RenaissanceChangeID.Performance

            Try
              ' The StatFunctions object caches Instrument Statistics Data and is shared between forms, thus
              ' it makes sense to clear the cache at this central point.

              SyncLock StatFunctions
                StatFunctions.ClearCache()
              End SyncLock

            Catch ex As Exception
            End Try

            ' Pertrac Data class responds to the Update Event. Does not need to be done here.
            Try
              ' the pertracdata object caches instrument returns data and is shared between forms, thus
              ' it makes sense to clear the cache at this central point.

              SyncLock PertracData
                PertracData.ClearDataCache()
              End SyncLock
            Catch ex As Exception
            End Try

          Case RenaissanceChangeID.tblGroupList

            Try

              'If (PertracLookupCollection IsNot Nothing) Then
              '	PertracLookupCollection.Clear()
              'End If

              'PertracLookupCollection = GetTblLookupCollection(RenaissanceGlobals.RenaissanceStandardDatasets.Mastername, "Mastername", "ID", "", False, True, True, 0, "")

              Call Refresh_MasternameDictionary()

              If (CustomFieldDataCache IsNot Nothing) Then
                CustomFieldDataCache.RefreshInstrumentNumerator()
              End If

            Catch ex As Exception
            End Try

          Case RenaissanceChangeID.tblGroupItemData

            Try
              ' The StatFunctions object caches Instrument Statistics Data and is shared between forms, thus
              ' it makes sense to clear the cache at this central point.

              SyncLock StatFunctions
                StatFunctions.ClearCache(False, True, False, False, False, thisUpdateDetail)
              End SyncLock

              SyncLock PertracData
                PertracData.ClearDataCache(False, True, False, False, False, thisUpdateDetail)
              End SyncLock

            Catch ex As Exception
            End Try

          Case RenaissanceChangeID.tblGroupItems

            ' Clear Only Non-Zero Group Data.

            If (CustomFieldDataCache IsNot Nothing) Then

              ' CustomFieldDataCache.RefreshInstrumentNumerator()

              If (thisUpdateDetail Is Nothing) OrElse (thisUpdateDetail.Length <= 0) Then
                CustomFieldDataCache.ClearDataCacheGroupData(0, 0)

              Else
                CustomFieldDataCache.ClearDataCacheGroupData(thisUpdateDetail)

              End If

            End If

            Try
              ' The StatFunctions object caches Instrument Statistics Data and is shared between forms, thus
              ' it makes sense to clear the cache at this central point.

              SyncLock StatFunctions
                StatFunctions.ClearCache(False, True, False, False, False, thisUpdateDetail)
              End SyncLock

            Catch ex As Exception
            End Try

            ' Pertrac Data class responds to the Update Event. Does not need to be done here.
            ' Do it anyway so that it is done first.

            Try
              ' The PertracData object caches Instrument Returns Data and is shared between forms, thus
              ' it makes sense to clear the cache at this central point.

              SyncLock PertracData
                PertracData.ClearDataCache(False, True, False, False, False, thisUpdateDetail)
              End SyncLock
            Catch ex As Exception
            End Try

          Case RenaissanceChangeID.tblPertracCustomFieldData

            ' Clear All Cache data relating or All, or Specific, Custom Fields.

            ' New Cache

            If (CustomFieldDataCache IsNot Nothing) Then
              If (thisUpdateDetail Is Nothing) OrElse (thisUpdateDetail.Length <= 0) Then
                CustomFieldDataCache.ClearDataCache()

              Else

                CustomFieldDataCache.ClearDataCache(thisUpdateDetail)
              End If

            End If

          Case RenaissanceChangeID.tblPertracCustomFields

            If (CustomFieldDataCache IsNot Nothing) Then
              If (thisUpdateDetail Is Nothing) OrElse (thisUpdateDetail.Length <= 0) Then
                CustomFieldDataCache.VerifyCustomFieldTypes(0)

                'ElseIf (IsNumeric(thisUpdateDetail)) AndAlso (CInt(thisUpdateDetail) > 0) Then
                '	CustomFieldDataCache.VerifyCustomFieldTypes(CInt(thisUpdateDetail))

              Else
                CustomFieldDataCache.VerifyCustomFieldTypes(thisUpdateDetail)

              End If
            End If

        End Select

      Next

      ' Re-load Datasets as appropriate to a connection or KnowledgeDate change.

      For Each UpdateID In System.Enum.GetValues(GetType(RenaissanceChangeID))
        ThisDataset = RenaissanceStandardDatasets.GetStandardDataset(UpdateID)

        If (Not (ThisDataset Is Nothing)) Then
          If (UpdateIsConnection Or (UpdateIsKnowledgeDate And ThisDataset.IsKnowledgeDated)) Then
            ReloadTable(ThisDataset.ChangeID, False)
          End If
        End If
      Next

    Catch ex As Exception
      Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error checking KnowledgeDate or Connection Update.", ex.StackTrace, True)
    End Try

    ' Post Update Event.

    Try
      RaiseEvent CTAAutoUpdate(Me, pEventArgs)
    Catch ex As Exception
      Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error raising AutoUpdate event.", ex.StackTrace, True)
    End Try

    ' Post 'Cascaded' Update messages
    ' This is structured so that Cascaded updates are broadcast after the primary Table update.

    If (Not (IDsToReload Is Nothing)) AndAlso (IDsToReload.Count > 0) Then
      For Each UpdateID In IDsToReload
        Try
          RaiseEvent CTAAutoUpdate(Me, New RenaissanceGlobals.RenaissanceUpdateEventArgs(UpdateID))
        Catch ex As Exception
          Call LogError(Me.Name & ", Main_RaiseEvent", LOG_LEVELS.Error, ex.Message, "Error raising AutoUpdate event.", ex.StackTrace, True)
          Exit For
        End Try
      Next
      IDsToReload.Clear()
    End If

    ' Pass Updates to the message server, if required

    If (pGlobalUpdate = True) Then
      If (Not (Me.CTATCPClient Is Nothing)) Then

        If pEventArgs.TablesChanged.GetLength(0) > 1 Then
          ' Multiple Updates, 
          ' Construct a Renaissance Update object 
          ' NPP Updated 17 April 2008 to add UpdateDetail functionality.

          Dim ChangeIDArray() As RenaissanceChangeID
          Dim DetailArray() As String

          ChangeIDArray = pEventArgs.TablesChanged
          DetailArray = pEventArgs.UpdateDetails()

          Dim thisRenaissanceUpdateClass As New RenaissanceUpdateClass(ChangeIDArray, DetailArray)
          Dim ThisMessageHeader As New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, "", "", 1, 0)

          ' Post Header and Update object to Queue for sending.

          Me.CTATCPClient.PostNewMessage(New MessageFolder(ThisMessageHeader, thisRenaissanceUpdateClass))

        Else
          ' Only a single Update Event.
          ' Post it as a simple Header based Update
          ' NPP Updated 17 April 2008 to add UpdateDetail functionality.

          Dim MessageString As String
          Dim UpdateString As String

          For Each thisChangeID In pEventArgs.TablesChanged
            If (Not (thisChangeID = RenaissanceGlobals.RenaissanceChangeID.None)) Then
              MessageString = thisChangeID.ToString
              UpdateString = pEventArgs.UpdateDetail(thisChangeID)
              If (UpdateString IsNot Nothing) AndAlso (UpdateString.Length > 0) Then
                MessageString &= "," & UpdateString
              End If

              Me.CTATCPClient.PostNewMessage(New MessageFolder(New TCPServerHeader(TCP_ServerMessageType.Client_ApplicationUpdateMessage, ApplicationName, "", MessageString, 1, 0)))
            End If
          Next
        End If
      End If

    End If
  End Sub

  Private Sub TCPMessageReceived(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)
    ' **************************************************************************************
    ' 
    ' Event handler for the TCP Client MessageReceivedEvent Event.
    ' This Routine should use the UpdateEventControl to marshal the update execution back to 
    ' the Main CTA User Interface thread.
    '
    ' **************************************************************************************

    Try
      UpdateEventControl.Invoke(UpdateEventControl.TCPMessageDelegate, New Object() {sender, e})
    Catch ex As Exception
    End Try
  End Sub

  Friend Sub ProcessTCPMessage(ByVal sender As System.Object, ByVal e As MessageReceivedEventArgs)
    ' **********************************************************************
    '
    ' Process MessageReceived events from the TCP Client.
    ' Usually this event is only trggered for Update Messages.
    '
    ' **********************************************************************
    Dim TerminateFlag As Integer = 0

    Select Case e.HeaderMessage.MessageType

      Case TCP_ServerMessageType.Server_ApplicationUpdateMessage, TCP_ServerMessageType.Client_ApplicationUpdateMessage

        ' **********************************************************************
        ' If this is an Update Message....
        ' **********************************************************************

        If (e.HeaderMessage.Application And (FCP_Application.Genoa Or FCP_Application.Renaissance)) <> FCP_Application.None Then

          ' **********************************************************************
          ' And it is for CTA...
          ' **********************************************************************

          If (e.HeaderMessage.FollowingMessageCount > 0) AndAlso _
          (e.FollowingObjects.Count > 0) AndAlso _
          (e.FollowingObjects(0).GetType Is GetType(RenaissanceUpdateClass)) Then

            ' **********************************************************************
            ' Multiple Updates have been sent as in a 'RenaissanceUpdateClass' class
            ' **********************************************************************

            Dim thisRenaissanceUpdateEventArgs As New RenaissanceUpdateEventArgs
            Dim thisRenaissanceUpdateClass As New RenaissanceUpdateClass
            Dim thisChangeID As RenaissanceChangeID

            ' Construct the local UpdateEvent Argument class

            thisRenaissanceUpdateClass = CType(e.FollowingObjects(0), RenaissanceUpdateClass)

            For Each thisChangeID In thisRenaissanceUpdateClass.ChangeIDs

              ' Don't Propagate Knowledgedate or DBConnection changes from other applications.

              If (thisChangeID <> RenaissanceChangeID.KnowledgeDate) And (thisChangeID <> RenaissanceChangeID.Connection) Then

                ' Add this ChangeID to the Update Class Object
                thisRenaissanceUpdateEventArgs.TableChanged(thisChangeID) = True
                thisRenaissanceUpdateEventArgs.UpdateDetail(thisChangeID) = thisRenaissanceUpdateClass.UpdateDetail(thisChangeID)

                ' ReLoad the related dataset
                Call ReloadTable(thisChangeID, False)

              End If

              If thisChangeID = RenaissanceChangeID.Terminate1 Then
                TerminateFlag = (TerminateFlag Or 1)
              End If
              If thisChangeID = RenaissanceChangeID.Terminate2 Then
                TerminateFlag = (TerminateFlag Or 2)
              End If
            Next

            ' **********************************************************************
            ' Handle the Shutdown Message.
            ' **********************************************************************
            If TerminateFlag = 3 Then
              Call Menu_File_Close_Click(Me, New System.EventArgs)

              Exit Sub
            End If

            ' **********************************************************************
            ' Trigger the local Changed Event
            ' **********************************************************************

            Main_RaiseEvent(thisRenaissanceUpdateEventArgs, False)

          Else

            ' **********************************************************************
            ' Only a single Update has been set, encoded in the MessageHeader String
            ' The Message string may either be an Integer relating to the RenaissanceChangeID Enum,
            ' OR the text representation of the Enum value, e.g. 'tblFund'.
            ' **********************************************************************

            Dim ChangeID As Integer    ' RenaissanceGlobals.RenaissanceChangeID
            Dim UpdateDetail As String
            ChangeID = e.UpdateMessage(FCP_Application.Genoa Or FCP_Application.Renaissance)
            UpdateDetail = e.UpdateDetail(FCP_Application.Genoa Or FCP_Application.Renaissance)

            If (ChangeID > 0) And (ChangeID <> RenaissanceChangeID.KnowledgeDate) Then
              Try
                ' **********************************************************************
                ' ReLoad the related dataset
                ' Trigger the local Changed Event
                '
                ' **********************************************************************

                Call ReloadTable(CType(ChangeID, RenaissanceChangeID), False)

                ' Raise Event.

                Main_RaiseEvent(New RenaissanceUpdateEventArgs(CType(ChangeID, RenaissanceChangeID), UpdateDetail), False)

              Catch ex As Exception
              End Try
            End If
          End If
        End If

      Case TCP_ServerMessageType.Server_NaplesDataAnswer, TCP_ServerMessageType.Client_NaplesDataAnswer





      Case Else

    End Select

  End Sub

  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    ' **************************************************************************************
    ' Eable / Disable menu items according to any changes made in the User Permissions table.
    '
    '
    ' **************************************************************************************

    If (Me.IsDisposed) Then Exit Sub

    ' Changes to the tblUserPermissions table :-
    Try
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Then
        Call SetMenuBarEnabled()
      End If
    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " CTA Forms Code"

  Friend Function New_CTAForm(ByVal pFormID As CTAFormID, Optional ByVal pShowForm As Boolean = True) As RenaissanceGlobals.RenaissanceFormHandle
    ' **********************************************************************
    ' Spawn new Form.
    ' **********************************************************************

    Dim newFormHandle As RenaissanceGlobals.RenaissanceFormHandle = Nothing
    Dim StartCursor As Cursor = Cursors.Default
    Dim IsNewForm As Boolean = False

    Try
      StartCursor = Me.Cursor
      Me.Cursor = Cursors.WaitCursor

      newFormHandle = CTAForms.UnusedItem(pFormID)

      ' Parannoia check for disposed forms. Should never occur. (ha !)
      Try
        If (Not (newFormHandle Is Nothing)) AndAlso (Not (newFormHandle.Form Is Nothing)) Then
          If (newFormHandle.Form.IsDisposed) Then
            CTAForms.Remove(newFormHandle)
            newFormHandle = Nothing
          End If
        End If
      Catch ex As Exception
        newFormHandle = Nothing
      End Try

      ' Create a New form if an existing one was not returned.
      If (newFormHandle Is Nothing) Then
        Try
          Dim ThisAssembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly
          Dim myForm As Form
          myForm = CType(ThisAssembly.CreateInstance("CTA." & System.Enum.GetName(GetType(CTAFormID), pFormID), True, Reflection.BindingFlags.Default, Nothing, New Object() {Me}, Nothing, Nothing), Form)

          If (myForm Is Nothing) Then
            newFormHandle = Nothing
            Call LogError(Me.Name & ", New_CTAForm", LOG_LEVELS.Warning, "", "Form " & System.Enum.GetName(GetType(CTAFormID), pFormID) & " does not seem to exist !", "", True)
          Else

            myForm.Icon = CType(Me.Icon.Clone, Drawing.Icon)

            newFormHandle = New RenaissanceGlobals.RenaissanceFormHandle(myForm, pFormID)
            Application.DoEvents()
            If Not (newFormHandle.Form Is Nothing) Then
              CTAForms.Add(newFormHandle)
              IsNewForm = True
            End If

          End If

        Catch ex As Exception
          newFormHandle = Nothing
          Call LogError(Me.Name & ", New_CTAForm", LOG_LEVELS.Error, ex.Message, "Error Loading Form " & System.Enum.GetName(GetType(CTAFormID), pFormID), ex.StackTrace, True)
        End Try

      Else
        ' Refurbish an existing form.

        ' newFormHandle.Form.Visible = True
        Try
          Dim StdForm As StandardCTAForm
          StdForm = CType(newFormHandle.Form, StandardCTAForm)
          StdForm.ResetForm()
        Catch ex As Exception
          newFormHandle = Nothing
          Call LogError(Me.Name & ", New_CTAForm", LOG_LEVELS.Error, ex.Message, "Error Re-Setting Form " & System.Enum.GetName(GetType(CTAFormID), pFormID), ex.StackTrace, True)
        End Try
      End If

      If (Me.IsMdiContainer) AndAlso (newFormHandle IsNot Nothing) Then
        Try
          newFormHandle.Form.MdiParent = Me
        Catch ex As Exception
        End Try
      End If

      Try
        If (newFormHandle IsNot Nothing) Then
          Dim StdForm As StandardCTAForm

          newFormHandle.InUse = True
          newFormHandle.Form.ShowInTaskbar = False

          ' Don't immediately show the Report or Drilldown form.
          ' Show all other forms, unless suppressed by the 'pShowForm' parameter.

          If (pShowForm) AndAlso (Not (pFormID = CTAFormID.frmViewReport)) Then

            If newFormHandle.Form.Visible = False Then
              newFormHandle.Form.Show()
            Else
              newFormHandle.Form.WindowState = FormWindowState.Normal
            End If

            Application.DoEvents()

            StdForm = CType(newFormHandle.Form, StandardCTAForm)
            If StdForm.FormOpenFailed Then
              newFormHandle.Form.Hide()
              RemoveFromFormsCollection(newFormHandle.Form)
            End If
          End If

          SetComboSelectionLengths(newFormHandle.Form)

        End If
      Catch ex As Exception
        Call LogError(Me.Name & ", New_CTAForm", LOG_LEVELS.Error, ex.Message, "Error Showing Form " & System.Enum.GetName(GetType(CTAFormID), pFormID), ex.StackTrace, True)
      End Try
    Catch ex As Exception
    Finally
      Me.CTAStatusLabel.Text = ""
      Me.Cursor = StartCursor
    End Try

    Try

      Call BuildWindowsMenu()

    Catch ex As Exception
    End Try

    Return newFormHandle
  End Function

  Public Sub RemoveFromFormsCollection(ByRef pForm As Form)
    CTAForms.Remove(pForm)
    Call BuildWindowsMenu()
  End Sub

  Public Sub RemoveFromFormsCollection(ByRef pRenaissanceFormHandle As RenaissanceGlobals.RenaissanceFormHandle)
    CTAForms.Remove(pRenaissanceFormHandle)
    Call BuildWindowsMenu()
  End Sub

  Public Sub HideInFormsCollection(ByRef pForm As Form)
    CTAForms.Hide(pForm)
    Call BuildWindowsMenu()
  End Sub

  Public Sub HideInFormsCollection(ByRef pRenaissanceFormHandle As RenaissanceGlobals.RenaissanceFormHandle)
    CTAForms.Hide(pRenaissanceFormHandle)
    Call BuildWindowsMenu()
  End Sub

  Friend Sub BuildWindowsMenu()
    ' ************************************************************************
    '
    '
    ' ************************************************************************

    Dim thisWindowsMenu As ToolStripMenuItem
    Dim ActiveWindows As ArrayList
    Dim WindowCount As Integer
    Dim ThisFormHandle As RenaissanceFormHandle

    thisWindowsMenu = Menu_Windows
    If (thisWindowsMenu.DropDownItems.Count > 0) Then
      thisWindowsMenu.DropDownItems.Clear()
    End If

    ActiveWindows = Me.CTAForms.AllActiveItems
    If (Not (ActiveWindows Is Nothing)) AndAlso (ActiveWindows.Count > 1) Then
      ' Index 0 should always be the Main Form, so skip it.
      Dim thisMenuItem As ToolStripMenuItem

      For WindowCount = 1 To (ActiveWindows.Count - 1)
        ThisFormHandle = CType(ActiveWindows(WindowCount), RenaissanceFormHandle)

        thisMenuItem = New ToolStripMenuItem(ThisFormHandle.Form.Text, Nothing, AddressOf Me.WindowsMenuEvent)
        thisMenuItem.Tag = (WindowCount)

        thisWindowsMenu.DropDownItems.Add(thisMenuItem)
      Next
    End If

  End Sub


  Private Sub WindowsMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' ************************************************************************
    ' Action Event for the Windows Menu.
    '
    ' ************************************************************************

    Dim SelectedMenuItem As ToolStripMenuItem
    Dim ActiveWindows As ArrayList
    Dim ThisFormHandle As RenaissanceFormHandle

    Try
      SelectedMenuItem = CType(sender, ToolStripMenuItem)
      ActiveWindows = Me.CTAForms.AllActiveItems

      ThisFormHandle = CType(ActiveWindows(CInt(SelectedMenuItem.Tag)), RenaissanceFormHandle)
      ThisFormHandle.Form.Activate()
      If (ThisFormHandle.Form.WindowState = FormWindowState.Minimized) Then
        ThisFormHandle.Form.WindowState = FormWindowState.Normal
      End If

    Catch ex As Exception
    End Try

  End Sub

  Public Sub SetComboSelectionLengths(ByRef pForm As Form, Optional ByVal pExceptControl As ComboBox = Nothing)
    ' ************************************************************************
    ' I have come accross an issue such that if an editable Combo box is used
    ' on a form, then when the form is drawn and GetFormData() is called, the
    ' text contents of the combo box appear as highlighted text. Most annoying.
    ' My initial solution involved setting the SelectionLength property to Zero
    ' which succeeded in clearing the text highlight, however it was found in
    ' operation that randomly and occasionally it would take several seconds for
    ' the "SelectionLength = 0" command to execute. Even More Annoying !
    ' Thus I have re-written this function to set focus to any appropriate Combos
    ' as this seems to be equally effective at clearing the highlighted text
    ' but will, I hope, avoid the aforementioned side affects.
    '
    ' ************************************************************************
    Dim thisControl As Control
    Dim thisCombo As ComboBox

    If (pForm Is Nothing) Then
      Exit Sub
    End If

    Try
      For Each thisControl In pForm.Controls
        If TypeOf thisControl Is ComboBox Then
          thisCombo = CType(thisControl, ComboBox)

          If (pExceptControl Is Nothing) OrElse (Not thisCombo.Equals(pExceptControl)) Then
            If thisCombo.DropDownStyle <> ComboBoxStyle.DropDownList Then
              thisCombo.Focus()
            End If
          End If
        End If
      Next
    Catch ex As Exception
    End Try

  End Sub

  Public Sub GenericFormResizeHandler(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      SetComboSelectionLengths(CType(sender, Form))
    Catch ex As Exception
    End Try
  End Sub

  Private Sub SetMenuBarEnabled()
    ' **********************************************************************
    ' 
    ' **********************************************************************

    Try
      If Me.CTAMenu.Items.Count > 0 Then
        SetMenuItemEnabled(CTAMenu.Items)
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetMenuItemEnabled(ByRef ItemArray As ToolStripItemCollection)
    ' **********************************************************************
    ' 
    ' **********************************************************************

    Dim thisItem As ToolStripItem
    Dim thisMenuItem As ToolStripMenuItem
    Dim ThisFeatureName As String
    Dim ThisFeatureType As PermissionFeatureType

    For Each thisItem In ItemArray
      Try
        If (Not (thisItem Is Nothing)) Then
          If (TypeOf thisItem Is ToolStripMenuItem) Then
            thisMenuItem = CType(thisItem, ToolStripMenuItem)

            If (thisMenuItem.DropDownItems.Count > 0) Then
              SetMenuItemEnabled(thisMenuItem.DropDownItems)
            ElseIf (thisItem.Tag Is Nothing) Then
              thisItem.Enabled = True
            Else
              ThisFeatureName = thisItem.Tag.ToString
              If ThisFeatureName.StartsWith("rpt") Then
                ThisFeatureType = PermissionFeatureType.TypeReport
              Else
                ThisFeatureType = PermissionFeatureType.TypeForm
              End If

              If (CheckPermissions(ThisFeatureName, ThisFeatureType) > 0) Then
                thisItem.Enabled = True
              Else
                thisItem.Enabled = False
              End If
            End If

          End If

        End If

      Catch ex As Exception
      End Try
    Next
  End Sub

  Public Function BuildStandardFormMenu( _
  ByRef RootMenu As MenuStrip, _
  ByVal pThisTable As DataTable, _
  ByRef SelectClick As System.EventHandler, _
  ByRef SortClick As System.EventHandler, _
  ByRef AuditReportClick As System.EventHandler) As MenuStrip

    ' **********************************************************************
    ' Standard routine to build the Standard Form menus - Select & Order
    ' **********************************************************************

    Dim ThisColumn As System.Data.DataColumn
    Dim SelectByMenuItem As New ToolStripMenuItem("&Select By")
    Dim OrderByItem As New ToolStripMenuItem("&Order By")
    Dim AuditReportItem As New ToolStripMenuItem("&Audit Reports")
    Dim newMenuItem As ToolStripMenuItem
    Dim Counter As Integer

    'SelectByMenuItem.Text = "&Select By"
    'OrderByItem.Text = "&Order By"
    'AuditReportItem.Text = "&Audit Reports"

    Counter = 0
    For Each ThisColumn In pThisTable.Columns

      If Not (SelectClick Is Nothing) Then
        'newMenuItem = New ToolStripMenuItem(ThisColumn.ColumnName)
        'AddHandler newMenuItem.Click, SelectClick
        newMenuItem = CType(SelectByMenuItem.DropDownItems.Add(ThisColumn.ColumnName, Nothing, SelectClick), ToolStripMenuItem)
        newMenuItem.Tag = Counter
      End If

      If Not (SortClick Is Nothing) Then
        newMenuItem = CType(OrderByItem.DropDownItems.Add(ThisColumn.ColumnName, Nothing, SortClick), ToolStripMenuItem)
        newMenuItem.Tag = Counter
      End If

      Counter += 1
    Next

    If Not (SelectClick Is Nothing) Then
      RootMenu.Items.Add(SelectByMenuItem)
    End If
    If Not (SortClick Is Nothing) Then
      RootMenu.Items.Add(OrderByItem)
    End If

    If Not (AuditReportClick Is Nothing) Then
      newMenuItem = CType(AuditReportItem.DropDownItems.Add("This Record", Nothing, AuditReportClick), ToolStripMenuItem)
      newMenuItem.Tag = 0

      newMenuItem = CType(AuditReportItem.DropDownItems.Add("All Records", Nothing, AuditReportClick), ToolStripMenuItem)
      newMenuItem.Tag = 1

      RootMenu.Items.Add(AuditReportItem)
    End If

    Return RootMenu

  End Function

#End Region

#Region " CTA Menu Event Code"

  Private Sub Menu_File_ToggleMDI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_File_ToggleMDI.Click
    ' **********************************************************************
    '
    ' **********************************************************************

    Dim HandlesToDelete As New ArrayList
    Dim thisFormHandle As RenaissanceGlobals.RenaissanceFormHandle

    If Menu_File_ToggleMDI.Checked = False Then
      Try
        Menu_File_ToggleMDI.Checked = True

        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        Me.MaximizeBox = True
        Me.IsMdiContainer = True

        For Each thisFormHandle In CTAForms
          Try
            If (thisFormHandle.Form Is Nothing) Then
              HandlesToDelete.Add(thisFormHandle)
            Else
              thisFormHandle.Form.Hide()

              Try
                thisFormHandle.Form.MdiParent = Me
              Catch ex As Exception
              End Try

              If thisFormHandle.InUse Then
                thisFormHandle.Form.Show()
              End If
            End If
          Catch ex As Exception
          End Try
        Next
      Catch ex As Exception
      End Try
    Else
      Try
        Menu_File_ToggleMDI.Checked = False

        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False

        For Each thisFormHandle In CTAForms
          Try
            If (thisFormHandle.Form Is Nothing) Then
              HandlesToDelete.Add(thisFormHandle)

            Else

              thisFormHandle.Form.Hide()

              Try
                thisFormHandle.Form.MdiParent = Nothing
              Catch ex As Exception
              End Try

              If thisFormHandle.InUse Then
                thisFormHandle.Form.Show()
              End If

            End If

          Catch ex As Exception
          End Try
        Next

        Me.IsMdiContainer = False
        Me.Height = 196
        Me.Width = 884

      Catch ex As Exception
      End Try

    End If

    If HandlesToDelete.Count > 0 Then
      Dim DeleteCounter As Integer

      For DeleteCounter = 0 To (HandlesToDelete.Count - 1)
        Try
          CTAForms.Remove(CType(HandlesToDelete(DeleteCounter), RenaissanceGlobals.RenaissanceFormHandle))
        Catch ex As Exception
        End Try
      Next

    End If

  End Sub

  Private Sub Menu_File_EnableReportFadeIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_File_EnableReportFadeIn.Click
    ' **********************************************************************
    '
    '
    ' **********************************************************************

    Try
      Menu_File_EnableReportFadeIn.Checked = (Not Menu_File_EnableReportFadeIn.Checked)
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Menu_File_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_File_Close.Click
    ' **********************************************************************
    '
    ' **********************************************************************
    Dim FormHandle As RenaissanceGlobals.RenaissanceFormHandle
    Dim StdFormHandle As StandardCTAForm

    While CTAForms.Count > 0
      FormHandle = CTAForms.Item(CTAForms.Count - 1)
      CTAForms.Remove(FormHandle)

      If TypeOf (FormHandle.Form) Is StandardCTAForm Then
        StdFormHandle = CType(FormHandle.Form, StandardCTAForm)
        StdFormHandle.CloseForm()
      Else
        FormHandle.Form.Close()
      End If
    End While

    Me.Close()
  End Sub

  ' **********************************************************************
  ' **********************************************************************
  ' 
  '  Menu.
  '
  ' **********************************************************************
  ' **********************************************************************

  Friend Sub Generic_FormMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    ' **********************************************************************
    '
    ' Open New form of type relating to Menu Item's tag
    '
    ' **********************************************************************

    Dim thisSender As ToolStripMenuItem
    Dim thisFormID As CTAFormID

    If (TypeOf sender Is ToolStripMenuItem) Then
      thisSender = CType(sender, ToolStripMenuItem)
    Else
      Exit Sub
    End If

    If (thisSender.Tag Is Nothing) Then
      Exit Sub
    End If

    Try
      Me.Cursor = Cursors.WaitCursor

      thisFormID = CType(System.Enum.Parse(GetType(CTAFormID), thisSender.Tag.ToString), CTAFormID)

      Call New_CTAForm(thisFormID)

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

  End Sub

  Private Sub Menu_KD_Refresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_KD_Refresh.Click
    ' **********************************************************************
    ' Use the Connection Update Event to refresh all tables
    ' **********************************************************************

    Call Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.Connection))

  End Sub

#End Region

#Region " CheckPermissions()"

  Public Function CheckPermissions(ByVal PermissionArea As String, ByVal PermissionType As RenaissanceGlobals.PermissionFeatureType) As Integer
    ' **********************************************************************
    ' Central Permissions checking Function.
    '
    ' Simply invokes the underlying SQL Strored procedure to check the available
    ' permissons of the current user.
    ' **********************************************************************

    If (ApplicationLockout) Then
      Return 0
    End If

    Return RenaissanceGlobals.PermissionBitmap.PermRead + RenaissanceGlobals.PermissionBitmap.PermInsert + RenaissanceGlobals.PermissionBitmap.PermUpdate + RenaissanceGlobals.PermissionBitmap.PermDelete

    'Dim PermissionsCommand As New SqlCommand
    'Dim RVal As Integer = 0

    'PermissionsCommand.Connection = Me.GetCTAConnection

    'PermissionsCommand.CommandType = CommandType.StoredProcedure
    'PermissionsCommand.CommandText = "[fn_Check_Permission]"
    'PermissionsCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

    'PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserName", System.Data.SqlDbType.VarChar, 100))
    'PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FunctionName", System.Data.SqlDbType.VarChar, 100))
    'PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@FunctionType", System.Data.SqlDbType.Int, 4))
    'PermissionsCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))

    'PermissionsCommand.Parameters("@UserName").Value = DBNull.Value
    'PermissionsCommand.Parameters("@FunctionName").Value = PermissionArea
    'PermissionsCommand.Parameters("@FunctionType").Value = PermissionType

    'Try

    '  If (Not (PermissionsCommand.Connection Is Nothing)) Then
    '    SyncLock PermissionsCommand.Connection
    '      PermissionsCommand.ExecuteNonQuery()
    '    End SyncLock
    '  End If

    'Catch ex As Exception
    '  PermissionsCommand.Parameters("@RETURN_VALUE").Value = 0

    'Finally

    '  If (Not (PermissionsCommand.Connection Is Nothing)) Then
    '    Try
    '      PermissionsCommand.Connection.Close()
    '    Catch ex As Exception
    '    End Try
    '  End If
    'End Try


    'RVal = CInt(PermissionsCommand.Parameters("@RETURN_VALUE").Value)

    'Try
    '  If (SecurityTestFlag1 Or SecurityTestFlag2 Or SecurityTestFlag3 Or SecurityTestFlag4) Then
    '    Select Case (CInt(Int(4 * Rnd() + 1)))
    '      Case 1
    '        If (SecurityTestFlag1) Then
    '          If (Not SecutityTestOne()) Then
    '            CheckPermissions = 0
    '            Return 0
    '          End If
    '        End If
    '      Case 2
    '        If (SecurityTestFlag2) Then
    '          If (Not SecutityTestTwo()) Then
    '            CheckPermissions = 0
    '            Return 0
    '          End If
    '        End If
    '      Case 3
    '        If (SecurityTestFlag3) Then
    '          If (Not SecutityTestThree()) Then
    '            CheckPermissions = 0
    '            Return 0
    '          End If
    '        End If
    '      Case 4
    '        If (SecurityTestFlag4) Then
    '          If (Not SecutityTestFour()) Then
    '            CheckPermissions = 0
    '            Return 0
    '          End If
    '        End If
    '    End Select
    '  End If
    'Catch ex As Exception
    '  CheckPermissions = 0
    'End Try

    'Return RVal

  End Function

  Private Function SecutityTestOne(Optional ByVal pForceTest As Boolean = False) As Boolean
    ' ***********************************************************************************************
    ' Check For valid MAC Address
    '
    ' ***********************************************************************************************

    Dim RVal As Boolean = False

    Try
      If (SecurityTestFlag1) OrElse (pForceTest) Then

        Dim MacConverter As MACConverterClass
        Dim LicensedLong As ULong = Math.Max(Default_Licensed_MAC, CULng(1))
        Dim LicensedString As String

        LicensedString = CType(Get_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\License", "VALUE1", LicensedLong.ToString), String)

        If (IsNumeric(LicensedString)) Then
          LicensedLong = CULng(LicensedString)
        End If

        ' Get Licensed MAC Address

        MacConverter = New MACConverterClass(LicensedLong)

        ' Compare to This machine's MAX Addresses

        Dim ThisMACString As String = MacConverter.MACString
        Dim NICs As ManagementClass = New ManagementClass("Win32_NetworkAdapter")
        Dim NICCol As ManagementObjectCollection = NICs.GetInstances()

        For Each ThisNic As ManagementObject In NICCol
          If (ThisNic IsNot Nothing) AndAlso (ThisNic("MACAddress") IsNot Nothing) Then
            If (ThisNic("MACAddress").ToString = ThisMACString) Then
              RVal = True

              Exit For
            End If
          End If
        Next

      Else
        RVal = True
      End If
    Catch ex As Exception
      RVal = False
    End Try

    Return RVal

  End Function

  Private Function SecutityTestTwo(Optional ByVal pForceTest As Boolean = False) As Boolean
    ' ***********************************************************************************************
    ' Check For Registry Expiry Date
    '
    ' ***********************************************************************************************

    Dim RVal As Boolean = False

    Try
      If (SecurityTestFlag2) OrElse (pForceTest) Then
        Dim LicensedString As String
        Dim TestDateString As String
        Dim LicensedLong As ULong = Math.Max(Default_Licensed_Date, CULng(1))
        Dim ExpiryDate As DateConverterClass
        Dim TestDate As DateConverterClass

        LicensedString = CType(Get_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\License", "VALUE2", LicensedLong.ToString), String)
        TestDateString = CType(Get_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\License", "VALUE21", New DateConverterClass(Now.Date).ULongVal.ToString), String)

        ExpiryDate = New DateConverterClass(CULng(LicensedString))
        TestDate = New DateConverterClass(CULng(TestDateString))

        If (ExpiryDate.Date1 >= Now.Date) AndAlso (ExpiryDate.Date1 <= Now.Date.AddYears(1)) AndAlso (Now.Date > CDate("1 Jan 2008")) AndAlso (Now.Date >= TestDate.Date1) Then
          ' Only test this once
          _SecurityTest2 = False
          RVal = True

          Set_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\License", "VALUE21", New DateConverterClass(Now.Date).ULongVal.ToString)
        End If
      Else
        RVal = True
      End If
    Catch ex As Exception
      Return False
    End Try

    Return RVal

  End Function

  Private Function SecutityTestThree() As Boolean
    ' ***********************************************************************************************
    ' Check For file present
    '
    ' ***********************************************************************************************

    Dim RVal As Boolean = False

    Try
      If (SecurityTestFlag3) Then

      Else
        RVal = True
      End If
    Catch ex As Exception
      Return False
    End Try

    Return RVal

  End Function

  Private Function SecutityTestFour() As Boolean
    ' ***********************************************************************************************
    ' 
    '
    ' ***********************************************************************************************

    Dim RVal As Boolean = False

    Try
      If (SecurityTestFlag4) Then

      Else
        RVal = True
      End If
    Catch ex As Exception
      Return False
    End Try

    Return RVal

  End Function

  Private Sub SetValidMac(ByVal pMacString As String)
    ' ***********************************************************************************************
    '
    ' ***********************************************************************************************

    Try
      SetValidMac(New MACConverterClass(pMacString).MACLong)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetValidMac(ByVal pMacLong As ULong)
    ' ***********************************************************************************************
    '
    ' ***********************************************************************************************

    Try

      Set_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\License", "VALUE1", pMacLong.ToString)

    Catch ex As Exception
    End Try
  End Sub

  Private Sub SetValidExpiryDate(ByVal pDate As Date)
    ' ***********************************************************************************************
    '
    ' ***********************************************************************************************

    Try
      SetValidExpiryDate(New DateConverterClass(pDate).ULongVal)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetValidExpiryDate(ByVal pDateLong As ULong)
    ' ***********************************************************************************************
    '
    ' ***********************************************************************************************

    Try

      Set_RegistryItem(Registry.CurrentUser, REGISTRY_BASE & "\License", "VALUE2", pDateLong.ToString)

    Catch ex As Exception
    End Try
  End Sub

  Private Sub Menu_System_SetMAC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_System_SetMAC.Click
    ' ***********************************************************************************************
    '
    ' ***********************************************************************************************

    Try
      Dim GetString As String

      GetString = InputBox("Enter MAC Security Code", "Register MAC", "")
      If IsNumeric(GetString) Then
        SetValidMac(CULng(GetString))

        If (SecutityTestOne(True)) Then
          MsgBox("MAC correctly set.")
        Else
          MsgBox("MAC test Fails.")
        End If

      End If

    Catch ex As Exception
    End Try
  End Sub

  Private Sub Menu_System_SetExpiryDate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_System_SetExpiryDate.Click
    ' ***********************************************************************************************
    '
    ' ***********************************************************************************************

    Try
      Dim GetString As String

      GetString = InputBox("Enter Expiry Date Security Code", "Register Expiry Date", "")
      If IsNumeric(GetString) Then
        SetValidExpiryDate(CULng(GetString))

        If (SecutityTestTwo(True)) Then
          MsgBox("Expiry Date OK.")
        Else
          MsgBox("Expiry Date test Fails.")
        End If

      End If

    Catch ex As Exception
    End Try
  End Sub

  Private Sub Menu_System_UpdateDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_System_UpdateDatabase.Click
    ' ***********************************************************************************************
    '
    ' ***********************************************************************************************
    Dim AppPath As String
    Dim SQLString As String = ""

    If MessageBox.Show("Run Database update script ?", "Update CTA Database", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.OK Then
      Exit Sub
    End If

    Try
      AppPath = Path.GetDirectoryName(Application.ExecutablePath)
      AppPath = Path.Combine(AppPath, "EurekaDbUpdate_23Apr2008.sql")

      If (File.Exists(AppPath)) Then
        Dim objReader As StreamReader
        Try

          objReader = New StreamReader(AppPath)
          SQLString = objReader.ReadToEnd()
          objReader.Close()
        Catch Ex As Exception
          SQLString = ""
        End Try
      Else
        MessageBox.Show("Update failed. Update script not found : '" & AppPath & "'")
      End If

      Dim SAConnection As SqlConnection = Nothing
      Dim UpdateCommand As New SqlCommand

      If (SQLString.Length = 17375) Then
        Dim ThisCommand As New SqlCommand
        Dim ConnectString As String

        Try
          ConnectString = "SERVER=" & SQLServerName & ";DATABASE=Renaissance;Trusted_Connection=False;User Id=sa;Password=teatray457;Application Name=" & Application.ProductName
          SAConnection = New SqlConnection(ConnectString)
          SAConnection.Open()

          UpdateCommand.CommandType = CommandType.Text
          UpdateCommand.CommandText = ""
          UpdateCommand.Connection = SAConnection
          UpdateCommand.CommandTimeout = 120

          Dim ScriptRows() As String = SQLString.Split(New Char() {Chr(13)})
          Dim SubString As String
          Dim RowCount As Integer

          SubString = ""
          For RowCount = 0 To (ScriptRows.Length - 1)
            If (ScriptRows(RowCount).Trim.ToUpper = "GO") Then
              If (SubString.Length > 0) Then
                UpdateCommand.CommandText = SubString
                UpdateCommand.ExecuteNonQuery()
              End If
              SubString = ""
            Else
              SubString = SubString & ScriptRows(RowCount) & Chr(13)
            End If
          Next

          MessageBox.Show("Update Completed")

        Catch ex As Exception
          MessageBox.Show("Update failed. Error : " & ex.Message)

        Finally
          If (SAConnection IsNot Nothing) Then
            Try
              SAConnection.Close()
            Catch ex As Exception
            Finally
              SAConnection = Nothing
            End Try

            UpdateCommand.Connection = Nothing
            UpdateCommand = Nothing
          End If

        End Try
      End If

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " Standard Format checking handlers "

  Public Sub Text_NotNull(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' **********************************************************************
    ' Standard File Format checking function.
    ' **********************************************************************

    Dim thisControl As System.Windows.Forms.Control
    Dim Parentform As StandardCTAForm
    Dim ParentObject As Object

    'ParentObject = sender.TopLevelControl
    ParentObject = sender
    thisControl = Nothing

    While Not (TypeOf (ParentObject) Is StandardCTAForm)
      Try
        thisControl = CType(ParentObject, Control)
        ParentObject = thisControl.Parent
      Catch ex As Exception
        Exit Sub
      End Try
    End While

    Parentform = CType(ParentObject, StandardCTAForm)

    If (Parentform.IsOverCancelButton = False) And (Parentform.IsInPaint = False) Then
      If thisControl.Text.Length = 0 Then
        MessageBox.Show("This field may not be left empty.", "Field Validation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
        thisControl.Focus()
      End If
    End If
  End Sub





#End Region

#Region " Establish Standard table datasets "

  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    ' Get Dataset, forcing a refresh if Noted IDs have changed.

    Return Load_Table(pDatasetDetails, pDatasetDetails.NotedIDsHaveChanged, True)
  End Function

  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    Return Load_Table(pDatasetDetails, pForceRefresh, True)
  End Function

  Public Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table
    ' **********************************************************************
    ' Routine to Load a standard dataset or retrieve an existing one if it already
    ' exists.
    ' **********************************************************************

    Dim myDataset As DataSet
    Dim myConnection As SqlConnection
    Dim myAdaptor As SqlDataAdapter

    If (pDatasetDetails Is Nothing) Then
      Return Nothing
    End If

    Dim THIS_TABLENAME As String = pDatasetDetails.TableName
    Dim THIS_ADAPTORNAME As String = pDatasetDetails.Adaptorname
    Dim THIS_DATASETNAME As String = pDatasetDetails.DatasetName

    ' StandardDatasetNames

    myConnection = Me.MainDataHandler.Get_Connection(CTA_CONNECTION)
    If myConnection Is Nothing Then
      Return Nothing
    End If

    myAdaptor = Me.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, CTA_CONNECTION, THIS_TABLENAME)
    myDataset = Me.MainDataHandler.Get_Dataset(THIS_DATASETNAME)

    'AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
    'AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
    'AddHandler myAdaptor.FillError, AddressOf OnRowFillError

    If myDataset Is Nothing Then

      myDataset = Me.MainDataHandler.Get_Dataset(THIS_DATASETNAME, True)

      SyncLock myAdaptor.SelectCommand.Connection
        SyncLock myDataset

          Try
            If (myDataset.Tables.Count > 0) Then
              myDataset.Tables(0).Clear()
            End If
          Catch ex As Exception
          End Try
          Try
            If myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate") Then
              myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = Me.Main_Knowledgedate
            End If
          Catch ex As Exception
          End Try

          Try
            Dim OrgText As String
            OrgText = Me.CTAStatusLabel.Text

            If (myDataset.Tables.Count > 0) Then
              Me.CTAStatusLabel.Text = "Loading " & myDataset.Tables(0).TableName
              Me.CTAStatusStrip.Refresh()
              Application.DoEvents()

              myDataset.EnforceConstraints = False
              myAdaptor.Fill(myDataset.Tables(0))
              myDataset.EnforceConstraints = True

            Else
              Me.CTAStatusLabel.Text = "Loading Table " & pDatasetDetails.TableName
              Me.CTAStatusStrip.Refresh()
              Application.DoEvents()

              myDataset.EnforceConstraints = False
              myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
              myDataset.EnforceConstraints = True

            End If

            ' Replace original text, if it has not already been cleared
            If CTAStatusLabel.Text.Length > 0 Then
              CTAStatusLabel.Text = OrgText
            End If

            Me.CTAStatusStrip.Refresh()
          Catch ex As Exception
            If (myDataset.Tables.Count > 0) Then
              CTAStatusLabel.Text = "Error Filling Table " & myDataset.Tables(0).TableName
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table " & myDataset.Tables(0).TableName, ex.StackTrace, True)
            Else
              CTAStatusLabel.Text = "Error Filling Table."
              Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling Table.", ex.StackTrace, True)
            End If
            CTAStatusLabel.Text = ""
          End Try

        End SyncLock
      End SyncLock


      ' Reset the IDs have Changed Flag, as the table has been re-loaded.
      pDatasetDetails.NotedIDsHaveChanged = False

    ElseIf (pForceRefresh = True) Or (pDatasetDetails.NotedIDsHaveChanged = True) Then

      SyncLock myAdaptor.SelectCommand.Connection
        SyncLock myDataset

          Try
            Dim OrgText As String
            OrgText = Me.CTAStatusLabel.Text

            Try
              If (myDataset.Tables.Count > 0) Then
                myDataset.Tables(0).Clear()
              End If
            Catch ex As Exception
            End Try

            Try
              If myAdaptor.SelectCommand.Parameters.Contains("@Knowledgedate") Then
                myAdaptor.SelectCommand.Parameters("@Knowledgedate").Value = Me.Main_Knowledgedate
              End If
            Catch ex As Exception
            End Try

            Me.CTAStatusLabel.Text = "Loading Table " & pDatasetDetails.TableName

            myDataset.EnforceConstraints = False
            If (myDataset.Tables.Count > 0) Then
              myAdaptor.Fill(myDataset.Tables(0))
            Else
              myAdaptor.Fill(myDataset, pDatasetDetails.TableName)
            End If
            myDataset.EnforceConstraints = True

            ' Reset the IDs have Changed Flag, as the table has been re-loaded.
            pDatasetDetails.NotedIDsHaveChanged = False

            If (pRaiseEvent = True) Then
              Call Me.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(pDatasetDetails.ChangeID), False)
            End If

            ' Replace original text, if it has not already been clearted
            If CTAStatusLabel.Text.Length > 0 Then
              CTAStatusLabel.Text = OrgText
            End If

          Catch ex As Exception
            Me.CTAStatusLabel.Text = "Error loading table " & pDatasetDetails.TableName
            Call LogError(Me.Name & ", Load_Table", LOG_LEVELS.Error, ex.Message, "Error Filling (Refresh) Table " & myDataset.Tables(0).TableName, ex.StackTrace, True)
            CTAStatusLabel.Text = ""
          End Try

        End SyncLock
      End SyncLock

      ' Reset the IDs have Changed Flag, as the table has been re-loaded.
      pDatasetDetails.NotedIDsHaveChanged = False
    End If

    Return myDataset
  End Function

  Public Function ReloadTable(ByVal pTableID As RenaissanceChangeID, ByVal pRaiseEvent As Boolean) As Boolean
    ' **********************************************************************
    ' Function to Re-Load a standard dataset if it is already loaded.
    '
    ' If the dataset is not already loaded, then the call to 'Get_Dataset' will return
    ' Nothing since insufficient of the constructor parameters are given.
    ' If a dataset IS returned, then the Load_Dataset funcion is used to refresh it.
    ' **********************************************************************
    Dim ThisStandardDataset As RenaissanceGlobals.StandardDataset = RenaissanceStandardDatasets.GetStandardDataset(pTableID)
    Dim myDataset As DataSet

    Try
      If Not (ThisStandardDataset Is Nothing) Then
        myDataset = Me.MainDataHandler.Get_Dataset(ThisStandardDataset.DatasetName)
        If (Not (myDataset Is Nothing)) Then
          If (Load_Table(ThisStandardDataset, True, pRaiseEvent) Is Nothing) Then
            Return False
          Else
            Return True
          End If
        End If
      End If
    Catch ex As Exception
    End Try

    Return False
  End Function


#End Region

#Region " Standard 'Set' Functions : Combos, Tooltips etc. "

  Public Sub SetTblGenericCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pDataRows As DataRow(), ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")

    SetTblGenericCombo(pCombo, pDataRows, pDisplayMember, pValueMember, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, New Object() {pLeadingBlankValue}, New String() {pLeadingBlankText})

  End Sub

  Public Sub SetTblGenericCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pDataRows As DataRow(), ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, ByVal pSELECTDISTINCT As Boolean, ByVal pOrderAscending As Boolean, ByVal pLeadingBlank As Boolean, ByVal pLeadingBlankValue() As Object, ByVal pLeadingBlankText() As String)
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Renaissance 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows() array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim lastRow As DataRow
    Dim newRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""
    Dim CurrentValueIsIndex As Boolean

    Dim DisplayFieldNames As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = Me.CTAStatusLabel.Text

      If (pDataRows.Length > 0) Then
        Me.CTAStatusLabel.Text = "Building " & pDataRows(0).Table.TableName & " Combo"
      Else
        Me.CTAStatusLabel.Text = "Building Combo"
      End If

      Me.CTAStatusStrip.Refresh()
      Application.DoEvents()

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        CurrentValueIsIndex = False
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Get Source Dataset
      ' pDataRows

      If (pDataRows Is Nothing) Then
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Resolve Display Field names

      DisplayFieldNames = pDisplayMember.Split(New Char() {CChar(","), CChar("|")})
      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {CChar(" ")})

          If (thisSortedRows.Length > 0) Then
            If thisSortedRows(0).Table.Columns.Contains(DisplayFieldNames(DCount)) = False Then
              DisplayFieldNames(DCount) = ""
            End If
          End If

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If
          End If
        Next
      Catch ex As Exception
      End Try


      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""

      ' Build new Source table
      ' Linking directly to the source had odd side affects.
      Dim newTable As New DataTable

      newTable.Clear()

      Try
        newTable.Columns.Add(New DataColumn("DM", GetType(System.String)))
        If (thisSortedRows.Length <= 0) Then
          newTable.Columns.Add(New DataColumn("VM", pLeadingBlankValue.GetType))
        Else
          If (thisSortedRows(0).Table.Columns(pValueMember) IsNot Nothing) Then
            newTable.Columns.Add(New DataColumn("VM", thisSortedRows(0).Table.Columns(pValueMember).DataType))
          Else
            newTable.Columns.Add(New DataColumn("VM", GetType(Integer)))
          End If
        End If
      Catch ex As Exception
        Me.CTAStatusLabel.Text = OrgText
        Exit Sub
      End Try

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""
        If (DisplayFieldNames(DCount).Length > 0) Then
          If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try

          If (pLeadingBlankValue IsNot Nothing) AndAlso (pLeadingBlankValue.Length > 0) Then

            For BlankValueIndex As Integer = 0 To (pLeadingBlankValue.Length - 1)

              newRow = newTable.NewRow

              If (pLeadingBlankText IsNot Nothing) AndAlso (pLeadingBlankText.Length > BlankValueIndex) Then
                newRow("DM") = pLeadingBlankText(BlankValueIndex)
              Else
                newRow("DM") = ""
              End If

              If (TypeOf pLeadingBlankValue(BlankValueIndex) Is String) AndAlso (pLeadingBlankValue(BlankValueIndex).ToString = "<DBNull>") Then
                newRow("VM") = DBNull.Value
              Else
                newRow("VM") = pLeadingBlankValue(BlankValueIndex)
              End If

              newTable.Rows.Add(newRow)

            Next


          End If
        Catch ex As Exception
        End Try
      End If

      lastRow = Nothing
      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String

        If thisSortedRows(0).Table.Columns.Contains(pValueMember) Then
          For Each thisRow In thisSortedRows

            If (thisRow.RowState And DataRowState.Detached) <> DataRowState.Detached Then

              If (pSELECTDISTINCT) Then
                If (Not (lastRow Is Nothing)) AndAlso (Not (thisRow(pValueMember).ToString = lastRow(pValueMember).ToString)) Then
                  lastRow = Nothing
                End If
              End If

              If (lastRow Is Nothing) Then
                newRow = newTable.NewRow

                Try
                  DisplayString = ""
                  For DCount = 0 To (DisplayFieldNames.Length - 1)
                    If (DisplayFieldNames(DCount).Length > 0) Then
                      If DisplayString.Length > 0 Then
                        DisplayString &= ", "
                      End If

                      If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                        DisplayString &= "<Null>"
                      Else
                        DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                      End If
                    End If
                  Next DCount

                  newRow("DM") = DisplayString

                  If thisRow.IsNull(pValueMember) Then
                    newRow("VM") = 0
                  Else
                    newRow("VM") = thisRow(pValueMember)
                  End If

                  newTable.Rows.Add(newRow)

                Catch ex As Exception
                End Try
              End If

              If pSELECTDISTINCT Then
                lastRow = thisRow
              End If

            End If ' Not Detached

          Next ' Row
        End If
      End If

      ' Link to new table
      pCombo.DisplayMember = "DM"
      pCombo.ValueMember = "VM"
      pCombo.DataSource = newTable

      ' Re-Size
      SetComboDropDownWidth(pCombo)

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            If (currentSelectedValue.ToString.Length > 0) Then
              If (pCombo.SelectedValue IsNot Nothing) AndAlso (pCombo.SelectedValue.GetType Is currentSelectedValue.GetType) Then
                pCombo.SelectedValue = currentSelectedValue
              End If
            End If
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Setting Std Combo", ex.StackTrace, True)
    End Try

    Me.CTAStatusLabel.Text = OrgText

  End Sub

  Public Sub SetTblGenericCombo(ByVal pCombo As Telerik.WinControls.UI.RadComboBox, ByVal pDataRows As DataRow(), ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Venice 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""
    Dim CurrentValueIsIndex As Boolean

    Dim DisplayFieldNames As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = Me.CTAStatusLabel.Text
      If (pDataRows.Length > 0) Then
        SetToolStripText(CTAStatusLabel, "Building " & pDataRows(0).Table.TableName & " Combo")
      Else
        SetToolStripText(CTAStatusLabel, "Building Combo")
      End If
      If Not (Me.InvokeRequired) Then
        Me.CTAStatusStrip.Refresh()
        Application.DoEvents()
      End If

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        CurrentValueIsIndex = False
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Resolve Display Field names

      DisplayFieldNames = pDisplayMember.Split(New Char() {CChar(","), CChar("|")})
      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {CChar(" ")})

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If
          End If
        Next
      Catch ex As Exception
      End Try

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""
      pCombo.Items.Clear()

      ' Get Source Dataset
      ' pDataRows
      If (pDataRows Is Nothing) Then
        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Build new Source 
      ' Linking directly to the source had odd side affects.

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""
        If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
          If (DisplayFieldNames(DCount).Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try
          If (TypeOf pLeadingBlankValue Is String) AndAlso (CType(pLeadingBlankValue, String) = "<DBNull>") Then
            pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, DBNull.Value))
          Else
            pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, pLeadingBlankValue))
          End If

        Catch ex As Exception
        End Try
      End If

      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String
        Dim LastValueMember As Object = Nothing

        Try
          pCombo.BeginUpdate()

          For Each thisRow In thisSortedRows
            If (pSELECTDISTINCT) Then
              If (Not (LastValueMember IsNot Nothing)) AndAlso (CompareValue(CType(thisRow(pValueMember), Global.System.IComparable), CType(LastValueMember, Global.System.IComparable)) <> 0) Then
                LastValueMember = Nothing
              End If
            End If

            If (LastValueMember Is Nothing) Then

              Try
                DisplayString = ""
                For DCount = 0 To (DisplayFieldNames.Length - 1)
                  If (DisplayFieldNames(DCount).Length > 0) Then
                    If DisplayString.Length > 0 Then
                      DisplayString &= ", "
                    End If

                    If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                      DisplayString &= "<Null>"
                    Else
                      DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                    End If
                  End If
                Next DCount

                If thisRow.IsNull(pValueMember) Then
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, 0))
                  If pSELECTDISTINCT Then
                    LastValueMember = 0
                  End If
                Else
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, thisRow(pValueMember)))
                  If pSELECTDISTINCT Then
                    LastValueMember = thisRow(pValueMember)
                  End If
                End If

              Catch ex As Exception
              End Try
            End If
          Next

        Catch ex As Exception
        Finally
          pCombo.EndUpdate()
        End Try
      End If

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            pCombo.SelectedValue = currentSelectedValue
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If


    Catch ex As Exception
    Finally

      SetToolStripText(CTAStatusLabel, OrgText)

    End Try

  End Sub

  Public Sub SetTblGenericCombo(ByVal pCombo As Object, ByVal pDataRowViews As DataRowView(), ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' Wrapper for the  SetTblGenericCombo() Routine to take a DataRowView Array instead of
    ' a DataRow Array.
    ' ************************************************************************************

    If (pCombo IsNot Nothing) AndAlso (pDataRowViews.Length > 0) Then
      Dim RowArray(pDataRowViews.Length - 1) As DataRow
      Dim RowCount As Integer

      For RowCount = 0 To (pDataRowViews.Length - 1)
        RowArray(RowCount) = pDataRowViews(RowCount).Row
      Next

      If (TypeOf pCombo Is ComboBox) Then
        SetTblGenericCombo(CType(pCombo, ComboBox), RowArray, pDisplayMember, pValueMember, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)
      ElseIf (TypeOf pCombo Is Telerik.WinControls.UI.RadComboBox) Then
        SetTblGenericCombo(CType(pCombo, Telerik.WinControls.UI.RadComboBox), RowArray, pDisplayMember, pValueMember, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)
      End If

    End If

  End Sub

  Public Sub SetTblGenericCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pTableID As RenaissanceGlobals.StandardDataset, ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' Wrapper for the  SetTblGenericCombo() Routine.
    ' ************************************************************************************

    Dim thisDataset As DataSet
    Dim thisSortedRows() As DataRow
    Dim SelectString As String
    Dim OrgText As String = ""

    Dim DisplayFieldNames As String()
    Dim DCount As Integer

    Try
      ' SetMasterNameCombo / LocationsCombo

      If (pTableID.ChangeID = RenaissanceGlobals.RenaissanceStandardDatasets.Mastername.ChangeID) Then
        If ((pSelectString.Length = 0) OrElse (pSelectString.ToUpper = "TRUE")) AndAlso (pValueMember = "ID") Then
          If (_MasternameDictionary Is Nothing) Then
            _MasternameDictionary = New LookupCollection(Of Integer, String)
          End If

          Call SetMasterNameCombo(pCombo, pSelectString, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)
          Exit Sub
        Else
          ' Non Dictionary Mastername Combo.

          Dim DebugPoint As Boolean

          DebugPoint = True
        End If
      End If

      If (pTableID.ChangeID = RenaissanceGlobals.RenaissanceStandardDatasets.tblLocations.ChangeID) AndAlso ((pSelectString.Length = 0) OrElse (pSelectString.ToUpper = "TRUE")) AndAlso (pValueMember = "CityID") Then
        If (_LocationsDictionary Is Nothing) Then
          ' This prevents the Locations collection being maintained unless a Locations Combo is actually requested.

          _LocationsDictionary = New LookupCollection(Of Integer, String)
        End If

        Call SetLocationsCombo(pCombo)
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pTableID Is Nothing) Then Exit Sub

      OrgText = Me.CTAStatusLabel.Text
      Me.CTAStatusLabel.Text = "Building " & pTableID.TableName & " Combo"
      Me.CTAStatusStrip.Refresh()
      Application.DoEvents()


      ' Resolve Display Field names
      DisplayFieldNames = pDisplayMember.Split(New Char() {CChar(","), CChar("|")})
      ' ReDim FormatStrings(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {CChar(" ")})

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If
          End If
        Next
      Catch ex As Exception
      End Try

      ' Get Source Dataset
      thisDataset = Me.Load_Table(pTableID, False)
      If Not (thisDataset Is Nothing) Then
        If pSelectString Is Nothing Then
          SelectString = "True"
        ElseIf pSelectString.Length <= 0 Then
          SelectString = "True"
        Else
          SelectString = pSelectString
        End If

        Try
          thisSortedRows = thisDataset.Tables(0).Select(SelectString, SelectSortString)
        Catch ex As Exception
          thisSortedRows = Nothing
        End Try
      Else
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      Call SetTblGenericCombo(pCombo, thisSortedRows, pDisplayMember, pValueMember, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)

    Catch ex As Exception
    Finally
      If (CTAStatusLabel.Text.Length > 0) Then
        CTAStatusLabel.Text = OrgText
      End If
    End Try

  End Sub

  Public Sub SetTblGenericCombo(ByVal pCombo As Telerik.WinControls.UI.RadComboBox, ByVal pTableID As RenaissanceGlobals.StandardDataset, ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    ' Wrapper for the  SetTblGenericCombo() Routine.
    ' ************************************************************************************

    Dim thisDataset As DataSet
    Dim thisSortedRows() As DataRow
    Dim SelectString As String
    Dim OrgText As String

    Dim DisplayFieldNames As String()
    Dim DCount As Integer
    Dim ReferentialDataset As RenaissanceDataClass.DSReferentialIntegrity = Nothing
    Dim ReferentialTable As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityDataTable = Nothing
    Dim IntegrityChecks() As RenaissanceDataClass.DSReferentialIntegrity.tblReferentialIntegrityRow
    Dim DisplayFieldNames_DependencyTable As RenaissanceChangeID()
    Dim DisplayFieldNames_DependencyField As String()

    Try
      If (pCombo Is Nothing) Then Exit Sub
      If (pTableID Is Nothing) Then Exit Sub

      OrgText = CTAStatusLabel.Text
      Me.CTAStatusLabel.Text = "Building " & pTableID.TableName & " Combo"
      Me.CTAStatusStrip.Refresh()
      Application.DoEvents()

      ' Resolve Display Field names
      DisplayFieldNames = pDisplayMember.Split(New Char() {CChar(","), CChar("|")})
      ' ReDim FormatStrings(DisplayFieldNames.Length - 1)
      ReDim DisplayFieldNames_DependencyTable(DisplayFieldNames.Length - 1)
      ReDim DisplayFieldNames_DependencyField(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {CChar(" ")})
          DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None
          DisplayFieldNames_DependencyField(DCount) = ""

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If

            ' Check Referential Dependency
            If (ReferentialTable Is Nothing) Then
              ReferentialDataset = CType(Load_Table(RenaissanceStandardDatasets.tblReferentialIntegrity, False), DSReferentialIntegrity)
              ReferentialTable = ReferentialDataset.tblReferentialIntegrity
            End If

            Try
              IntegrityChecks = CType(ReferentialTable.Select("(FeedsTable = '" & pTableID.TableName & "') AND (FeedsField = '" & DisplayFieldNames(DCount) & "')"), DSReferentialIntegrity.tblReferentialIntegrityRow())
              If (IntegrityChecks IsNot Nothing) AndAlso (IntegrityChecks.Length > 0) Then

                DisplayFieldNames_DependencyTable(DCount) = CType(System.Enum.Parse(GetType(RenaissanceChangeID), IntegrityChecks(0).TableName), RenaissanceChangeID)
                DisplayFieldNames_DependencyField(DCount) = IntegrityChecks(0).DescriptionField
              End If
            Catch ex As Exception
              DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None
              DisplayFieldNames_DependencyField(DCount) = ""
            End Try

          End If
        Next
      Catch ex As Exception
      End Try

      ' Get Source Dataset
      thisDataset = Me.Load_Table(pTableID, False)
      If Not (thisDataset Is Nothing) Then
        If pSelectString Is Nothing Then
          SelectString = "True"
        ElseIf pSelectString.Length <= 0 Then
          SelectString = "True"
        Else
          SelectString = pSelectString
        End If

        Try
          thisSortedRows = thisDataset.Tables(0).Select(SelectString, SelectSortString)
        Catch ex As Exception
          thisSortedRows = Nothing
        End Try
      Else
        Try
          ' Clear Existing Binding
          pCombo.DataSource = Nothing
          pCombo.DisplayMember = ""
          pCombo.ValueMember = ""
          pCombo.Items.Clear()
        Catch ex As Exception
        End Try

        Exit Sub
      End If

      Call SetTblGenericCombo(pCombo, thisSortedRows, DisplayFieldNames, pValueMember, DisplayFieldNames_DependencyTable, DisplayFieldNames_DependencyField, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)

    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetTblGenericCombo(ByVal pCombo As Telerik.WinControls.UI.RadComboBox, ByVal pDataRows As DataRow(), ByVal DisplayFieldNames As String(), ByVal pValueMember As String, ByVal DisplayFieldNames_DependencyTable As RenaissanceChangeID(), ByVal DisplayFieldNames_DependencyField As String(), ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")

    SetTblGenericCombo(pCombo, pDataRows, DisplayFieldNames, pValueMember, DisplayFieldNames_DependencyTable, DisplayFieldNames_DependencyField, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, New Object() {pLeadingBlankValue}, New String() {pLeadingBlankText})

  End Sub

  Private Sub SetTblGenericCombo(ByVal pCombo As Telerik.WinControls.UI.RadComboBox, ByVal pDataRows As DataRow(), ByVal DisplayFieldNames As String(), ByVal pValueMember As String, ByVal DisplayFieldNames_DependencyTable As RenaissanceChangeID(), ByVal DisplayFieldNames_DependencyField As String(), ByVal pSelectString As String, ByVal pSELECTDISTINCT As Boolean, ByVal pOrderAscending As Boolean, ByVal pLeadingBlank As Boolean, ByVal pLeadingBlankValue() As Object, ByVal pLeadingBlankText() As String)
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Renaissance 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows() array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""
    Dim CurrentValueIsIndex As Boolean

    'Dim DisplayFieldNames_DependencyTable As RenaissanceChangeID()
    'Dim DisplayFieldNames_DependencyField As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Try
      If (pCombo Is Nothing) OrElse (pCombo.IsDisposed) Then Exit Sub
      If (pDataRows Is Nothing) Then Exit Sub

      OrgText = CTAStatusLabel.Text

      If (pDataRows.Length > 0) Then
        Me.CTAStatusLabel.Text = "Building " & pDataRows(0).Table.TableName & " Combo"
      Else
        Me.CTAStatusLabel.Text = "Building Combo"
      End If

      Me.CTAStatusStrip.Refresh()
      Application.DoEvents()

      ' Preserve Existing Combo Value

      CurrentValueIsIndex = True
      If pCombo.DropDownStyle = ComboBoxStyle.DropDownList Then
        If pCombo.SelectedIndex >= 0 Then
          currentSelectedValue = pCombo.SelectedValue
        ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
          currentSelectedValue = pCombo.Text
        Else
          currentSelectedValue = Nothing
        End If
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        If pCombo.SelectedIndex >= 0 Then
          currentSelectedValue = pCombo.SelectedValue
        Else
          CurrentValueIsIndex = False
          currentSelectedValue = pCombo.SelectedText
        End If
      End If

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      ElseIf pCombo.DropDownStyle = ComboBoxStyle.DropDown Then
        currentSelectedValue = pCombo.Text
      Else
        currentSelectedValue = Nothing
      End If

      ' Resolve Display Field names

      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""
      pCombo.Items.Clear()

      ' Get Source Dataset
      ' pDataRows
      If (pDataRows Is Nothing) Then
        Exit Sub
      End If

      thisSortedRows = pDataRows

      ' Build new Source 
      ' Linking directly to the source had odd side affects.

      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""

        If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
          If (DisplayFieldNames(DCount).Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank) Then
        Try
          If (pLeadingBlankValue IsNot Nothing) AndAlso (pLeadingBlankValue.Length > 0) Then

            For BlankValueIndex As Integer = 0 To (pLeadingBlankValue.Length - 1)

              If (pLeadingBlankText IsNot Nothing) AndAlso (pLeadingBlankText.Length > BlankValueIndex) Then
                ' Text Exists

                If (TypeOf pLeadingBlankText(BlankValueIndex) Is String) AndAlso (CType(pLeadingBlankText(BlankValueIndex), String) = "<DBNull>") Then
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText(BlankValueIndex), DBNull.Value))
                Else
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText(BlankValueIndex).ToString, pLeadingBlankValue(BlankValueIndex)))
                End If

              Else
                ' Text Not Exist, Just Value

                pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem("", pLeadingBlankValue(BlankValueIndex)))

              End If

              'If (TypeOf pLeadingBlankValue Is String) AndAlso (CType(pLeadingBlankValue, String) = "<DBNull>") Then
              '	pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, DBNull.Value))
              'Else
              '	pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(pLeadingBlankText, pLeadingBlankValue))
              'End If

            Next

          End If


        Catch ex As Exception
        End Try
      End If

      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String
        Dim LastValueMember As Object = Nothing

        Try
          pCombo.BeginUpdate()

          For Each thisRow In thisSortedRows
            If (pSELECTDISTINCT) Then
              If (Not (LastValueMember IsNot Nothing)) AndAlso (CompareValue(CType(thisRow(pValueMember), Global.System.IComparable), CType(LastValueMember, Global.System.IComparable)) <> 0) Then
                LastValueMember = Nothing
              End If
            End If

            If (LastValueMember Is Nothing) Then

              Try
                DisplayString = ""
                For DCount = 0 To (DisplayFieldNames.Length - 1)
                  If (DisplayFieldNames(DCount).Length > 0) Then
                    If DisplayString.Length > 0 Then
                      DisplayString &= ", "
                    End If

                    If (DisplayFieldNames_DependencyTable(DCount) = RenaissanceChangeID.None) Then
                      If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                        DisplayString &= "<Null>"
                      Else
                        DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                      End If
                    Else
                      If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                        DisplayString &= "<Null>"
                      Else
                        Try
                          DisplayString &= LookupTableValue(Me, RenaissanceGlobals.RenaissanceStandardDatasets.GetStandardDataset(DisplayFieldNames_DependencyTable(DCount)), CInt(thisRow(DisplayFieldNames(DCount))), DisplayFieldNames_DependencyField(DCount)).ToString
                        Catch ex As Exception
                        End Try
                      End If
                    End If
                  End If
                Next DCount

                If thisRow.IsNull(pValueMember) Then
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, 0))
                  If pSELECTDISTINCT Then
                    LastValueMember = 0
                  End If
                Else
                  pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(DisplayString, thisRow(pValueMember)))
                  If pSELECTDISTINCT Then
                    LastValueMember = thisRow(pValueMember)
                  End If
                End If

              Catch ex As Exception
              End Try
            End If

          Next

        Catch ex As Exception
        Finally
          pCombo.EndUpdate()
        End Try

      End If

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          If CurrentValueIsIndex Then
            pCombo.SelectedValue = currentSelectedValue
          Else
            pCombo.SelectedText = currentSelectedValue.ToString
            pCombo.Text = currentSelectedValue.ToString
          End If
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    End Try

    Me.CTAStatusLabel.Text = OrgText

  End Sub

  Private Sub Refresh_MasternameDictionary()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    If (_MasternameDictionary Is Nothing) Then Exit Sub

    Try

      Dim MasternameDataView As DataView = PertracData.GetPertracInstruments  ' DSMastername.tblMasternameDataTable = CType(Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.Mastername).Tables(0), DSMastername.tblMasternameDataTable)
      Dim ExistingKeys() As Integer
      Dim KeyCounter As Integer = 0
      Dim TableCounter As Integer = 0
      Dim ValueOrdinal As Integer = MasternameDataView.Table.Columns("PertracCode").Ordinal
      Dim DisplayOrdinal As Integer = MasternameDataView.Table.Columns("ListDescription").Ordinal

      ' Add Default Row to the Dictionary
      If (_MasternameDictionary.Count = 0) Then
        _MasternameDictionary.Add(0, "")
      End If

      MasternameDataView.Sort = "PertracCode"

      ExistingKeys = CType(Me._MasternameDictionary.KeyArray, Integer())
      Array.Sort(ExistingKeys)

      While (KeyCounter < ExistingKeys.Length) OrElse (TableCounter < MasternameDataView.Count)
        If (KeyCounter < ExistingKeys.Length) AndAlso (TableCounter < MasternameDataView.Count) Then
          Select Case (ExistingKeys(KeyCounter)).CompareTo(MasternameDataView(TableCounter)(ValueOrdinal))
            Case Is > 0
              ' Add Entry to Dictionary

              _MasternameDictionary.Add(CInt(MasternameDataView(TableCounter)(ValueOrdinal)), MasternameDataView(TableCounter)(DisplayOrdinal).ToString)
              TableCounter += 1

            Case 0
              ' Keys are the Same

              If ExistingKeys(KeyCounter) > 0 Then
                If CStr(_MasternameDictionary(ExistingKeys(KeyCounter))) <> (MasternameDataView(TableCounter)(DisplayOrdinal).ToString) Then
                  _MasternameDictionary(ExistingKeys(KeyCounter)) = (MasternameDataView(TableCounter)(DisplayOrdinal).ToString)
                End If
              End If

              TableCounter += 1
              KeyCounter += 1

            Case Is < 0
              ' Delete Dictionary Entry

              If ExistingKeys(KeyCounter) > 0 Then
                _MasternameDictionary.Remove(ExistingKeys(KeyCounter))
              End If

              KeyCounter += 1

          End Select

        ElseIf (KeyCounter < ExistingKeys.Length) Then
          ' Delete Dictionary Entry

          _MasternameDictionary.Remove(ExistingKeys(KeyCounter))
          KeyCounter += 1

        ElseIf (TableCounter < MasternameDataView.Count) Then
          ' Add Entry to Dictionary

          _MasternameDictionary.Add(CInt(MasternameDataView(TableCounter)(ValueOrdinal)), MasternameDataView(TableCounter)(DisplayOrdinal).ToString)
          TableCounter += 1

        Else
          Exit While
        End If
      End While

    Catch ex As Exception
    Finally
      _MasternameDictionary.Sort()
    End Try

  End Sub

  Private Sub Refresh_LocationsDictionary()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    If (_LocationsDictionary Is Nothing) Then Exit Sub

    Try

      Dim tblLocations As DSLocations.tblLocationsDataTable = CType(Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblLocations).Tables(0), DSLocations.tblLocationsDataTable)
      Dim SelectedRows() As DSLocations.tblLocationsRow
      Dim ExistingKeys() As Integer
      Dim KeyCounter As Integer = 0
      Dim TableCounter As Integer = 0
      Dim ValueString As String

      ' Add Default Row to the Dictionary
      If (_LocationsDictionary.Count = 0) Then
        _LocationsDictionary.Add(0, "")
      End If

      SelectedRows = CType(tblLocations.Select("True", "CityID"), DSLocations.tblLocationsRow())
      ExistingKeys = CType(Me._LocationsDictionary.KeyArray, Integer())
      Array.Sort(ExistingKeys)

      While (KeyCounter < ExistingKeys.Length) OrElse (TableCounter < SelectedRows.Length)
        If (KeyCounter < ExistingKeys.Length) AndAlso (TableCounter < SelectedRows.Length) Then
          Select Case (ExistingKeys(KeyCounter)).CompareTo(SelectedRows(TableCounter).CityID)
            Case Is > 0
              ' Add Entry to Dictionary
              ValueString = SelectedRows(TableCounter).City & ", " & SelectedRows(TableCounter).RegionName

              _LocationsDictionary.Add(SelectedRows(TableCounter).CityID, ValueString)
              TableCounter += 1

            Case 0
              ' Keys are the Same
              ValueString = SelectedRows(TableCounter).City & ", " & SelectedRows(TableCounter).RegionName

              If CStr(_LocationsDictionary(ExistingKeys(KeyCounter))) <> ValueString Then
                _LocationsDictionary(ExistingKeys(KeyCounter)) = ValueString
              End If

              TableCounter += 1
              KeyCounter += 1

            Case Is < 0
              ' Delete Dictionary Entry

              If ExistingKeys(KeyCounter) > 0 Then
                _LocationsDictionary.Remove(ExistingKeys(KeyCounter))
              End If

              KeyCounter += 1

          End Select

        ElseIf (KeyCounter < ExistingKeys.Length) Then
          ' Delete Dictionary Entry

          _LocationsDictionary.Remove(ExistingKeys(KeyCounter))
          KeyCounter += 1

        ElseIf (TableCounter < SelectedRows.Length) Then
          ' Add Entry to Dictionary
          ValueString = SelectedRows(TableCounter).City & ", " & SelectedRows(TableCounter).RegionName

          _LocationsDictionary.Add(SelectedRows(TableCounter).CityID, ValueString)
          TableCounter += 1

        Else
          Exit While
        End If
      End While

    Catch ex As Exception
    Finally
      _LocationsDictionary.Sort()
    End Try

  End Sub

  Public Sub SetTblGenericCombo(ByRef pCombo As System.Windows.Forms.ComboBox, ByRef pEnumeration As Type, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankName As String = "", Optional ByVal pLeadingBlankValue As Object = "<DBNull>")
    ' ************************************************************************************
    ' Wrapper for the  SetTblGenericCombo() Routine. Accepts an Enumeration Type.
    ' ************************************************************************************

    Dim newRow As DataRow
    Dim currentSelectedValue As Object
    Dim OrgText As String = ""

    Dim EnumNames As String()
    Dim EnumCounter As Integer
    Dim CharCounter As Integer
    Dim thischar As Char

    Dim ThisName As String
    Dim NewNameBuilder As System.Text.StringBuilder

    Try

      OrgText = Me.CTAStatusLabel.Text
      Me.CTAStatusLabel.Text = "Building " & pEnumeration.Name & " Combo"
      Me.CTAStatusStrip.Refresh()
      Application.DoEvents()

      ' Preserve Existing Combo Value

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      Else
        currentSelectedValue = Nothing
      End If

      EnumNames = System.Enum.GetNames(pEnumeration)

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""

      ' Build new Source table
      ' Linking directly to the source had odd side affects.
      Dim newTable As New DataTable

      newTable.Clear()

      Try
        newTable.Columns.Add(New DataColumn("DM", GetType(System.String)))
        newTable.Columns.Add(New DataColumn("VM", GetType(System.Object)))
      Catch ex As Exception
        Exit Sub
      End Try

      If (pLeadingBlank = True) Then
        Try
          newRow = newTable.NewRow

          newRow("DM") = pLeadingBlankName
          If (TypeOf pLeadingBlankValue Is String) AndAlso (pLeadingBlankValue.ToString = "<DBNull>") Then
            newRow("VM") = DBNull.Value
          Else
            newRow("VM") = pLeadingBlankValue
          End If

          newTable.Rows.Add(newRow)
        Catch ex As Exception
        End Try
      End If

      If EnumNames.Length > 0 Then
        For EnumCounter = 0 To (EnumNames.Length - 1)
          newRow = newTable.NewRow

          NewNameBuilder = New System.Text.StringBuilder
          ThisName = EnumNames(EnumCounter)
          For CharCounter = 0 To (ThisName.Length - 1)
            thischar = CChar(ThisName.Substring(CharCounter, 1))
            If (CharCounter > 0) AndAlso (Char.IsUpper(thischar)) Then
              NewNameBuilder.Append(" ")
            End If
            NewNameBuilder.Append(thischar)
          Next

          newRow("DM") = NewNameBuilder.ToString
          newRow("VM") = CInt(System.Enum.Parse(pEnumeration, EnumNames(EnumCounter)))

          newTable.Rows.Add(newRow)
        Next

      End If

      ' Link to new table
      Try
        pCombo.DisplayMember = "DM"
        pCombo.ValueMember = "VM"
      Catch ex As Exception
      End Try
      pCombo.DataSource = newTable
      Try
        pCombo.DisplayMember = "DM"
        pCombo.ValueMember = "VM"
      Catch ex As Exception
      End Try

      ' Re-Size
      SetComboDropDownWidth(pCombo)

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          pCombo.SelectedValue = currentSelectedValue
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    End Try

    Me.CTAStatusLabel.Text = OrgText

  End Sub

  Public Sub SetTblGenericCombo(ByRef pCombo As Telerik.WinControls.UI.RadComboBox, ByRef pEnumeration As Type, Optional ByVal pLeadingBlank As Boolean = False)

    Dim currentSelectedValue As Object
    Dim OrgText As String = ""

    Dim EnumNames As String()
    Dim EnumCounter As Integer
    Dim CharCounter As Integer
    Dim thischar As Char

    Dim ThisName As String
    Dim NewNameBuilder As System.Text.StringBuilder

    Try

      OrgText = Me.CTAStatusLabel.Text
      SetToolStripText(CTAStatusLabel, "Building " & pEnumeration.Name & " Combo")
      If Not (Me.InvokeRequired) Then
        Me.CTAStatusStrip.Refresh()
        Application.DoEvents()
      End If

      ' Preserve Existing Combo Value

      If pCombo.SelectedIndex >= 0 Then
        currentSelectedValue = pCombo.SelectedValue
      Else
        currentSelectedValue = Nothing
      End If

      EnumNames = System.Enum.GetNames(pEnumeration)

      ' Clear Existing Binding
      pCombo.DataSource = Nothing
      pCombo.DisplayMember = ""
      pCombo.ValueMember = ""
      pCombo.Items.Clear()

      ' Build new Source table
      ' Linking directly to the source had odd side affects.

      If (pLeadingBlank = True) Then
        Try
          pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem("", DBNull.Value))

        Catch ex As Exception
        End Try
      End If

      If EnumNames.Length > 0 Then
        For EnumCounter = 0 To (EnumNames.Length - 1)

          NewNameBuilder = New System.Text.StringBuilder
          ThisName = EnumNames(EnumCounter)
          For CharCounter = 0 To (ThisName.Length - 1)
            thischar = CChar(ThisName.Substring(CharCounter, 1))
            If (CharCounter > 0) AndAlso (Char.IsUpper(thischar)) Then
              NewNameBuilder.Append(" ")
            End If
            NewNameBuilder.Append(thischar)
          Next

          pCombo.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(NewNameBuilder.ToString, CInt(System.Enum.Parse(pEnumeration, EnumNames(EnumCounter)))))
        Next

      End If

      ' Re-set existing value
      If Not (currentSelectedValue Is Nothing) Then
        Try
          pCombo.SelectedValue = currentSelectedValue
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    Finally

      SetToolStripText(CTAStatusLabel, OrgText)

    End Try

  End Sub

  Public Sub SetMasterNameCombo(ByVal pCombo As System.Windows.Forms.ComboBox, ByVal pSelectString As String, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "")
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Dim currentSelectedValue As Object = Nothing
    Dim IsCurrentValue As Boolean = False
    Dim currentText As String = ""

    Dim OrgText As String = ""

    Try
      If (pCombo Is Nothing) Then Exit Sub

      OrgText = CTAStatusLabel.Text
      Me.CTAStatusLabel.Text = "Building Mastername Combo"

      Try
        If (pCombo.SelectedIndex >= 0) Then
          currentSelectedValue = pCombo.SelectedValue
          IsCurrentValue = True
        ElseIf (pCombo.Text.Length > 0) Then
          currentText = pCombo.Text
        End If
      Catch ex As Exception
        currentSelectedValue = Nothing
      End Try

      Dim bs As BindingSource

      ' Clear existing BS ?

      If (pCombo.DataSource IsNot Nothing) Then
        If (GetType(BindingSource).IsInstanceOfType(pCombo.DataSource)) Then
          Try
            bs = CType(pCombo.DataSource, BindingSource)
            bs.Sort = ""
            bs.DataSource = Nothing
            bs.Clear()
            bs = Nothing

            pCombo.DisplayMember = ""
            pCombo.ValueMember = ""
            pCombo.DataSource = Nothing

          Catch ex As Exception
          End Try
        Else
          Try
            pCombo.DisplayMember = ""
            pCombo.ValueMember = ""
            pCombo.DataSource = Nothing
          Catch ex As Exception
          End Try
        End If
      End If


      ' Build Combo Bs.

      bs = New BindingSource

      bs.DataSource = _MasternameDictionary
      pCombo.DisplayMember = "Value"
      pCombo.ValueMember = "Key"
      pCombo.DataSource = bs

      If (IsCurrentValue) AndAlso (currentSelectedValue IsNot Nothing) Then
        pCombo.SelectedValue = currentSelectedValue
      ElseIf (currentText.Length > 0) Then
        pCombo.Text = currentText
      ElseIf (pCombo.Items.Count > 0) Then
        pCombo.SelectedIndex = 0
      End If

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Setting Mastername Combo", ex.StackTrace, True)
    Finally
      Me.CTAStatusLabel.Text = OrgText
    End Try


  End Sub

  Public Sub SetLocationsCombo(ByVal pCombo As System.Windows.Forms.ComboBox, Optional ByVal pLeadingBlank As Boolean = False)
    ' ************************************************************************************
    '
    ' ************************************************************************************

    Dim currentSelectedValue As Object = Nothing
    Dim IsCurrentValue As Boolean = False
    Dim currentText As String = ""

    Dim OrgText As String = ""

    Try
      If (pCombo Is Nothing) Then Exit Sub

      OrgText = Me.CTAStatusLabel.Text
      Me.CTAStatusLabel.Text = "Building Locations Combo"

      If (pCombo.SelectedIndex >= 0) Then
        currentSelectedValue = pCombo.SelectedValue
        IsCurrentValue = True
      ElseIf (pCombo.Text.Length > 0) Then
        currentText = pCombo.Text
      End If

      Dim bs As BindingSource

      ' Clear existing BS ?

      If (pCombo.DataSource IsNot Nothing) AndAlso (GetType(BindingSource).IsInstanceOfType(pCombo.DataSource)) Then
        Try
          bs = CType(pCombo.DataSource, BindingSource)
          bs.Sort = ""
          bs.DataSource = Nothing
          bs.Clear()
          bs = Nothing
        Catch ex As Exception
        End Try
      End If

      ' Build Combo Bs.

      bs = New BindingSource

      bs.DataSource = _LocationsDictionary
      pCombo.DisplayMember = "Value"
      pCombo.ValueMember = "Key"
      pCombo.DataSource = bs

      If (IsCurrentValue) AndAlso (currentSelectedValue IsNot Nothing) Then
        pCombo.SelectedValue = currentSelectedValue
      ElseIf (currentText.Length > 0) Then
        pCombo.Text = currentText
      ElseIf (pCombo.Items.Count > 0) Then
        pCombo.SelectedIndex = 0
      End If

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Setting Locations Combo", ex.StackTrace, True)
    Finally
      Me.CTAStatusLabel.Text = OrgText
    End Try


  End Sub

  Friend Function GetTblLookupCollection(ByVal pTableID As RenaissanceGlobals.StandardDataset, ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "") As RenaissanceGlobals.LookupCollection(Of Object, Object)
    ' ************************************************************************************
    ' Wrapper for the  SetTblGenericCombo() Routine.
    ' ************************************************************************************

    Dim thisDataset As DataSet
    Dim thisSortedRows() As DataRow
    Dim SelectString As String
    Dim OrgText As String = ""

    Dim DisplayFieldNames As String()
    Dim DCount As Integer

    Try
      If (pTableID Is Nothing) Then Return New RenaissanceGlobals.LookupCollection(Of Object, Object)

      OrgText = Me.CTAStatusLabel.Text
      CTAStatusLabel.Text = "Building " & pTableID.TableName & " Collection"
      CTAStatusStrip.Refresh()
      Application.DoEvents()


      ' Resolve Display Field names
      DisplayFieldNames = pDisplayMember.Split(New Char() {CChar(","), CChar("|")})
      ' ReDim FormatStrings(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {CChar(" ")})

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If
          End If
        Next
      Catch ex As Exception
      End Try

      ' Get Source Dataset
      thisDataset = Me.Load_Table(pTableID, False)
      If Not (thisDataset Is Nothing) Then
        If pSelectString Is Nothing Then
          SelectString = "True"
        ElseIf pSelectString.Length <= 0 Then
          SelectString = "True"
        Else
          SelectString = pSelectString
        End If

        Try
          thisSortedRows = thisDataset.Tables(0).Select(SelectString, SelectSortString)
        Catch ex As Exception
          thisSortedRows = Nothing
        End Try
      Else
        Return New RenaissanceGlobals.LookupCollection(Of Object, Object)
      End If


      Return GetTblLookupCollection(thisSortedRows, pDisplayMember, pValueMember, pSelectString, pSELECTDISTINCT, pOrderAscending, pLeadingBlank, pLeadingBlankValue, pLeadingBlankText)

    Catch ex As Exception
    Finally
      If CTAStatusLabel.Text.Length > 0 Then
        CTAStatusLabel.Text = OrgText
        CTAStatusStrip.Refresh()
        Application.DoEvents()
      End If
    End Try

    Return New RenaissanceGlobals.LookupCollection(Of Object, Object)

  End Function

  Friend Function GetTblLookupCollection(ByVal pDataRows As DataRow(), ByVal pDisplayMember As String, ByVal pValueMember As String, ByVal pSelectString As String, Optional ByVal pSELECTDISTINCT As Boolean = False, Optional ByVal pOrderAscending As Boolean = True, Optional ByVal pLeadingBlank As Boolean = False, Optional ByVal pLeadingBlankValue As Object = "<DBNull>", Optional ByVal pLeadingBlankText As String = "") As RenaissanceGlobals.LookupCollection(Of Object, Object)
    ' ************************************************************************************
    ' The process for setting standard combos has been split into two parts.
    ' The first part (the other function) takes a Renaissance 'StandardDataset' parameter
    ' and then creates a DataRow array in accordance with the other parameters.
    ' This DataRow array is then passed to this procedure and the Combo is populated.
    '
    ' It has been done this way so that the Selection Combos on Add/Edit forms can be
    ' populated using just this procedure and the standard form selectedrows() array.
    '
    ' ************************************************************************************

    Dim thisSortedRows() As DataRow
    Dim thisRow As DataRow
    Dim lastRow As DataRow
    Dim OrgText As String = ""

    Dim DisplayFieldNames As String()
    Dim FormatStrings As String()
    Dim DCount As Integer

    Dim RVal As New RenaissanceGlobals.LookupCollection(Of Object, Object)

    Try
      If (pDataRows Is Nothing) Then Return RVal

      OrgText = Me.CTAStatusLabel.Text

      If (pDataRows.Length > 0) Then
        Me.CTAStatusLabel.Text = "Building " & pDataRows(0).Table.TableName & " Combo"
      Else
        Me.CTAStatusLabel.Text = "Building Combo"
      End If

      Me.CTAStatusStrip.Refresh()
      Application.DoEvents()

      ' Get Source Dataset
      ' pDataRows

      If (pDataRows Is Nothing) Then

        Return RVal
      End If

      thisSortedRows = pDataRows

      ' Resolve Display Field names

      DisplayFieldNames = pDisplayMember.Split(New Char() {CChar(","), CChar("|")})
      ReDim FormatStrings(DisplayFieldNames.Length - 1)

      Dim SelectSortString As String = ""
      Try

        For DCount = 0 To (DisplayFieldNames.Length - 1)
          DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Trim(New Char() {CChar(" ")})

          If (thisSortedRows.Length > 0) Then
            If thisSortedRows(0).Table.Columns.Contains(DisplayFieldNames(DCount)) = False Then
              DisplayFieldNames(DCount) = ""
            End If
          End If

          If (DisplayFieldNames(DCount).Length > 0) Then
            If (DCount > 0) AndAlso (SelectSortString.Length > 0) Then
              SelectSortString &= ", "
            End If

            If (DisplayFieldNames(DCount).ToUpper.EndsWith(" DESC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 5)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf (DisplayFieldNames(DCount).ToUpper.EndsWith(" ASC")) Then
              DisplayFieldNames(DCount) = DisplayFieldNames(DCount).Substring(0, DisplayFieldNames(DCount).Length - 4)
              SelectSortString &= DisplayFieldNames(DCount)
            ElseIf pOrderAscending Then
              SelectSortString &= DisplayFieldNames(DCount)
            Else
              SelectSortString &= (DisplayFieldNames(DCount) & " DESC")
            End If
          End If
        Next
      Catch ex As Exception
      End Try

      ' 
      ' Set Date Format strings for Display fields as necessary

      For DCount = 0 To (DisplayFieldNames.Length - 1)
        FormatStrings(DCount) = ""
        If (DisplayFieldNames(DCount).Length > 0) Then
          If (thisSortedRows IsNot Nothing) AndAlso (thisSortedRows.Length > 0) Then
            If thisSortedRows(0).Table.Columns(DisplayFieldNames(DCount)).DataType Is GetType(System.DateTime) Then
              FormatStrings(DCount) = DISPLAYMEMBER_DATEFORMAT
            End If
          End If
        End If
      Next DCount

      ' Leading Blank.

      If (pLeadingBlank = True) Then
        Try
          If (TypeOf pLeadingBlankValue Is String) AndAlso (pLeadingBlankValue.ToString = "<DBNull>") Then
            RVal.Add(DBNull.Value, pLeadingBlankText)
          Else
            RVal.Add(pLeadingBlankValue, pLeadingBlankText)
          End If

        Catch ex As Exception
        End Try
      End If

      lastRow = Nothing
      If (Not (thisSortedRows Is Nothing)) AndAlso (thisSortedRows.Length > 0) Then
        Dim DisplayString As String

        If thisSortedRows(0).Table.Columns.Contains(pValueMember) Then
          For Each thisRow In thisSortedRows
            If (pSELECTDISTINCT) Then
              If (Not (lastRow Is Nothing)) AndAlso (Not (thisRow(pValueMember).ToString = lastRow(pValueMember).ToString)) Then
                lastRow = Nothing
              End If
            End If

            If (lastRow Is Nothing) Then
              Try
                DisplayString = ""
                For DCount = 0 To (DisplayFieldNames.Length - 1)
                  If (DisplayFieldNames(DCount).Length > 0) Then
                    If DisplayString.Length > 0 Then
                      DisplayString &= ", "
                    End If

                    If thisRow.IsNull(DisplayFieldNames(DCount)) Then
                      DisplayString &= "<Null>"
                    Else
                      DisplayString &= Format(thisRow(DisplayFieldNames(DCount)), FormatStrings(DCount))
                    End If
                  End If
                Next DCount

                If thisRow.IsNull(pValueMember) Then
                  RVal.Add(0, DisplayString)
                Else
                  RVal.Add(thisRow(pValueMember), DisplayString)
                End If

              Catch ex As Exception
              End Try
            End If

            If pSELECTDISTINCT Then
              lastRow = thisRow
            End If
          Next
        End If
      End If

    Catch ex As Exception
      LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Setting Std Combo", ex.StackTrace, True)
    Finally
      If (CTAStatusLabel.Text.Length > 0) Then
        CTAStatusLabel.Text = OrgText
      End If
    End Try


    Return RVal
  End Function

  Public Sub SetComboDropDownWidth(ByRef pCombo As System.Windows.Forms.ComboBox)
    ' ************************************************************************************
    ' 
    ' ************************************************************************************

    Dim thisDrowDownWidth As Integer
    Dim SizingBitmap As Bitmap
    Dim SizingGraphics As Graphics
    Dim stringSize As SizeF

    Dim ItemString As String
    Dim ItemObject As Object
    Dim ItemCount As Integer

    If (pCombo Is Nothing) Then
      Exit Sub
    End If

    If (pCombo.Items.Count <= 0) Then
      Exit Sub
    End If

    Try

      thisDrowDownWidth = pCombo.DropDownWidth
      SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
      SizingGraphics = Graphics.FromImage(SizingBitmap)

      For ItemCount = 0 To (pCombo.Items.Count - 1)
        ItemObject = pCombo.Items(ItemCount)
        ItemString = ""

        If (pCombo.DataSource Is Nothing) Then
          ItemString = CStr(ItemObject)
        ElseIf GetType(DataRowView).IsInstanceOfType(ItemObject) Then
          ItemString = CType(ItemObject, DataRowView).Row(pCombo.DisplayMember).ToString()
        ElseIf GetType(DataRow).IsInstanceOfType(ItemObject) Then
          ItemString = CType(ItemObject, DataRow)(pCombo.DisplayMember).ToString()
        ElseIf GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ItemObject) Then
          ItemString = CType(ItemObject, KeyValuePair(Of Integer, String)).Value
        ElseIf TypeOf ItemObject Is ILookup Then
          ItemString = CType(ItemObject, ILookup).Value.ToString.ToUpper
        End If

        If (ItemString.Length > 0) Then
          stringSize = SizingGraphics.MeasureString(ItemString, pCombo.Font)
          If (stringSize.Width) > thisDrowDownWidth Then
            thisDrowDownWidth = CInt(stringSize.Width)
          End If
        End If
      Next

      If (pCombo.DropDownWidth < thisDrowDownWidth) Then
        pCombo.DropDownWidth = thisDrowDownWidth
      End If

    Catch ex As Exception
    End Try

  End Sub

  Public Sub ClearComboSelection(ByRef pCombo As System.Windows.Forms.ComboBox)
    ' ************************************************************************************
    ' 
    ' ************************************************************************************

    Try
      If (TypeOf pCombo.FindForm Is StandardCTAForm) Then
        If CType(pCombo.FindForm, StandardCTAForm).InUse = False Then
          Exit Sub
        End If
      End If
    Catch ex As Exception
    End Try

    Try
      If pCombo.Items.Count > 0 Then
        pCombo.SelectedIndex = 0
        pCombo.SelectedIndex = -1
      End If
    Catch ex As Exception
    End Try
  End Sub

  Public Sub SetFormToolTip(ByRef thisForm As Form, ByRef pFormTooltip As ToolTip)
    ' ************************************************************************************
    ' 
    ' ************************************************************************************

    Dim ToolTipDS As DSVeniceFormTooltips
    Dim SelectedToolTipRows() As DSVeniceFormTooltips.tblVeniceFormTooltipsRow
    Dim ToolTipRow As DSVeniceFormTooltips.tblVeniceFormTooltipsRow

    Try
      pFormTooltip.RemoveAll()

      ToolTipDS = CType(Me.Load_Table(RenaissanceStandardDatasets.tblVeniceFormTooltips), DSVeniceFormTooltips)

      If (Not (ToolTipDS Is Nothing)) AndAlso (Not (ToolTipDS.tblVeniceFormTooltips Is Nothing)) Then
        SelectedToolTipRows = CType(ToolTipDS.tblVeniceFormTooltips.Select("FormName='" & thisForm.Name & "'"), DSVeniceFormTooltips.tblVeniceFormTooltipsRow())

        If (Not (SelectedToolTipRows Is Nothing)) AndAlso (SelectedToolTipRows.Length > 0) Then
          For Each ToolTipRow In SelectedToolTipRows

            SetControlToolTip(Me, pFormTooltip, ToolTipRow.ControlName, ToolTipRow.ToolTip, True)

          Next

        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Function SetControlToolTip(ByVal ParentControl As Control, ByRef pFormTooltip As ToolTip, ByVal TargetControlName As String, ByVal ControlToolTipText As String, Optional ByVal ExitOnOneFind As Boolean = True) As Boolean
    ' ************************************************************************************
    ' 
    ' ************************************************************************************

    Try

      If (ParentControl.Name = TargetControlName) Then
        pFormTooltip.SetToolTip(ParentControl, ControlToolTipText)

        If (ExitOnOneFind) Then
          Return True
        End If
      End If

      For Each c As Control In ParentControl.Controls
        Try

          If (c.Name = TargetControlName) Then
            pFormTooltip.SetToolTip(c, ControlToolTipText)

            If (ExitOnOneFind) Then
              Return True
            End If
          End If

          If (c.Controls IsNot Nothing) AndAlso (c.Controls.Count > 0) Then
            If (SetControlToolTip(c, pFormTooltip, TargetControlName, ControlToolTipText, ExitOnOneFind)) And (ExitOnOneFind) Then
              Return True
            End If
          End If

        Catch ex As Exception
        End Try

      Next

    Catch ex As Exception
    End Try

  End Function

#End Region

#Region " Generic Form Update Code"

  Public Function AddFormUpdate(ByVal pForm As Windows.Forms.Form, ByRef pUpdateProcess As RenaissanceTimerUpdateClass.UpdateProcessDelegate, Optional ByVal pUpdatePeriod As Integer = 400) As RenaissanceTimerUpdateClass

    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try
      If (pForm Is Nothing) OrElse (pUpdateProcess Is Nothing) Then
        Return Nothing
        Exit Function
      End If

      SyncLock FormUpdateObjects
        If (FormUpdateObjects.ContainsKey(pForm)) Then
          Try
            FormUpdateObjects(pForm).Clear()
          Catch ex As Exception
          End Try

          FormUpdateObjects.Remove(pForm)
        End If

        FormUpdateObjects.Add(pForm, New RenaissanceTimerUpdateClass(pUpdateProcess, pUpdatePeriod))

        Return FormUpdateObjects(pForm)

      End SyncLock

    Catch ex As Exception
    End Try

    Return Nothing

  End Function

  Public Sub RemoveFormUpdate(ByVal pForm As Windows.Forms.Form)

    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try
      If (pForm Is Nothing) Then
        Exit Sub
      End If

      SyncLock FormUpdateObjects
        If (FormUpdateObjects.ContainsKey(pForm)) Then
          Try
            FormUpdateObjects(pForm).Clear()
          Catch ex As Exception
          End Try

          FormUpdateObjects.Remove(pForm)
        End If

      End SyncLock

    Catch ex As Exception
    End Try

  End Sub

  Private Sub FormUpdateDataTimer_Tick(ByVal sender As Object, ByVal Args As EventArgs)
    ' ************************************************************************************
    '
    '
    '
    ' ************************************************************************************

    Try

      Dim KeyForms() As Windows.Forms.Form = Nothing
      Dim ThisForm As Windows.Forms.Form = Nothing

      If (FormUpdateObjects.Count > 0) Then
        SyncLock FormUpdateObjects
          KeyForms = CType(Array.CreateInstance(GetType(Windows.Forms.Form), FormUpdateObjects.Count), Form())
          FormUpdateObjects.Keys.CopyTo(KeyForms, 0)
        End SyncLock
      Else
        Exit Sub
      End If

      If (KeyForms IsNot Nothing) AndAlso (KeyForms.Length > 0) Then
        For Each ThisForm In KeyForms
          If (ThisForm IsNot Nothing) Then
            If (ThisForm.IsDisposed) Then
              SyncLock FormUpdateObjects
                Try
                  FormUpdateObjects(ThisForm).Clear()
                Catch ex As Exception
                End Try

                FormUpdateObjects.Remove(ThisForm)
              End SyncLock
            ElseIf (ThisForm.Created) Then
              Try
                FormUpdateObjects(ThisForm).CheckUpdate()
              Catch ex As Exception
              End Try
            End If
          End If
        Next
      End If

    Catch ex As Exception
    End Try

  End Sub


#End Region

#Region " Geneic Combo Select Code"

  ' There Is an issue with the ComboBox control whereby if you select the combobox and type 
  ' One or more characters to select an item and then leave the combo by pressing the TAB key,
  ' Then the SelectedIndex (and other) properties revert to their starting values.
  ' This seems to be a generic .NET problem and is not specific to any of the coding in CTA.
  ' After EXTENSIVE research, I found that the SelectedIndex property reverts somehow between
  ' the 'Leave' and 'LostFocus' events. Furthermore I found that you can reset the SelectedIndex 
  ' in the 'LostFocus' event and it sticks.
  ' So this is what I have done :- Use the 'Tag' property to persist the SelectedIndex value.
  ' The Tag is initialised in the 'GotFocus' Event, Updated in the 'SelectedIndexChanged'
  ' Event and Re-set in the 'LostFocus' Event.
  '

  Public Sub GenericCombo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      If TypeOf sender Is ComboBox Then
        Dim thisCombo As ComboBox
        thisCombo = CType(sender, ComboBox)
        thisCombo.Tag = CType(sender, ComboBox).SelectedIndex
      End If
    Catch ex As Exception
    End Try
  End Sub

  Public Sub GenericCombo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      If TypeOf sender Is ComboBox Then
        Dim thisCombo As ComboBox
        thisCombo = CType(sender, ComboBox)
        thisCombo.Tag = CType(sender, ComboBox).SelectedIndex
      End If
    Catch ex As Exception
    End Try
  End Sub

  Public Sub GenericCombo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      If TypeOf sender Is ComboBox Then
        Dim thisCombo As ComboBox
        thisCombo = CType(sender, ComboBox)

        If (Not (thisCombo.Tag Is Nothing)) AndAlso IsNumeric(thisCombo.Tag) AndAlso (thisCombo.Items.Count > 0) Then
          If thisCombo.SelectedIndex <> CInt(thisCombo.Tag) Then
            thisCombo.SelectedIndex = CInt(thisCombo.Tag)
          End If
        End If
      End If
    Catch ex As Exception
    End Try
  End Sub


  Public Sub ComboSelectAsYouType(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *******************************************************************************
    ' This routine provides the 'Select-as-you-type' functionallity on some of the combo boxes.
    ' 
    ' It does not allow you to type an item that is not in the combo items.
    '
    ' Note that it will only work for Combo Boxes derived from a Data Table or View.
    ' *******************************************************************************

    Dim SelectString As String
    'Dim selectRow As DataRow
    'Dim selectRowView As DataRowView
    'Dim IndexCounter As Integer
    Dim ThisComboBox As ComboBox
    Dim StartingIndex As Integer = 0

    Try
      SyncLock sender

        ' Cast the sender to type Combo Box.
        ' In the event of an error, exit silently.
        Try
          ThisComboBox = CType(sender, ComboBox)
        Catch ex As Exception
          Exit Sub
        End Try


        ' Check for <Enter>

        If e.KeyCode = System.Windows.Forms.Keys.Enter Then
          ThisComboBox.SelectionStart = 0
          ThisComboBox.SelectionLength = ThisComboBox.Text.Length
          Exit Sub
        End If

        SelectString = ThisComboBox.Text

        If (ThisComboBox.Tag IsNot Nothing) AndAlso IsNumeric(ThisComboBox.Tag) AndAlso (SelectString.Length > 1) Then
          StartingIndex = CInt(ThisComboBox.Tag)
        End If

        If e.KeyCode = System.Windows.Forms.Keys.Back Then
          If SelectString.Length > 0 Then
            SelectString = SelectString.Substring(0, SelectString.Length - 1)
          End If
          StartingIndex = (-1)
        End If

        If e.KeyCode = System.Windows.Forms.Keys.Down Then
          Exit Sub
        End If

        If e.KeyCode = System.Windows.Forms.Keys.Down Then
          Exit Sub
        End If

startSearch:

        ' Search the Combo Data Items for an item starting with the typed text.

        If SelectString.Length <= 0 Then
          If ThisComboBox.Items.Count > 0 Then
            ThisComboBox.SelectedIndex = 0
            ThisComboBox.SelectionStart = 0
            ThisComboBox.SelectionLength = ThisComboBox.Text.Length
          Else
            ThisComboBox.SelectedIndex = -1
          End If

          Exit Sub
        End If

        Dim IndexString As String

        Try
          Dim RIndex As Integer

          If (StartingIndex <= 0) AndAlso (ThisComboBox.Items.Count > 1000) Then
            StartingIndex = BestGuessStartingIndex(ThisComboBox, SelectString, StartingIndex)
          End If

          RIndex = ThisComboBox.FindString(SelectString, Math.Max(0, StartingIndex - 1))
          If (RIndex >= 0) Then
            If (ThisComboBox.SelectedIndex <> RIndex) Then
              IndexString = GetComboItemStringValue(ThisComboBox.Items(RIndex), ThisComboBox.DisplayMember)

              'If GetType(DataRowView).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), DataRowView)(ThisComboBox.DisplayMember).ToString.ToUpper
              'ElseIf GetType(DataRow).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), DataRow)(ThisComboBox.DisplayMember).ToString.ToUpper
              'ElseIf GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), KeyValuePair(Of Integer, String)).Value
              'ElseIf TypeOf ThisComboBox.Items(RIndex) Is ILookup Then
              '	IndexString = CType(ThisComboBox.Items(RIndex), ILookup).Value.ToString.ToUpper
              'Else
              '	Try
              '		IndexString = CStr(ThisComboBox.Items(RIndex)).ToUpper
              '	Catch ex As Exception
              '		Exit Sub
              '	End Try
              'End If

              ThisComboBox.SelectedIndex = RIndex
              ThisComboBox.SelectionStart = SelectString.Length
              ThisComboBox.SelectionLength = IndexString.Length - SelectString.Length
            End If
            Exit Sub
          End If
        Catch ex As Exception
        End Try

        ' String not found
        SelectString = SelectString.Substring(0, SelectString.Length - 1)
        GoTo startSearch

      End SyncLock

    Catch ex As Exception
    End Try


  End Sub

  Public Sub ComboSelectAsYouType_NoMatch(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    ' *******************************************************************************
    ' This routine provides the 'Select-as-you-type' functionallity on some of the combo boxes.
    ' 
    ' It does allow you to type an item that is not in the combo items.
    '
    ' Note that it will only work for Combo Boxes derived from a Data Table or View.
    ' *******************************************************************************

    Dim SelectString As String
    'Dim selectRow As DataRow
    'Dim selectRowView As DataRowView
    'Dim IndexCounter As Integer
    Dim ThisComboBox As ComboBox
    Dim StartingIndex As Integer = 0

    Try

      ' Cast the sender to type Combo Box.
      ' In the event of an error, exit silently.
      Try
        ThisComboBox = CType(sender, ComboBox)
      Catch ex As Exception
        Exit Sub
      End Try

      ' Check for <Enter>

      If e.KeyCode = System.Windows.Forms.Keys.Enter Then
        ThisComboBox.SelectionStart = 0
        ThisComboBox.SelectionLength = ThisComboBox.Text.Length
        Exit Sub
      End If

      SelectString = ThisComboBox.Text

      If (ThisComboBox.Tag IsNot Nothing) AndAlso IsNumeric(ThisComboBox.Tag) AndAlso (SelectString.Length > 1) Then
        StartingIndex = CInt(ThisComboBox.Tag)
      End If

      If e.KeyCode = System.Windows.Forms.Keys.Back Then
        If SelectString.Length > 0 Then
          SelectString = SelectString.Substring(0, SelectString.Length - 1)
        End If
      End If

      If e.KeyCode = System.Windows.Forms.Keys.Down Then
        Exit Sub
      End If

      If e.KeyCode = System.Windows.Forms.Keys.Down Then
        Exit Sub
      End If

startSearch:

      ' Search the Combo Data Items for an item starting with the typed text.

      If SelectString.Length <= 0 Then
        If ThisComboBox.Items.Count > 0 Then
          ThisComboBox.SelectedIndex = 0
          ThisComboBox.SelectionStart = 0
          ThisComboBox.SelectionLength = ThisComboBox.Text.Length
        Else
          ThisComboBox.SelectedIndex = -1
        End If

        Exit Sub
      End If

      Dim IndexString As String

      Try
        Dim RIndex As Integer

        If (StartingIndex <= 0) AndAlso (ThisComboBox.Items.Count > 1000) Then
          StartingIndex = BestGuessStartingIndex(ThisComboBox, SelectString, StartingIndex)
        End If

        RIndex = ThisComboBox.FindString(SelectString, Math.Max(0, StartingIndex - 1))
        If (RIndex >= 0) Then
          If (ThisComboBox.SelectedIndex <> RIndex) Then
            IndexString = GetComboItemStringValue(ThisComboBox.Items(RIndex), ThisComboBox.DisplayMember)

            'If GetType(DataRowView).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), DataRowView)(ThisComboBox.DisplayMember).ToString.ToUpper
            'ElseIf GetType(DataRow).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), DataRow)(ThisComboBox.DisplayMember).ToString.ToUpper
            'ElseIf GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ThisComboBox.Items(RIndex)) Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), KeyValuePair(Of Integer, String)).Value
            'ElseIf TypeOf ThisComboBox.Items(RIndex) Is ILookup Then
            '	IndexString = CType(ThisComboBox.Items(RIndex), ILookup).Value.ToString.ToUpper
            'Else
            '	Try
            '		IndexString = CStr(ThisComboBox.Items(RIndex)).ToUpper
            '	Catch ex As Exception
            '		Exit Sub
            '	End Try
            'End If

            ThisComboBox.SelectedIndex = RIndex
            ThisComboBox.SelectionStart = SelectString.Length
            ThisComboBox.SelectionLength = IndexString.Length - SelectString.Length
          End If
          Exit Sub
        End If
      Catch ex As Exception
      End Try

      ' String not found
      ThisComboBox.Tag = Nothing ' Stop LotFocus Event setting the SelectedIndex
      Exit Sub

    Catch ex As Exception
    End Try

  End Sub

  Private Function BestGuessStartingIndex(ByVal ThisComboBox As ComboBox, ByVal SelectString As String, ByVal OriginalStartingIndex As Integer) As Integer

    Dim RVal As Integer = OriginalStartingIndex

    Try
      Dim TopIndex As Integer = 0
      Dim BottomIndex As Integer = ThisComboBox.Items.Count - 1
      Dim MidIndex As Integer
      Dim ThisString As String
      Dim CVal As Double

      While (BottomIndex - TopIndex) > 5
        MidIndex = CInt((TopIndex + BottomIndex) / 2)

        ThisString = GetComboItemStringValue(ThisComboBox.Items(MidIndex), ThisComboBox.DisplayMember)
        CVal = ThisString.CompareTo(SelectString)

        If (CVal = 0) Then
          RVal = MidIndex
          Exit While
        ElseIf (CVal > 0) Then
          BottomIndex = MidIndex
        Else
          TopIndex = MidIndex
        End If

        RVal = TopIndex
      End While

    Catch ex As Exception
    End Try

    Return RVal

  End Function

  Private Function GetComboItemStringValue(ByVal pComboItem As Object, ByVal DisplayMember As String) As String

    If TypeOf pComboItem Is DataRowView Then ' GetType(DataRowView).IsInstanceOfType(ThisComboBox.Items(IndexCounter)) Then
      Return CType(pComboItem, DataRowView).Row(DisplayMember).ToString.ToUpper
    ElseIf TypeOf pComboItem Is DataRow Then ' GetType(DataRow).IsInstanceOfType(ThisComboBox.Items(IndexCounter)) Then
      Return CType(pComboItem, DataRow)(DisplayMember).ToString.ToUpper
    ElseIf TypeOf pComboItem Is KeyValuePair(Of Integer, String) Then ' GetType(KeyValuePair(Of Integer, String)).IsInstanceOfType(ThisComboBox.Items(IndexCounter)) Then
      Return CType(pComboItem, KeyValuePair(Of Integer, String)).Value.ToUpper
    ElseIf TypeOf pComboItem Is ILookup Then   'TypeOf ThisComboBox.Items(IndexCounter) Is ILookup Then
      Return CType(pComboItem, ILookup).Value.ToString.ToUpper
    Else
      Try
        Return CStr(pComboItem).ToUpper
      Catch ex As Exception
      End Try
    End If

    Return ""

  End Function


#End Region

#Region " Generic C1Chart Code"

  Public Sub C1Chart_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Dim ThisChart As C1.Win.C1Chart.C1Chart
    Dim BorderSize As Integer = 5

    Try
      If Not (TypeOf sender Is C1.Win.C1Chart.C1Chart) Then
        Exit Sub
      End If

      ThisChart = CType(sender, C1.Win.C1Chart.C1Chart)

      If (ThisChart.Legend.Visible) Then
        Select Case ThisChart.Legend.Compass
          Case C1.Win.C1Chart.CompassEnum.East
            If (ThisChart.ChartArea.Location.X <> BorderSize) OrElse (ThisChart.ChartArea.Location.Y <> BorderSize) Then
              ThisChart.ChartArea.Location = New Drawing.Point(BorderSize, BorderSize)
            End If

            ThisChart.ChartArea.Size = New Size(ThisChart.Legend.Location.X - ((ThisChart.ChartArea.Location.X + BorderSize)), ThisChart.Size.Height - (2 * BorderSize))

          Case C1.Win.C1Chart.CompassEnum.North
            If (ThisChart.ChartArea.Location.X <> BorderSize) OrElse (ThisChart.ChartArea.Location.Y <> (ThisChart.Legend.Location.Y + ThisChart.Legend.Size.Height + BorderSize)) Then
              ThisChart.ChartArea.Location = New Drawing.Point(BorderSize, ThisChart.Legend.Location.Y + ThisChart.Legend.Size.Height + BorderSize)
            End If

            ThisChart.ChartArea.Size = New Size(ThisChart.Size.Width - (ThisChart.ChartArea.Location.X + BorderSize), ThisChart.Size.Height - (ThisChart.ChartArea.Location.Y + BorderSize))

          Case C1.Win.C1Chart.CompassEnum.South
            If (ThisChart.ChartArea.Location.X <> BorderSize) OrElse (ThisChart.ChartArea.Location.Y <> BorderSize) Then
              ThisChart.ChartArea.Location = New Drawing.Point(BorderSize, BorderSize)
            End If

            ThisChart.ChartArea.Size = New Size(ThisChart.Size.Width - (ThisChart.ChartArea.Location.X + BorderSize), ThisChart.Size.Height - (ThisChart.Legend.Location.Y + BorderSize))

          Case C1.Win.C1Chart.CompassEnum.West
            ThisChart.ChartArea.Location = New Drawing.Point(ThisChart.Legend.Location.X + ThisChart.Legend.Size.Width + BorderSize, BorderSize)

            ThisChart.ChartArea.Size = New Size(ThisChart.Size.Width - (ThisChart.ChartArea.Location.X + BorderSize), ThisChart.Size.Height - (2 * BorderSize))

        End Select

      Else
        If (ThisChart.ChartArea.Location.X <> BorderSize) OrElse (ThisChart.ChartArea.Location.Y <> BorderSize) Then
          ThisChart.ChartArea.Location = New Drawing.Point(BorderSize, BorderSize)
        End If

        ThisChart.ChartArea.Size = New Size(ThisChart.Size.Width - (2 * ThisChart.ChartArea.Location.X), ThisChart.Size.Height - (2 * BorderSize))
      End If

    Catch ex As Exception
    End Try

  End Sub

#End Region

#Region " tblSystemString Functions "

  Friend Function Get_SystemString(ByVal pStringName As String) As String

    Dim adpSystemStrings As SqlDataAdapter
    Dim tblSystemstrings As New DataTable

    adpSystemStrings = Me.MainDataHandler.Get_Adaptor("adp_GetSystemString", CTA_CONNECTION, "tblSystemStrings")

    If (adpSystemStrings Is Nothing) Then
      Return ""
    End If

    Try
      SyncLock adpSystemStrings.SelectCommand.Connection
        SyncLock adpSystemStrings
          adpSystemStrings.SelectCommand.Parameters("@StringDescription").Value = pStringName
          adpSystemStrings.Fill(tblSystemstrings)
        End SyncLock
      End SyncLock
    Catch ex As Exception
      Me.LogError(Me.Name & ", Get_SystemString()", LOG_LEVELS.Error, ex.Message, "Failed to Get System String.", ex.StackTrace, True)

      Return ""
    End Try

    If (tblSystemstrings Is Nothing) OrElse (tblSystemstrings.Rows.Count <= 0) Then
      Return ""
    End If

    Try
      Return CStr(tblSystemstrings.Rows(0)("StringValue"))
    Catch ex As Exception
      Me.LogError(Me.Name & ", Get_SystemString()", LOG_LEVELS.Error, ex.Message, "Failed to Get System String.", ex.StackTrace, True)
      Return ""
    End Try

    Return ""

  End Function

  Friend Function Set_SystemString(ByVal pStringName As String, ByVal pStringValue As String) As String

    Dim adpSystemStrings As SqlDataAdapter
    Dim tblSystemstrings As New DataTable

    adpSystemStrings = Me.MainDataHandler.Get_Adaptor("adp_GetSystemString", CTA_CONNECTION, "tblSystemStrings")

    If (adpSystemStrings Is Nothing) Then
      Return ""
    End If

    Try
      SyncLock adpSystemStrings.InsertCommand.Connection
        SyncLock adpSystemStrings
          adpSystemStrings.InsertCommand.Parameters("@StringDescription").Value = pStringName
          adpSystemStrings.InsertCommand.Parameters("@StringValue").Value = pStringValue

          Call adpSystemStrings.InsertCommand.ExecuteNonQuery()

          Return pStringValue

        End SyncLock
      End SyncLock
    Catch ex As Exception
      Me.LogError(Me.Name & ", Set_SystemString()", LOG_LEVELS.Error, ex.Message, "Failed to Set System String.", ex.StackTrace, True)

      Return ""
    End Try

    Return ""

  End Function


#End Region

#Region " AdaptorUpdate, SQL Connection Synchlock functions."

  Public Function AdaptorUpdate(ByVal FormName As String, ByVal pAdaptor As SqlDataAdapter, ByVal pTable As DataTable) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      For Each thisDataRow In pTable.Rows
        If (thisDataRow.RowState = DataRowState.Added) Or (thisDataRow.RowState = DataRowState.Deleted) Or (thisDataRow.RowState = DataRowState.Modified) Then
          EditState = thisDataRow.RowState.ToString & " Record"
          LogString = Me.GetStandardLogString(thisDataRow)
          Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
        End If
      Next
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pTable)
  End Function

  Public Function AdaptorUpdate(ByVal FormName As String, ByRef pAdaptor As SqlDataAdapter, ByRef pDataSet As DataSet) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim thisTable As DataTable
    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      For Each thisTable In pDataSet.Tables
        For Each thisDataRow In thisTable.Rows
          If (thisDataRow.RowState = DataRowState.Added) Or (thisDataRow.RowState = DataRowState.Deleted) Or (thisDataRow.RowState = DataRowState.Modified) Then
            EditState = thisDataRow.RowState.ToString & " Record"
            LogString = Me.GetStandardLogString(thisDataRow)
            Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
          End If
        Next
      Next
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pDataSet)
  End Function

  Public Function AdaptorUpdate(ByVal FormName As String, ByVal pAdaptor As SqlDataAdapter, ByVal pDataRows() As DataRow) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      For Each thisDataRow In pDataRows
        If (thisDataRow IsNot Nothing) Then
          If (thisDataRow.RowState = DataRowState.Added) Or (thisDataRow.RowState = DataRowState.Deleted) Or (thisDataRow.RowState = DataRowState.Modified) Then
            EditState = thisDataRow.RowState.ToString & " Record"
            LogString = Me.GetStandardLogString(thisDataRow)
            Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
          End If
        End If
      Next
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pDataRows)
  End Function

  Public Function AdaptorUpdate(ByVal FormName As String, ByRef pAdaptor As SqlDataAdapter, ByRef pDataSet As DataSet, ByVal pTableName As String) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim thisDataRow As DataRow
    Dim LogString As String
    Dim EditState As String

    Try
      For Each thisDataRow In pDataSet.Tables(pTableName).Rows
        If (thisDataRow.RowState = DataRowState.Added) Or (thisDataRow.RowState = DataRowState.Deleted) Or (thisDataRow.RowState = DataRowState.Modified) Then
          EditState = thisDataRow.RowState.ToString & " Record"
          LogString = Me.GetStandardLogString(thisDataRow)
          Me.LogError(FormName, LOG_LEVELS.Changes, EditState, LogString, "", False)
        End If
      Next
    Catch ex As Exception
    End Try

    Return MainDataHandler.AdaptorUpdate(pAdaptor, pDataSet, pTableName)
  End Function

  Public Function AdaptorUpdate(ByVal FormName As String, ByVal pStdTable As RenaissanceGlobals.StandardDataset, ByVal pDataRows() As DataRow) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim ThisAdaptor As SqlDataAdapter

    Try
      ThisAdaptor = Me.MainDataHandler.Get_Adaptor(pStdTable.Adaptorname, CTA_CONNECTION, pStdTable.TableName)

      If ThisAdaptor IsNot Nothing Then
        Try
          If (ThisAdaptor.InsertCommand IsNot Nothing) AndAlso (ThisAdaptor.InsertCommand.Parameters.Contains("@Knowledgedate")) Then
            ThisAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = Main_Knowledgedate
          End If
        Catch ex As Exception
        End Try
        Try
          If (ThisAdaptor.UpdateCommand IsNot Nothing) AndAlso (ThisAdaptor.UpdateCommand.Parameters.Contains("@Knowledgedate")) Then
            ThisAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = Main_Knowledgedate
          End If
        Catch ex As Exception
        End Try

        Return AdaptorUpdate(FormName, ThisAdaptor, pDataRows)
      Else
        Return 0
      End If
    Catch ex As Exception
    End Try

    Return 0

  End Function

  Public Function AdaptorUpdate(ByVal FormName As String, ByVal pStdTable As RenaissanceGlobals.StandardDataset, ByVal pDataTable As DataTable) As Integer
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim ThisAdaptor As SqlDataAdapter

    Try
      ThisAdaptor = Me.MainDataHandler.Get_Adaptor(pStdTable.Adaptorname, CTA_CONNECTION, pStdTable.TableName)

      If ThisAdaptor IsNot Nothing Then
        Try
          If (ThisAdaptor.InsertCommand IsNot Nothing) AndAlso (ThisAdaptor.InsertCommand.Parameters.Contains("@Knowledgedate")) Then
            ThisAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = Main_Knowledgedate
          End If
        Catch ex As Exception
        End Try
        Try
          If (ThisAdaptor.UpdateCommand IsNot Nothing) AndAlso (ThisAdaptor.UpdateCommand.Parameters.Contains("@Knowledgedate")) Then
            ThisAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = Main_Knowledgedate
          End If
        Catch ex As Exception
        End Try

        Return AdaptorUpdate(FormName, ThisAdaptor, pDataTable)
      Else
        Return 0
      End If
    Catch ex As Exception
    End Try

    Return 0

  End Function

  Public Function GetCTAConnection() As SqlConnection Implements StandardRenaissanceMainForm.GetRenaissanceConnection
    ' ************************************************************************************
    ' 
    ' 
    ' ************************************************************************************

    Dim RVal As SqlConnection = Nothing

    Try
      RVal = New SqlConnection(Me.SQLConnectString)

      If (RVal.State And ConnectionState.Open) <> ConnectionState.Open Then
        RVal.Open()
      End If

      Return RVal
    Catch ex As Exception
      Try
        If (RVal IsNot Nothing) Then
          RVal.Close()
        End If
      Catch Inner_Ex As Exception
      End Try
    End Try

    Return Nothing
  End Function

  Public Function GetCTAAsynchConnection() As SqlConnection

    Dim RVal As SqlConnection = Nothing

    Try
      RVal = New SqlConnection(Me.SQLAsynchConnectString)
      RVal.Open()

      Return RVal
    Catch ex As Exception
      Try
        If (RVal IsNot Nothing) Then
          RVal.Close()
        End If
      Catch Inner_Ex As Exception
      End Try
    End Try

    Return Nothing
  End Function

#End Region

#Region " Error Logging "

  Public Function LogError(ByVal p_Error_Location As String, _
   ByVal p_System_Error_Number As Integer, _
   ByVal p_System_Error_Description As String, _
   Optional ByVal p_Error_Message As String = "", _
   Optional ByVal p_Error_StackTrace As String = "", _
   Optional ByVal p_Show_Error As Boolean = True, _
   Optional ByVal p_MessageHeading As String = "", _
   Optional ByVal p_MessageButtons As System.Windows.Forms.MessageBoxButtons = MessageBoxButtons.OK, _
   Optional ByVal p_MessageBoxIcon As System.Windows.Forms.MessageBoxIcon = MessageBoxIcon.Exclamation, _
   Optional ByVal p_Log_to As Integer = 0) As Integer _
   Implements StandardRenaissanceMainForm.LogError

    '**************************************************************************************************
    ' Public function returning public variables
    '
    ' Purpose:  Display Error messages and log to Database / File if required
    '
    ' Accepts:  p_Error_Location as String            : Location of Error (for debugging and logging purposes)
    '           p_System_Error_Number As Integer      : System 'Err'
    '           p_System_Error_Description As String  : System 'Error'
    '           p_Error_Message as String (Optional)  : Meaningful User Error message
    '           p_Error_StackTrace as String (Optional)  : StackTrace Property of Error message
    '           p_Show_Error As Boolean (Optional)    : If TRUE then a MsgBox is diplayed
    '           p_MessageHeading as String (Optional) : Title for Error message box
    '           p_MessageButtons                      : Button(s) to show on the error message
    '           p_MessageBoxIcon                      : Icon to show on the Error message
    '           p_Log_to As Integer (Optional)        : Write log conditions
    '
    ' Returns:  0 or error number on fail, only returns an error if logged to File / Table and the update fails
    '**************************************************************************************************

    If (p_MessageHeading.Length <= 0) Then
      p_MessageHeading = "CTA Message"
    End If

    ' Display Error message if required.

    If p_Show_Error = True Then
      MessageBox.Show(p_System_Error_Number.ToString & vbCrLf & _
      p_System_Error_Description & vbCrLf & _
      p_Error_Message, p_MessageHeading, p_MessageButtons, p_MessageBoxIcon, MessageBoxDefaultButton.Button1)
    End If

    ' Log to DB, if possible.

    Dim InsertCommand As New SqlCommand
    Dim myConnection As SqlConnection
    myConnection = MainDataHandler.Get_Connection(CTA_CONNECTION)

    If Not (myConnection Is Nothing) Then
      Try
        InsertCommand.CommandText = "[adp_tblINVEST_ERROR_LOG_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

        InsertCommand.Connection = Me.GetCTAConnection

        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@WindowsUserName", System.Data.SqlDbType.NVarChar, 100))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ErrorNumber", System.Data.SqlDbType.Int, 4))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ErrorLocation", System.Data.SqlDbType.NVarChar, 250))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SystemErrorDescription", System.Data.SqlDbType.NVarChar, 250))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserErrorDescription", System.Data.SqlDbType.NVarChar, 450))
        InsertCommand.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CallStack", System.Data.SqlDbType.Text, 2147483647))

        InsertCommand.Parameters("@WindowsUserName").Value = Environment.UserName
        InsertCommand.Parameters("@ErrorNumber").Value = p_System_Error_Number
        InsertCommand.Parameters("@ErrorLocation").Value = p_Error_Location
        InsertCommand.Parameters("@SystemErrorDescription").Value = p_System_Error_Description
        InsertCommand.Parameters("@UserErrorDescription").Value = p_Error_Message
        InsertCommand.Parameters("@CallStack").Value = p_Error_StackTrace

        If (p_Error_Message.Length > 450) And (p_Error_StackTrace.Length = 0) Then
          InsertCommand.Parameters("@UserErrorDescription").Value = "See StackTrace."
          InsertCommand.Parameters("@CallStack").Value = p_Error_Message
        End If


        Try

          If (Not (InsertCommand.Connection Is Nothing)) Then
            SyncLock InsertCommand.Connection
              InsertCommand.ExecuteNonQuery()
            End SyncLock
          End If

        Catch ex As Exception

        Finally

          If (Not (InsertCommand.Connection Is Nothing)) Then
            Try
              InsertCommand.Connection.Close()
            Catch ex As Exception
            End Try
          End If
        End Try

      Catch ex As Exception
      End Try
    End If

    Return 0

  End Function

  Public Function GetStandardLogString(ByRef thisDataRow As DataRow) As String
    '**************************************************************************************************
    '
    '
    '**************************************************************************************************

    Dim LogString As String
    LogString = ""

    Try
      Dim ItemCount As Integer
      Dim thisItem As Object
      Dim TableName As String
      Dim ItemName As String

      TableName = ""
      Try
        TableName = thisDataRow.Table.TableName.ToUpper
        If TableName.StartsWith("TBL") Then
          TableName = TableName.Substring(3)
        End If
      Catch ex As Exception
      End Try

      LogString = thisDataRow.RowState.ToString & ", " & thisDataRow.Table.TableName & ", "

      If (thisDataRow.RowState And DataRowState.Deleted) <> DataRowState.Deleted Then
        For ItemCount = 0 To (thisDataRow.ItemArray.Length - 1)
          thisItem = thisDataRow.ItemArray(ItemCount)
          ItemName = thisDataRow.Table.Columns(ItemCount).ColumnName
          If ItemName.ToUpper.StartsWith(TableName) Then
            ItemName = ItemName.Substring(TableName.Length)
          End If

          LogString &= ItemName & "="

          If (thisItem Is Nothing) Then
            LogString &= "Null,"
          Else
            LogString &= "`" & thisItem.ToString & "`,"
          End If
        Next
      End If
    Catch ex As Exception
    End Try

    Return LogString

  End Function

#End Region

#Region " Grid Clipboard Copy Utility"

  Public Sub CopyGridSelection(ByVal ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid, Optional ByVal IncludeColumnZero As Boolean = False, Optional ByVal PerformanceGridStyle As Boolean = False, Optional ByVal CopyWholeGrid As Boolean = False)
    ' **************************************************************************
    '
    '
    ' **************************************************************************

    Try
      Dim ClipString As String = ThisGrid.Clip
      Dim HeaderString As String = ""
      Dim ColCount As Integer

      Dim PertracID As ULong = 0UL
      Dim PertracIDString As String
      Dim StartCol As Integer = ThisGrid.Selection.c1
      Dim EndCol As Integer = ThisGrid.Selection.c2
      Dim StartRow As Integer = ThisGrid.Selection.r1
      Dim EndRow As Integer = ThisGrid.Selection.r2

      If (CopyWholeGrid) Then
        StartCol = 0
        EndCol = ThisGrid.Cols.Count - 1

        If (IncludeColumnZero) Then
          StartRow = Math.Min(1, ThisGrid.Rows.Count - 1)
        Else
          StartRow = 0
        End If
        EndRow = ThisGrid.Rows.Count - 1

        ClipString = ThisGrid.GetCellRange(StartRow, StartCol, EndRow, EndCol).Clip
      End If

      If (ThisGrid.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.Row) Or (ThisGrid.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange) Or (ThisGrid.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox) Then
        StartCol = 0
        EndCol = ThisGrid.Cols.Count - 1
      End If

      If PerformanceGridStyle Then
        If (IsNumeric(ThisGrid.Tag)) Then
          PertracID = CULng(ThisGrid.Tag)
        End If
        PertracIDString = PertracID.ToString("###0")

        Dim ThisRow As Integer
        Dim ThisCol As Integer

        If StartCol <= 0 Then StartCol = 1
        If StartRow <= 0 Then StartRow = 1
        If EndCol = (ThisGrid.Cols.Count - 1) Then EndCol -= 1 ' Strip off YTD

        HeaderString = "ID" & Chr(9) & "Date" & Chr(9) & "Return" & Chr(13)
        ClipString = ""

        For ThisRow = StartRow To EndRow
          For ThisCol = StartCol To EndCol

            If (ThisRow < ThisGrid.Rows.Count) AndAlso (ThisGrid.Item(ThisRow, ThisCol) IsNot Nothing) Then
              If IsDate("1 " & ThisGrid.Item(0, ThisCol).ToString & " " & ThisGrid.Item(ThisRow, 0).ToString) And IsNumeric(ThisGrid.Item(ThisRow, ThisCol)) Then
                ClipString &= PertracIDString & Chr(9) & FitDateToPeriod(DealingPeriod.Monthly, CDate("1 " & ThisGrid.Item(0, ThisCol).ToString & " " & CStr(ThisGrid.Item(ThisRow, 0))), True).ToString(QUERY_SHORTDATEFORMAT) & Chr(9) & CDbl(ThisGrid.Item(ThisRow, ThisCol)).ToString("###0.00##%") & Chr(13)
              Else
                ClipString &= PertracIDString & Chr(9) & ThisGrid.Item(0, ThisCol).ToString & " " & ThisGrid.Item(ThisRow, 0).ToString & Chr(9) & ThisGrid.Item(ThisRow, ThisCol).ToString & Chr(13)
              End If
            End If
          Next
        Next

      ElseIf (IncludeColumnZero = False) OrElse (ThisGrid.Selection.c1 = 0) Then

        For ColCount = StartCol To EndCol
          If ColCount > StartCol Then HeaderString &= Chr(9)
          HeaderString &= CStr(ThisGrid.Item(0, ColCount))
        Next
        HeaderString &= Chr(13)

      Else

        Dim ClipStrings() As String = ClipString.Split(New Char() {Chr(13)})
        Dim RowCount As Integer

        HeaderString = CStr(ThisGrid.Item(0, 0))
        For ColCount = StartCol To EndCol
          HeaderString &= Chr(9)
          HeaderString &= CStr(ThisGrid.Item(0, ColCount))
        Next
        HeaderString &= Chr(13)

        ClipString = ""
        For RowCount = StartRow To EndRow
          If (RowCount < ThisGrid.Rows.Count) Then
            ClipString &= CStr(ThisGrid.Item(RowCount, 0)) ' 1st column
            ClipString &= Chr(9) & ClipStrings(RowCount - StartRow) ' Selection

            ClipString &= Chr(13)
          End If
        Next
      End If

      Clipboard.Clear()
      Clipboard.SetData(System.Windows.Forms.DataFormats.Text, (HeaderString & ClipString))

    Catch ex As Exception
    End Try
  End Sub

  Public Sub CopyListSelection(ByVal ThisList As ListBox)
    ' **************************************************************************
    '
    '
    ' **************************************************************************

    Try
      Dim ClipString As String = ""
      Dim ThisClipRow As String = ""
      Dim ColCount As Integer
      Dim ThisItem As Object

      For Each ThisItem In ThisList.SelectedItems
        If TypeOf (ThisItem) Is DataRowView Then
          If ThisList.DisplayMember.Length > 0 Then
            ThisClipRow = CType(ThisItem, DataRowView).Row(ThisList.DisplayMember).ToString
            If ThisList.ValueMember.Length > 0 Then
              ThisClipRow &= Chr(9) & CType(ThisItem, DataRowView).Row(ThisList.ValueMember).ToString
            End If
          Else
            For ColCount = 0 To CType(ThisItem, DataRowView).Row.ItemArray.Length - 1
              If ColCount > 0 Then ThisClipRow &= Chr(9)

              ThisClipRow &= CType(ThisItem, DataRowView).Row.ItemArray(ColCount).ToString
            Next
          End If
        ElseIf TypeOf (ThisItem) Is DataRow Then
          If ThisList.DisplayMember.Length > 0 Then
            ThisClipRow = CType(ThisItem, DataRow)(ThisList.DisplayMember).ToString
            If ThisList.ValueMember.Length > 0 Then
              ThisClipRow &= Chr(9) & CType(ThisItem, DataRow)(ThisList.ValueMember).ToString
            End If
          Else
            For ColCount = 0 To CType(ThisItem, DataRow).ItemArray.Length - 1
              If ColCount > 0 Then ThisClipRow &= Chr(9)

              ThisClipRow &= CType(ThisItem, DataRow).ItemArray(ColCount).ToString
            Next
          End If
        Else
          ThisClipRow = ThisItem.ToString
        End If

        ClipString &= ThisClipRow & Chr(13)

      Next

      Clipboard.Clear()
      Clipboard.SetData(System.Windows.Forms.DataFormats.Text, ClipString)

    Catch ex As Exception
    End Try

  End Sub


#End Region

#Region " Disable / Enable Form Controls"


  Friend Function DisableFormControls(ByRef thisForm As Form) As ArrayList
    Dim thisControl As Control
    Dim ReturnArray As New ArrayList

    Try
      For Each thisControl In thisForm.Controls
        If (thisControl.Visible = True) AndAlso (thisControl.Enabled = True) Then

          If Not (TypeOf thisControl Is StatusStrip) Then
            ReturnArray.Add(thisControl)
            thisControl.Enabled = False
          End If

        End If
      Next
    Catch ex As Exception
    End Try

    Return ReturnArray
  End Function

  Friend Sub EnableFormControls(ByVal ControlArray As ArrayList)
    Dim thisControl As Control

    Try
      If (ControlArray Is Nothing) Then
        Exit Sub
      End If

      For Each thisControl In ControlArray
        If (thisControl IsNot Nothing) Then
          If (thisControl.IsDisposed = False) Then
            thisControl.Enabled = True
          End If
        End If
      Next
    Catch ex As Exception
    End Try
  End Sub

#End Region

#Region " StatusBar Update Event Code."

  Private Delegate Sub IncrementStatusBarDelegate(ByVal thisBar As ToolStripProgressBar)

  Friend Sub IncrementStatusBar(ByVal thisProgressbar As ToolStripProgressBar)
    Static LastUpdateTime As Date = #1/1/1900#
    Dim LastObjectUpdateTime As Date

    If (thisProgressbar Is Nothing) OrElse (thisProgressbar.IsDisposed) Then
      Exit Sub
    End If

    If thisProgressbar.Owner.InvokeRequired Then
      ' InvokeRequired required compares the thread ID of the
      ' calling thread to the thread ID of the creating thread.
      ' If these threads are different, it returns true.
      Dim d As New IncrementStatusBarDelegate(AddressOf IncrementStatusBar)
      thisProgressbar.Owner.Invoke(d, New Object() {thisProgressbar})
      Exit Sub
    End If

    Try
      ' Make sure the bar is Visible.
      If (thisProgressbar.Visible = False) Then
        thisProgressbar.Visible = True
        thisProgressbar.Owner.PerformLayout()
      End If

      ' Impose a minimum time between status bar updates.
      If (thisProgressbar.Tag IsNot Nothing) AndAlso (TypeOf thisProgressbar.Tag Is Date) Then
        LastObjectUpdateTime = CType(thisProgressbar.Tag, Date)
      Else
        LastObjectUpdateTime = LastUpdateTime
      End If

      If (Now() - LastObjectUpdateTime).TotalSeconds >= 0.25 Then
        If (thisProgressbar.Tag IsNot Nothing) AndAlso (Not (TypeOf thisProgressbar.Tag Is Date)) Then
          LastUpdateTime = Now()
        Else
          thisProgressbar.Tag = Now()
        End If

        ' Progress the Status Bar....

        If (thisProgressbar.Value < thisProgressbar.Maximum) Then
          If (thisProgressbar.Value + thisProgressbar.Step) <= thisProgressbar.Maximum Then
            thisProgressbar.Value += thisProgressbar.Step
          Else
            thisProgressbar.Value = thisProgressbar.Maximum
          End If
        Else
          thisProgressbar.Value = thisProgressbar.Minimum
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Delegate Sub IncrementProgressBarDelegate(ByVal thisBar As Windows.Forms.ProgressBar)

  Friend Sub IncrementProgressBar(ByVal thisProgressbar As Windows.Forms.ProgressBar)
    Static LastUpdateTime As Date = #1/1/1900#
    Dim LastObjectUpdateTime As Date

    If (thisProgressbar Is Nothing) OrElse (thisProgressbar.IsDisposed) Then
      Exit Sub
    End If

    If thisProgressbar.InvokeRequired Then
      ' InvokeRequired required compares the thread ID of the
      ' calling thread to the thread ID of the creating thread.
      ' If these threads are different, it returns true.
      Dim d As New IncrementProgressBarDelegate(AddressOf IncrementProgressBar)
      thisProgressbar.Invoke(d, New Object() {thisProgressbar})
      Exit Sub
    End If

    Try
      ' Make sure the bar is Visible.
      If (thisProgressbar.Visible = False) Then
        thisProgressbar.Visible = True
        thisProgressbar.PerformLayout()
      End If

      ' Impose a minimum time between status bar updates.
      If (thisProgressbar.Tag IsNot Nothing) AndAlso (TypeOf thisProgressbar.Tag Is Date) Then
        LastObjectUpdateTime = CType(thisProgressbar.Tag, Date)
      Else
        LastObjectUpdateTime = LastUpdateTime
      End If

      If (Now() - LastObjectUpdateTime).TotalSeconds >= 0.25 Then
        If (thisProgressbar.Tag IsNot Nothing) AndAlso (Not (TypeOf thisProgressbar.Tag Is Date)) Then
          LastUpdateTime = Now()
        Else
          thisProgressbar.Tag = Now()
        End If

        ' Progress the Status Bar....

        If (thisProgressbar.Value < thisProgressbar.Maximum) Then
          If (thisProgressbar.Value + thisProgressbar.Step) <= thisProgressbar.Maximum Then
            thisProgressbar.Value += thisProgressbar.Step
          Else
            thisProgressbar.Value = thisProgressbar.Maximum
          End If
        Else
          thisProgressbar.Value = thisProgressbar.Minimum
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub


  ' This is the method to run when the timer is raised.
  Friend Sub IncrementStatusBarTimerEvent(ByVal myObject As Object, _
  ByVal myEventArgs As EventArgs)
    If (myObject Is Nothing) OrElse (Not (TypeOf myObject Is Windows.Forms.Timer)) Then
      Exit Sub
    End If

    Dim mytimer As Windows.Forms.Timer = CType(myObject, Windows.Forms.Timer)

    If (mytimer.Tag Is Nothing) Then
      Exit Sub
    End If

    If (TypeOf mytimer.Tag Is ToolStripProgressBar) Then
      IncrementStatusBar(CType(mytimer.Tag, ToolStripProgressBar))
    ElseIf (TypeOf mytimer.Tag Is Windows.Forms.ProgressBar) Then
      IncrementProgressBar(CType(mytimer.Tag, Windows.Forms.ProgressBar))
    End If
  End Sub

#End Region

#Region " Cross Thread marshalled control code"

  Private Delegate Sub SetToolStripTextDelegate(ByVal thisStripLabel As ToolStripLabel, ByVal StripText As String)

  Friend Sub SetToolStripText(ByVal thisStripLabel As ToolStripLabel, ByVal StripText As String)

    ' InvokeRequired required compares the thread ID of the
    ' calling thread to the thread ID of the creating thread.
    ' If these threads are different, it returns true.
    If thisStripLabel.Owner.InvokeRequired Then
      Dim d As New SetToolStripTextDelegate(AddressOf SetToolStripText)
      thisStripLabel.Owner.Invoke(d, New Object() {thisStripLabel, StripText})
    Else
      thisStripLabel.Text = StripText
      thisStripLabel.Owner.Refresh()
    End If
  End Sub

  Private Delegate Sub SetToolStripStatusLabelTextDelegate(ByVal thisStripLabel As ToolStripStatusLabel, ByVal StripText As String)

  Friend Sub SetToolStripStatusLabelText(ByVal thisStripLabel As ToolStripStatusLabel, ByVal StripText As String)

    ' InvokeRequired required compares the thread ID of the
    ' calling thread to the thread ID of the creating thread.
    ' If these threads are different, it returns true.
    If thisStripLabel.Owner.InvokeRequired Then
      Dim d As New SetToolStripStatusLabelTextDelegate(AddressOf SetToolStripStatusLabelText)
      thisStripLabel.Owner.Invoke(d, New Object() {thisStripLabel, StripText})
    Else
      thisStripLabel.Text = StripText
      thisStripLabel.Owner.Refresh()
    End If
  End Sub

#End Region


  Public Class FontChangedEventArgs
    Inherits EventArgs

    Public NewFontSize As Single

    Public Sub New(ByVal pFontSize As Single)
      MyBase.New()
      Me.NewFontSize = pFontSize
    End Sub
  End Class

  Public Event CTA_FontSizeChanged(ByVal sender As Object, ByVal e As FontChangedEventArgs)

  Private Sub Menu_Font_Small_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Menu_Font_Small.Click, Menu_Font_Medium.Click, Menu_Font_Large.Click, Menu_Font_XLarge.Click
    Dim ThisMenu As ToolStripMenuItem = CType(sender, ToolStripMenuItem)

    Menu_Font_Small.Checked = False
    Menu_Font_Medium.Checked = False
    Menu_Font_Large.Checked = False
    Menu_Font_XLarge.Checked = False

    ThisMenu.Checked = True

    If (Menu_Font_Small.Checked) Then
      _SelectedFontSize = 6
    ElseIf (Menu_Font_Medium.Checked) Then
      _SelectedFontSize = 8.25
    ElseIf (Menu_Font_Large.Checked) Then
      _SelectedFontSize = 10
    ElseIf (Menu_Font_XLarge.Checked) Then
      _SelectedFontSize = 12
    End If

    ' Label_TestText.Font = New Font(Label_TestText.Font.FontFamily, SelectedSize, Label_TestText.Font.Style)

    RaiseEvent CTA_FontSizeChanged(Me, New FontChangedEventArgs(_SelectedFontSize))

  End Sub

  Public Sub SetControlFont(ByRef ThisControl As Control, ByVal pNewSize As Single)
    Dim ChildControl As Control

    Try
      If (TypeOf ThisControl Is ListBox) Then
        ThisControl.Font = New Font(ThisControl.Font.FontFamily, pNewSize, ThisControl.Font.Style)
      ElseIf (TypeOf ThisControl Is TabPage) Then
        CType(ThisControl, TabPage).Font = New Font(ThisControl.Font.FontFamily, pNewSize, ThisControl.Font.Style)
      End If

      For Each ChildControl In ThisControl.Controls
        SetControlFont(ChildControl, pNewSize)
      Next
    Catch ex As Exception
    End Try

  End Sub

  Public Sub AddCopyMenuToChart(ByVal TargetChart As Object) ' ByRef pChart As Dundas.Charting.WinControl.Chart)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim CopyDundasChart As Dundas.Charting.WinControl.Chart
    Dim CopyC1Chart As C1.Win.C1Chart.C1Chart
    Dim ChartContextMenuStrip As ContextMenuStrip

    Try

      If (TypeOf TargetChart Is Dundas.Charting.WinControl.Chart) Then
        CopyDundasChart = CType(TargetChart, Dundas.Charting.WinControl.Chart)

        ChartContextMenuStrip = New ContextMenuStrip
        ChartContextMenuStrip.Tag = CopyDundasChart
        AddHandler ChartContextMenuStrip.Opening, AddressOf Me.Chart_cms_Opening
        ChartContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

        CopyDundasChart.ContextMenuStrip = ChartContextMenuStrip

      ElseIf (TypeOf TargetChart Is C1.Win.C1Chart.C1Chart) Then
        CopyC1Chart = CType(TargetChart, C1.Win.C1Chart.C1Chart)

        ChartContextMenuStrip = New ContextMenuStrip
        ChartContextMenuStrip.Tag = CopyC1Chart
        AddHandler ChartContextMenuStrip.Opening, AddressOf Me.Chart_cms_Opening
        ChartContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

        CopyC1Chart.ContextMenuStrip = ChartContextMenuStrip

      End If

    Catch ex As Exception
    End Try

  End Sub

  Sub Chart_cms_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Dim ChartContextMenuStrip As ContextMenuStrip
    Dim CopyMenuItem As ToolStripMenuItem

    Try

      If Not (TypeOf sender Is ContextMenuStrip) Then
        Exit Sub
      End If

      ChartContextMenuStrip = CType(sender, ContextMenuStrip)
      ChartContextMenuStrip.Items.Clear()

      ' Add custom item (Form)

      e.Cancel = False

      CopyMenuItem = New ToolStripMenuItem("Copy to clipboard", Nothing, AddressOf Menu_CopyToClipboard)
      CopyMenuItem.Tag = ChartContextMenuStrip.Tag
      ChartContextMenuStrip.Items.Add(CopyMenuItem)

      CopyMenuItem = New ToolStripMenuItem("Zoom Reset", Nothing, AddressOf Menu_ZoomReset)
      CopyMenuItem.Tag = ChartContextMenuStrip.Tag
      ChartContextMenuStrip.Items.Add(CopyMenuItem)

    Catch ex As Exception

    End Try

  End Sub

  Private Sub Menu_CopyToClipboard(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    'Private Sub Menu_CopyToClipboard(ByVal sender As Object, ByVal e As System.EventArgs)
    '	' *****************************************************************************************
    '	'
    '	'
    '	' *****************************************************************************************
    '	Dim CopyMenuItem As ToolStripMenuItem
    '	Dim CopyChart As Dundas.Charting.WinControl.Chart

    '	Try
    '		CopyMenuItem = CType(sender, ToolStripMenuItem)
    '		CopyChart = CType(CopyMenuItem.Tag, Dundas.Charting.WinControl.Chart)


    '		Dim memStream As MemoryStream = New MemoryStream
    '		CopyChart.SaveAsImage(memStream, System.Drawing.Imaging.ImageFormat.Tiff)

    '		My.Computer.Clipboard.SetImage(Image.FromStream(memStream))

    '	Catch ex As Exception

    '	End Try

    'End Sub

    Dim CopyMenuItem As ToolStripMenuItem
    Dim CopyDundasChart As Dundas.Charting.WinControl.Chart
    Dim CopyC1Chart As C1.Win.C1Chart.C1Chart

    Try
      Dim memStream As MemoryStream = New MemoryStream

      CopyMenuItem = CType(sender, ToolStripMenuItem)

      If (TypeOf CopyMenuItem.Tag Is Dundas.Charting.WinControl.Chart) Then
        CopyDundasChart = CType(CopyMenuItem.Tag, Dundas.Charting.WinControl.Chart)

        CopyDundasChart.SaveAsImage(memStream, System.Drawing.Imaging.ImageFormat.Tiff)

      ElseIf (TypeOf CopyMenuItem.Tag Is C1.Win.C1Chart.C1Chart) Then
        CopyC1Chart = CType(CopyMenuItem.Tag, C1.Win.C1Chart.C1Chart)

        CopyC1Chart.SaveImage(memStream, System.Drawing.Imaging.ImageFormat.Tiff)
      End If

      My.Computer.Clipboard.SetImage(Image.FromStream(memStream))

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Menu_ZoomReset(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim ThisMenuItem As ToolStripMenuItem
    Dim ThisDundasChart As Dundas.Charting.WinControl.Chart

    Try
      ThisMenuItem = CType(sender, ToolStripMenuItem)

      If (TypeOf ThisMenuItem.Tag Is Dundas.Charting.WinControl.Chart) Then
        ThisDundasChart = CType(ThisMenuItem.Tag, Dundas.Charting.WinControl.Chart)

        ThisDundasChart.ChartAreas(0).AxisX.View.ZoomReset(0)
        ThisDundasChart.ChartAreas(0).CursorX.Position = -999
        ThisDundasChart.ChartAreas(0).AxisY.View.ZoomReset(0)
        ThisDundasChart.ChartAreas(0).CursorY.Position = -999

      ElseIf (TypeOf ThisMenuItem.Tag Is C1.Win.C1Chart.C1Chart) Then

      End If

    Catch ex As Exception
    End Try

  End Sub


#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region



  Private Sub Button_Debug_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Debug.Click

    Dim DateValues(20) As Date
    Dim DayName(20) As String
    Dim DayEquivalent(20) As Boolean
    Dim FittedDateValues(20) As Date
    Dim PriceIndex(20) As Integer
    Dim PeriodCount(20) As Integer

    Dim Index As Integer

    For Index = 0 To 20
      DateValues(Index) = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(DealingPeriod.Daily, Now.Date, (Index - 10))
      DayName(Index) = DateValues(Index).DayOfWeek.ToString
      DayEquivalent(Index) = RenaissanceUtilities.DatePeriodFunctions.AreDatesEquivalent(DealingPeriod.Daily, Now.Date, DateValues(Index))
      FittedDateValues(Index) = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(DealingPeriod.Daily, Now.Date.AddDays(Index - 10))
      PriceIndex(Index) = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(DealingPeriod.Daily, Now.Date, DateValues(Index))
      PeriodCount(Index) = RenaissanceUtilities.DatePeriodFunctions.GetPeriodCount(DealingPeriod.Daily, Now.Date, DateValues(Index))



    Next


    Index = 0










    Exit Sub

    'Dim DataCommand As New SqlCommand

    'Dim T1 As Date
    'Dim T2 As Date
    'Dim rval As Integer

    'T1 = Now

    'Try
    '  DataCommand.Connection = Me.GetCTAConnection
    '  DataCommand.CommandType = CommandType.Text
    '  DataCommand.CommandTimeout = 600

    '  DataCommand.CommandText = "IF object_id('tempdb..#tblTemp') IS NOT NULL DROP TABLE #tblTemp"
    '  DataCommand.ExecuteNonQuery()

    '  DataCommand.CommandText = "CREATE TABLE #tblTemp ([InstrumentID] [int] NOT NULL , PRIMARY KEY ([InstrumentID]))"
    '  DataCommand.ExecuteNonQuery()

    '  Dim myTable As New DataTable
    '  myTable.Columns.Add("InstrumentID", GetType(Integer))

    '  For Index As Integer = 0 To 10000
    '    'DataCommand.CommandText = "INSERT INTO tblTemp(InstrumentID) SELECT " & Index.ToString
    '    'DataCommand.ExecuteNonQuery()

    '    myTable.Rows.Add(New Object() {Index})
    '  Next

    '  Dim myBulkCopy As New SqlBulkCopy(DataCommand.Connection)

    '  myBulkCopy.DestinationTableName = "#tblTemp"
    '  myBulkCopy.WriteToServer(myTable)

    '  myBulkCopy = Nothing

    '  DataCommand.CommandText = "SELECT COUNT(*) FROM #tblTemp"
    '  rval = CInt(DataCommand.ExecuteScalar)


    'Catch ex As Exception

    'Finally
    '  If (DataCommand IsNot Nothing) Then
    '    Try
    '      If (DataCommand.Connection IsNot Nothing) Then
    '        DataCommand.Connection.Close()
    '      End If
    '    Catch ex As Exception
    '    Finally
    '      DataCommand.Connection = Nothing
    '    End Try
    '  End If
    'End Try

    'T2 = Now

    'MessageBox.Show(rval.ToString & ", Time = " & (T2 - T1).TotalSeconds.ToString, "", MessageBoxButtons.OK)

    Exit Sub


    ' Call UpdatePertracPerformance(Me, "M:\Pertrac\Master.mdb", 0, True, True)

    'Dim CustomFieldDataCache As New CustomFieldDataCacheClass(Me)
    'Dim Result As Object

    'Result = CustomFieldDataCache.GetDataPoint(3, 0, 9876)


    'CustomFieldDataCache.LoadCustomFieldData(3)

    'CustomFieldDataCache.LoadCustomFieldData(4, 10)

    'CustomFieldDataCache.ClearDataCache("3, 4.10, 5")

    'CustomFieldDataCache.ClearDataCache(4)

    'CustomFieldDataCache.LoadCustomFieldData(4, 10)

    'CustomFieldDataCache.GetCustomFieldDataStatus(2)
    'CustomFieldDataCache.GetCustomFieldDataStatus(3)
    'CustomFieldDataCache.GetCustomFieldDataStatus(4, 9)
    'CustomFieldDataCache.GetCustomFieldDataStatus(4, 10)

    'CustomFieldDataCache.GetCustomField_InstrumentDataPresent(3, 123456)
    'CustomFieldDataCache.GetCustomField_InstrumentDataPresent(3, 10, 12345)

    'Exit Sub

    'Dim pEventArgs As New RenaissanceGlobals.RenaissanceUpdateEventArgs

    'pEventArgs = New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblBookmark, "0")

    'pEventArgs.TableChanged(RenaissanceChangeID.tblBookmark) = True
    'pEventArgs.UpdateDetail(RenaissanceChangeID.tblBookmark) = "0"
    'pEventArgs.TableChanged(RenaissanceChangeID.tblMonthlyComment) = True
    'pEventArgs.UpdateDetail(RenaissanceChangeID.tblMonthlyComment) = "1"

    'Main_RaiseEvent(pEventArgs)

    'Exit Sub


    'MessageBox.Show(New MACConverterClass("00-1D-E0-2D-6A-1D").MACLong.ToString, "MAC")
    'MessageBox.Show(New DateConverterClass(CDate("9 July 2008")).ULongVal.ToString, "Date")

    'Exit Sub

    'SetValidMac("00-0F-FE-64-42-03")
    'SetValidExpiryDate(CDate("1 Jul 2007"))

    'Dim Test As Boolean

    '_SecurityTest1 = True
    'Test = SecutityTestOne()

    '_SecurityTest2 = True
    'Test = SecutityTestTwo()

    'Test = Test


  End Sub

  Public Function Load_Table1(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pUpdateDetail As String, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As System.Data.DataSet Implements RenaissanceGlobals.StandardRenaissanceMainForm.Load_Table

  End Function
End Class
